 <div class="modal fade" id="drivermodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Alert</h4>
        </div>
        <div class="modal-body">
        <div id="alertBox"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="review">Review</button>
          
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
    <div class="modal fade" id="drivermodal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver No Job Alert</h4>
        </div>
        <div class="modal-body">
        <div id="alertBox1"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="review1">Review</button>
          
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  


<footer id="footer-bar" class="row">
    <p id="footer-copyright" class="col-xs-12">
        Powered by Infinite Cabs. Copyright ⓒ 2020 Infinite Cabs, All rights reserved.
    </p>
    
    <script>
    
          $.ajax({
        url: "<?php echo base_url(); ?>admin/checkDriverRisk",
        type: "post",
       
        success: function (response) {

           $('.count').text(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
         
    function request(){
            

             setTimeout(request(),1000); 
        }
      request();
        $(document).ready(function(){
         
           $('#page-wrapper').addClass('nav-small'); 
        });


        
    </script>
</footer>
