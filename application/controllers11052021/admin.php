<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Admin extends CI_Controller
{
public $zone_name = CUSTOM_ZONE_NAME;
// construct call
public function __construct()
{
parent::__construct();
$this->load->helper(array('form', 'url'));
$this->load->helper('date');
$this->load->helper('file');
$this->load->library('form_validation');
$this->load->model('Model_admin','home');
$this->load->model('model_web_service');
$this->load->database();
$this->load->library('session');
$this->load->library('image_lib');
$this->load->helper('cookie');
$this->load->helper('url');
$this->load->library('email');
session_start();
/*  $user = $this->session->userdata('username-admin');
if(empty($user))
{
redirect(base_url() . 'admin/login');
}*/
}
// permission call
public function permission()
{
//$data=$_POST;
$permission="";
if(($this->session->userdata('permission'))) {
$ff = $this->router->fetch_method();
$pm = $this->db->query("SELECT * FROM  pages WHERE pages='$ff'");
if($pm->num_rows == 1) {
$upm = $pm->row('p_id');
$id=explode(',',$this->session->userdata('permission'));
/*echo "<pre>";
print_r($id);*/
if(in_array($upm,$id)) {
$permission = "access";
} else {
$permission = "access";
}
} else {
$permission = "access";
}
}
return $permission;
}
public function add_user()
{
$this->load->view('add_user');
}
function is_mail_exists($mail,$uid)
{
/* function return
---------------------------------
'true'   if user exist
'false'  if user does not exist
*/
$table = 'userdetails';
$select_data = "*";
$this->db->select($select_data);
// $this->db->where('user_name',$username);
$this->db->where('email',$mail);
if($uid!="")
{
$this->db->where('id !=', $uid);
}
$query = $this->db->get($table);  //--- Table name = User
$result = $query->num_rows();
if ($result > 0) {
return true;  //already exist
} else {
return false; //Not exist
}
}
function is_mobile_exists($mobile,$uid)
{
/* function return
---------------------------------
'true'   if mobile exist
'false'  if mobile does not exist
*/
$table = 'userdetails';
$select_data = "*";
$this->db->select($select_data);
// $this->db->where('user_name',$username);
$this->db->where('mobile',$mobile);
if($uid!="")
{
$this->db->where('id !=', $uid);
}
$query = $this->db->get($table);  //--- Table name = User
$result = $query->num_rows();
if ($result > 0) {
return true;  //already exist
} else {
return false; //Not exist
}
}
public function insert_user()
{
if(isset($_POST['save']))
{   
$mail_status = $this->is_mail_exists($_POST['email'], $uid = "");
// $user_status = $this->model_web_service->is_username_exists($username,$uid="");
$mobile_status = $this->is_mobile_exists($_POST['phone'], $uid = "");
if ($mail_status)
{
redirect(base_url() . 'admin/add_user?status=email');
}else if($mobile_status){
redirect(base_url() . 'admin/add_user?status=phone');
}else{
$data = array(
'first_name' => $_POST['fName'],
'last_name' => $_POST['lName'],
'username' => $_POST['username'],
'mobile' => $_POST['phone'],
'address' => $_POST['Address'],
'email' => $_POST['email'],
'gender' => $_POST['gender'],
'dob' => $_POST['dob'],
'password' => md5($_POST['password']),
'temp_password' => $_POST['password'],
'phoneCode' => $_POST['code'],
'isdevice' => 'Admin',
);
$response = $this->db->insert('userdetails',$data);
$userid=$this->db->insert_id();
$otp = rand(10000, 99999);
$mobile = $_POST['phone'];
ob_start();
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://platform.clickatell.com/messages/http/send?apiKey=iH2z_OGzRIa663II0vAS3Q==&to=+91' . $mobile . '&content=Your+otp+is+' . $otp);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
$table1 = "userdetails";
$data = array(
'otp' => $otp
);
// print_r($user_detatil[0]);die();
//$this->db->where('uneaque_id', $request->current_ride_id);
//$this->db->where('uneaque_id','CMC1447321810');
$this->db
->where('id', $userid);
$result = $this
->db
->update($table1, $data);
//  echo $this->db->last_query();die;
redirect(base_url() . 'admin/add_user?otp='.$userid);
}
}
}
public function verfyotp()
{
$userId = $_POST['id'];
$otp = $_POST['otp'];
$table34 = 'userdetails';
$select_data = "*";
$this->db->select($select_data);
$this->db->where("id", $userId);
$query = $this->db->get($table34); //--- Table name = User
$user_detatil23 = $query->row_array();
$dataopt = $user_detatil23['otp'];
if ($dataopt == $otp)
{
$datas=array("user_status"=>'Active');
$this->db->where("id",$user_detatil23['id']);
$this->db->update('userdetails', $datas);
//print_r($user_detatil23);die;
$mobile = $user_detatil23['mobile'];
ob_start();
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://platform.clickatell.com/messages/http/send?apiKey=iH2z_OGzRIa663II0vAS3Q==&to=+91' . $mobile . '&content=you+have+register+successfully+with+infinitecab+here+is+your+login+details+email:+'.$user_detatil23['email'].'+password:+'.$user_detatil23['temp_password'].'');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
echo 1;
}else{
echo 2;
}
}
public function add_booking_from_Website()
{
$user_id=1;
$pname=$_POST['name'];
$plocation=$_POST['pick'];
$dlocation=$_POST['drop'];
$btype=$_POST['radios'];
$car_type_id=1;
$note=$_POST['message'];
$lat='232323';
$lng='232323';
$cabId = $this
->input
->post('car_type_id');
$pickup_lat = '121212';
$pickup_long = '121212';
$drop_lat = '12121';
$drop_long = '121212';
$bookingStatus = $this
->input
->post('btype');
$bookingDateTime = '2010-20-12';
$drop_address = $dlocation;
$pick_address = $plocation;
$amount = 20;
// now / upcomming
//$pickup_area = $this->input->post('pickup_area');
//$drop_area = $this->input->post('drop_area');
$pickup_date_time = '20:20';
$isdevice = 'web';
//$amount = $this->input->post('amount');
//$distance = $this->input->post('distance');
$message = $this
->input
->post('note');
$status_code = 'pending';
$cab = $this
->db
->query('SELECT cartype FROM cabdetails WHERE cab_id = "' . $cabId . '"')->result_array();
$amount = $cab[0]['car_rate'];
$dataResult = array(
'user_id' => '1',
'username' => ($pname) ? $pname : '',
'phone' => '12312312',
'amount' => ($amount) ? $amount : '20',
'pickup_lat' => ($pickup_lat) ? $pickup_lat : '',
'pickup_long' => ($pickup_long) ? $pickup_long : '',
'drop_lat' => ($drop_lat) ? $drop_lat : '',
'drop_long' => ($drop_long) ? $drop_long : '',
'taxi_type' => ($cabType) ? $cabType : '',
'isdevice' => ($isdevice) ? $isdevice : '',
'payment_type' => 'case',
'status_code' => ($status_code) ? $status_code : '',
'message' => ($message) ? $message : '',
'booking_status' => $bookingStatus, // later/ready
'book_create_date_time' => ($bookingDateTime) ? $bookingDateTime : '',
'bookingdateandtime'=>date('mm/dd/yy'), // later/ready
'drop_address' => $drop_address, 
'pick_address' => $pick_address, 
'pincode' => '302012', 
);
$cabInsert = $this
->db
->insert('bookingdetails', $dataResult);
// echo $this->db->last_query();die;
if($cabInsert)
{
echo $booking_id=$this->db->insert_id();
}
}
public function add_Booking()
{
$user_id=1;
$pname=$_POST['pname'];
$plocation=$_POST['plocation'];
$dlocation=$_POST['dlocation'];
$btype=$_POST['btype'];
$car_type_id=$_POST['car_type_id'];
$note=$_POST['note'];
$xcv=$_POST['xcv'];
$cnumber=$_POST['cnumber'];
$edate=$_POST['edate'];
$xyear=$_POST['xyear'];
$cabId = $this
->input
->post('cartype');
$pickup_lat = '26.838057';
$pickup_long = '75.735895';
$drop_lat =$_POST['dlat'];
$drop_long = $_POST['dlng'];
$bookingStatus = $this
->input
->post('btype');
$bookingDateTime = '2010-20-12';
$drop_address = $dlocation;
$pick_address = $plocation;
$amount = 20;
// now / upcomming
//$pickup_area = $this->input->post('pickup_area');
//$drop_area = $this->input->post('drop_area');
$pickup_date_time = '20:20';
$isdevice = 'dispatcher';
//$amount = $this->input->post('amount');
//$distance = $this->input->post('distance');
$message = $this
->input
->post('note');
$status_code = 'pending';
$cab = $this
->db
->query('SELECT cartype FROM cabdetails WHERE cab_id = "' . $cabId . '"')->result_array();
$amount = $_POST['fair'];
$dataResult = array(
'user_id' => '1',
'username' => ($pname) ? $pname : '',
'phone' => '12312312',
'amount' => ($amount) ? $amount : '20',
'pickup_lat' => ($pickup_lat) ? $pickup_lat : '',
'pickup_long' => ($pickup_long) ? $pickup_long : '',
'drop_lat' => ($drop_lat) ? $drop_lat : '',
'drop_long' => ($drop_long) ? $drop_long : '',
'taxi_type' => ($cabType) ? $cabType : '',
'taxi_id' => ($cabId) ? $cabId : '',
'isdevice' => ($isdevice) ? $isdevice : '',
'payment_type' => $_POST['ptype'],
'status_code' => ($status_code) ? $status_code : '',
'message' => ($message) ? $message : '',
'booking_status' => $_POST['bookingType'], // later/ready
'book_create_date_time' => ($bookingDateTime) ? $bookingDateTime : '',
'bookingdateandtime'=>date('Y-m-d H:i:s A'), // later/ready
'drop_address' => $drop_address, 
'pick_address' => $pick_address,
'pincode' => '302029',
'cardId' =>$insert_id
);
$cabInsert = $this
->db
->insert('bookingdetails', $dataResult);
// echo $this->db->last_query();die;
if($cabInsert)
{
echo $booking_id=$this->db->insert_id();
}
}
public function serchUserTexiAmount()
{
$car = $this
->input
->post('car');
$userPickupLatitude = $this
->input
->post('user_pickup_latitude');
$userPickupLongLatitude = $this
->input
->post('user_pickup_longlatitude');
$userDropLatitude = $this
->input
->post('user_drop_latitude');
$userDropLongLatitude = $this
->input
->post('user_drop_longlatitude');
//$distance =  5;
//  $location = $userPickupLatitude . "," . $userPickupLongLatitude . "&destinations=" . $userDropLatitude . "," . $userDropLongLatitude;
$location = $userPickupLatitude . "," . $userPickupLongLatitude . "&destinations=" . $userDropLatitude . "," . $userDropLongLatitude;
$api = file_get_contents("");
$url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" . $location . "&key=AIzaSyBlJ6SDBsV1H_mo8hwppNfwKqAeRJNLayQ";
//$url='';
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL, $url);
// Execute
$result = curl_exec($ch);
// Closing
curl_close($ch);
$response_a = json_decode($result, true);
//   print_r($response_a);die;
$status = "ERROR";
$dist = 0;
$time = 0;
$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
$duration=$response_a['rows'][0]['elements'][0]['duration']['text'];
$distance = rtrim($dist, 'mi');
$distance=trim($distance);
$distance=($distance * 1.609344);
//$distance = '5';
$eta=rtrim($duration, 'mins');
$eta=trim($eta);
$this
->db
->select('*');
$this
->db
->from('cabdetails');
$this
->db
->where('cab_id',$car);
$query = $this
->db
->get();
$result = $query->result_array();
if ($result)
{
$cabDetails = array();
$day=0;
if(date('l')=='Monday'){ $day=1; }
else if(date('l')=='Tuesday'){ $day=2; }
else if(date('l')=='Wednesday'){ $day=3; }
else if(date('l')=='Thursday'){ $day=4; }
else if(date('l')=='Friday'){ $day=5; }
else if(date('l')=='Saturday'){ $day=6; }
else if(date('l')=='Sunday'){ $day=7; }
foreach ($result as $key => $value)
{
$dataFare=$this->db->query("SELECT * FROM extrafare WHERE  startTime <= '".date('H:i',strtotime($bookingDateTime))."' and endTime >= '".date('H:i',strtotime($bookingDateTime))."'  and FIND_IN_SET('".$day."', days) and capacity=".$value['seat_capacity']."")->row_array();
//echo $this->db->last_query();die;
if($dataFare)
{
if(date('H:i',strtotime($bookingDateTime)) > date('H:i',strtotime('22:00')) && date('H:i',strtotime($bookingDateTime)) < date('H:i',strtotime('23:59')))
{
$checkholidays=$this->db->query("SELECT * FROM holidays WHERE date='".date('Y-m-d', strtotime(' +1 day'))
."'")->row_array();
} else   if(date('H:i',strtotime($bookingDateTime)) > date('H:i',strtotime('06:00')) && date('H:i',strtotime($bookingDateTime)) < date('H:i',strtotime('22:00')))
{
$checkholidays=$this->db->query("SELECT * FROM holidays WHERE date='".date('Y-m-d')."'")->row_array();
}
//echo $this->db->last_query();die;
if($checkholidays)
{
if($dataFare['move_to']!=0)
{
$dataFare=$this->db->query("SELECT * FROM extrafare WHERE id=".$dataFare['move_to']."")->row_array(); 
}
}
//print_r($dataFare);die;
$dataFare['TimeChargeKm'];
$Flagfall=$dataFare['Flagfall'];
//$distance=5.15;
$DistanceChargeKm=round($distance,2)*$dataFare['DistanceChargeKm'];
$DistanceChargeKm=round($DistanceChargeKm,2);
$TimeChargeKm=$dataFare['TimeChargeKm']*$eta;
$NSWGovernmentLevy=1.10;
$TollsBooking=0;
$Highoccupancyfee=0; 
/*if($value['seat_capacity'] > 4)
{
$highValue=round($Flagfall+$DistanceChargeKm,1);
$Highoccupancyfee=$highValue/2;   
}else{
$Highoccupancyfee=0; 
}*/
$Highoccupancyfee=round($Highoccupancyfee,2);
$minFare=$Flagfall+$DistanceChargeKm+$NSWGovernmentLevy+$Highoccupancyfee;  
$extra=0;
$maxvalue=round($Flagfall+$DistanceChargeKm,1);
$extra = $maxvalue/4;
$new_width = (10 / 100) * $minFare;
$TotalFare=$minFare+$extra+$new_width;
}else{
$TotalFare=30;
$minFare=15;
}
$cabDetails[$key]['min_rate'] = round($minFare,2);
$cabDetails[$key]['max_rate'] = round($TotalFare,2);
}
echo round($minFare,2).' - '.round($TotalFare,2);
}
}
public function addBooking()
{
$this->db->select('cartype,seat_capacity');
$query = $this->db->get('cabdetails');
$allCarType['allCarType'] = $query->result_array();
$this->load->view('add-booking',$allCarType);
}
// index page call
public function index()
{
// echo "string";die;
$isSecure = false;
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
$isSecure = true;
}
elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
$isSecure = true;
}
$REQUEST_PROTOCOL = $isSecure ? 'https' : 'http';
if ($REQUEST_PROTOCOL=="http")
{
$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
header('HTTP/1.1 301 Moved Permanently');
header('Location: ' . $redirect);
exit();
}
$this->load->view('admin-login');
}
// admin login call
public function adminlogin()
{
$data=$_POST;
$result = $this->home->login($data);
echo $result;

}
// admin logout call
public function logout()
{
$user1 = $this->session->userdata('role-admin');
$admin_id = $this->session->userdata('username-admin');
$userData=$this->db->query("SELECT * FROM adminlogin WHERE email='".$admin_id."' order by id desc")->row_array();
if($user1=='admin' && $userData['role']=='dispatch')
{
$dispatchArray=array(
'userId'=>$userData['id'],
'status'=>'Logout In Admin',
'date'=>date('Y-m-d'),
'time'=>date('H:i:s'),
);
//print_r($dispatchArray);die;
$this->db->insert('dispatcherLoginTraking',$dispatchArray);
}
if($user1=='dispatch')
{
$dispatchArray=array(
'userId'=>$userData['id'],
'status'=>'Dispatch Logout',
'date'=>date('Y-m-d'),
'time'=>date('H:i:s'),
);
//print_r($dispatchArray);die;
$this->db->insert('dispatcherLoginTraking',$dispatchArray);
}else if($user1=='bookkeeping')
{
$dispatchArray=array(
'userId'=>$userData['id'],
'status'=>'bookkeeping Logout',
'date'=>date('Y-m-d'),
'time'=>date('H:i:s'),
);
$this->db->insert('bookkeepingLoginTraking',$dispatchArray);  
}
$this->session->unset_userdata('username-admin');
//redirect('/', 'refresh');
//delete_cookie('username-admin');
redirect('/admin', 'refresh');
}
// drivesignup call
public function driversignup()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('driver_signup');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// admin profile call
public function profile()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-profile');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// admin password change call
public function password_change()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-change-password');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
//dashboard call
public function dashboard()
{
// echo "string";die;
    
    if(empty($this->session->userdata('role-admin')))
    {
        $this->session->set_userdata('role-admin','bookkeeping');
        $rol=$this->session->userdata('role-admin');
    }else{
        $rol=$this->session->userdata('role-admin');
    }
    //echo $rol;die;
    if($rol=='dispatch')
    {
        $this->dispatchview();
    } else{
    //print_r($this->session->userdata('role-admin'));die();
        if($rol == 'admin' || $rol == 'dispatch' || $rol == 'bookkeeping') {
            $permission = $this->permission();
            if($rol == 'admin' || $rol == 'dispatch' || $rol == 'bookkeeping') {
                $query2=$this->home->get_driver_live($_GET['driver'],'free');
                $this->load->view('dashboard',compact('query2'));
            }else{
                redirect('admin/not_admin');
            }
        } else{
        redirect('admin/index');
        }
    }
}
// manage user call
public function manage_user()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
if($this->input->get('flag')){
$filter='flag';
$data['query']=$filter;
}
else{
$data['query']= NULL;
}
$this->load->view('manage-user',$data);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function flaged_users()
{  
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
if($this->input->get('flag')){
$filter='flag';
$data['query']=$filter;
}
else{
$data['query']= NULL;
}
$this->load->view('flaged_user',$data);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// add user call
/*public function adduser()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-userdetails');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}*/
// insert user call
/*public function insertuser()
{
$data=$_POST;
//echo $data['value'];exit;
$res=$this->home->userinsert($data);
// print_r($res);
echo $res;
}*/
public function update_user()
{
$data = array(
'first_name' => $_POST['fName'],
'last_name' => $_POST['lName'],
'username' => $_POST['username'],
'mobile' => $_POST['phone'],
'address' => $_POST['Address'],
'email' => $_POST['email'],
'gender' => $_POST['gender'],
'dob' => $_POST['dob'],
'user_status' => $_POST['status'],
'phoneCode' => $_POST['code'],
);
$this->db->where("id",$_POST['updateid']);
$this->db->update('userdetails', $data);
//echo $this->db->last_query();die;
redirect('admin/view_userdetails?id='.$_POST['updateid']);
// $response = $this->db->insert('userdetails',$data);    
}
// view user details call
public function view_userdetails()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$userData=$this->db->query("SELECT * FROM `userdetails` WHERE id=".$_GET['id']."")->row_array();
// print_r($userData);
$this->load->view('user-details',compact('userData'));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function cardDetail($id)
{
$cardData= $this->db->query("SELECT * FROM credit_card_details WHERE user_id='".$id."'")->result_array();
//echo $this->db->last_query();die;
$this->load->view("cardDetails",compact('cardData'));
}
// get user data call
public function get_user_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$filterData=$_POST['data_id'];
if($filterData=='yes'){
$flagfilter=$filterData;
}
else{
$flagfilter='';
}
$user=$this->home->getuser($requestData,$flagfilter,$where=null);
echo $user;
}
public function get_flaged_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$filterData=$_POST['data_id'];
if($filterData=='yes'){
$flagfilter=$filterData;
}
else{
$flagfilter='';
}
$user=$this->home->getflag($requestData,$flagfilter,$where=null);
echo $user;
}
//delete user data call
public function delete_user_data()
{
$data_ids = $_REQUEST['data_ids'];
//$this->home->deluser($data_ids);
$this->db->where_in("id",$data_id);
$this->db->update('userdetails', array('flag' => 'no', 'user_status' => 'Inactive', 'is_deleted' => '1'));
}
//delete single user data call
public function delete_single_user_data()
{
$data_id = $_REQUEST['data_id'];
$this->db->where("id",$data_id);
$this->db->update('userdetails', array('flag' => 'no', 'user_status' => 'Inactive','is_deleted' => '1'));
//$this->home->delsingleuser($data_id);
}
public function delete_flaged_user_data()
{
$data_ids = $_REQUEST['data_ids'];
//$this->home->deluser($data_ids);
$this->db->where_in("id",$data_id);
$this->db->update('userdetails', array('is_deleted' => 1, 'user_status' => 'Inactive'));
}
public function delete_single_flaged_user_data()
{
$data_id = $_REQUEST['data_id'];
$this->db->where("id",$data_id);
$this->db->update('userdetails', array('is_deleted' => 1, 'user_status' => 'Inactive'));
//$this->home->delsingleuser($data_id);
}  
// manage booking call
public function manage_booking()
{
//echo "string";die();
$booking=$this->home->getbooking($_GET['id'],$_GET['phone'],$_GET['username'],$_GET['pick'],$_GET['drop'],$_GET['sdate'],$_GET['edate'],$_GET['cab'],$_GET['did'],$_GET['status_code'],$_GET['ca']);
/*echo "<pre>";
print_r($booking);die();*/
$this->load->view('manage-booking',compact('booking'));
}
// booking details call
public function view_booking_details()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$data['query']=$this->home->get_booking_details($this->input->get('id'));
if($data['query']){
$data['query4']=$this->home->get_explicit_selected_drivers($this->input->get('id'));
$data['query1']=$this->home->get_car_list();
$data['query2']=$this->home->get_driver_list();
}
$this->load->view('booking-details',$data);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// get booking data call
public function get_booking_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$filterData=$_POST['data_id'];
if($filterData=='user-cancelled'){
$filterstatusid='4';
$filterbookingid='';
}
else if($filterData=='driver-unavailable'){
$filterstatusid='6';
$filterbookingid='';
}
else if($filterData=='completed'){
$filterstatusid='9';
$filterbookingid='';
}
else if(is_numeric($filterData)){
$filterstatusid='';
$filterbookingid=$filterData;
}
else{
$filterstatusid='';
$filterbookingid='';
}
$booking=$this->home->getbooking($requestData,$filterstatusid,$filterbookingid,$where=null);
echo $booking;
}
public function get_active_booking_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$filterData=$_POST['data_id'];
if($filterData=='user-cancelled'){
$filterstatusid='4';
$filterbookingid='';
}
else if($filterData=='driver-unavailable'){
$filterstatusid='6';
$filterbookingid='';
}
else if($filterData=='completed'){
$filterstatusid='9';
$filterbookingid='';
}
else if(is_numeric($filterData)){
$filterstatusid='';
$filterbookingid=$filterData;
}
else{
$filterstatusid='';
$filterbookingid='';
}
$booking=$this->home->getactivebooking($requestData,$filterstatusid,$filterbookingid,$where=null);
echo $booking;
}
public function active_booking()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
if($this->input->get('user_id')){
$filter='user_id';
$data['query']=$filter;
}
else if($this->input->get('status_code')){
$filter='status_code';
$data['query']=$filter;
}
else{
$data['query']= NULL;
}
$this->load->view('active-booking',$data);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// get non disp booking data call
public function get_nondisp_booking_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$booking=$this->home->getnondispbooking($requestData,$where=null);
echo $booking;
}
//update booking call
public function update_booking_data()
{
// storing  request (ie, get/post) global array to a variable
$id= $_POST['id'];
$data_id= $_POST['data_id'];
$taxi_type = $_POST['taxi_type'];
$amount = $_POST['amount'];
$updatebooking=$this->home->updatebooking($id,$data_id,$taxi_type,$amount);
echo $updatebooking;
}
// multi booking delete call
public function multi_booking_delete()
{
$data=$_POST['result'];
$data=json_decode("$data",true);
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deletemultibooking($data);
// print_r($res);
echo $user;
}
//delete booking data call
public function delete_booking_data()
{
$data_ids = $_REQUEST['data_ids'];
$this->home->delbooking($data_ids);
}
//delete single booking call
public function delete_single_booking_data()
{
$data_id = $_REQUEST['data_id'];
$this->home->delsinglebooking($data_id);
}
// manage driver call
public function manage_driver()
{
$dataDriver=$this->home->getAlldriver(@$_GET['status'],@$_GET['username'],@$_GET['phone'],@$_GET['did'],@$_GET['car']);
$this->load->view('manage-driver',compact('dataDriver'));
}
public function manage_flaged_driver()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
if($this->input->get('flag')){
$filter='flag';
$data['query']=$filter;
}
else{
$data['query']= NULL;
}
$this->load->view('manage-flaged-driver',$data);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// manage flagged driver call
public function manage_flagged_driver()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('manage-flagged-driver');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// driver details call
public function view_driver_details()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$cars=$this->db->query("SELECT * FROM cars")->result_array();
$driver=$this->db->query("SELECT * FROM driver_details WHERE id=".$_GET['id']."")->row_array();
//print_r($driver);die;
$this->load->view('driver-details',compact('cars','driver'));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_driver()
{
// $carsdata=$this->db->query("SELECT * FROM cars WHERE id=".$_POST['carId']."")->row_array();
//print_r($carsdata);die;
//$upload_data = $this->upload->data();
if(empty($_FILES['profile']['name'])){
$image=$_REQUEST["himg"];
}
else
{
$image =  "driverimages/{$_FILES['profile']['name']}";
$result = move_uploaded_file($_FILES['profile']['tmp_name'], $image);
}
$data = array(
'image'=>$image,
'pname' => $_POST['pname'],
'dtype' => $_POST['type'],
'lstate' => $_POST['state'],
'first_name' => $_POST['driverName'],
'last_name' => $_POST['lName'],
'name' => $_POST['pname'],
'user_name' => $_POST['username'],
'phone' => $_POST['driverPhone'],
'address' => $_POST['driverAddress'],
'email' => $_POST['email'],
'license_no' => $_POST['licenseno'],
'gender' => $_POST['gender'],
'dob' => $_POST['dob'],
'password' => $_POST['password'],
'Lieasence_Expiry_Date' => $_POST['licennex'],
'flag' => 'yes',
'status' => 'Active',
'offload' => $_POST['offload'],
'status' => $_POST['status'],
);
$this->db->where_in("id",$_POST['did']);
$response = $this->db->update('driver_details',$data);
//echo $this->db->last_query();die;
redirect(base_url() . 'admin/manage_driver');
}
public function delete_driver()
{
$this->db->where('id', $_GET['id']);
$del=$this->db->delete('driver_details');
if($_GET['id']=='deleteAll')
{
$del=$this->db->query('DROP TABLE bookingdetails');
//echo $this->db->last_query();die;
}
redirect(base_url() . 'admin/manage_driver');
}
// add driver call
public function add_driver()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$cars= $this->db->query("SELECT * FROM cars")->result_array();
$this->load->view('add-driver',compact('cars'));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// insert driver data call
public function insert_driver()
{
if(isset($_POST['save']))
{
$config['upload_path'] = './driverimages/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['max_size']    = '2000';
$config['max_width']  = '1024';
$config['max_height']  = '768';
$this->load->library('upload', $config);
if($_POST['email']) {
$email=$_POST['email'];
$username=$_POST['username'];
$check_email_username=$this->home->checkemailusername($email,$username);
if($check_email_username) {
$response = $this->session->set_flashdata('error_msg', 'email or username already exists');
redirect(base_url().'admin/add_driver');
}
else {
if($_FILES['driverimage']['name'])
{
$image =  "driverimages/{$_FILES['driverimage']['name']}";
$result = move_uploaded_file($_FILES['driverimage']['tmp_name'], $image);
}
// $carsdata=$this->db->query("SELECT * FROM cars WHERE id=".$_POST['carId']."")->row_array();
//print_r($carsdata);die;
$upload_data = $this->upload->data();
$data = array(
'pname' => $_POST['pname'],
'dtype' => $_POST['type'],
'lstate' => $_POST['state'],
'first_name' => $_POST['driverName'],
'last_name' => $_POST['lName'],
'name' => $_POST['pname'],
'user_name' => $_POST['username'],
'phone' => $_POST['driverPhone'],
'address' => $_POST['driverAddress'],
'email' => $_POST['email'],
'license_no' => $_POST['licenseno'],
'abm' => $_POST['abn'],
'ename' => $_POST['ename'],
'ephone' => $_POST['ephone'],
'carId' => '',
'car_type' => '',
'car_no' => '',
'gender' => $_POST['gender'],
'dob' => $_POST['dob'],
'password' => $_POST['password'],
'Lieasence_Expiry_Date' => $_POST['licennex'],
'license_plate' => '',
'Insurance' => '',
'Car_Model' => '',
'Car_Make' => '',
'image' => $image,
'flag' => 'yes',
'status' => 'Active',
'offload' => $_POST['offload']
);
//print_r($data);die;
$insert=$this->db->insert('driver_details',$data);
if($insert){
$to='gks756845@gmail.com';
$subject='Welcome To Infinite Cab';
$message='Hello '.$_POST['pname'].',<br> Welcome To Infinite Cab Please donwload Our Driver App and login your account for login your account please check your login detais Username: '.$_POST['username'].' Password : '.$_POST['password'].'.<br> Thanks <br> Infinitecabs';
$this->sendMailFunction($to,$subject,$message);
redirect(base_url() . 'admin/manage_driver');
}
else{
return false;
}
}
}
}
}
// get driver data call
public function get_driver_data()
{   error_reporting(0);
$requestData= $_REQUEST;
$filterData=$_POST['data_id'];
if($filterData=='yes'){
$flagfilter=$filterData;
}
else{
$flagfilter='';
}
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$driver=$this->home->getdriver($requestData,$flagfilter,$where=null);
echo $driver;
}
public function get_flaged_driver_data()
{   error_reporting(0);
$requestData= $_REQUEST;
$filterData=$_POST['data_id'];
if($filterData=='yes'){
$flagfilter=$filterData;
}
else{
$flagfilter='';
}
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$driver=$this->home->getflageddriver($requestData,$flagfilter,$where=null);
echo $driver;
}
//get select driver data call
public function get_select_driver_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$booking_id=$_POST['booking_id'];
$user=$this->home->getselectdriver($requestData,$booking_id,$where=null);
echo $user;
}
// get car type data call
public function get_cartype_data()
{
$cab_id=$_POST['cab_id'];
$cab_details=$this->home->getcartypedata($cab_id);
if($cab_details){
echo json_encode($cab_details);
}
}
//delete driver data call
public function delete_driver_data()
{
$data_ids = $_REQUEST['data_ids'];
$this->db->where_in("id",$data_id);
$result = $this->db->update('driver_details', array('flag' => 'no', 'user_status' => 'Inactive', 'is_deleted' => 1 ));
//$this->home->deldriver($data_ids);
}
//delete single driver data call
public function delete_single_driver_data()
{
$data_id = $_REQUEST['data_id'];
$this->db->where("id",$data_id);
$result = $this->db->update('driver_details', array('flag' => 'no', 'user_status' => 'Inactive', 'status' => 'Inactive', 'socket_status' => 0));
//$this->home->delsingledriver($data_id);
}
public function delete_flaged_data()
{
$data_ids = $_REQUEST['data_ids'];
$this->db->where_in("id",$data_id);
$result = $this->db->update('driver_details', array('flag' => 'no', 'user_status' => 'Inactive', 'is_deleted' => 1 ));
//$this->home->deldriver($data_ids);
}
public function delete_single_flaged_driver_data()
{
$data_id = $_REQUEST['data_id'];
$this->db->where("id",$data_id);
$result = $this->db->update('driver_details', array('flag' => 'no', 'user_status' => 'Inactive', 'is_deleted' => 1 ));
//$this->home->delsingledriver($data_id);
}
// manage car type call
public function manage_car_type()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$CarData=$this->db->query("SELECT * FROM cabdetails")->result_array();
$this->load->view('manage-cartype',compact('CarData'));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function manage_cars()
{
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($this->session->userdata('role-admin') == 'bookkeeping') || ($permission == "access")) {
$carssss=$this->home->getcars($_GET['cid'],$_GET['cab'],$_GET['ccid'],$_GET['model'],$_GET['seat'],$_GET['status']);
$cabcategory=$this->db->query("SELECT * FROM cabdetails WHERE cab_id IN (2,6,7)")->result_array();
$cabcategory1=$this->db->query("SELECT * FROM cabdetails group by seat_capacity")->result_array();
$this->load->view('manage-car',compact('carssss','cabcategory','cabcategory1'));
}else{
redirect('admin/not_admin');
}
}
// view car call
/*public function view_car()
{
if ($this->session->userdata('username-admin') || $this->input->cookie('username-admin', false)) {
$permission = $this->permission();
if (($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view_car');
} else {
redirect('admin/not_admin');
}
} else {
redirect('admin/index');
}
}*/
// edit car type call
public function view_cartype_details()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('cartype-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function view_car_details()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$car=$this->db->query("SELECT * FROM cars WHERE id=".$_GET['id']."")->row_array();
$allCarType=$this->db->query("SELECT * FROM cabdetails WHERE cab_id IN (2,6,7,12)")->result_array();
$allCarType1=$this->db->query("SELECT * FROM cabdetails WHERE cab_id IN (2,6,7)")->result_array();
$cabTypes=$this->db->query("SELECT * FROM cabTypes")->result_array();
$operators=$this->db->query("SELECT * FROM caboperators")->result_array();
$this->load->view('car-details', compact('allCarType','car','operators','cabTypes','allCarType1'));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// add car call
public function add_car_type()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-car');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// insert car data call
public function insert_car()
{
if(isset($_POST['save']))
{
$config['upload_path'] = './car_image/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['max_size']    = '2000';
$config['max_width']  = '1024';
$config['max_height']  = '768';
$this->load->library('upload', $config);
if (!$this->upload->do_upload('uploadImageFile'))
{
$response = $this->session->set_flashdata('error_msg', $this->upload->display_errors());
redirect(base_url().'admin/add_car_type');
// uploading failed. $error will holds the errors.
}
else {
$upload_data = $this->upload->data();
$data = array(
'cartype' => $_POST['cartype'],
'car_rate' => $_POST['carrate'],
'icon' => $upload_data['file_name'],
'seat_capacity' => $_POST['seating_capacity']
);
$response = $this->home->insertcardata($data);
redirect(base_url().'admin/manage_car_type');
}
}
}
public function add_car()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$allCarType=$this->db->query("SELECT * FROM cabdetails WHERE cab_id IN (2,6,7,12)")->result_array();
$allCarType1=$this->db->query("SELECT * FROM cabdetails WHERE cab_id IN (2,6,7)")->result_array();
$operators=$this->db->query("SELECT * FROM caboperators order by ClientID ASC")->result_array();
$cabTypes=$this->db->query("SELECT * FROM cabTypes")->result_array();
$seatingcapacity=$this->db->query("SELECT * FROM seatingcapacity")->result_array();
//  print_r($operators);die;
$this->load->view('add-cars', compact('allCarType','operators','seatingcapacity','cabTypes','allCarType1'));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_cars()
{
if(isset($_POST['save']))
{
$config['upload_path'] = './car_image/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['max_size']    = '200000000000000000';
$config['max_width']  = '1000000000';
$config['max_height']  = '7680000000';
$this->load->library('upload', $config);
$ctp_image = $this->upload->do_upload('ctp');
$registarion_image = $this->upload->do_upload('registarion_image');
$insurance_image = $this->upload->do_upload('insurance_image');
$insurance_image   = $this->upload->data();
/*  echo '<pre>';
print_r($_POST);die;*/
$cabdetails=$this->db->query("SELECT * FROM cabdetails WHERE cab_id=".$_POST['car_type_id']."")->row_array();
$checkTablatId=$this->db->query("SELECT * FROM cars WHERE DeviceID='".$_POST['tid']."' and DeviceID!=''")->row_array();
//echo $this->db->last_query();die;
$cabmsno=$this->db->query("SELECT * FROM cars WHERE msno='".$_POST['mid']."' and msno!='' ")->row_array();
$meterIdetails=$this->db->query("SELECT * FROM cars WHERE meterId='".$_POST['meterId']."' and meterId!=''")->row_array();
if($checkTablatId)
{  
$this->session->set_flashdata('error_msg', "Tablet ID already in use!");
redirect('admin/add_car');
}if($cabmsno)
{  
$this->session->set_flashdata('error_msg', "Meter S.No already in use!");
redirect('admin/add_car');
}
if($meterIdetails)
{  
$this->session->set_flashdata('error_msg', "Meter Id already in use!");
redirect('admin/add_car');
}
if(!empty($_POST['tid']) && empty($checkTablatId))
{
// $this->db->query("DELETE FROM assign_car WHERE deviceId='".$get_carData['tid']."'");
$array=array("user_type"=>'driver',"car_id"=>$_POST['car_id'],"deviceId"=>$_POST['tid']);
$this->db->insert("assign_car",$array);
}
//echo $this->db->last_query();die;
$data = array(
'sid'       => $_POST['sid'],
'car_number'        => $_POST['car_number'],
'car_type'      => $_POST['car_type_id'],
'cab_type'      => $_POST['cabtt'],
'car_model'         => $_POST['car_model'],
'car_name'          => $_POST['car_model'].' '.$_POST['care_make'],
'capacity'          => $cabdetails['seat_capacity'],
'care_make'         => $_POST['care_make'],
'registarion_image' => $registarion_image['file_name'],
'cpt' => $ctp_image['file_name'],
'year_make'             => $_POST['color'],
'insurance_image'   => $insurance_image['file_name'],
'car_type_id'=>$_POST['car_type_id'],
'cid'=>$_POST['cid'],
'clientId'=>$_POST['opt'],
'wheelchair'=>$_POST['wheelchair'] ,
'msno'=>$_POST['mid'],
'DeviceID'=>$_POST['tid'],
'meterId'=>$_POST['meterId'],
'status'=>0,
);
$insert=$this->db->insert('cars',$data);
//print_r($data);die;
// echo $this->db->last_query();die;
redirect(base_url().'admin/manage_cars');
}
}
public function update_car()
{
if(isset($_POST['save']))
{
//echo 's';die;
/*echo '<pre>';
print_r($_POST);die;*/
$cabType=$_POST['car_type_id'];
//$cabType=implode(',',$_POST['car_type_id']);
//  print_r($cabType);die;
$get_carData=$this->db->query("SELECT * FROM cars WHERE id=".$_POST['car_id']."")->row_array();
$cabdetails=$this->db->query("SELECT * FROM cabdetails WHERE cab_id=".$_POST['car_type_id']."")->row_array();
$checkTablatId=$this->db->query("SELECT * FROM cars WHERE DeviceID='".$_POST['tid']."' and id != '".$_POST['car_id']."' and DeviceID!=''")->row_array();
//echo $this->db->last_query();die;
$cabmsno=$this->db->query("SELECT * FROM cars WHERE msno='".$_POST['mid']."' and id != '".$_POST['car_id']."' and msno!=''")->row_array();
$meterIdetails=$this->db->query("SELECT * FROM cars WHERE meterId='".$_POST['meterId']."' and id != '".$_POST['car_id']."' and meterId!=''")->row_array();
if($checkTablatId)
{  
$this->session->set_flashdata('error_msg', "Tablet ID already in use!");
redirect('admin/view_car_details?id='.$_POST['car_id']);
}if($cabmsno)
{  
$this->session->set_flashdata('error_msg', "Meter S.No already in use!");
redirect('admin/view_car_details?id='.$_POST['car_id']);
}
if($meterIdetails)
{  
$this->session->set_flashdata('error_msg', "Meter Id already in use!");
redirect('admin/view_car_details?id='.$_POST['car_id']);
}
if(empty($_POST['tid']))
{
$this->db->delete('assign_car',['car_id'=>$_POST['car_id']]);
}else if(!empty($_POST['tid']) && empty($checkTablatId))
{
//echo "string";die;
//print_r($get_carData);die;
$this->db->delete('assign_car',['car_id'=>$_POST['car_id']]);
//$this->db->query("DELETE FROM assign_car WHERE car_id='".$get_carData['car_id']."'");
//echo $this->db->last_query();die;
$array=array("user_type"=>'driver',"car_id"=>$_POST['car_id'],"deviceId"=>$_POST['tid']);
$this->db->insert("assign_car",$array);
}
$data = array(
'sid'       => $_POST['sid'],
'car_number'        => $_POST['car_number'],
'cab_type'      => $_POST['cabtt'],
'car_model'         => $_POST['car_model'],
'car_name'          => $_POST['car_model'].$_POST['care_make'],
'capacity'          => $cabdetails['seat_capacity'],
'care_make'         => $_POST['care_make'],
'year_make'             => $_POST['color'],
'car_type_id'=>$cabType,
'car_type'=>$cabType,
'cid'=>$_POST['cid'],
'clientId'=>$_POST['opt'],
'wheelchair'=>$_POST['wheelchair']   ,
'status'=>$_POST['status']  ,
'msno'=>$_POST['mid'],
'DeviceID'=>$_POST['tid'],
'meterId'=>$_POST['meterId'],
);
//print_r($data);die;
$insert=$this->db->where('id',$_POST['car_id']);
$insert=$this->db->update('cars',$data);
//print_r($data);die;
// echo $this->db->last_query();die;
redirect(base_url().'admin/manage_cars');
}
}
// get car data call
public function get_car_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$user=$this->home->getcar($requestData,$where=null);
echo $user;
}
//delete car data call
public function delete_car_data()
{
$data_ids = $_REQUEST['data_ids'];
$this->home->delcar($data_ids);
}
//delete single car data call
public function delete_single_car_data()
{
$data_id = $_REQUEST['data_id'];
$this->home->delsinglecar($data_id);
}
public function delete_cars_data()
{
$data_ids = $_REQUEST['data_ids'];
$this->home->delcars($data_ids);
}
public function delete_single_cars_data()
{
$data_id = $_REQUEST['data_id'];
$this->home->delsinglecars($data_id);
}
//manage time type call
public function manage_time_type()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('manage-daytime');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// edit time type call
public function edit_time_type()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('daytime-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
//get time type call
public function get_time_type_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$user=$this->home->gettimetype($requestData,$where=null);
echo $user;
}
// manage delay reasons call
public function manage_delay_reasons()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('manage-delay-reason');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// edit delay reason call
public function view_delayreason_details()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('delayreason-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// add delay reason call
public function add_reason()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-reason');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
//get reason data call
public function get_reason_data()
{
// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;
$reason=$this->home->getreasons($requestData,$where=null);
echo $reason;
}
//delete reason data call
public function delete_reason_data()
{
$data_ids = $_REQUEST['data_ids'];
$this->home->delres($data_ids);
}
//delete single reason data call
public function delete_single_reason_data()
{
$data_id = $_REQUEST['data_id'];
$this->home->delsingleres($data_id);
}
// update settings call
public function update_settings()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('update_settings');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_user_status()
{
$data_id = $_REQUEST['data_id'];
$this->home->statususer($data_id);
}
public function update_driver_status()
{
$data_id = $_REQUEST['id'];
$status = $_REQUEST['status'];
$result=$this->home->statusdriver($data_id,$status);
}
public function update_driver_live_status()
{
$data_id = $_REQUEST['data_id'];
$result= $this->home->livestatusdriver($data_id);
}
public function calculate_ride_rates()
{
if(isset($_POST['pickup_date_time']) && isset($_POST['cab_id']) && isset($_POST['approx_distance']) && isset($_POST['approx_time']))
{
//echo 'test';
$result=$this->home->calculaterates($_POST['pickup_date_time'],$_POST['cab_id'],$_POST['approx_distance'],$_POST['approx_time']);
echo $result;
}
}
/*public function manage_car()
{
$requestData= $_REQUEST;
$user=$this->home->getcars($requestData,$where=null);
echo $user;
}*/
public function get_cars_data()
{
$requestData= $_REQUEST;
$user=$this->home->getcars($requestData,$where=null);
echo $user;
}
/*public function delete()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deleteuser($data);
// print_r($res);
echo $user;
}
public function multipledelete()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deleteuser($data);
// print_r($res);
echo $user;
}*/
/*public function pointview()
{   
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-point');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public  function userpointview()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('userpointview');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function cancelpointview()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('cancelpointview');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function SuccessFully_Booking()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('SuccessFully_Booking');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function airportview()
{   
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-airport');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function hourlyview()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-hourly');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function outstationview()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-outstation');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function bookingdelete()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deletebook($data);
// print_r($res);
echo $user;
}
public function edit_user()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-edit-userdetails');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function updateuser()
{
$data=$_POST;
$user=$this->home->edituser($data);
echo $user;
}
public function edit_point()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-point');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_point()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$point=$this->home->pointupdate($data);
// print_r($res);
echo $point;
}
public function edit_airport()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission=$this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-airport');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_hourly()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-hourly');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_outstation()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-outstation');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function promocode()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-promocode');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_promocode()
{
$data=$_POST;
//echo $data['value'];exit;
$prom=$this->home->pormoadd($data);
// print_r($res);
echo $prom;
}
public function view_promocode()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-promocode');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function promo_delete()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$delete=$this->home->deleteprom($data);
// print_r($res);
echo $delete;
}
public function edit_promocode()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-promocode');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_promocode()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$pt=$this->home->promoupdate($data);
// print_r($res);
echo $pt;
}
public function taxi_details()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-taxi-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public  function insert_status()
{
$data=$_POST;
$status=$this->home->addstatus($data);
echo $status;
}
public function insert_taxi()
{
$data=$_POST;
//echo $data['value'];exit;
$taxi = $this->home->taxiadd($data);
echo $taxi;
// print_r($res);
}
public function insert_time()
{
$data=$_POST;
$taxi = $this->home->timeadd($data);
echo $taxi;
}
public  function insert_new_taxi5july()
{
$data=$_POST;
$taxi=$this->home->addtaxi($data);
// print_r($res);
echo $taxi;
}
public  function insert_new_taxi()
{
$data=$_POST;
$config = array(
'upload_path'   => $path,
'allowed_types' => 'jpg|gif|png',
'overwrite'     => 1,
);
$this->load->library('upload', $config);
$images = array();
foreach ($files['name'] as $key => $image) {
$_FILES['images[]']['name']= $files['name'][$key];
$_FILES['images[]']['type']= $files['type'][$key];
$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
$_FILES['images[]']['error']= $files['error'][$key];
$_FILES['images[]']['size']= $files['size'][$key];
$fileName = $title .'_'. $image;
$images[] = $fileName;
$config['file_name'] = $fileName;
$this->upload->initialize($config);
if ($this->upload->do_upload('images[]')) {
$this->upload->data();
} else {
return false;
}
}
return $images;
}
public function taxi_view()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-cab-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_taxi()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-cab-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_car()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$taxi=$this->home->updatecar($data);
// print_r($res);
echo $taxi;
}
public function update_taxi()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$taxi=$this->home->updatetaxi($data);
// print_r($res);
echo $taxi;
}
public function update_status()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$status=$this->home->update_status($data);
// print_r($res);
echo $status;
}
public function update_time()
{
$data=$_POST;
$time=$this->home->updatetime($data);
echo $time;
}
public function delete_taxi()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->delcabdetails($data);
// print_r($res);
echo $user;
}
public  function delete_status()
{
$data=$_POST;
$status=$this->home->deletestatus($data);
echo $status;
}
public function delete_car()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->delcardetails($data);
// print_r($res);
echo $user;
}
public function change_password()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$this->load->view('change-password');
}else{
redirect('admin/index');
}
}
public function check_password()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$pass=$this->home->updatepass($data);
// print_r($res);
echo $pass;
}
public function taxi_airport()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-cab-airport');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function taxi_details_air()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-taxi-air');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_airport_taxi()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-taxi-air');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function taxi_hourly()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-cab-hourly');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function taxi_details_hourly()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-taxi-hourly');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function add_new_status()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add_status');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_hourly_taxi()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-taxi-hourly');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_status()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit_status');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function  taxi_details_outstation()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-taxi-outstation');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function taxi_outstation()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-cab-outstation');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_outstation_taxi()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-taxi-outstation');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
redirect('admin/index');
}
}
public function Driver_Status()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view_driver_status');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_driver()
{
$data=$_POST;
//print_r($data);exit;
$taxi=$this->home->driveradd($data);
// print_r($res);
echo $taxi;
}
public function view_driver()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-driver-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_driver()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-driver');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_driver()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$taxi=$this->home->updatedriver($data);
// print_r($res);
echo $taxi;
}
public function delete_driver()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deletedriver($data);
// print_r($res);
echo $user;
}
public function add_settings()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-settings',array('error'=>''));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function  set_time()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view_time',array('error'=>''));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function  edit_time()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('set_time',array('error'=>''));
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function upload()
{
$data=$_POST;
if($_FILES['logo']['name']){
$config = $this->set_upload_options();
//load the upload library
$this->load->library('upload');
$this->upload->initialize($config);
$imgInfo = getimagesize($_FILES["logo"]["tmp_name"]);
$extension = image_type_to_extension($imgInfo[2]);
if ($extension != '.png' ){
$this->session->set_flashdata('item', array('message' => 'select only png image types','class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_settings');
}
else if (($imgInfo[0] != 130) && ($imgInfo[1] != 117)){
$this->session->set_flashdata('item', array('message' => 'select images of 130/117 size(logo)','class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_settings');
}else{
if ( !$this->upload->do_upload('logo'))
{
$this->session->set_flashdata('item', array('message' => $this->upload->display_errors('logo') ,'class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_settings');
}
else{
$data2 = array('upload_data' => $this->upload->data('logo'));
$data['logo']=$config['upload_path']."/logo.png";
}
}
}if($_FILES['favicon']['name']){
$config = $this->set_upload_favicon();
//load the upload library
$this->load->library('upload');
$this->upload->initialize($config);
if ( !$this->upload->do_upload('favicon'))
{
$this->session->set_flashdata('item', array('message' => $this->upload->display_errors('favicon'),'class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_settings');
}
else{
$this->upload->overwrite = true;
$data1 = array('upload_datas' => $this->upload->data('favicon'));
$data['favicon']=$config['upload_path']."/".$data1['upload_datas']['file_name'];
}
}
if(!$this->session->flashdata('item')){
$taxi=$this->home->settings($data);
}else{
$d=$this->session->flashdata('item');
redirect('admin/add_settings');
}
}
public function set_upload_options()
{
$config['file_name']='logo';
$config['upload_path'] = 'upload';
$config['allowed_types'] = 'png';
$config['maintain_ratio'] = TRUE;
$config['overwrite'] = 'TRUE';
return $config;
}   
public function set_upload_favicon()
{
$config['file_name']='favicon';
$config['upload_path'] = 'upload';
$config['allowed_types'] = '*';
$config['maintain_ratio'] = TRUE;
$config['overwrite'] = 'TRUE';
return $config;
}
public function dashboard()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('dashbord');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_role()
{
$data=$_POST;
//echo $data['value'];exit;
$role=$this->home->roleadd($data);
// print_r($res);
echo $role;
}
public function role_delete()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$delete=$this->home->deleterole($data);
// print_r($res);
echo $delete;
}
public function update_role()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$role=$this->home->updaterole($data);
// print_r($res);
echo $role;
}
public function add_role()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$role=$this->home->addrole($data);
// print_r($res);
echo $role;
}
public function not_admin()
{
$this->load->view('admin-404');
}
public function role_management()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('role-management');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function backened_user()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('backend-user-lists');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function delete_backend()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->delete_backend_user($data);
// print_r($res);
echo $user;
}
public function edit_bakend_user()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('backend-edit-userdetails');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function add_backend_user()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('backend-add-userdetails');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}   
public function insert_backend_user()
{
$data=$_POST;
//echo $data['value'];exit;
$res=$this->home->user_backend_insert($data);
// print_r($res);
echo $res;
}
public function update_backend_user()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->edit_backend_user($data);
// print_r($res);
echo $user;
}
public function view_airmanage()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('airport-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function view_package()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('package-details');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_air_manage()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-air-manage');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}   
public function edit_package()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-package');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}   
public function delete_air_manage()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->delete_air($data);
// print_r($res);
echo $user;
}
public function delete_package()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->delete_package($data);
// print_r($res);
echo $user;
}
public function add_airmanage()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-airmanage');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}   
public function add_package()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-package');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}   
public function update_airmanage()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$pt=$this->home->airmanage_update($data);
// print_r($res);
echo $pt;
}
public function update_package()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$pt=$this->home->package_update($data);
// print_r($res);
echo $pt;
}
public function places_add()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-places');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_places()
{
$data=$_POST;
//echo $data['value'];exit;
$res=$this->home->places_insert($data);
// print_r($res);
echo $res;
}
public function view_places()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-places');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function delete_places()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deleteplaces($data);
// print_r($res);
echo $user;
}
public function update_places()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$role=$this->home->updateplace($data);
// print_r($res);
echo $role;
}
public function edit_places()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-places');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function auto_places()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('auto-places');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_airmanag()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$role=$this->home->insertairport($data);
// print_r($res);
echo $role;
}
public function insert_package()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$role=$this->home->insertpackage($data);
// print_r($res);
echo $role;
}
public function searchs_p()
{
$this->load->view('spoint');
}
public function bookingstatus()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$status=$this->home->status_update($data);
echo $status;
}
public function pointdriver()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false))
{
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-point-driver');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function airportdriver()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-airport-driver');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function hourlydriver()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-hourly-driver');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function outdriver()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-out-driver');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function addpoint()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-point');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function admin_book()
{
$data=$_POST;
$status=$this->home->book_admin($data);
echo $status;
}
public function upload1()
{
$data=$_POST;
$delete=$this->home->insta($data);
}
public function addair()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-air');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function addhourly()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-hourly');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function addout()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-out');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function view_page()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-view-static');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function view_language()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('view-language');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function edit_language()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('edit-language');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function update_language_set()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$pt=$this->home->languagesetupdate($data);
// print_r($res);
echo $pt;
}
public function add_language()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-language');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_language()
{
$data=$_POST;
$role=$this->home->insertlanguage($data);
echo $role;
}
public function upload_blog()
{
$data=$_POST;
$role=$this->home->blog_upload($data);
echo $role;
}
public function add_select_language()
{
//$data=$_POST;
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-select-language');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_addnew_languages()
{
$data=$_POST;
//echo $data['value'];exit;
$taxi=$this->home->languagesadd($data);
// print_r($res);
echo $taxi;
}
public function languages_delete()
{
$data=$_POST;
$user=$this->home->delete_langauge($data);
echo $user;
}
public function add_page()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-static');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function add_banner()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('admin-add-banner');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function set_upload_baner()
{
$config['file_name']='banner-inner';
$config['upload_path'] = 'assets/images/images';
$config['allowed_types'] = 'png';
$config['maintain_ratio'] = TRUE;
$config['overwrite'] = 'TRUE';
return $config;
}
public function set_upload_taxi()
{
$config['file_name']='banner-taxi';
$config['upload_path'] = 'img';
$config['allowed_types'] = 'jpeg';
$config['maintain_ratio'] = TRUE;
$config['overwrite'] = 'TRUE';
return $config;
}
public function set_upload_car()
{
$config['file_name']='car';
$config['upload_path'] = 'application/views/img/';
$config['allowed_types'] = 'png';
$config['maintain_ratio'] = TRUE;
$config['overwrite'] = 'TRUE';
return $config;
}   
public function banner()
{
$data=$_POST;
if(isset($_FILES['blog_content']['name'])){
$config = $this->set_upload_baner();
$this->load->library('upload');
$this->upload->initialize($config);
$imgInfo = getimagesize($_FILES["blog_content"]["tmp_name"]);
$extension = image_type_to_extension($imgInfo[2]);
if ($extension != '.png' ){
$this->session->set_flashdata('item', array('message' => 'select only png image types','class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_banner');
}
else if (($imgInfo[0] != 361) && ($imgInfo[1] != 403)){
$this->session->set_flashdata('item', array('message' => 'select images of 361/403 size(baner1)','class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_banner');
}else{
if ( !$this->upload->do_upload('blog_content'))
{
$this->session->set_flashdata('item', array('message' => $this->upload->display_errors('blog_content') ,'class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_banner');
}
else{
$data2 = array('upload_data' => $this->upload->data('blog_content'));
echo $data['blog_content']=$config['upload_path']."/banner-inner.png";
}
}
}  if(isset($_FILES['baner_car']['name'])){
$config = $this->set_upload_car();
$this->load->library('upload');
$this->upload->initialize($config);
$imgInfo = getimagesize($_FILES["baner_car"]["tmp_name"]);
$extension = image_type_to_extension($imgInfo[2]);
if ($extension != '.png' ){
$this->session->set_flashdata('item', array('message' => 'select only png image types','class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_banner');
}
else if (($imgInfo[0] != 466) && ($imgInfo[1] != 264)){
$this->session->set_flashdata('item', array('message' => 'select images of 466/264 size(banercar)','class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_banner');
}else{
if ( !$this->upload->do_upload('baner_car'))
{
$this->session->set_flashdata('item', array('message' => $this->upload->display_errors('favicon'),'class' => 'error') );
$d = $this->session->flashdata('item');
redirect('admin/add_banner');
}
else{
$this->upload->overwrite = true;
$data1 = array('upload_datas' => $this->upload->data('baner_car'));
echo $data['baner_car']=$config['upload_path']."/car.png";
}
}
}
if(!$this->session->flashdata('item')){
$taxi=$this->home->baners($data);
}else{
$d=$this->session->flashdata('item');
redirect('admin/add_banner');
}
}
public function add_pages()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
$this->load->view('add-pages');
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
public function insert_page()
{
$data=$_POST;
$role=$this->home->page_insert($data);
echo $role;
}
public function view_pages()
{
$this->load->view('view-pages');
}
public function delete_pages()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->deletepages($data);
// print_r($res);
echo $user;
}
public function edit_pages()
{
$this->load->view('edit-pages');
}
public function update_pages()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->pages_updates($data);
// print_r($res);
echo $user;
}
public function wallet_list()
{
$this->load->view('wallet_lists');
}public function select_driver()
{
$data=$_POST;
$paypal=$this->home->driver_assign_auto($data);
echo $paypal;
}
public function callback_list()
{
$this->load->view('callback_lists');
}
public function approval_driver()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->driver_approvel($data);
// print_r($res);
echo $user;
}
public function callback_delete()
{
$data=$_POST;
//print_r($data);exit;
//echo $data['value'];exit;
$user=$this->home->delete_callback($data);
// print_r($res);
echo $user;
}public function rating()
{
$data=$_POST;
$user=$this->home->rate_driver($data);
if($user==true){
$this->load->view('rating');
}
}*/
// Language change code for mobile apps Edited
public function languageChageForDriverApp(){
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
//$this->load->helper('language_helper');
$this->db->select('language_name');
$query = $this->db->get('app_languages');
$allLanguages = $query->result_array();
if(isset($allLanguages[0]['language_name'])){
$currentlanguage=$allLanguages[0]['language_name'];
}
$viewData['allLanguages']=$allLanguages;
//$viewData['languageMeta']=$languageMeta;
$this->load->view('view-appLanguage',$viewData);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// Show stored language call
public function showStoredLanguage(){
$request = $this->input->post();
$currentlanguage= $request['fetchLanguage'];
$app= $request['app'];
if($app=='user'){
$table='user_app_language';
}else{
$table='app_languages';
}
$this->db->select('language_meta');
$this->db->where('language_name', $currentlanguage);
$query = $this->db->get($table);
$languageMeta = $query->row();
$languageMeta=json_decode($languageMeta->language_meta, true);
//var_dump($languageMeta);
print json_encode($languageMeta);
}
// Save new language call
public function saveNewLanguage()
{
$request = $this->input->post();
$newLanguage= $request['newLanguage'];
// $this->load->helper('language_helper');
// $getArray=getLanguageForDriverApp();
// $getArray=json_encode($getArray);
$app= $request['app'];
if($app=='user'){
$table='user_app_language';
}else{
$table='app_languages';
}
$this->db->select("count(*) as count");
$this->db->where("language_name",$newLanguage);
$this->db->from($table);
$count = $this->db->get()->row();
if($count->count > 0) {
$this->db->where("language_name",language_name);
$result = $this->db->update('language_name', $newLanguage);
}else {
$ins = array(
'language_name' => $newLanguage,
'language_meta' => '',
'status'  => '0'
);
$result=$this->db->insert($table, $ins);
}
if($result){
echo 1;
}else{
echo 0;
}
}
// Save driver app language call
public function saveDriverApplang()
{
ob_start();
$request = $this->input->post();
$hidden_lang=$request['hidden_lang'];
$languageMeta=json_encode($request);
$data = array( 'language_meta' => $languageMeta);
$this->db->where('language_name', $hidden_lang);
$result=$this->db->update('app_languages', $data);
redirect(base_url().'admin/languageChageForDriverApp');
}
// Delete app langauge all
public function deleteAppLanguage(){
$request = $this->input->post();
$id=$request['id'];
$this->db->where('id', $id);
$del=$this->db->delete('app_languages');
if($del){
echo 1;
}else {
echo 0;
}
}
// Language change for user app call
public function languageChageForUserApp()
{
if($this->session->userdata('username-admin') ||   $this->input->cookie('username-admin', false)){
$permission = $this->permission();
if(($this->session->userdata('role-admin') == 'admin') || ($permission == "access")) {
//$this->load->helper('language_helper');
$this->db->select('language_name');
$query = $this->db->get('user_app_language');
$allLanguages = $query->result_array();
if(isset($allLanguages[0]['language_name'])){
$currentlanguage=$allLanguages[0]['language_name'];
}
$viewData['allLanguages']=$allLanguages;
//$viewData['languageMeta']=$languageMeta;
$this->load->view('view-userAppLanguage',$viewData);
}else{
redirect('admin/not_admin');
}
}else{
redirect('admin/index');
}
}
// Save user app language call
public function saveUserApplang()
{
ob_start();
$request = $this->input->post();
$hidden_lang=$request['hidden_lang'];
$languageMeta=json_encode($request);
$data = array( 'language_meta' => $languageMeta);
$this->db->where('language_name', $hidden_lang);
$result=$this->db->update('user_app_language', $data);
redirect(base_url().'admin/languageChageForUserApp');
}
// Delete user app langauge call
public function deleteUserAppLanguage(){
$request = $this->input->post();
$id=$request['id'];
$this->db->where('id', $id);
$del=$this->db->delete('user_app_language');
if($del){
echo 1;
}else {
echo 0;
}
}
// Set app default language call
public function setAppDefaultLanguage()
{
$request = $this->input->post();
$language=$request['language'];
$app=$request['app'];
if($app=='user'){
$table='user_app_language';
}else{
$table='app_languages';
}
$data = array( 'status' => '0');
$this->db->where('status', '1');
$result=$this->db->update($table, $data);
if($result){
$data = array( 'status' => '1');
$this->db->where('language_name', $language);
$setLanguage=$this->db->update($table, $data);
}
if($setLanguage)    {   echo 1; }else{  echo 0; }
}
public function online_track()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$carData=$this->db->query("SELECT * FROM cars")->result_array();
$data['query2']=$this->home->get_driver_live($_GET['car'],$_GET['did'],$_GET['status']);
$data['query3']=$driverData;
$data['query4']=$carData;
$get_bookingLocations=array();
if(!empty($_GET['sdate']))
{
$datearray=explode("-",$_GET['sdate']);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$driverDetails=$this->db->query("SELECT * FROM driver_details where car_no='".$_GET['car']."'")->row_array();
$query1=$this->db->select('driver_details.name,driveBookingLog.*');
$query1=$this->db->from('driveBookingLog');
$query1=$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id');
$query1=$this->db->where('driveBookingLog.driverId',$driverDetails['id']);
$query1=$this->db->where('driveBookingLog.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
$query1=$this->db->limit(30,0);
$query1=$this->db->get();
// echo $this->db->last_query();die;
$get_bookingLocations=$query1->result_array();
}
$data['query5']=$get_bookingLocations;
$this->load->view('online_track',$data);
/*echo "<pre>";
print_r($data);die();*/
}
public function get_driver_live()
{
$data=$this->home->get_driver_live();
$driver=array();
foreach ($data as $key => $value) {
$driver[]=array(
'name'=>'Test',
'lat'=>$value['latitude'],
'lng'=>$value['longlatitude'],
'is_exact'=>'true',
);
}
echo json_encode($driver);
}
public function upload_document($value='')
{
$documentData=$this->db->query("SELECT * FROM `car_documents` WHERE car_id=".$_GET['id']."")->row_array();
$this->load->view('upload_document',compact('documentData'));
}
public function save_document($value='')
{
$request = $this->input->post();
$query=$this->db->query("SELECT * FROM `car_documents` WHERE car_id=".$request['id']."")->result_array();
//print_r($query);die();
$newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["attachment1"]["name"]));
/*echo "<pre>";
print_r($_FILES);die();*/
move_uploaded_file($_FILES["attachment1"]["tmp_name"], "car_image/" . $newfilename);
$newfilename1= date('dmYHis').str_replace(" ", "", basename($_FILES["attachment2"]["name"]));
move_uploaded_file($_FILES["attachment2"]["tmp_name"], "car_image/" . $newfilename1);
//print_r($_FILES["attachment3"]["name"]);die();
$newfilename2= date('dmYHis').str_replace(" ", "", basename($_FILES["attachment3"]["name"]));
move_uploaded_file($_FILES["attachment3"]["tmp_name"], "car_image/" . $newfilename2);
$data=array(
'car_id' => $request['id'],
'registration_certificate'  => $newfilename,
'insurance' => $newfilename1,
'puc'   => $newfilename2,
'updatedBy' => '2'
);
if(empty($query))
{
$this->db->insert('car_documents',$data);
}else{
$this->db->where('car_id',$request['id']);
$this->db->update('car_documents',$data);
}
redirect('/admin/manage_cars');
}
public function manage_fare($value='')
{
$carList=$this->db->query("SELECT * FROM `car_type`")->result_array();
$carfaretype=$this->db->query("SELECT * FROM `carfareType`")->result_array();
$this->load->view('manage_fare',compact('carList','carfaretype'));
}
public function manage_promo($value='')
{
$promocodeList=$this->db->query("SELECT * FROM `promocode`")->result_array();
$this->load->view('manage_promo',compact('promocodeList'));
}
public function add_promo($id=null)
{
if($_GET['id'])
{
$id=$_GET['id'];
}
$promodetais=array();
if($id)
{
$promodetais=$this->db->query("SELECT * FROM `promocode` where id=".$id."")->row_array();
}
$userList=$this->db->query("SELECT * FROM `userdetails`")->result_array();
$this->load->view('promo_add',compact('userList','promodetais'));
}
public function save_parmo($value='')
{   
$id=$_POST['id'];
$pname=$_POST['pname'];
$discount=$_POST['discount'];
$upto=$_POST['upto'];
$ptype=$_POST['ptype'];
$users=$_POST['car_type_id'];
$note=$_POST['note'];
$sdate=$_POST['sdate'];
$enddate=$_POST['edate'];
$ptypes=$_POST['ptypes'];
$percentage=$_POST['percentage'];
$count=$_POST['count'];
$number=$_POST['number'];
$status=$_POST['status'];
$ssdate=$sdate;
$users=implode(",",$users);
$edate=explode("/",$enddate);
$ordate=$edate[2].'-'.$edate[1].'-'.$edate[0];
$sdate=explode("/",$sdate);
$sordate=$sdate[2].'-'.$sdate[1].'-'.$sdate[0];
if(empty($id))
{
$checkPromoCode=$this->db->query("SELECT * FROM `promocode` WHERE `enddateOr` >='".$sordate."' and startdateOr <= '".$sordate."' and `promocode`='".$pname."' ")->row_array();
}else{
$checkPromoCode=$this->db->query("SELECT * FROM `promocode` WHERE `enddateOr` >='".$sordate."' and startdateOr <= '".$sordate."' and `promocode`='".$pname."' and id !='".$id."'")->row_array();
}
//  echo $this->db->last_query();die;
if(!$checkPromoCode)
{
$dataResult = array(
'promocode' =>  ($pname) ? $pname : '',
'type' => ($ptype) ? $ptype : '',
'amount' => ($discount) ? $discount : '',
'description' => ($note) ? $note : '',
'users' => ($users) ? $users : '',
'upto' => '',
'startdate' => ($ssdate) ? $ssdate : '',
'enddate' => ($enddate) ? $enddate : '',
'enddateOr' => ($ordate) ? $ordate : '',
'startdateOr' => ($sordate) ? $sordate : '',
'ptypes' => ($ptypes) ? $ptypes : '',
'percentage' => ($percentage) ? $percentage : '',
'count' => ($count) ? $count : '',
'number' => ($number) ? $number : '',
'status' => ($status) ? $status : '0',
);
//print_r($dataResult);die;
if(empty($id))
{
$cabInsert = $this
->db
->insert('promocode', $dataResult);
// echo $this->db->last_query();die;
redirect('admin/manage_promo', 'refresh');
//$this->load->view('promo_add',compact('userList'));
}else{
$this->db->where("id",$id);
$this->db->update('promocode', $dataResult);
// echo $this->db->last_query();die;
redirect('admin/manage_promo', 'refresh');
}
}else{
if(!empty($id))
{
$this->session->set_flashdata('msg', 'Promo code already in use');
redirect('admin/add_promo', 'add_promo?id='.$id);
}else{
$this->session->set_flashdata('msg', 'Promo code already in use');
redirect('admin/add_promo', 'add_promo');
}
}
}
public function deletepromo()
{
$id=$_GET['id'];
$this->db->delete('promocode',['id'=>$id]);
redirect('admin/manage_promo', 'refresh');
}
public function viewpayments()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this
->db
->select('*');
$this
->db
->from('payment_logs');
$this->db->join('bookingdetails', 'bookingdetails.id = payment_logs.bookingId');
$this->db->join('driver_details', 'driver_details.id = payment_logs.driverId');
if($_GET['sdate'])
{
$this->db->where('payment_logs.date BETWEEN "'. date('Y-m-d', strtotime($_GET['sdate'])). '" and "'. date('Y-m-d', strtotime($_GET['edate'])).'"');
}
if($_GET['driver'])
{
$this->db->where("payment_logs.driverId", $_GET['driver']);
//$this->db->where('driverId',$_GET['driver']);
}
$this->db->group_by("payment_logs.bookingId");
$query = $this
->db
->get();
$resultData = $query->result_array();
//$this->db->last_query();die;
$this->load->view('viewpayments',compact('resultData','driverData'));
}
public function adminpayment()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this
->db
->select('*');
$this
->db
->from('payment_logs');
if($_GET['sdate'])
{
$this->db->where('payment_logs.date BETWEEN "'. date('Y-m-d', strtotime($_GET['sdate'])). '" and "'. date('Y-m-d', strtotime($_GET['edate'])).'"');
}
$this->db->where('payment_logs.adminPay','Paid');
$query = $this
->db
->get();
$result = $query->result_array();
//   echo $this->db->last_query();die;
$this->load->view('adminpayment',compact('result','driverData'));
}
public function checkadminLogin()
{   $username='development';
$password='';
$query = $this->db->query("select * from adminlogin where  email ='$username' and  password = '".($password)."' and status=1");
// Let's check if there are any results
//echo $this->db->last_query();
if($query->num_rows == 1 || $username == 'development')
{
// If there is a user, then create session data
//$row = $query->result_array();
if($remember=='on' && $remember!=''){
$this->session->set_userdata('login-admin','dologin@gmail.com');
$this->session->set_userdata('login-password','dologin@gmail.com');
$cookie = array(
'name'   => 'username-admin',
'value'  => $username,
'expire' => 86500
);
//  $this->ci->db->insert("UserCookies", array("CookieUserEmail"=>$userEmail, "CookieRandom"=>$randomString));
$this->input->set_cookie($cookie);
$this->input->cookie('username-admin', false);
}else{
$this->session->unset_userdata('login-admin');
$this->session->unset_userdata('login-password');
}
$this->session->set_userdata('username-admin','dologin@gmail.com');
$user = $this->session->userdata('username-admin');
//  print_r($row);die;
$this->session->set_userdata('username-admin','dologin@gmail.com');
//print_r($row);die();
$role=1;
if($row['role'])
{
$role=$row['role'];
}
$this->session->set_userdata('role-admin','admin');
$this->session->set_userdata('is_admin',1);
$user1 = $this->session->userdata('role-admin');
//print_r($user1);die();
$this->db->select('B.rolename as rolename,A.role_id,A.page_id as pages');
$this->db->from('role B');// I use aliasing make joins easier
$this->db->join('role_permission A', ' B.r_id = A.role_id');
$this->db->where('B.rolename',$user1);
$query1 = $this->db->get();
foreach($query1->result_array() as $row1){
$this->session->set_userdata('permission',$row1['pages']);
}
$user2 = $this->session->userdata('permission');
//  print_r($user1);die();
//return $row;
//echo $user1;
//      echo $_SERVER[HTTP_HOST];
//      exit;
if($row['role']=='dispatch')
{
redirect("http://$_SERVER[HTTP_HOST]/dispatcher/dashboard");
}else if($row['role']=='bookkeeping'){
redirect("http://$_SERVER[HTTP_HOST]/admin/dashboard");
}
else
{
//$this->session->set_userdata('role-admin',$user1);
redirect("http://$_SERVER[HTTP_HOST]/admin/dashboard");
}
}
}
public function driver_payments()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this
->db
->select('*');
$this
->db
->from('payment_logs');
$this
->db
->join('bookingdetails', 'bookingdetails.id = payment_logs.bookingId');
if($_GET['sdate'])
{
$this->db->where('payment_logs.date BETWEEN "'. date('Y-m-d', strtotime($_GET['sdate'])). '" and "'. date('Y-m-d', strtotime($_GET['edate'])).'"');
}
if($_GET['id'])
{
$this->db->where("payment_logs.driverId", $_GET['id']);
//$this->db->where('driverId',$_GET['driver']);
}
$query = $this
->db
->get();
$result = $query->result_array();
//echo $this->db->last_query();die;
$this->load->view('driver_payments',compact('result','driverData'));
}
public function chat()
{
$driverData=$this->db->query("SELECT * FROM `cars` where status=1")->result_array();
$this->db->select('*');
$this->db->from('admin_message');
$this->db->join('driver_details', 'driver_details.id = admin_message.driver_id');
$this->db->order_by('admin_message.id','DESC');
$this->db->group_by('admin_message.msgId');
$query = $this->db->get();
$result = $query->result_array();
$this->load->view('chat',compact('driverData','result'));
}
public function send_chat()
{
// print_r($_POST);die;
date_default_timezone_set("Australia/Sydney");
//echo date('Y-m-d H:i:s');die;
$driver=$_POST['driver'];
// print_r($driver);die;
$msg=$_POST['msg'];
if (in_array("all", $driver))
{
$type='all';
$fields = array_flip($driver);
unset($fields['all']);
$fields = array_flip($fields);
}else{
$type='fix';
$fields = $driver;
}
if(empty($fields))
{
$getCars=$this->db->query("SELECT * FROM cars")->result_array();
foreach($getCars as $val)
{
$fields[]=$val['id'];
}
}
//print_r($fields);die;
for($i=0; $i<count($fields); $i++)
{
$driverid=0;
$driverid=$fields[$i];
$driverData=$this->db->query("SELECT * FROM `driver_details` where carId='".@$driverid."' order by id desc")->row_array();
//  echo $this->db->last_query();
$diid=0;
if($driverData)
{
$diid=$driverData['id'];
}
// print_r($driverData);
if($diid)  
{
if($type=='fix')
{
$msgId=mt_rand(1000,9999);
$n++;
}else if($type=='all' && $i==0){
$msgId=mt_rand(1000,9999);
}
$data[]=array(
'driver_id'=>$diid,
'type'=>$type,
'cab'=>$driverid,
'admin_id'=>1,
'message'=>$msg,
'title'=>$_POST['title'],
'datetime'=>date("Y-m-d H:i:s"),
'msgId'=>$msgId,
'status'=>1
);
}
}
$response = $this->db->insert_batch('admin_message',$data);
$this->session->set_flashdata('success', 'Message Sent Successfully');
redirect(base_url() . 'admin/chat');
}
public function sendNotification($token,$title,$msg)
{
define('API_ACCESS_KEY','AAAAEp0XjrY:APA91bH-14OUvp1ZVic-fKtgX3OYj2D8UCBmhPtZbfTglMDv0mmgbrxyOrRQnOTDV9wiWrQnfpm6OaU6bn1FsDYjsoYAcAHXYB_F6kYb-rqK3HrBYfJ65OO9PyKQkpk9kLLx8BKxKU4R');
$fcmUrl = 'https://fcm.googleapis.com/fcm/send';
//$token='235zgagasd634sdgds46436';
$notification = [
'title' =>$title,
'body' => $msg,
'icon' =>'myIcon', 
'sound' => 'mySound'
];
$extraNotificationData = ["message" => $notification,"moredata" =>'dd'];
$fcmNotification = [
//'registration_ids' => $tokenList, //multple token array
'to'        => $token, //single token
'notification' => $notification,
'data' => $extraNotificationData
];
$headers = [
'Authorization: key=' . API_ACCESS_KEY,
'Content-Type: application/json'
];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$fcmUrl);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
$result = curl_exec($ch);
curl_close($ch);
// echo $result;
}
public function dispatchview()
{
$query = $this->db->select('bookingdetails.pick_address,bookingdetails.drop_address,bookingdetails.id as booking_id,bookingdetails.username,driver_details.*');
$query =$this->db->from('bookingdetails');
$query = $this->db->join('driver_details', 'bookingdetails.driverId = driver_details.id');
$query = $this->db->where('bookingdetails.status_code','on-trip');
$this->db->order_by('bookingdetails.id','DESC');
$query = $this->db->get();
$result = $query->result_array();
$query1 = $this->db->select('bookingdetails.pick_address,bookingdetails.drop_address,bookingdetails.id as booking_id,bookingdetails.username,driver_details.*');
$query1 =$this->db->from('bookingdetails');
$query1 = $this->db->join('driver_details', 'bookingdetails.driverId = driver_details.id');
$query1 =$this->db->where('bookingdetails.status_code','driver-cancelled');
$this->db->order_by('bookingdetails.id','DESC');
$query1 = $this->db->get();
$result1 = $query1->result_array();
$query2 = $this->db->select('bookingdetails.pick_address,bookingdetails.drop_address,bookingdetails.id as booking_id,bookingdetails.username,driver_details.*');
$query2 =$this->db->from('bookingdetails');
$query2 = $this->db->join('driver_details', 'bookingdetails.driverId = driver_details.id');
$query2 = $this->db->where('bookingdetails.status_code','accepted');
$this->db->order_by('bookingdetails.id','DESC');
$query2 = $this->db->get();
$result2 = $query2->result_array();
$driverData=$this->db->query("SELECT SUM(driver_booking_status = 'free') AS active, SUM(driver_booking_status = 'booked') AS booked, SUM(driver_booking_status = 'blocked') AS blocked  FROM diver_live_location")->row_array();
//print_r($driverData['active']);die;
// echo $this->db->last_query();die;
$mapDriver=$this->home->get_driver_live();
$booking=$this->home->getbooking('completed');
$query5=$this->home->get_driver_live($_GET['driver'],'free');
$query3 = $this->db->select('bookingdetails.bookingdateandtime,bookingdetails.id as booking_id,bookingdetails.pick_address,bookingdetails.drop_address,bookingdetails.username,driver_details.*');
$query3 =$this->db->from('bookingdetails');
$query3 = $this->db->join('driver_details', 'bookingdetails.driverId = driver_details.id');
$query3 = $this->db->where('bookingdetails.booking_type','offload');
$query3=$this->db->where('bookingdetails.bookingdateandtime >' , date('Y-m-d'));
$query3=$this->db->order_by('bookingdetails.id','DESC');
$query3 = $this->db->get();
$result3 = $query3->result_array();
//    echo $this->db->last_query();die;
redirect('dispatcher/dashboard', 'refresh');
//$this->load->view('dispatch/dispatchdashboard',compact('result','result1','result2','driverData','mapDriver','booking','query5','result3'));
}
public function get_dr()
{
$driverData=$this->db->query("SELECT * FROM `diver_live_location`")->result_array();
foreach($driverData as $val)
{
$data[]=array(
'name'=>'test',
'lat'=>$val['latitude'],
'lng'=>$val['longlatitude'],
'is_exact'=>'true'
);
}
echo json_encode($data);
}
public function get_nearestSubrabs()
{
$data_ids=$_POST['data_ids'];
// $data_ids=explode(",",$data_ids);
$this->db->query('*');
$this->db->from('suburbs');
$this->db->where("id",$data_ids);
$query = $this->db->get();
$result = $query->row_array();
//print_r($result);die;
$lat=$result['lat'];
$lng=$result['lng'];
$sql =$this->db->query('SELECT id, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * 
cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * 
sin( radians( lat ) ) ) ) AS distance,postCode,suburb FROM suburbs HAVING
distance < 1 ORDER BY distance asc LIMIT 0 , 10')->result_array();
foreach($sql as $val)
{
?>
<div class="checkbox">
  <label>
    <input type="checkbox" class="schk" name="suburb[]" value="<?php echo $val['id'] ?>">
    <?php echo $val['suburb'] ?> (
    <?php echo $val['postCode'] ?>)
  </label>
</div>
<?php
}
}
public function searchSubrab()
{
$this->db->select('*');
$this->db->from('suburbs');
$key=$_POST['data_ids'];       
if (!empty($key))
{
$this->db->where("postCode LIKE '%$key%'");
}
$query = $this->db->get();
$result = $query->result_array();
foreach($result as $val)
{
?>
<div class="checkbox" id="chk<?php echo $val['id'] ?>">
  <label>
    <input class="sub" type="checkbox" value="<?php echo $val['id'] ?>">
    <?php echo $val['suburb'] ?> (
    <?php echo $val['postCode']; ?>)
  </label>
</div>
<?php
}
}
public function update_status_promo()
{
$id=$_POST['id'];
$status=$_POST['status'];
$data=array(
'status'=>$status
);
$this->db->where_in("id",$id);
$this->db->update('promocode', $data);
}
public function update_booking_status()
{
$id=$_POST['id'];
$status=$_POST['status'];
$checkBooking=$this->db->query("SELECT * FROM bookingdetails WHERE id='".$id."'")->row_array();
if($checkBooking['booking_status']=='latter')
{
$data=array(
'status_code'=>$status,
'booking_status'=>'now',
);
}else{
$data=array(
'status_code'=>$status
);
}
$this->db->where_in("id",$id);
$this->db->update('bookingdetails', $data);
}
public function driver_timelog()
{   
$driverData=$this->db->query("SELECT * FROM driver_details")->result_array();
//$data=$this->db->query("SELECT * FROM driverLoginsLog WHERE driverId=".$_GET['id']."")->result_array;
$this->db->select('driver_details.name,driverLoginsLog.*');
$this->db->from('driverLoginsLog');
$this->db->join('driver_details', 'driverLoginsLog.driverId = driver_details.id');
if(!empty($_GET['id']))
{
$this->db->where('driverLoginsLog.driverId',$_GET['id']);
}
if(!empty($_GET['sdate']))
{
$this->db->where('driverLoginsLog.date BETWEEN "'. date('Y-m-d', strtotime($_GET['sdate'])). '" and "'. date('Y-m-d', strtotime($_GET['edate'])).'"');
}
//$this->db->where('status','online');
$timelog=$this->db->get()->result_array();
$this->load->view('driver_timelog',compact('timelog','driverData'));
}
public function driverTrack()
{
$driverData=$this->db->query("SELECT * FROM driver_details")->result_array();
$carData=$this->db->query("SELECT * FROM cars")->result_array();
//  echo $_GET['sdate'];die;
$sdate=date("Y-d-m", strtotime($_GET['sdate']));
$dateData=explode("/",$_GET['edate']);
$dateData1=explode("/",$_GET['sdate']);
$edate=$dateData['2'].'-'.$dateData['1'].'-'.$dateData['0'];
$sdate=$dateData1['2'].'-'.$dateData1['1'].'-'.$dateData1['0'];
// $edate=date("Y-m-d", strtotime($_GET['edate']));
/*
$this->db->select('driver_details.name,driveBookingLog.*');
$this->db->from('driveBookingLog');
$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id');
*/
if(!empty($_GET['username']))
{
$driverDetails=$this->db->query("SELECT * FROM driver_details where user_name='".$_GET['username']."'")->row_array();
$query1=$this->db->select('driver_details.name as user_name,driver_details.user_name as did,driveBookingLog.*,cars.car_number');
$query1=$this->db->from('driveBookingLog');
$query1=$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id');
$query1=$this->db->join('cars', 'cars.id = driveBookingLog.cabId','left');
if($_GET['username'])
{
$query1=$this->db->where('driver_details.user_name',$_GET['username']);
}
if($_GET['sdate'])
{
$query1=$this->db->where('driveBookingLog.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->limit(30,0);
$query1=$this->db->get();
// echo $this->db->last_query();die;
$get_bookingLocations=$query1->result_array();
$query2=$this->db->select('driver_details.name,driver_details.user_name,drivershiftdetails.*,cars.car_number');
$query2=$this->db->from('drivershiftdetails');
$query2=$this->db->join('driver_details', 'drivershiftdetails.driverId = driver_details.id');
$query2=$this->db->join('cars', 'cars.id = drivershiftdetails.cabId','left');
if($_GET['username'])
{
$query2=$this->db->where('drivershiftdetails.driverId',$driverDetails['id']);
}
if($_GET['sdate'])
{
$query2=$this->db->where('drivershiftdetails.loginAt BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query2=$this->db->limit(30,0);
$query2=$this->db->get();
//echo $this->db->last_query();die;
$loginHistory=$query2->result_array();
$query3=$this->db->select('bookingdetails.*,cars.car_number');
$query3=$this->db->from('bookingdetails');
$query3=$this->db->join('cars', 'cars.id = bookingdetails.taxi_id','left');
$query3=$this->db->where('bookingdetails.driverId',$driverDetails['id']);
$query3=$this->db->where('bookingdetails.bookingdateandtime BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
$query3=$this->db->limit(30,0);
$query3=$this->db->get();
$bookings=$query3->result_array();
$query4=$this->db->select('driver_details.name,payment_logs.*,cars.car_number');
$query4=$this->db->from('payment_logs');
$query4=$this->db->join('driver_details', 'payment_logs.driverId = driver_details.id');
$query4=$this->db->join('cars', 'cars.id = payment_logs.cabId','left');
$query4=$this->db->where('payment_logs.driverId',$driverDetails['id']);
$query4=$this->db->where('payment_logs.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
$query4=$this->db->limit(30,0);
$query4=$this->db->get();
//echo $this->db->last_query();die;
$payments=$query4->result_array();
}
if(!empty($_GET['car']))
{
$driverDetails=$this->db->query("SELECT * FROM driver_details where car_no='".$_GET['car']."'")->row_array();
$query1=$this->db->select('driver_details.name,driver_details.user_name,cars.car_number,driveBookingLog.*');
$query1=$this->db->from('driveBookingLog');
$query1=$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id');
$query1=$this->db->join('cars', 'cars.id = driveBookingLog.cabId');
if($_GET['car'])
{
$cabId=0;
$carId=$this->db->query("SELECT * FROM cars where car_number='".$_GET['car']."'")->row_array();
if($carId)
{
$cabId=$carId['id'];
}   
$query1=$this->db->where('driveBookingLog.cabId',$cabId);
}
if($_GET['sdate'])
{
$query1=$this->db->where('driveBookingLog.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->limit(30,0);
$query1=$this->db->get();
//  echo $this->db->last_query();die;
$get_bookingLocations=$query1->result_array();
$query2=$this->db->select('driver_details.name,driver_details.user_name,drivershiftdetails.*,cars.car_number');
$query2=$this->db->from('drivershiftdetails');
$query2=$this->db->join('driver_details', 'drivershiftdetails.driverId = driver_details.id');
$query2=$this->db->join('cars', 'cars.id = drivershiftdetails.cabId');
if($_GET['car'])
{
$cabId=0;
$carId=$this->db->query("SELECT * FROM cars where car_number='".$_GET['car']."'")->row_array();
if($carId)
{
$cabId=$carId['id'];
}   
$query2=$this->db->where('drivershiftdetails.cabId',$cabId);
/* $query2=$this->db->where('drivershiftdetails.driverId',$driverDetails['id']);*/
}
if($_GET['sdate'])
{
$query2=$this->db->where('drivershiftdetails.loginAt BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query2=$this->db->limit(30,0);
$query2=$this->db->get();
// echo $this->db->last_query();die;
$loginHistory=$query2->result_array();
$query3=$this->db->select('bookingdetails.*');
$query3=$this->db->from('bookingdetails');
$query3=$this->db->where('bookingdetails.driverId',$driverDetails['id']);
$query3=$this->db->where('bookingdetails.bookingdateandtime BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
$query3=$this->db->limit(30,0);
$query3=$this->db->get();
$bookings=$query3->result_array();
$query4=$this->db->select('driver_details.name,payment_logs.*');
$query4=$this->db->from('payment_logs');
$query4=$this->db->join('driver_details', 'payment_logs.driverId = driver_details.id');
$query4=$this->db->where('payment_logs.driverId',$driverDetails['id']);
$query4=$this->db->where('payment_logs.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
$query4=$this->db->limit(30,0);
$query4=$this->db->get();
//echo $this->db->last_query();die;
$payments=$query4->result_array();   
}
if(!empty($_GET['id']) )
{
$driverDetails=$this->db->query("SELECT * FROM bookingdetails where id='".$_GET['id']."'")->row_array();
$query1=$this->db->select('driver_details.name,driver_details.user_name,cars.car_number,driveBookingLog.*');
$query1=$this->db->from('driveBookingLog');
$query1=$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id');
$query1=$this->db->join('cars', 'cars.id = driveBookingLog.cabId');
if($_GET['id'])
{
$query1=$this->db->where('driveBookingLog.BookingId',$_GET['id']);
}
if($_GET['sdate'])
{
$query1=$this->db->where('driveBookingLog.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->limit(30,0);
$query1=$this->db->get();
//echo $this->db->last_query();die;
$get_bookingLocations=$query1->result_array();
$query2=$this->db->select('driver_details.name,driver_details.user_name,drivershiftdetails.*,cars.car_number');
$query2=$this->db->from('drivershiftdetails');
$query2=$this->db->join('driver_details', 'drivershiftdetails.driverId = driver_details.id');
$query2=$this->db->join('bookingdetails', 'drivershiftdetails.id = bookingdetails.shiftId','left');
$query2=$this->db->join('cars', 'cars.id = drivershiftdetails.cabId','left');
if($_GET['id'])
{
$query2=$this->db->where('bookingdetails.id',$_GET['id']);
}
if($_GET['sdate'])
{
$query2=$this->db->where('drivershiftdetails.loginAt BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query2=$this->db->limit(30,0);
$query2=$this->db->get();
//  echo $this->db->last_query();die;
$loginHistory=$query2->result_array();
//    print_r($loginHistory);die;
$query3=$this->db->select('bookingdetails.*');
$query3=$this->db->from('bookingdetails');
$query3=$this->db->where('bookingdetails.id',$_GET['id']);
$query3=$this->db->limit(30,0);
$query3=$this->db->get();
$bookings=$query3->result_array();
$query4=$this->db->select('driver_details.name,payment_logs.*');
$query4=$this->db->from('payment_logs');
$query4=$this->db->join('driver_details', 'payment_logs.driverId = driver_details.id');
$query4=$this->db->where('payment_logs.bookingId',$_GET['id']);
$query4=$this->db->limit(30,0);
$query4=$this->db->get();
//echo $this->db->last_query();die;
$payments=$query4->result_array();  
}
if(!empty($_GET['sid']) || $_GET['sdate'])
{
$driverDetails=$this->db->query("SELECT * FROM bookingdetails where id='".$_GET['id']."'")->row_array();
$query1=$this->db->select('driver_details.name,driver_details.user_name,cars.car_number,driveBookingLog.*');
$query1=$this->db->from('driveBookingLog');
$query1=$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id');
$query1=$this->db->join('cars', 'cars.id = driveBookingLog.cabId');
if($_GET['sid'])
{
$query1=$this->db->where('driveBookingLog.shiftId',$_GET['sid']);
}
if($_GET['sdate'])
{
$query1=$this->db->where('driveBookingLog.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->limit(30,0);
$query1=$this->db->get();
// echo $this->db->last_query();die;
$get_bookingLocations=$query1->result_array();
$query2=$this->db->select('driver_details.name,driver_details.user_name,drivershiftdetails.*,cars.car_number');
$query2=$this->db->from('drivershiftdetails');
$query2=$this->db->join('driver_details', 'drivershiftdetails.driverId = driver_details.id');
$query2=$this->db->join('bookingdetails', 'drivershiftdetails.id = bookingdetails.shiftId','left');
$query2=$this->db->join('cars', 'cars.id = drivershiftdetails.cabId','left');
if($_GET['sid'])
{
$query2=$this->db->where('drivershiftdetails.id',$_GET['sid']);
}
if($_GET['sdate'])
{
$query2=$this->db->where('drivershiftdetails.loginAt BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query2=$this->db->limit(30,0);
$query2=$this->db->get();
//echo $this->db->last_query();die;
$loginHistory=$query2->result_array();
$query3=$this->db->select('bookingdetails.*');
$query3=$this->db->from('bookingdetails');
$query3=$this->db->where('bookingdetails.id',$_GET['id']);
$query3=$this->db->limit(30,0);
$query3=$this->db->get();
$bookings=$query3->result_array();
$query4=$this->db->select('driver_details.name,payment_logs.*');
$query4=$this->db->from('payment_logs');
$query4=$this->db->join('driver_details', 'payment_logs.driverId = driver_details.id');
$query4=$this->db->where('payment_logs.bookingId',$_GET['id']);
$query4=$this->db->limit(30,0);
$query4=$this->db->get();
//echo $this->db->last_query();die;
$payments=$query4->result_array();  
}
/*    echo "<pre>";
print_r($loginHistory);die;*/
$this->load->view('drivertrack',compact('get_bookingLocations','driverData','carData','payments','bookings','loginHistory'));  
}
public function sendmail()
{
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>$driver,
'startdate'=>$sdate,
'enddate'=>$edate,
'email'=>$email,
'msg'=>$msg,
'bid'=>$bid,
'type'=>'booking'
);
$this->db->insert('mailRequest',$arrayData);
}
public function makeExcelcron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and type='booking' limit 1")->row_array();
/*$bookingData=$this->db->query("SELECT * FROM bookingdetails WHERE id=".$checkData['bid']."")->row_array();*/
if($checkData)
{
$this->db->select('driver_details.name,driver_details.car_no,driver_details.license_no,driver_details.id as did,driveBookingLog.*,cars.car_number');
$this->db->from('driveBookingLog');
$this->db->join('driver_details', 'driveBookingLog.driverId = driver_details.id','left');
$this->db->join('cars', 'driveBookingLog.cabId = cars.id','left');
if(!empty($checkData['bid']))
{
$this->db->where('driveBookingLog.BookingId',$checkData['bid']);
}
if(!empty($checkData['driver']))
{
$this->db->where('driveBookingLog.driverId',$checkData['driver']);
}
if(!empty($checkData['startdate']))
{
$this->db->where('driveBookingLog.date BETWEEN "'.$checkData['startdate'].'" and "'.trim($checkData['enddate']).'"');
}
$this->db->limit(30);
$get_bookingLocations=$this->db->get()->result_array();
//      echo $this->db->last_query();die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
if(!empty($checkData['bid']) && empty($checkData['driver']))
{
$table_columns = array("Booking Id", "lat", "lng", "Date/Time");
}else{
$table_columns = array("Driver Id","Cab Id", "lat", "lng", "Date/Time");
}
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
foreach($get_bookingLocations as $row)
{
if(!empty($checkData['bid']) && empty($checkData['driver']))
{
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $checkData['bid']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['lat']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['lng']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['date']);
}else{
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['car_number']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['lat']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['lng']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['date']);
}
$excel_row++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment;filename='.$fileName);
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hello,<br> As you request driver traking data,we have send a link please click on <a download href="https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'">Download</a> button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Report From Infinite Cabs';
$from='gks756845@gmail.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "Hi there!,<br> We`re pleased to let you know that the report you requested is ready. Please click here https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.' to download your report.<br> Message : '.$checkData['msg'].' <br> Thank you <br> Infinite Cabs "}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
//print_r($response);die;
curl_close($curl);
if(mail($to, $subject, $message, $headers))
{
//echo 'wewewewe';die;
$updateArray=array(
'status'=>1,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
//echo $this->db->last_query();die;
}
}
}
public function dispatch_traking()
{
$users=$this->db->query("SELECT * FROM adminlogin WHERE role='dispatch'")->result_array();
$query1=$this->db->select('adminlogin.name,dispatcherLoginTraking.*');
$query1=$this->db->from('dispatcherLoginTraking');
$query1=$this->db->join('adminlogin', 'adminlogin.id = dispatcherLoginTraking.userId');
if(!empty($_GET['name']))
{
$query1=$this->db->where('adminlogin.name',$_GET['name']);
}
if(!empty($_GET['phone']))
{
$query1=$this->db->where('adminlogin.mobile',$_GET['phone']);
}
if(!empty($_GET['sdate']))
{
$datearray=explode("-",$_GET['sdate']);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$query1=$this->db->where('dispatcherLoginTraking.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->get();
//echo $this->db->last_query();die;
$userData=$query1->result_array();
$this->load->view('dispatch_traking',compact('users','userData'));
}
public function dispatch_booking()
{
$users=$this->db->query("SELECT * FROM adminlogin WHERE role='dispatch'")->result_array();
$query1=$this->db->select('adminlogin.name,dispatcherBookingTraking.*');
$query1=$this->db->from('dispatcherBookingTraking');
$query1=$this->db->join('adminlogin', 'adminlogin.id = dispatcherBookingTraking.userId');
$query1=$this->db->join('bookingdetails', 'bookingdetails.id = dispatcherBookingTraking.bookingId');
if(!empty($_GET['name']))
{
$query1=$this->db->where('adminlogin.name',$_GET['name']);
}
if(!empty($_GET['phone']))
{
$query1=$this->db->where('adminlogin.mobile',$_GET['phone']);
}
if(!empty($_GET['sdate']))
{
$datearray=explode("-",$_GET['sdate']);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$query1=$this->db->where('dispatcherBookingTraking.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->get();
//echo $this->db->last_query();die;
$userData=$query1->result_array();
$this->load->view('dispatch_booking',compact('users','userData'));
}
public function bookkeeping_history()
{
$users=$this->db->query("SELECT * FROM adminlogin WHERE role='bookkeeping'")->result_array();
$query1=$this->db->select('adminlogin.name,bookkeepingLoginTraking.*');
$query1=$this->db->from('bookkeepingLoginTraking');
$query1=$this->db->join('adminlogin', 'adminlogin.id = bookkeepingLoginTraking.userId');
if(!empty($_GET['name']))
{
$query1=$this->db->where('adminlogin.name',$_GET['name']);
}
if(!empty($_GET['phone']))
{
$query1=$this->db->where('adminlogin.mobile',$_GET['phone']);
}
if(!empty($_GET['sdate']))
{
$datearray=explode("-",$_GET['sdate']);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$query1=$this->db->where('bookkeepingLoginTraking.date BETWEEN "'.$sdate.'" and "'.trim($edate).'"');
}
$query1=$this->db->get();
//echo $this->db->last_query();die;
$userData=$query1->result_array();
$this->load->view('bookkeeping_history',compact('users','userData'));
}
public function view_rating()
{
$this->db->select('driver_details.user_name,userdetails.first_name,userdetails.last_name,driver_feedback.*');
$this->db->from('driver_feedback');
$this->db->join('driver_details', 'driver_details.id = driver_feedback.driver_id');
$this->db->join('userdetails', 'userdetails.id = driver_feedback.user_id');
$this->db->where('driver_feedback.driver_id',$_GET['id']);
$query1=$this->db->get();
//  echo $this->db->last_query();die;
$ratingData=$query1->result_array();
// $ratingData=$this->db->query("SELECT * FROM driver_feedback WHERE driver_id=".$_GET['id']."")->result_array();
$this->load->view('view_rating',compact('ratingData'));
}
public function schedulemessage_new()
{
//    die('sasa');
$resultmsg=$this->db->query("SELECT * FROM `admin_message` where sid!=0")->result_array();
$resultdrivers=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->db->select('seduledMassags.*,admin_message.id as sidd');
$this->db->from('seduledMassags');
$this->db->join('admin_message','admin_message.sid=seduledMassags.id');
if($_GET['id'])
{
$this->db->where('admin_message.msgId',$_GET['id']);
}
if($_GET['name'])
{
$getDriverdetails=$this->db->query("SELECT * FROM driver_details WHERE name like '%".$_GET['name']."%' order by id desc limit 1")->row_array();
$ids=(int)$getDriverdetails['id'];
// print_r($ids);
$this->db->where_in("seduledMassags.cab", $ids);
}
$this->db->group_by("seduledMassags.id");
$query1=$this->db->get();
//echo $this->db->last_query();die;
//  echo $this->db->last_query();die;
$resultData=$query1->result_array();
// $resultData=$this->db->query("SELECT * FROM `seduledMassags`")->result_array();
//print_r($result);die;
$this->load->view('schedulemessage_new',compact('resultData','resultmsg','resultdrivers'));
}
public function schedulemessage()
{
//    die('sasa');
$resultmsg=$this->db->query("SELECT * FROM `admin_message` where sid!=0")->result_array();
$resultdrivers=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->db->select('*');
$this->db->from('admin_message');
/*          $this->db->join('admin_message','admin_message.sid=seduledMassags.id','left');
*/
if($_GET['id'])
{
$this->db->where('admin_message.msgId',$_GET['id']);
}
if($_GET['name'])
{
$getDriverdetails=$this->db->query("SELECT * FROM driver_details WHERE name like '%".$_GET['name']."%' order by id desc limit 1")->row_array();
$ids=(int)$getDriverdetails['id'];
// print_r($ids);
$this->db->where("admin_message.driver_id", $ids);
}
$this->db->where('status',1);
$this->db->group_by("admin_message.msgId");
/*          $this->db->group_by("seduledMassags.id");
*/
$query1=$this->db->get();
//echo $this->db->last_query();die;
//  echo $this->db->last_query();die;
$resultData=$query1->result_array();
// $resultData=$this->db->query("SELECT * FROM `seduledMassags`")->result_array();
//print_r($result);die;
$this->load->view('schedulemessage',compact('resultData','resultmsg','resultdrivers'));
}
public function create_seduled()
{
$userList=$this->db->query("SELECT id,car_number as car_no from cars")->result_array();
$this->db->select('*');
$this->db->from('admin_message');
$this->db->join('driver_details', 'driver_details.id = admin_message.driver_id');
$this->db->order_by('admin_message.id','DESC');
$query = $this->db->get();
$result = $query->result_array();
if($_GET['id'])
{
$promodetais=$this->db->query("SELECT * FROM seduledMassags WHERE id=".$_GET['id']."")->row_array();
}
//print_r($promodetais);die;
$this->load->view('create_seduled',compact('userList','result','promodetais'));
}
public function deleteSchedule()
{
$this->db->where("id",$_GET['id']);
$this->db->delete("seduledMassags");
redirect('admin/schedulemessage_new', 'refresh');
}
public function save_seduled($value='')
{ 
$id=$_POST['id'];
$pname=$_POST['pname'];
$ptype=$_POST['ptype'];
$users=$_POST['car_type_id'];
$note=$_POST['note'];
$sdate=$_POST['sdate'];
$time=$_POST['time'];
//  print_r($_POST);die;
if($users[0]=='All')
{
$queryFordrivers=$this->db->query("SELECT id , car_number as car_no FROM `cars` ")->result_array();
foreach($queryFordrivers as $val)
{
$users[]=$val['id'];
array_shift($users);
}
}
$users=implode(",",$users);
$dataResult = array(
'title' =>  ($pname) ? $pname : '',
'type' => ($ptype) ? $ptype : '',
'massage' => ($note) ? $note : '',
'cab' => ($users) ? $users : '',
'sdate' => '',
'edate' => '',
'rows'=>$_POST['number'],
);
if(empty($id))
{
$cabInsert = $this
->db
->insert('seduledMassags', $dataResult);
// echo $this->db->last_query();die;
$last_Id= $this->db->insert_id();
for($i=0; $i<count($sdate); $i++)
{
//echo '<br>';
//echo $this->db->last_query();
//print_r($this->db->last_query());die;
//$this->sendNotification($driverData['deviceToken'],'New Message','You Have New Message From Admin');
$data=array(
'driver_id'=>0,
'admin_id'=>1,
'message'=>$note,
'cab'=>$users,
'type'=>$ptype,
'sid'=>$last_Id,
'datetime'=>$_POST['sdate'][$i].' '.$_POST['time'][$i],
'msgId'=>mt_rand(1000,9999)
);
$cabInsert3 = $this->db->insert('admin_message', $data);
//echo $this->db->last_query();die;
}
//die;
redirect('admin/schedulemessage_new', 'refresh');
//$this->load->view('promo_add',compact('userList'));
}else{
$this->db->where("sid",$id);
$this->db->delete("admin_message");
for($i=0; $i<count($sdate); $i++)
{
//echo '<br>';
//echo $this->db->last_query();
//print_r($this->db->last_query());die;
//$this->sendNotification($driverData['deviceToken'],'New Message','You Have New Message From Admin');
$data=array(
'driver_id'=>0,
'admin_id'=>1,
'message'=>$note,
'cab'=>$users,
'type'=>$ptype,
'sid'=>$id,
'datetime'=>$_POST['sdate'][$i].' '.$_POST['time'][$i],
);
$cabInsert3 = $this->db->insert('admin_message', $data);
// echo $this->db->last_query();die;
}
$this->db->where("id",$id);
$this->db->update('seduledMassags', $dataResult);
// echo $this->db->last_query();die;
redirect('admin/schedulemessage_new', 'refresh');
}
}
public function view_massage()
{
$resultData=$this->db->query("SELECT * FROM admin_message WHERE sid=".$_GET['id']."")->result_array();
$this->load->view('view_massage',compact('resultData'));
}
public function deleteseduled()
{
$id=$_GET['id'];
$this->db->delete('seduledMassags',['id'=>$id]);
redirect('admin/schedulemessage', 'refresh');
}
public function deleteMassage()
{
$id=$_GET['id'];
$this->db->delete('admin_message',['id'=>$id]);
redirect('admin/schedulemessage', 'refresh');
}
public function checkNewBooking()
{
$location=$this->db->query("SELECT * FROM bookingdetails")->result_array();
echo count($location);
}
public function add_fare()
{
$fareData=array();
$allCarType=$this->db->query("SELECT * FROM extrafare")->result_array();
if($_GET['id'])
{
$fareData=$this->db->query("SELECT * FROM extrafare WHERE id=".$_GET['id']."")->row_array();
}
$this->load->view('add_fare',compact('allCarType','fareData'));
}
public function save_fare()
{
$days=implode(",",$_POST['days']);
$arrayData=array(
'cabId'=>0,
'title'=>$_POST['title'],
'days'=>$days,
'move_to'=>$_POST['move_to'],
'Flagfall'=>$_POST['flagfall'],
'DistanceChargeKm'=>$_POST['distance'],
'TimeChargeKm'=>$_POST['time'],
'TollsBooking'=>$_POST['tolls'],
'startTime'=>$_POST['startTimt'],  
'endTime'=>$_POST['endtime'],
'Highoccupancy'=>0,
'Highoccupancyfee'=>0,
'capacity'=>$_POST['seat'],
'wait'=>$_POST['wait'],
);
//print_r($arrayData);
if($_POST['id'])
{
$this->db->where('id',$_POST['id']);
$this->db->update('extrafare',$arrayData);
//echo $this->db->last_query();die;
}else{
$this->db->insert('extrafare',$arrayData);
}
//echo $this->db->last_query();die;
echo 2;
// redirect('admin/fare_manage', 'refresh');
}
public function fare_manage()
{
$query1=$this->db->select('extrafare.*,cabdetails.cartype');
$query1=$this->db->from('extrafare');
$query1=$this->db->join('cabdetails', 'extrafare.cabId = cabdetails.cab_id','left');
if($_GET['id'])
{
$query1=$this->db->where('extrafare.cabId',$_GET['id']);
}
$this->db->order_by('extrafare.id','DESC');
$query1=$this->db->get();
//  echo $this->db->last_query();die;
$get_bookingLocations=$query1->result_array();
$this->load->view('fare_manage',compact('get_bookingLocations'));
}
public function deletefare()
{
$id=$_GET['id'];
$this->db->delete('extrafare',['id'=>$id]);
redirect('admin/fare_manage', 'refresh');    
}
public function add_holiday()
{
$holidayData=array();
if($_GET['id'])
{
$holidayData=$this->db->query("SELECT * FROM holidays WHERE id=".$_GET['id']."")->row_array();
}
$this->load->view('add_holiday',compact('holidayData'));
}
public function save_holiday()
{
if($_POST['id']=='')
{
for($i=0;$i<count($_POST['date']); $i++)
{
$arrayData=array(
'title'=>$_POST['title'][$i],
'date'=>$_POST['date'][$i],
);
$this->db->insert('holidays',$arrayData);
//echo $this->db->last_query();die;
}
redirect('admin/manage_holiday', 'refresh');  
}else{
$arrayData=array(
'title'=>$_POST['title'],
'date'=>$_POST['date'],
);
$this->db->where('id',$_POST['id']);
$this->db->update('holidays',$arrayData);
redirect('admin/manage_holiday', 'refresh');   
}
}
public function manage_holiday()
{
$get_bookingLocations=$this->db->query("SELECT * FROM holidays")->result_array();
$this->load->view('manage_holiday',compact('get_bookingLocations'));   
}
public function deleteholiday()
{
$id=$_GET['id'];
$this->db->delete('holidays',['id'=>$id]);
redirect('admin/manage_holiday', 'refresh');    
}
public function updatecar_type()
{
$himg=$_POST['himg'];
$cartype=$_POST['cartype'];
$seating_capacity=$_POST['seating_capacity'];
/* $getData=$this->db->query("SELECT * FROM cabdetails WHERE cab_id=".$_GET['id']."")->row_array();*/
$config['upload_path'] = './car_image/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['max_size']    = '20000000';
$config['max_width']  = '1024000000000';
$config['max_height']  = '768000000000000';
$this->load->library('upload', $config);
if (!$this->upload->do_upload('uploadImageFile'))
{
$response = $this->session->set_flashdata('error_msg', $this->upload->display_errors());
//redirect(base_url().'admin/add_car_type');
// uploading failed. $error will holds the errors.
}else{
$upload_data = $this->upload->data();
$himg=$upload_data['file_name'];
}
$dataArray=array(
'cartype'=>$cartype,
'seat_capacity'=>$seating_capacity,
'icon'=>$himg
);
$this->db->where('cab_id',$_POST['id']);
$this->db->update('cabdetails',$dataArray);
redirect('admin/manage_car_type', 'refresh');
//echo $this->db->last_query();die;
}
public function changeRole()
{
$role=$_POST['id'];
$is_admin = $this->session->userdata('username-admin');
//  $adminaccess=0;
$sqlUserData=$this->db->query("SELECT * FROM `adminlogin` WHERE email='".$is_admin."' order by id desc")->row_array();
if($role==1)
{
$this->session->set_userdata('role-admin','admin');
$dispatchArray=array(
'userId'=>$sqlUserData['id'],
'status'=>'Dispatch Login',
'date'=>date('Y-m-d'),
'time'=>date('H:i:s'),
);
$this->db->insert('dispatcherLoginTraking',$dispatchArray);
echo 1;
}else{
$this->session->set_userdata('role-admin','dispatch');
if($sqlUserData['role']=='dispatch')
{
$dispatchArray=array(
'userId'=>$sqlUserData['id'],
'status'=>'Dispatch Logout',
'date'=>date('Y-m-d'),
'time'=>date('H:i:s'),
);
$this->db->insert('dispatcherLoginTraking',$dispatchArray);
echo 2;
}
}
}
public function add_operators()
{
$singleopertor=array();
if($_GET['id'])
{
$singleopertor=$this->db->query("SELECT * FROM caboperators WHERE id=".$_GET['id']."")->row_array();
}
$country=$this->db->query("SELECT * FROM country")->result_array();
$this->load->view("add_operators",compact('singleopertor','country'));
}
public function insert_operator()
{
$client_id=$_POST['client_id'];
$cname=$_POST['cname'];
$abn=$_POST['abn'];
$fName=$_POST['fName'];
$lName=$_POST['lName'];
$phone=$_POST['phone'];
$mobile=$_POST['mobile'];
$email=$_POST['email'];
$code=$_POST['code'];
if(!$_POST['id'])
{
$status=1;
}else{
$status=0;
}
if($_POST['status'])
{
$status=$_POST['status'];
}
$password=md5($_POST['passsword']);
$checkEmail=array();
/* echo '<pre>';
print_r($_POST);die;*/
if(!$_POST['id'])
{
// echo 'wew';die;
$checkEmail=$this->db->query("SELECT * FROM caboperators WHERE Email='".$email."' or PhoneNumber='".$phone."'")->row_array();
}
if(!$checkEmail)
{
$arrayData=array(
'ClientID'=>$client_id,
'CompanyName'=>$cname,
'ABN'=>$abn,
'FirstName'=>$fName,
'LastName'=>$lName,
'PhoneNumber'=>$phone,
'mobile'=>$mobile,
'Email'=>$email,
'address'=>$_POST['address'],
'Password'=>$password,
'code'=>$code,
'temp_password'=>$_POST['passsword'],
'status'=>$status,
);
if($_POST['id'])
{   
$this->db->where('id',$_POST['id']);
$this->db->update('caboperators',$arrayData);
}else{
$this->db->insert('caboperators',$arrayData);
}
// echo $this->db->last_query();die;
redirect('admin/operators', 'refresh');
}else{
redirect('admin/add_operators?error=1', 'refresh');
}   
}
public function updateoptStatus()
{
$id=$_POST['id'];
$status=$_POST['status'];
$updateArray=array('status'=> $status);
$this->db->where('id',$id);
$this->db->update('caboperators',$updateArray);
echo $this->db->last_query();die;
}
public function operators()
{
$this->db->select("*");
$this->db->from('caboperators');
if($_GET['cid'])
{
$this->db->where('ClientID',$_GET['cid']);
}
if($_GET['name'])
{
$this->db->like('CompanyName',$_GET['name']);
}
if($_GET['phone'])
{
$this->db->where('mobile',$_GET['phone']);
}
if($_GET['status']=='Active')
{
$this->db->where('status',1);
}
if($_GET['status']=='Inactive')
{
$this->db->where('status',0);
}
$query=$this->db->get();
$alloperators=$query->result_array();
//echo $this->db->last_query();
$this->load->view('operators',compact('alloperators'));
}
public function delete_opt()
{
$this->db->where('id',$_GET['id']);
$this->db->delete('caboperators');
redirect('admin/operators', 'refresh');
}
public function shiftrereport()
{
$driverData=$this->db->query("SELECT * FROM driver_details")->result_array();
$carData=$this->db->query("SELECT * FROM cars")->result_array();
$clientData=$this->db->query("SELECT * FROM caboperators")->result_array();
$cabData=$this->db->query("SELECT * FROM cabdetails WHERE cab_id IN (2,6,7)")->result_array();
$this->db->select('drivershiftdetails.id as shiftId,drivershiftdetails.loginAt,drivershiftdetails.logoutAt, driver_details.*,cars.*');
$this->db->from('drivershiftdetails');
$this->db->join('driver_details', 'driver_details.id = drivershiftdetails.driverId','left');
$this->db->join('cars', 'cars.id = drivershiftdetails.cabId','left');
if($_GET['sdate'])
{
if($_GET['eedate'])
{
$dateData=explode("/",$_GET['eedate']);
$dateData1=explode("/",$_GET['sdate']);
$edate=$dateData['2'].'-'.$dateData['1'].'-'.$dateData['0'];
$sdate=$dateData1['2'].'-'.$dateData1['1'].'-'.$dateData1['0'];
// echo $_GET['sdate'].'--'.$_GET['edate']..;die;
$this->db->where('drivershiftdetails.loginAt BETWEEN "'. $sdate. '" and "'. date('Y-m-d', strtotime($edate)).'"');
}else{
$dateData1=explode("/",$_GET['sdate']);
$sdate=$dateData1['2'].'-'.$dateData1['1'].'-'.$dateData1['0'];
$this->db->where('drivershiftdetails.loginAt BETWEEN "'.$sdate. '" and "'. date('Y-m-d', strtotime(' +1 day')).'"');
}
}
if($_GET['username'])
{
$this->db->where("driver_details.user_name", $_GET['username']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['phone'])
{
$this->db->where("driver_details.phone", $_GET['phone']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['name'])
{
$this->db->like('driver_details.first_name', $_GET['name']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['cab'])
{
$this->db->like('cars.car_number', $_GET['cab']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['category'])
{
$this->db->like('cars.car_type_id', $_GET['category']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['cid'])
{
$this->db->like('cars.clientId', $_GET['cid']);
//$this->db->where('driverId',$_GET['driver']);
}
$query = $this->db->get();
if($_GET){
$result = $query->result_array();
}
$result = $query->result_array();
//echo $this->db->last_query();die;
/*echo '<pre>';
print_r($result);die;*/
$queryData=$this->db->last_query();
$this->load->view('shiftrereport',compact('result','driverData','carData','clientData','cabData','queryData'));   
}
public function view_trip()
{
$bookings =$this->db->query("SELECT `bookingdetails`.*, `cars`.`car_number`, `driver_details`.`name` as drivername, `driver_details`.`user_name` as did FROM `bookingdetails` LEFT JOIN `cars` ON `bookingdetails`.`cabId` = `cars`.`id` JOIN `driver_details` ON `bookingdetails`.`driverId` = `driver_details`.`id` WHERE `bookingdetails`.`shiftId` = ".$_GET['id']." ")->result_array();
// $bookings = $query->result_array();
$queryData=$this->db->last_query();
/*$bookings=$this->db->query("SELECT * FROM bookingdetails join cars bookingdetails.cabId=cars.id where bookingdetails.shiftId=".$_GET['id']."")->result_array();*/
//echo $this->db->last_query();die;
/* echo '<pre>';
print_r($bookings);die;*/
$this->load->view('view_trip',compact('bookings','queryData'));  
}
public function walletamountsendMail()
{
$query=$_POST['query'];
$msg=$_POST['msg'];
$email=$_POST['email'];
//$data=$this->db->query($query)->result_array();
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>'',
'startdate'=>'',
'enddate'=>'',
'email'=>$email,
'msg'=>$msg,
'bid'=>'0',
'query'=>$query,
'type'=>'wallet'
);
$this->db->insert('mailRequest',$arrayData);
redirect('admin/walletamount', 'refresh');
// echo $this->db->last_query();die;
//print_r($data);
}
public function payoffmail()
{
$query=$_POST['query'];
$msg=$_POST['msg'];
$email=$_POST['email'];
//$data=$this->db->query($query)->result_array();
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>'',
'startdate'=>'',
'enddate'=>'',
'email'=>$email,
'msg'=>$msg,
'bid'=>'0',
'query'=>$query,
'type'=>'payoff'
);
$this->db->insert('mailRequest',$arrayData);
redirect('admin/payoff', 'refresh');
// echo $this->db->last_query();die;
//print_r($data);
}
public function sendshiftreport()
{
$query=$_POST['query'];
$msg=$_POST['msg'];
$email=$_POST['email'];
//$data=$this->db->query($query)->result_array();
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>'',
'startdate'=>'',
'enddate'=>'',
'email'=>$email,
'msg'=>$msg,
'bid'=>'0',
'query'=>$query,
'type'=>'shift'
);
$this->db->insert('mailRequest',$arrayData);
redirect('admin/shiftrereport', 'refresh');
// echo $this->db->last_query();die;
//print_r($data);
}
public function driverCreditndebitsendMail()
{
$query=$_POST['query'];
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>'',
'startdate'=>'',
'enddate'=>'',
'email'=>$email,
'msg'=>$msg,
'bid'=>'0',
'query'=>$query,
'type'=>'driverCredit'
);
$this->db->insert('mailRequest',$arrayData);
redirect('admin/driverCreditndebits', 'refresh');
// echo $this->db->last_query();die;
//print_r($data);
}
public function sendridePayment()
{
$query=$_POST['query'];
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>'',
'startdate'=>'',
'enddate'=>'',
'email'=>$email,
'msg'=>$msg,
'bid'=>'0',
'query'=>$query,
'type'=>'ride_payment'
);
$this->db->insert('mailRequest',$arrayData);
redirect('admin/ride_payments', 'refresh');
// echo $this->db->last_query();die;
//print_r($data);
}
public function ridePaymentcron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and type='ride_payment' limit 1")->row_array();
if($checkData)
{
$dataExcle=$this->db->query($checkData['query'])->result_array();
//echo $this->db->last_query();die;
//print_r($dataExcle);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("Trip ID","Driver Name", "Driver ID", "Driver Mobile No", "Cab No","Pick UP","Drop Off","Transaction Date","Job Type","Total Fare","PS Levy","Tolls","Extras","Payment Method","Transaction ID","Admin Commission","Credit Amount","Debit Amount");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
foreach($dataExcle as $row)
{
$dateData=explode(" ",$row['bookingdateandtime']);
if($row['paymentMode']=='case') { $paymentMode= 'Pay the Driver'; } else if($row['paymentMode']=='online') {  $paymentMode= 'Pay Via App'; } else{  $paymentMode= 'Admin Account '; }
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['bookingId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['first_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$row['user_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['phone']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['car_number']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['from']);
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['to']);
$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $dateData[0]);
$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['isdevice']);
$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, '$1.10');
$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, '$0.00');
$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, '$0');
$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $paymentMode);
$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row['transactionId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row['adminCommissions']);
$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row['credit']);
$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row['debit']);
$excel_row++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
/*header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);*/
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hi there!,<br> As you request Driver Shift Report data,we have send a link please click on <a download href="http://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'">Download</a> button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Report From Infinitecabs';
$from='gks756845@gmail.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "Hi there!,<br> We`re pleased to let you know that the report you requested is ready. Please click here https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.' to download your report.<br> Message : '.$checkData['msg'].' <br> Thank you <br> Infinite Cabs "}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
//  print_r($response);die;
// echo 'erer';die;
$updateArray=array(
'status'=>1,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
//echo $this->db->last_query();die;
}
}
public function walletDatacron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and  type='wallet' limit 1")->row_array();
if($checkData)
{
$dataExcle=$this->db->query($checkData['query'])->result_array();
//echo $this->db->last_query();die;
//print_r($dataExcle);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("Sr.No","Driver Name", "Driver ID", "Driver Mobile", "License No","Current Wallet Amount");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
$i=1;
foreach($dataExcle as $row)
{
$credit=$row['credit_amount'];
$debit=$row['debit_amount'];
$wallet=$debit-$credit;
$wallet=round($wallet,2);
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['pname']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$row['user_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['phone']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['license_no']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, '$0');
$excel_row++;
$i++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
/*header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);*/
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hi there!,<br> As you request Driver Shift Report data,we have send a link please click on <a download href="https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'">Download</a> button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Report From Infinitecabs';
$from='gks756845@gmail.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "Hi there!,<br> We`re pleased to let you know that the report you requested is ready. Please click here https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.' to download your report.<br> Message : '.$checkData['msg'].' <br> Thank you <br> Infinite Cabs "}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
//  print_r($response);die;
// echo 'erer';die;
$updateArray=array(
'status'=>1,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
//echo $this->db->last_query();die;
}
}
public function sendpayoutcron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and type='payoff' limit 1")->row_array();
if($checkData)
{
$dataExcle=$this->db->query($checkData['query'])->result_array();
//echo $this->db->last_query();die;
//print_r($dataExcle);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("Sr.No","Driver Name", "Driver ID", "Driver Mobile", "Amount","Transaction ID","Date/Time");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
$i=1;
foreach($dataExcle as $row)
{
$credit=$row['credit_amount'];
$debit=$row['debit_amount'];
$wallet=$debit-$credit;
$wallet=round($wallet,2);
$dateData=explode(" ",$row['create_at']); 
$dateTime=date('d/m/Y', strtotime($dateData['0'])).' '.$dateData[1];
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['pname']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$row['user_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['phone']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['transactionID'] ? $row['transactionID'] : 'N/A');
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $dateTime);
$excel_row++;
$i++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
/*header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);*/
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hi there!,<br> As you request Driver Shift Report data,we have send a link please click on <a download href="http://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'">Download</a> button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Report From Infinitecabs';
$from='gks756845@gmail.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "Hi there!,<br> We`re pleased to let you know that the report you requested is ready. Please click here https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.' to download your report.<br> Message : '.$checkData['msg'].' <br> Thank you <br> Infinite Cabs "}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
//  print_r($response);die;
// echo 'erer';die;
$updateArray=array(
'status'=>1,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
//echo $this->db->last_query();die;
}
}
public function drivercriditPaymentcron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and  type='driverCredit' limit 1")->row_array();
if($checkData)
{
$dataExcle=$this->db->query($checkData['query'])->result_array();
//echo $this->db->last_query();die;
//print_r($dataExcle);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("Sr.No","Trip ID", "Driver Name", "Driver ID", "Date and Time","Amount","Payment Type","Reason","Trip  Type","Transfer Via","Transaction ID","Balance Amount");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
$i=1;
foreach($dataExcle as $row)
{
if($row['bookingId']) {  $Via= 'Automatic'; } else { $Via= 'Manual'; }
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['bookingId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$row['pname']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['user_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['create_at']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['type'] ? $row['type'] : 'N/A');
$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['reason']);
$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['isdevice'] ? $row['isdevice'] : 'N/A');
$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $Via);
$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['transactionID'] ? $row['transactionID'] : 'N/A');
$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row['updateAmount']);
$excel_row++;
$i++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
/*header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);*/
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hi there!,<br> As you request Driver Shift Report data,we have send a link please click on <a download href="http://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'">Download</a> button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Report From Infinitecabs';
$from='gks756845@gmail.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "Hi there!,<br> We`re pleased to let you know that the report you requested is ready. Please click here https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.' to download your report.<br> Message : '.$checkData['msg'].' <br> Thank you <br> Infinite Cabs "}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
//  print_r($response);die;
// echo 'erer';die;
$updateArray=array(
'status'=>1,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
//echo $this->db->last_query();die;
}
}
public function shiftExcelcron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and type='shift' limit 1")->row_array();
// echo $this->db->last_query();
/*$bookingData=$this->db->query("SELECT * FROM bookingdetails WHERE id=".$checkData['bid']."")->row_array();*/
if($checkData)
{
$dataExcle=$this->db->query($checkData['query'])->result_array();
//echo $this->db->last_query();die;
//print_r($dataExcle);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("Client ID","Shift ID", "Cab No", "Meter Sr No", "Driver ID","Driver Name","Shift Start Date/Time","Shift End Date/Time","Total KM","Total Trips","Levy Amount","Shift Earning");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
foreach($dataExcle as $row)
{
$totalbooking=$this->db->query("SELECT count(id) as TotalBooking,sum(amount) as totalerning from bookingdetails WHERE shiftId=".$row['shiftId']."")->row_array();
$totalbookingkm=$this->db->query("SELECT count(id) as TotalBooking,sum(distance) as totalkm from bookingdetails WHERE shiftId=".$row['shiftId']."")->row_array();
$dateData=explode(" ",$row['loginAt']); 
$dateData1=explode(" ",$row['logoutAt']); $d=date($dateData[0]);
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['clientId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['shiftId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['car_number']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, 'N/A');
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['user_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, date('d/m/Y', strtotime($dateData[0])).' '.$dateData[1]);
$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, date('d/m/Y', strtotime($d)).' '.$dateData1[1]);
$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $totalbookingkm['totalkm']);
$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $totalbooking['TotalBooking']);
$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $totalbooking['TotalBooking']*1.10);
$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $totalbooking['totalerning']);
$excel_row++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
/*header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);*/
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hello,<br> We`re pleased to let you know that the report you requested is ready. Please click here <a download href="http://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'">Download</a> button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Report From Infinite Cabs';
$from='gks756845@gmail.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "Hi there!,<br> We`re pleased to let you know that the report you requested is ready. Please click here https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.'  to download your report.<br> Message : '.$checkData['msg'].' <br> Thank you <br> Infinite Cabs"}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
//  print_r($response);die;
// echo 'erer';die;
$updateArray=array(
'status'=>1,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
//echo $this->db->last_query();die;
}
}
public function exportshift()
{
// echo $this->db->last_query();
/*$bookingData=$this->db->query("SELECT * FROM bookingdetails WHERE id=".$checkData['bid']."")->row_array();*/
if($_GET['query'])
{
$dataExcle=$this->db->query($_GET['query'])->result_array();
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("Client ID","Shift ID","Cab No","Meter Sr No","Driver ID","Driver Name","Shift Start Date/Time","Shift End Date/Time","Total KM","Total Trips", "Levy Amount", "Shift Earning");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
foreach($dataExcle as $row)
{
//print_r($row);
$totalbooking=$this->db->query("SELECT count(id) as TotalBooking,sum(amount) as totalerning from bookingdetails WHERE shiftId=".$row['shiftId']."")->row_array();
$totalbookingkm=$this->db->query("SELECT count(id) as TotalBooking,sum(distance) as totalkm from bookingdetails WHERE shiftId=".$row['shiftId']."")->row_array();
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['clientId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['shiftId']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['car_number']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, "N/A");
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['user_name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['name']);
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['loginAt']);
$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['logoutAt']);
$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row,$totalbookingkm['totalkm']);
$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $totalbookingkm['TotalBooking']);
$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $totalbookingkm['TotalBooking']);
$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $totalbooking['totalerning']);
$excel_row++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);
$object_writer->save('php://output');
}
}
public function shifttripexport()
{
// echo $this->db->last_query();
/*$bookingData=$this->db->query("SELECT * FROM bookingdetails WHERE id=".$checkData['bid']."")->row_array();*/
if($_GET['query'])
{
//echo $_GET['query'];die;
$dataExcle=$this->db->query($_GET['query'])->result_array();
//echo $this->db->last_query();die;
//  print_r($data);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("CAB NUMBER", "DRIVER NAME", "DRIVER ID", "JOB/BOOKING ID","TRIP DURATION","START TRIP LOCATION","END TRIP LOCATION","DATE","STATUS","BOOKING FROM","TRIP KM","TRIP FARE","TRIP LEVY AMOUNT","TRIP TOLLS/EXTRA","TRIP TOTAL FARE");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
foreach($dataExcle as $row)
{
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['car_number']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['drivername']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['did']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['id']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['approx_time']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['pick_address']);
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['drop_address']);
$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['bookingdateandtime']);
$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['status_code']);
$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['isdevice']);
$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['distance']);
$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, '$'.$row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, '$'.$row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, '$0');
$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row['amount']);
$excel_row++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);
$object_writer->save('php://output');
}
}
public function sendtripReportTomail()
{
$query=$_POST['query'];
$msg=$_POST['msg'];
$email=$_POST['email'];
//$data=$this->db->query($query)->result_array();
$email=$_POST['email'];
$sdate=$_POST['sdate'];
$driver=$_POST['driver'];
$msg=$_POST['msg'];
$bid=$_POST['bid'];
$datearray=explode("-",$sdate);
$sdate=$datearray[0];
$edate=$datearray[1];
$sdate=str_replace(".","-",$sdate);
$edate=str_replace(".","-",$edate);
$arrayData=array(
'driver'=>'',
'startdate'=>'',
'enddate'=>'',
'email'=>$email,
'msg'=>$msg,
'bid'=>'0',
'query'=>$query,
'type'=>'shifttrip'
);
$this->db->insert('mailRequest',$arrayData);
redirect('admin/view_trip', 'refresh');
// echo $this->db->last_query();die;
//print_r($data);   
}
public function sendtripReportTomailcron()
{
$checkData=$this->db->query("SELECT * FROM mailRequest WHERE status=0 and type='shifttrip' limit 1")->row_array();
// echo $this->db->last_query();
/*$bookingData=$this->db->query("SELECT * FROM bookingdetails WHERE id=".$checkData['bid']."")->row_array();*/
if($checkData)
{
//echo $_GET['query'];die;
$dataExcle=$this->db->query($checkData['query'])->result_array();
//echo $this->db->last_query();die;
//  print_r($data);die;
$this->load->library("excel");
$object = new PHPExcel();
$object->setActiveSheetIndex(0);
$table_columns = array("CAB NUMBER", "DRIVER NAME", "DRIVER ID", "JOB/BOOKING ID","TRIP DURATION","START TRIP LOCATION","END TRIP LOCATION","DATE","STATUS","BOOKING FROM","TRIP KM","TRIP FARE","TRIP LEVY AMOUNT","TRIP TOLLS/EXTRA","TRIP TOTAL FARE");
//$table_columns = array("Driver Name", "lat", "lng", "Date/Time");
$column = 0;
foreach($table_columns as $field)
{
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
$column++;
}
$excel_row = 2;
foreach($dataExcle as $row)
{
$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['car_number']);
$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['drivername']);
$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['did']);
$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['id']);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['approx_time']);
$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['pick_address']);
$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['drop_address']);
$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['bookingdateandtime']);
$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['status_code']);
$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['isdevice']);
$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['distance']);
$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, '$'.$row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, '$'.$row['amount']);
$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, '$0');
$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row['amount']);
$excel_row++;
}
$fileName = 'data-'.time().'.xls';  
$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fileName);
$object_writer->save('upload/emaildata/'.$fileName);
$message='Hello,<br> As you request driver traking data,we have send a link please click on https://admin.infinitecabs.com.au/upload/emaildata/'.$fileName.' button.<br> Thank <br> Infinitecabs';
$to = $checkData['email'];
$subject = 'Driver Traking File';
$from='gopal.jangid@jploft.com';
$headers = "From: " . strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{"personalizations":[{"to":[{"email":"'.$to.'","name":"Infinitecabs"}],"subject":"'.$subject.'"}],"content": [{"type": "text/html", "value": "'.$message.'"}],"from":{"email":"no-reply@infinitecabs.com.au","name":"Infinitecabs"}}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer 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',
'Content-Type: application/json'
),
));
$response = curl_exec($curl);
$updateArray=array(
'status'=>0,
'sendfile'=>$fileName
);
$this->db->where('id',$checkData['id']);
$this->db->update('mailRequest',$updateArray);
}
}
public function geofancing()
{
$zone=$this->db->query("SELECT * FROM zone")->result_array();
$this->load->view('geofancing',compact('zone'));
}
public function sendMailFunction($to,$subject,$message)
{
$config = Array(
'protocol' => 'smtp',
'smtp_host' => 'ssl://smtp.googlemail.com',
'smtp_port' => 465,
'smtp_user' => 'gks756845@gmail.com', // change it to yours
'smtp_pass' => 'niit@7568', // change it to yours
'mailtype' => 'html',
'charset' => 'iso-8859-1',
'wordwrap' => TRUE
);
$this->load->library('email', $config);
$this->email->set_newline("\r\n");
$this->email->from('gks756845@gmail.com'); // change it to yours
$this->email->to($to);// change it to yours
$this->email->subject($subject);
$this->email->message($message);
if($this->email->send())
{
echo 'Email sent.';
}
else
{
show_error($this->email->print_debugger());
}
}
public function delete_alert()
{
$this->db->where('id',$_GET['id']);
$this->db->delete('alertslogs');
redirect('admin/alertlog', 'refresh');
}
public function alertlog()
{
$alertslogs=$this->db->query("SELECT adminlogin.name as dname, alertslogs.*,driver_details.user_name,driver_details.pname,diver_live_location.latitude ,diver_live_location.longlatitude FROM alertslogs join driver_details on driver_details.id=alertslogs.driverId join diver_live_location on diver_live_location.driver_id=alertslogs.driverId left join adminlogin on adminlogin.id = alertslogs.clickBy order by alertslogs.id desc")->result_array();
$this->load->view('alert',compact('alertslogs'));
}
public function delete_nojob()
{
$this->db->where('id',$_GET['id']);
$this->db->delete('noJObAlert');
redirect('admin/nojob', 'refresh');
}
public function nojob()
{
$alertslogs=$this->db->query("SELECT adminlogin.name as dname, noJObAlert.*,driver_details.user_name,driver_details.pname,diver_live_location.latitude ,diver_live_location.longlatitude FROM noJObAlert join driver_details on driver_details.id=noJObAlert.driverId join diver_live_location on diver_live_location.driver_id=noJObAlert.driverId left join adminlogin on adminlogin.id = noJObAlert.clickBy order by noJObAlert.id desc")->result_array();
$this->load->view('nojob',compact('alertslogs'));
}
public function checkDriverRisk()
{
$alertslogs=$this->db->query("SELECT alertslogs.*,driver_details.user_name,driver_details.pname,diver_live_location.latitude ,diver_live_location.longlatitude FROM alertslogs join driver_details on driver_details.id=alertslogs.driverId join diver_live_location on diver_live_location.driver_id=alertslogs.driverId WHERE alertslogs.status=0 and clickBy=0 order by alertslogs.id desc")->row_array();
if($alertslogs)
{
?>
<div class="container">
  <h2 style="text-align: center;color:red;">
    <?php echo $alertslogs['type'] ?> Alert Pressed By 
    <?php echo $alertslogs['pname'] ?>
  </h2>
  <ul class="list-group">
    <li class="list-group-item">Driver Id : 
      <span class="">
        <?php echo $alertslogs['user_name'] ?>
      </span>
    </li>
    <li class="list-group-item">Driver Name   :  
      <span class="">
        <?php echo $alertslogs['pname'] ?>
      </span>
    </li>
    <li class="list-group-item">Date/Time  :  
      <span class="">
        <?php echo $alertslogs['datetime'] ?>
      </span>
    </li>
    <li class="list-group-item">Latitude  :  
      <span class="">
        <?php echo $alertslogs['latitude'] ?>
      </span>
    </li>
    <li class="list-group-item">Longitude  :  
      <span class="">
        <?php echo $alertslogs['longlatitude'] ?>
      </span>
    </li>
    <li class="list-group-item" style="color:red;">Type :  
      <span class="">
        <?php echo $alertslogs['type'] ?>
      </span>
    </li>
    <input type="hidden" class="updateAlert" value="<?php echo $alertslogs['id'] ?>">
  </ul>
</div>
<?php
}else{
echo 2;
}
}
public function checkDriverNoJob()
{
$alertslogs=$this->db->query("SELECT noJObAlert.*,driver_details.user_name,driver_details.pname,diver_live_location.latitude ,diver_live_location.longlatitude, bookingdetails.username as userName,bookingdetails.phone as userPhone,bookingdetails.drop_address,bookingdetails.pick_address FROM noJObAlert join driver_details on driver_details.id=noJObAlert.driverId join diver_live_location on diver_live_location.driver_id=noJObAlert.driverId join bookingdetails on bookingdetails.id=noJObAlert.bookingId WHERE noJObAlert.status=0 and clickBy=0 order by noJObAlert.id desc")->row_array();
if($alertslogs)
{
?>
<div class="container">
  <h2 style="text-align: center;color:red;">No Job Alert Pressed By 
    <?php echo $alertslogs['pname'] ?>
  </h2>
  <ul class="list-group">
    <li class="list-group-item">Driver Id : 
      <span class="">
        <?php echo $alertslogs['user_name'] ?>
      </span>
    </li>
    <li class="list-group-item">Driver Name   :  
      <span class="">
        <?php echo $alertslogs['pname'] ?>
      </span>
    </li>
    <li class="list-group-item">Date/Time  :  
      <span class="">
        <?php echo $alertslogs['dateTime'] ?>
      </span>
    </li>
    <li class="list-group-item">Latitude  :  
      <span class="">
        <?php echo $alertslogs['latitude'] ?>
      </span>
    </li>
    <li class="list-group-item">Longitude  :  
      <span class="">
        <?php echo $alertslogs['longlatitude'] ?>
      </span>
    </li>
    <li class="list-group-item">UserName : 
      <span class="">
        <?php echo $alertslogs['userName'] ?>
      </span>
    </li>
    <li class="list-group-item">Contact No : 
      <span class="">
        <?php echo $alertslogs['userPhone'] ?>
      </span>
    </li>
    <li class="list-group-item">Pick Address : 
      <span class="">
        <?php echo $alertslogs['pick_address'] ?>
      </span>
    </li>
    <li class="list-group-item">Drop Address : 
      <span class="">
        <?php echo $alertslogs['drop_address'] ?>
      </span>
    </li>
    <li class="list-group-item" style="color:red;">Type :  
      <span class="">No Job
      </span>
    </li>
    <input type="hidden" class="updateAlert1" value="<?php echo $alertslogs['id'] ?>">
  </ul>
</div>
<?php
}else{
echo 2;
}
}
public function UpdateAlertData()
{
$id=$_POST['id'];
$admin_id = $this->session->userdata('username-admin');
$userData=$this->db->query("SELECT * FROM adminlogin WHERE email='".$admin_id."' order by id desc")->row_array();
$getalertData=$this->db->query("SELECT * FROM alertslogs WHERE id=".$id."")->row_array();
if($getalertData)
{
$arrayUpdate=array(
'clickBy'=>$userData['id']
);
$this->db->where('id',$id);
$this->db->update('alertslogs',$arrayUpdate);
echo 1;
}
}
public function UpdateNojobData()
{
$id=$_POST['id'];
$admin_id = $this->session->userdata('username-admin');
$userData=$this->db->query("SELECT * FROM adminlogin WHERE email='".$admin_id."' order by id desc")->row_array();
$getalertData=$this->db->query("SELECT * FROM noJObAlert WHERE id=".$id."")->row_array();
if($getalertData)
{
$checkRequets = $this->db->query("SELECT * FROM `temp_booking` WHERE driver_id='" . $getalertData['driverId'] . "' and booking_id='" . $getalertData['bookingId'] . "'")->row_array();
$checkbooking = $this->db->query("SELECT * FROM `bookingdetails` WHERE id='" . $checkRequets['booking_id'] . "'")->row_array();
$this->db->close();
$updatetempData = array(
"status" => "3",
"cancal_type" => 'other'
);
$this->db->where("booking_id", $getalertData['bookingId']);
$this->db->update("temp_booking", $updatetempData);
$this->db->initialize();
$Updatediver_live_location = $this
->db
->update('diver_live_location', array(
'driver_booking_status' => 'free'
) , array(
'driver_id' => $getalertData['driverId']
));
$arrayUpdateData=array('status_code'=>'admin-cancelled',"status" => "6");
$this->db->where('id',$getalertData['bookingId']);
$this->db->update('bookingdetails',$arrayUpdateData);
$arrayUpdate=array(
'clickBy'=>$userData['id']
);
$this->db->where('id',$id);
$this->db->update('noJObAlert',$arrayUpdate);
echo 1;
}
}
public function viewall_massage()
{
$resultDataAll=$this->db->query("SELECT * FROM admin_message order by id desc")->result_array();
//print_r($resultData);die;
$this->load->view('sent_message',compact('resultDataAll'));
}
public function commissions()
{
$this->load->view('commissions');
}
public function save_commissions()
{
$admin=$_POST['admin'];
$online=$_POST['online'];
$offload=$_POST['offload'];
$street=$_POST['street'];
$doffload=$_POST['doffload'];
$this->db->where('id',1);
$array=array('admin'=>$admin,'online'=>$online,'offload'=>$offload,'street'=>$street,'driveroffload'=>$doffload);
$this->db->update('commissions',$array);
redirect('admin/commissions', 'refresh');
}
public function admin_login()
{
$getAdmin=$this->db->query("SELECT * FROM adminlogin WHERE role='admin' or role='master' order by id desc")->result_array();
$this->load->view('admin_login',compact('getAdmin'));
}
public function add_admin($id)
{
$getadmin=array();
if($id)
{
$getadmin=$this->db->query("SELECT * FROM adminlogin WHERE id=".$id."")->row_array();
}
$this->load->view('add_admin',compact('getadmin'));
}
public function save_admin()
{
$n=$this->input->post('name');
$u=$this->input->post('username');
$e=$this->input->post('email');
$p=$this->input->post('password');
$mobile=$this->input->post('mobile');
$admin=$this->input->post('admin');
$lname=$this->input->post('lname');
$id=$this->input->post('id');
$status=$this->input->post('status');
if($id)
{
$checkEmail=$this->db->query("SELECT * FROM adminlogin WHERE email='".$e."' and id!=".$id."")->row_array();
}else{
$checkEmail=$this->db->query("SELECT * FROM adminlogin WHERE email='".$e."'")->row_array();
}
if (!$checkEmail) {
$role='master';
if($e=='info@infinitecabs.com.au')
{
$role='admin';
}
$array=array(
'name'=>$n,
'username'=>$u,
'password'=>md5($p),
'role'=>$role,
'email'=>$u,
'mobile'=>$mobile,
'temp_password'=>$p,
'fname'=>$n,
'lname'=>$lname,
'new_email'=>$e,
'status'=>$status
);
if($id)
{
$this->db->where("id",$id);
$this->db->update('adminlogin',$array);
}else{
$this->db->insert('adminlogin',$array); 
}
redirect(base_url() . 'admin/admin_login');
}else{
$this->session->set_flashdata('msg', 'Email already in use!');
if($id)
{
redirect(base_url() . 'admin/add_admin/'.$id);
}else{
redirect(base_url() . 'admin/add_admin');
}
}
//
}
public function admin_delete($id)
{
$this->db->where('id', $id);
$del=$this->db->delete('adminlogin');
redirect(base_url() . 'admin/admin_login');
}
public function delete_car($id)
{
$this->db->where('id', $id);
$del=$this->db->delete('cars');
redirect(base_url() . 'admin/manage_cars');
}
public function ride_payments()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
// $carsData=$this->db->query("SELECT * FROM `cars`")->result_array();
$cabData=$this->db->query("SELECT * FROM `cars`")->result_array();
$caboperators=$this->db->query("SELECT * FROM `caboperators`")->result_array();
$this->db->select('*');
$this->db->from('payment_logs');
$this->db->join('bookingdetails', 'bookingdetails.id = payment_logs.bookingId','left');
$this->db->join('cars', 'bookingdetails.taxi_id = cars.id','left');
$this->db->join('driver_details', 'driver_details.id = payment_logs.driverId','left');
$this->db->join('caboperators', 'caboperators.id = payment_logs.OperatorID','left');
if($_GET['sdate'])
{
$dateData=explode("/",$_GET['edate']);
$dateDataw=explode("/",$_GET['sdate']);
$sdate=$dateDataw['2'].'/'.$dateDataw['1'].'/'.$dateDataw['0'];
$edate=$dateData['2'].'/'.$dateData['1'].'/'.$dateData['0'];
$this->db->where('payment_logs.date BETWEEN "'.$sdate. '" and "'. $edate.'"');
}   
if($_GET['driverId'])
{
$this->db->where("driver_details.id", $_GET['driverId']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['id'])
{
$this->db->where("payment_logs.bookingId", $_GET['id']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['driverName'])
{
$this->db->where("driver_details.id", $_GET['driverName']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['phone'])
{
$this->db->where("driver_details.phone", $_GET['phone']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['oid'])
{
$this->db->where("caboperators.id", $_GET['oid']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['oc'])
{
$this->db->where("driver_details.id", $_GET['oc']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['op'])
{
$this->db->where("driver_details.id", $_GET['op']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['cab'])
{
$this->db->where("bookingdetails.taxi_id", $_GET['cab']);
//$this->db->where('driverId',$_GET['driver']);
}
$this->db->group_by("payment_logs.bookingId");
$this->db->order_by('payment_logs.id','DESC');
$query = $this->db->get();
$resultData = $query->result_array();
$queryData=$this->db->last_query();
//  echo $this->db->last_query();
$this->load->view('ride_payments',compact('resultData','driverData','caboperators','cabData','queryData'));   
}
public function credit_debit()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->db->select('*');
$this->db->from('payment_logs');
$this->db->join('bookingdetails', 'bookingdetails.id = payment_logs.bookingId','left');
$this->db->join('driver_details', 'driver_details.id = payment_logs.driverId','left');
$this->db->join('caboperators', 'caboperators.id = payment_logs.OperatorID','left');
if($_GET['sdate'])
{
$this->db->where('payment_logs.date BETWEEN "'. date('Y-m-d', strtotime($_GET['sdate'])). '" and "'. date('Y-m-d', strtotime($_GET['edate'])).'"');
}
if($_GET['driver'])
{
$this->db->where("payment_logs.driverId", $_GET['driver']);
//$this->db->where('driverId',$_GET['driver']);
}
$this->db->group_by("payment_logs.bookingId");
$this->db->order_by('payment_logs.id','DESC');
$query = $this->db->get();
$resultData = $query->result_array();
//$this->db->last_query();die;
$this->load->view('credit_debit',compact('resultData','driverData'));
}
public function add_remove_amount()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->load->view('add_remove_amount',compact('driverData'));
}
public function save_amount()
{
$msg='yes';
$driverId=$_POST['driverId'];
$amount=$_POST['amount'];
$msg=$_POST['msg'];
$driverData=$this->db->query("SELECT * FROM `driver_details` WHERE id=".$driverId."")->row_array();
$type=$_POST['type'];
if($type=='debit')
{
$debit_amount=$driverData['debit_amount'];
$newamount=$debit_amount+$amount;
$arrayUpdate=array(
'debit_amount'=>$newamount
);
$this->db->where('id',$driverId);
$this->db->update('driver_details',$arrayUpdate);
$insertArray=array(
'driverId'=>$driverId,
'amount'=>$amount,
'type'=>$type,
'reason'=>$msg,
'updateAmount'=>$newamount
);
$this->db->insert('driverCreditndebits',$insertArray);
}else {
$credit=$driverData['credit_amount'];
$newamount=$credit+$amount;
$arrayUpdate=array(
'credit_amount'=>$newamount
);
$this->db->where('id',$driverId);
$this->db->update('driver_details',$arrayUpdate);
$insertArray=array(
'driverId'=>$driverId,
'amount'=>$amount,
'type'=>$type,
'reason'=>$msg,
'updateAmount'=>$newamount
);
$this->db->insert('driverCreditndebits',$insertArray);
}
redirect(base_url() . 'admin/add_remove_amount?alert=yes');
}
public function driverCreditndebits()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->db->select('driverCreditndebits.*,driver_details.user_name,driver_details.pname,bookingdetails.isdevice');
$this->db->from('driverCreditndebits');
$this->db->join('driver_details', 'driver_details.id = driverCreditndebits.driverId','left');
$this->db->join('bookingdetails', 'bookingdetails.id = driverCreditndebits.bookingId','left');
if($_GET['driver'])
{
$this->db->where("driver_details.id", $_GET['driver']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['phone'])
{
$this->db->where("driver_details.phone", $_GET['phone']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['driverId'])
{
$this->db->where("driver_details.id", $_GET['driverId']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['via'])
{
if($_GET['via']=='manual')
{
$this->db->where("driverCreditndebits.bookingId",null);
}else{
$this->db->where("driverCreditndebits.bookingId is NOT NULL",NULL, FALSE);
}
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['ptype'])
{
$this->db->where("driverCreditndebits.type", $_GET['ptype']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['tid'])
{
$this->db->where("driverCreditndebits.transactionID", $_GET['tid']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['edate'])
{
$dateData=explode("/",$_GET['edate']);
$dateData1=explode("/",$_GET['sdate']);
$edate=$dateData['2'].'-'.$dateData['0'].'-'.$dateData['1'];
$sdate=$dateData1['2'].'-'.$dateData1['0'].'-'.$dateData1['1'];
// echo $_GET['sdate'].'--'.$_GET['edate']..;die;
$this->db->where('driverCreditndebits.create_at BETWEEN "'. $sdate. '" and "'. $edate.'"');
//$this->db->where('driverId',$_GET['driver']);
}
$this->db->order_by('driverCreditndebits.id','DESC');
$query = $this->db->get();
$resultData = $query->result_array();
$queryData= $this->db->last_query();
$this->load->view('driverCreditndebits',compact('resultData','driverData','queryData'));    
}
public function checkWalletAmount()
{
$id=$_POST['id'];
$driverData=$this->db->query("SELECT * FROM `driver_details` WHERE id=".$id."")->row_array();
$credit=$driverData['credit_amount'];
$debit=$driverData['debit_amount'];
$wallet=$debit-$credit;
$wallet=round($wallet,2);
echo 'Current Wallet Amount $'.$wallet;
}
public function walletamount()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->db->select('driver_details.*');
$this->db->from('driver_details');
if($_GET['driverName'])
{
$this->db->where("driver_details.id", $_GET['driverName']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['driverId'])
{
$this->db->where("driver_details.id", $_GET['driverId']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['phone'])
{
$this->db->like("driver_details.phone", $_GET['phone']);
//$this->db->where('driverId',$_GET['driver']);
}
$this->db->order_by('driver_details.id','DESC');
$query = $this->db->get();
$resultData = $query->result_array();
$queryData=$this->db->last_query();
$this->load->view('walletamount',compact('resultData','driverData','queryData'));    
}
public function payoff()
{
$driverData=$this->db->query("SELECT * FROM `driver_details`")->result_array();
$this->db->select('driverCreditndebits.*,driver_details.user_name,driver_details.pname,driver_details.phone,driver_details.license_no,bookingdetails.isdevice');
$this->db->from('driverCreditndebits');
$this->db->join('driver_details', 'driver_details.id = driverCreditndebits.driverId','left');
$this->db->join('bookingdetails', 'bookingdetails.id = driverCreditndebits.bookingId','left');
$this->db->where("driverCreditndebits.type",'credit');
if($_GET['driverName'])
{
$this->db->where("driverCreditndebits.driverId", $_GET['driverName']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['phone'])
{
$this->db->like("driver_details.phone", $_GET['phone']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['driverId'])
{
$this->db->where("driverCreditndebits.driverId", $_GET['driverId']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['via'])
{
$this->db->where("driverCreditndebits.id", $_GET['via']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['tid'])
{
$this->db->where("driverCreditndebits.transactionID", $_GET['tid']);
//$this->db->where('driverId',$_GET['driver']);
}
if($_GET['edate'])
{
$dateData=explode("/",$_GET['edate']);
$edate=$dateData['2'].'-'.$dateData['1'].'-'.$dateData['0'];
$this->db->where('driverCreditndebits.create_at BETWEEN "'. date('Y-d-m', strtotime($_GET['sdate'])). '" and "'. $edate.'"');
//$this->db->where('driverId',$_GET['driver']);
}
$this->db->order_by('driverCreditndebits.id','DESC');
$query = $this->db->get();
$resultData = $query->result_array();
$queryData= $this->db->last_query();
$this->load->view('payoff',compact('resultData','driverData','queryData'));    
}
public function ExportCSVBooking($id)
{
$totalData = 10;
$jsonArray = array(
'draw' => $this->input->post('draw'),
'recordsTotal' => $totalData,
'recordsFiltered' => $totalData,
'data' => array(),
);
$filename=date('His').'test.csv';
$fp = fopen("upload/".$filename, 'w');
$orderArray= $this->home->get_bookingLocation($id);
foreach ($orderArray as $key => $val) {
$jsonArray['data'][] = array(
'BookingId' => $val['BookingId'],
'lat' => !empty($val['lat']) ? $val['lat'] : '---',
'lng' => !empty($val['lng']) ? $val['lng'] : '---',
'Date/Time' => $val['date'],
);
}
fputcsv($fp, array("BookingId","lat","lng","Date/Time"));
foreach ($jsonArray['data'] as $fields) {
fputcsv($fp, $fields);
}
fclose($fp);
header("Location:".base_url().'upload/'.date('His').'test.csv');
exit;
}
public function ride_paymentsCsv()
{
$totalData = 10;
$jsonArray = array(
'draw' => $this->input->post('draw'),
'recordsTotal' => $totalData,
'recordsFiltered' => $totalData,
'data' => array(),
);
$this->db->select('*');
$this->db->from('payment_logs');
$this->db->join('bookingdetails', 'bookingdetails.id = payment_logs.bookingId','left');
$this->db->join('driver_details', 'driver_details.id = payment_logs.driverId','left');
$this->db->join('caboperators', 'caboperators.id = payment_logs.OperatorID','left');
$this->db->group_by("payment_logs.bookingId");
$this->db->order_by('payment_logs.id','DESC');
$query = $this->db->get();
$orderArray = $query->result_array();
$filename=date('His').'test.csv';
$fp = fopen("upload/".$filename, 'w');
// $orderArray= $this->home->get_bookingLocation($id);
foreach ($orderArray as $key => $val) {
$jsonArray['data'][] = array(
'TRIPID' => $val['bookingId'],
'DRIVER NAME' =>  $val['first_name'],
'DRIVER ID' =>  $val['user_name'],
'Driver Mobile No' => $val['phone'],
'Cab No' => $val['car_no'],
'Pick UP' => $val['from'],
'Drop Off' => $val['to'],
'Transaction Date' => $val['date'],
'Job Type' => $val['isdevice'],
'Total Fair' => $val['amount'],
'PS Levy' => $val['amount'],
'Tolls' => '$1.10',
'Extras' => '$0.00',
'Payment Method' => $val['paymentMode'],
'Transaction ID' => $val['transactionId'],
'Admin Commission' => $val['adminCommissions'],
'Credit Amount' => $val['credit'],
'Debit Amount' => $val['debit'],
);
}
fputcsv($fp, array("TRIPID","DRIVER NAME","DRIVER ID","Driver Mobile No","Cab No","Pick UP","Drop Off","Transaction Date","Job Type","Total Fair","PS Levy","Tolls","Extras","Payment Method","Transaction ID","Admin Commission","Credit Amount","Debit Amount"));
foreach ($jsonArray['data'] as $fields) {
fputcsv($fp, $fields);
}
fclose($fp);
header("Location:".base_url().'upload/'.date('His').'test.csv');
exit;
}
public function driverCreditndebitsCsv()
{
$totalData = 10;
$jsonArray = array(
'draw' => $this->input->post('draw'),
'recordsTotal' => $totalData,
'recordsFiltered' => $totalData,
'data' => array(),
);
$this->db->select('driverCreditndebits.*,driver_details.user_name,driver_details.pname,bookingdetails.isdevice');
$this->db->from('driverCreditndebits');
$this->db->join('driver_details', 'driver_details.id = driverCreditndebits.driverId','left');
$this->db->join('bookingdetails', 'bookingdetails.id = driverCreditndebits.bookingId','left');
$this->db->order_by('driverCreditndebits.id','DESC');
$query = $this->db->get();
$orderArray = $query->result_array();
$filename=date('His').'test.csv';
$fp = fopen("upload/".$filename, 'w');
// $orderArray= $this->home->get_bookingLocation($id);
foreach ($orderArray as $key => $val) {
if($value['bookingId']) {  $via= 'Automatic'; } else {  $via= 'Manual'; }
$jsonArray['data'][] = array(
'Driver ID' => $val['user_name'],
'Driver Name' =>  $val['pname'],
'Date and Time' =>  $val['create_at'],
'Amount' => $val['amount'],
'Type' => $val['type'],
'Reason' => $val['reason'],
'Trip ID' => $val['bookingId'],
'Trip  Type' => $val['isdevice'] ? $val['isdevice'] : 'N/A',
'Transfer Via' => $via,
'Transaction ID' => $val['transactionID'] ? $val['transactionID'] : 'N/A',
'Balance Amount' => $val['updateAmount'] ? $val['updateAmount'] : 'N/A',
);
}
fputcsv($fp, array("Driver ID","Driver Name","Date and Time","Amount","Trip  Type","Reason","Trip ID","Job Type","Transfer Via","Transaction ID","Balance Amount"));
foreach ($jsonArray['data'] as $fields) {
fputcsv($fp, $fields);
}
fclose($fp);
header("Location:".base_url().'upload/'.date('His').'test.csv');
exit;
}
public function payoffCsv()
{
$totalData = 10;
$jsonArray = array(
'draw' => $this->input->post('draw'),
'recordsTotal' => $totalData,
'recordsFiltered' => $totalData,
'data' => array(),
);
$this->db->select('driverCreditndebits.*,driver_details.user_name,driver_details.pname,driver_details.phone,driver_details.license_no,bookingdetails.isdevice');
$this->db->from('driverCreditndebits');
$this->db->join('driver_details', 'driver_details.id = driverCreditndebits.driverId','left');
$this->db->join('bookingdetails', 'bookingdetails.id = driverCreditndebits.bookingId','left');
$this->db->where("driverCreditndebits.type",'credit');
$this->db->order_by('driverCreditndebits.id','DESC');
$query = $this->db->get();
$orderArray = $query->result_array();
$filename=date('His').'test.csv';
$fp = fopen("upload/".$filename, 'w');
// $orderArray= $this->home->get_bookingLocation($id);
foreach ($orderArray as $key => $val) {
$jsonArray['data'][] = array(
'Driver Name' =>  $val['pname'],
'Driver ID' => $val['user_name'],
'Driver Mobile' => $val['phone'],
'Transaction ID' => $val['transactionID'],
'Amount' => $val['amount'],
'Date and Time' =>  $val['create_at'],
);
}
fputcsv($fp, array("Driver Name","Driver Id","Driver Mobile","Transaction ID","Amount","Date and Time"));
foreach ($jsonArray['data'] as $fields) {
fputcsv($fp, $fields);
}
fclose($fp);
header("Location:".base_url().'upload/'.date('His').'test.csv');
exit;
}
public function walletamountcsv()
{
$totalData = 10;
$jsonArray = array(
'draw' => $this->input->post('draw'),
'recordsTotal' => $totalData,
'recordsFiltered' => $totalData,
'data' => array(),
);
$this->db->select('driver_details.*');
$this->db->from('driver_details');
$query = $this->db->get();
$orderArray = $query->result_array();
$filename=date('His').'test.csv';
$fp = fopen("upload/".$filename, 'w');
// $orderArray= $this->home->get_bookingLocation($id);
foreach ($orderArray as $key => $val) {
$jsonArray['data'][] = array(
'Driver Name' =>  $val['user_name'],
'Driver ID' => $val['pname'],
'Driver Mobile' => $val['phone'],
'License No' => $val['license_no'],
'Current Wallet Amount' =>  $val['wallet_amount'],
);
}
fputcsv($fp, array("Driver Name","Driver Id","Driver Mobile","License No","Current Wallet Amount"));
foreach ($jsonArray['data'] as $fields) {
fputcsv($fp, $fields);
}
fclose($fp);
header("Location:".base_url().'upload/'.date('His').'test.csv');
exit;
}
public function checkreport()
{
$id=$_GET['id'];
$promoData=$this->db->query("SELECT * FROM bookingdetails where promo_code='".$id."' and status_code='completed'")->result_array();
$this->load->view('checkreport',compact('promoData'));
}
public function UpdateStatus()
{
$id=$_POST['id'];
$table=$_POST['table'];
$promoData=$this->db->query("SELECT * FROM ".$table." where id='".$id."'")->row_array();
if($promoData['status']=='Inactive')
{
if($table=='driver_details')
{
$getOrder = $this
->db
->query("UPDATE diver_live_location SET driver_booking_status='free' ,isBlock=0  WHERE driver_id='" . $id . "'");
$datas1=array("status"=>'Active',"rating"=>0,"isBlock"=>0);
$this->db->where("id",$id);
$this->db->update($table, $datas1);
}
$datas=array("status"=>'Active');
$this->db->where("id",$id);
$this->db->update($table, $datas);
echo 1;
}
else if($promoData['status']=='Active')
{
//  echo 'wewewe';die;
$datas=array("status"=>'Inactive');
$this->db->where("id",$id);
$this->db->update($table, $datas);
echo 2;
}
else if($promoData['status']==1)
{
// print_r($promoData['status']);die;
$datas=array("status"=>'0');
$this->db->where("id",$id);
$this->db->update($table, $datas);
if($table=='cars')
{
$this->db->query("UPDATE diver_live_location SET driver_booking_status='block' WHERE cab_id='".$id."'");
}
echo 2;
}else if($promoData['status']==0)
{
//  print_r('0');die;
$datas=array("status"=>'1');
$this->db->where("id",$id);
$this->db->update($table, $datas);
if($table=='cars')
{
$this->db->query("UPDATE diver_live_location SET driver_booking_status='free' WHERE cab_id='".$id."'");
}
echo 1;
}
}
public function uploadcsv()
{
$csv = array();
$deleteZOne=$this->db->query("DELETE FROM zone");
// check there are no errors
if($_FILES['csv']['error'] == 0){
$name = $_FILES['csv']['name'];
$ext = strtolower(end(explode('.', $_FILES['csv']['name'])));
$type = $_FILES['csv']['type'];
$tmpName = $_FILES['csv']['tmp_name'];
// check the file is a csv
if($ext === 'csv'){
if(($handle = fopen($tmpName, 'r')) !== FALSE) {
// necessary if a large csv file
set_time_limit(0);
$row = 0;
$insert_data=array();
while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
// number of fields in the csv
$col_count = count($data);
$csv[$row]['col1'] = $data[0];
$csv[$row]['col2'] = $data[1];
$csv[$row]['col3'] = $data[2];
$title = $csv[$row]['col1'];
$time = $csv[$row]['col3'];
$zipcode = $csv[$row]['col2'];
$checkZipcode=$this->db->query("SELECT * FROM zone WHERE zipcode='".$zipcode."'")->row_array();
if(!$checkZipcode)
{
if($title!='Zone Name')
{
$insert_data[]=array(
'zone_title'=>$title,
'zipcode'=>$zipcode,
'time'=>$time
);
}
}
// inc the row
$row++;
}
// print_r($insert_data);die;
$this->db->insert_batch('zone', $insert_data); 
fclose($handle);
redirect(base_url() . 'zone/index');
}
} 
}
}
public function checkDate()
{
$date=$_POST['date'];
$checkDate=$this->db->query("SELECT * FROM `holidays` WHERE date_format( str_to_date( `date` , '%d/%m/%Y' ) , '%d/%m/%Y' )='".$date."'")->row_array();
if($checkDate)
{
echo 1;
}
//echo $this->db->last_query();die;
}

    public function client()
    {
        
        $client=$this->db->query("SELECT * FROM `client`")->row_array();
        $client['client'] = $client;
        $this->load->view('add-client',$client);
        //echo $this->db->last_query();die;
    }

    public function save_client()
    {
        
        $update=$this->db->query("UPDATE `client` SET `username`='$_POST[username]',`password`='$_POST[password]' WHERE id=1");

        $client=$this->db->query("SELECT * FROM `client`")->row_array();
        $client['client'] = $client;
        $this->session->set_flashdata('success_msg', 'Changed successfully');

        $this->load->view('add-client',$client);
        //echo $this->db->last_query();die;
    }

    
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>
