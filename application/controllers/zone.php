<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zone extends MY_Controller
{
	public $zone_name = CUSTOM_ZONE_NAME;
	
	// construct call
	public function __construct()
	{
	parent::__construct();
	$this->load->helper(array('form', 'url'));
	$this->load->helper('date');
	$this->load->helper('file');
	$this->load->library('form_validation');
	$this->load->model('model_zone','zone');
	$this->load->database();
	$this->load->library('session');
	$this->load->library('image_lib');
	$this->load->helper('cookie');
	$this->load->helper('url');
	$this->load->library('email');
	$this->load->model('Model_admin','home');
	session_start();
	error_reporting(0);
	}

	// permission call
	public function permission()
	{
		//$data=$_POST;
		$permission="";

		if(($this->session->userdata('permission'))) {
			$ff = $this->router->fetch_method();

			$pm = $this->db->query("SELECT * FROM  pages WHERE pages='$ff'");

			if($pm->num_rows == 1) {
				$upm = $pm->row('p_id');
				$id=explode(',',$this->session->userdata('permission'));
				if(in_array($upm,$id)) {
					$permission = "access";
				} else {
					$permission = "failed";
					redirect('admin/not_admin');
				}
			} else {
				$permission = "failed";
			}
		}
		return $permission;
	}

	// index page call
	public function index()
	{
		 
		$zone=$this->zone->get_zone();
		 $query2=$this->home->get_driver_livezone();
		 //print_r($query2);die;
		//echo "string";die();
		$cars=$this->db->query("SELECT * FROM cars")->result_array();
   	$this->load->view('zone/index',compact('zone','cars','query2'));
	}
	public function get_zone()
	{
		//echo "string";die();
		$zone['zone']=$this->zone->get_zone();

	}
	public function createzone()
	{
		//echo "string";die();
		$suburb['suburb']=$this->zone->get_suburbs();

		$this->load->view('zone/createZone',$suburb);
	}
	public function zoneDetails($id,$sid=null)
	{
	
        $result = $this->db->query("SELECT * FROM zone WHERE id=".$id."")->row_array();
        
        $this->db->select('cars.car_number as cnumber,driver_details.*,driver_zone_status.id as zid,driver_zone_status.position,driver_zone_status.date as edate,diver_live_location.latitude as dlat,diver_live_location.longlatitude as dlng,diver_live_location.driver_booking_status');
        $this->db->from('driver_zone_status');
        $this->db->join('driver_details', 'driver_zone_status.driverID = driver_details.id','left');
        $this->db->join('diver_live_location', 'driver_zone_status.driverID = diver_live_location.driver_id','left');
        
			$this->db->join('cars', 'cars.id = diver_live_location.cab_Id','left');
			 $this->db->where('driver_zone_status.zoneId',$id);
			$where = "(diver_live_location.driver_booking_status ='free' or diver_live_location.driver_booking_status='booked')";

			$this->db->having($where);

         //$this->db->where('driver_details.car_no','');
       
        $driver_zone_status=$this->db->get()->result_array();
        //echo $this->db->last_query();die;
        //print_r($driver_zone_status);die();
        $query2=$this->home->get_driver_livezone($id);

//	print_r($query2);die;
		$this->load->view('zone/zoneDetials',compact('result','id','driver_zone_status','query2'));
	}
	public function saveZone($value='')
	{
        /* echo '<pre>';
        print_r($_POST);die;*/
        $title = $this->input->post('title');
        $time = $this->input->post('time');
        $zipcode = $this->input->post('zipcode');
        
        $checkZipcode=$this->db->query("SELECT * FROM zone WHERE zipcode='".$zipcode."'")->row_array();
        if($checkZipcode)
        {
            echo 1;
        }else{
        
        $insert_data=array(
        'zone_title'=>$title,
        'zipcode'=>$zipcode,
        'time'=>$time
        
        );
        
        $this->db->insert('zone', $insert_data);
        $lastId=$this->db->insert_id();
        
            echo 2;
        }
	}
	public function delete_zone()
	{	
		$id=$this->input->post('data_id');
		$where = array("id" => $id);
        $this->db->where('id',$id);
        $this->db->delete("zone");
        
       	return true;
	}
	
		public function updateZone()
	{	
		$id=$this->input->post('data_id');
		$title=$this->input->post('title');
		$time=$this->input->post('time');
		$zipcode=$this->input->post('zipcode');
		
		$data = array("zone_title" => $title,"time" => $time,"zipcode" => $zipcode);
        $this->db->where('id',$id);
        $this->db->update("zone",$data);
        //echo $this->db->last_query();die;
        
       	return true;
	}

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>