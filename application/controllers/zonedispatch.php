<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zonedispatch extends MY_Controller
{
	public $zone_name = CUSTOM_ZONE_NAME;
	
	// construct call
	public function __construct()
	{
	parent::__construct();
	$this->load->helper(array('form', 'url'));
	$this->load->helper('date');
	$this->load->helper('file');
	$this->load->library('form_validation');
	$this->load->model('model_zone','zone');
	$this->load->database();
	$this->load->library('session');
	$this->load->library('image_lib');
	$this->load->helper('cookie');
	$this->load->helper('url');
	$this->load->library('email');
	session_start();
	}

	// permission call
	public function permission()
	{
		//$data=$_POST;
		$permission="";

		if(($this->session->userdata('permission'))) {
			$ff = $this->router->fetch_method();

			$pm = $this->db->query("SELECT * FROM  pages WHERE pages='$ff'");

			if($pm->num_rows == 1) {
				$upm = $pm->row('p_id');
				$id=explode(',',$this->session->userdata('permission'));
				if(in_array($upm,$id)) {
					$permission = "access";
				} else {
					$permission = "failed";
					redirect('admin/not_admin');
				}
			} else {
				$permission = "failed";
			}
		}
		return $permission;
	}

	// index page call
	public function index()
	{
		 
		$zone=$this->zone->get_zone();
		//echo "string";die();
		$cars=$this->db->query("SELECT * FROM cars")->result_array();
   	$this->load->view('dispatch/zone/index',compact('zone','cars'));
	}
	public function get_zone()
	{
		//echo "string";die();
		$zone['zone']=$this->zone->get_zone();

	}
	public function createzone()
	{
		//echo "string";die();
		$suburb['suburb']=$this->zone->get_suburbs();

		$this->load->view('dispatch/zone/createZone',$suburb);
	}
	public function zoneDetails($id,$sid=null)
	{
	
        $result = $this->db->query("SELECT * FROM zone WHERE id=".$id."")->row_array();
        
        $this->db->select('driver_details.*,driver_zone_status.id as zid,driver_zone_status.position,driver_zone_status.date as edate,diver_live_location.latitude as dlat,diver_live_location.longlatitude as dlng,diver_live_location.driver_booking_status');
        $this->db->from('driver_zone_status');
        $this->db->join('driver_details', 'driver_zone_status.driverID = driver_details.id');
        $this->db->join('diver_live_location', 'driver_zone_status.driverID = diver_live_location.driver_id');
        
           $this->db->join('cars', 'cars.id = driver_details.carId');
         //$this->db->where('driver_details.car_no','');
        $this->db->where('driver_zone_status.zoneId',$id);
        $driver_zone_status=$this->db->get()->result_array();
       // echo $this->db->last_query();die;
        //print_r($driver_zone_status);die();

		$this->load->view('dispatch/zone/zoneDetials',compact('result','id','driver_zone_status'));
	}
	public function saveZone($value='')
	{
	   /* echo '<pre>';
	    print_r($_POST);die;*/
		$title = $this->input->post('title');
		$suburb = $this->input->post('suburb');


		$this->zone->save_zone($this->input->post());

		//$this->load->view('zone/index');
		redirect('zone/index');
		
	}
	public function delete_zone()
	{	
		$id=$this->input->post('data_id');
		$where = array("id" => $id);
        $this->db->where('id',$id);
        $this->db->delete("zone");
        
       	return true;
	}
	
		public function updateZone()
	{	
		$id=$this->input->post('data_id');
		$title=$this->input->post('title');
		$time=$this->input->post('time');
		
		$data = array("zone_title" => $title,"time" => $time);
        $this->db->where('id',$id);
        $this->db->update("zone",$data);
        
       	return true;
	}

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>