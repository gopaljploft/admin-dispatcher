<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bookkeeping extends CI_Controller
{
	public $zone_name = CUSTOM_ZONE_NAME;
	
	// construct call
	public function __construct()
	{
	parent::__construct();
	$this->load->helper(array('form', 'url'));
	$this->load->helper('date');
	$this->load->helper('file');
	$this->load->library('form_validation');
	$this->load->model('Model_admin','home');
	$this->load->database();
	$this->load->library('session');
	$this->load->library('image_lib');
	$this->load->helper('cookie');
	$this->load->helper('url');
	$this->load->library('email');
	$this->load->model('model_dispatch');
	$this->load->model('model_bookkeeping');
	session_start();
	}

	// permission call
	public function index()
	{

		//$data=$_POST;

		
    $dispatch['dispatchuser']=$this->model_bookkeeping->displayrecords();

	$this->load->view('bookkeeping',$dispatch);
	
       	
	}

	public function save()
	{

		$this->load->view('add-bookkeeping');
	

       	if($this->input->post('save'))
		{


		$n=$this->input->post('name');
		$m=$this->input->post('age');
		$e=$this->input->post('email');
		$p=$this->input->post('password');
		$mobile=$this->input->post('phone');
			$lname=$this->input->post('lname');
$checkemail=$this->db->query("SELECT * FROM adminlogin WHERE email='".$e."'")->row_array();
			$checkmobile=$this->db->query("SELECT * FROM adminlogin WHERE mobile='".$m."'")->row_array();
		$errormsg=array('error'=>'');
		if($checkemail)
		{
		    	$errormsg=array('error'=>'email');
		    	redirect("bookkeeping/save?error=email",compact('errormsg'));  
		}else if($checkmobile)
		{
		    $errormsg=array('error'=>'mobile');
		    	redirect("bookkeeping/save?error=mobile",compact('errormsg'));  
		}		
		$this->model_bookkeeping->saverecords($n,$m,$e,$p,$mobile,$lname);	
		 ob_start();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://platform.clickatell.com/messages/http/send?apiKey=iH2z_OGzRIa663II0vAS3Q==&to=+91' . $mobile . '&content=you+have+register+successfully+with+infinitecab+bookkeeping+solution+here+is+your+login+details+email:+'.$e.'+password:+'.$p.'');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
		redirect("bookkeeping/index");  
		}
	}

	public function edit($id)
	{
      
    

       $result['firstrecord']=$this->model_bookkeeping->getuserdata($id);

		$this->load->view('edit-bookkeeping',$result);


       	if($this->input->post('save'))
		{
           
           $id =  $_GET['id'];
		 $postData = $this->input->post();
		
		$this->model_bookkeeping->updateUser($postData,$id);

		redirect("bookkeeping/index");  
		}
	}

	public function delete($id)
	{
	
	$this->model_bookkeeping->deleterecords($id);
	redirect("bookkeeping/index");  
	}



	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>