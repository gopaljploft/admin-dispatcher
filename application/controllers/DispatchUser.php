<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DispatchUser extends CI_Controller
{
	public $zone_name = CUSTOM_ZONE_NAME;
	
	// construct call
	public function __construct()
	{
	parent::__construct();
	$this->load->helper(array('form', 'url'));
	$this->load->helper('date');
	$this->load->helper('file');
	$this->load->library('form_validation');
	$this->load->model('Model_admin','home');
	$this->load->database();
	$this->load->library('session');
	$this->load->library('image_lib');
	$this->load->helper('cookie');
	$this->load->helper('url');
	$this->load->library('email');
	$this->load->model('model_dispatch');
	session_start();
	}

	// permission call
	public function index()
	{

		//$data=$_POST;

		
    $dispatch['dispatchuser']=$this->model_dispatch->displayrecords();

	$this->load->view('dispatch',$dispatch);
	
       	
	}

	public function save()
	{
        
		$this->load->view('add-dispatchuser');
	

       	if($this->input->post('save'))
		{
           // echo 'wew';die;
		$n=$this->input->post('name');
		$m=$this->input->post('age');
		$e=$this->input->post('email');
		$p=$this->input->post('password');
		$mobile=$this->input->post('mobile');
		$admin=$this->input->post('admin');
		$lname=$this->input->post('lname');
		
		$checkemail=$this->db->query("SELECT * FROM adminlogin WHERE email='".$e."'")->row_array();
			$checkmobile=$this->db->query("SELECT * FROM adminlogin WHERE mobile='".$m."'")->row_array();
		$errormsg=array('error'=>'');
		if($checkemail)
		{
		    	$errormsg=array('error'=>'email');
		    	redirect("DispatchUser/save?error=email");  
		}else if($checkmobile)
		{
		    $errormsg=array('error'=>'mobile');
		    	redirect("DispatchUser/save?error=mobile");  
		}
		
		$this->model_dispatch->saverecords($n,$m,$e,$p,$mobile,$admin,$lname);	
		 ob_start();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://platform.clickatell.com/messages/http/send?apiKey=iH2z_OGzRIa663II0vAS3Q==&to=+91' . $mobile . '&content=you+have+register+successfully+with+infinitecab+dispatcher+solution+here+is+your+login+details+email:+'.$e.'+password:+'.$p.'');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
		redirect("DispatchUser/index");  
		}
	}

	public function edit($id)
	{
      
    

       $result['firstrecord']=$this->model_dispatch->getuserdata($id);

		$this->load->view('edit-dispatchuser',$result);


       	if($this->input->post('save'))
		{
           
           $id =  $_GET['id'];
		 $postData = $this->input->post();
		
		$this->model_dispatch->updateUser($postData,$id);

		redirect("DispatchUser/index");  
		}
	}

	public function delete($id)
	{
	
	$this->model_dispatch->deleterecords($id);
	redirect("DispatchUser/index");  
	}



	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>