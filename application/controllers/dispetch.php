<?php
error_reporting(0);
ob_start();
//error_reporting(0);
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN']))
{
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400'); // cache for 1 day
    
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
{

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class web_service extends CI_Controller
{
    public $zone_name = CUSTOM_ZONE_NAME;
    //public $zone_name = 'Asia/Kuwait';
    public function __construct()
    {
        parent::__construct();

        $this
            ->load
            ->helper('form');
        $this
            ->load
            ->helper('url');
        $this
            ->load
            ->helper('file');
        $this
            ->load
            ->helper('JWT');
        // $this->load->library('form_validation');
        $this
            ->load
            ->model('model_web_service');
        $this
            ->load
            ->database();
        // $this->load->library('session');
        $this
            ->load
            ->library('image_lib');
        // $this->load->helper('cookie');
        $this
            ->load
            ->library('email');
        // $this->load->library('pagination');
        //date_default_timezone_set("Asia/Kolkata");
        // session_start();
        
    }

    public function index()
    {
        
        
    }

    public function login()
    {
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $key_status = $this
            ->model_web_service
            ->authenticate_key($request);

        if ($key_status)
        {
            $this->do_login($request);
        }
        else
        {
            $finresult[] = array(
                'statusCode' => '501',
                'message' => 'Login failed, Secret key miss match',
                'data' => array() ,
            );
            print json_encode($finresult);
        }

    }

    function do_login($request)
    {
        /*$url = $_SERVER['REQUEST_URI'];
        $email1 = explode('email=', $url);
        $email2 = $email1[1];
        $email3 = explode('&', $email2);
        $email = urldecode($email3[0]);
        
        $username1 = explode('username=', $url);
        $username2 = $username1[1];
        $username3 = explode('&', $username2);
        $username = urldecode($username3[0]);*/
        $email = $this
            ->input
            ->post('email');
        $password = $this
            ->input
            ->post('password');
        $deviceToken = $this
            ->input
            ->post('deviceToken');
        // yes / no
        $table_time_details = 'time_detail';
        $table = 'userdetails';
        $select_data = "*";
        $this
            ->db
            ->select($select_data);
        $this
            ->db
            ->where("(email = '$email' OR username = '$email' OR mobile = '$email' )");
        $query = $this
            ->db
            ->get($table);
        $result = $query->result_array();
        $result = $this
            ->model_web_service
            ->login($email, $password);
        $table_cab_details = 'cabdetails';

        $status = "SELECT * FROM `userdetails` where email='$email' OR username='$email'";
        $statusrs = mysql_query($status);
        $datastatus = mysql_fetch_array($statusrs);
        $statuschk = $datastatus['user_status'];

        if ($result && $statuschk == 'Active')
        {
            //$this->db->select('*');
            //$this->db->from('cabdetails');
            // $this->db->join('Car_Type', 'Car_Type.car_type = cabdetails.cartype');
            //$this->db->order_by("cab_id", "desc");
            //$query = $this->db->get();
            $statusd = "update `userdetails` set device_token='$deviceToken' where id='" . $result['id'] . "' ";
            $statusrs = mysql_query($statusd);
            $result1 = array();
            $result1['id'] = ($result['id']) ? $result['id'] : '';
            $result1['first_name'] = ($result['first_name']) ? $result['first_name'] : '';
            $result1['last_name'] = ($result['last_name']) ? $result['last_name'] : '';
            $result1['email'] = ($result['email']) ? $result['email'] : '';
            $result1['mobile'] = ($result['mobile']) ? $result['mobile'] : '';
            $result1['device_id'] = ($result['device_id']) ? $result['device_id'] : '';
            $result1['user_status'] = ($result['user_status']) ? $result['user_status'] : '';
            $result1['image'] = ($result['image']) ? $result['image'] : '';
            $result1['notification'] = ($result['notification']) ? $result['notification'] : '';

            $result_cab_details = $query->result_array();
            $result_cab_time = $this
                ->model_web_service
                ->get_table('*', $table_time_details);
            $table_setting_details = 'settings';
            $result_setting = $this
                ->model_web_service
                ->get_table('country,currency', $table_setting_details);
            $finresult['statusCode'] = '200';
            $finresult['message'] = 'Successfully Logged in';
            /*$finresult['Isactive']=$statuschk;
            $finresult['time_detail']=$result_cab_time;
            $finresult['country_detail']=$result_setting;*/
            $finresult['data'] = $result1;
            echo json_encode($finresult);
        }
        elseif ($statuschk == 'Inactive')
        {

            $finresult['statusCode'] = '501';
            $finresult['message'] = 'Your account has been temporarily locked. Please contact our admin for further details.';
            $finresult['data'] = array(
                'status' => $statuschk
            );
            echo json_encode($finresult);

        }
        else
        {
            $finresult['statusCode'] = '401';
            $finresult['message'] = 'Login failed, Please enter correct login details';
            $finresult['data'] = array(
                'email' => $email
            );
            print json_encode($finresult);
        }
    }

   

}

