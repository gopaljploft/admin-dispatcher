<!DOCTYPE html>



<html>



<head>



	<meta charset="UTF-8" />



	<meta name="viewport" content="width=device-width, initial-scale=1.0" />



	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />







	<title>Manage Levy Tracker - Infinite Cab</title>







	<!-- bootstrap -->



	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />







	<!-- RTL support - for demo only -->



	<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>



	<!--



    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />



    And add "rtl" class to <body> element - e.g. <body class="rtl">



    -->







	<!-- libraries -->



	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />



	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />







	<!-- global styles -->



	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />







	<!-- this page specific styles -->



	<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



	<link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">







	<!-- Favicon -->



	<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />







	<!-- google font libraries -->



	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>







	<!--[if lt IE 9]>



	<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>



	<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>



	<![endif]-->







	<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>



</head>



<body>



<div class="cover"></div>



<div id="theme-wrapper">



	<?php



	include"includes/admin_header.php";



	?>



	<div id="page-wrapper" class="container">



		<div class="row">



			<?php



			include"includes/admin_sidebar.php";



			?>



			<div id="content-wrapper">



				<div class="row" style="opacity: 1;">



					<div class="col-lg-12">



						<div class="row">



							<div class="col-lg-12">



								<div id="content-header" class="clearfix">



									<div class="pull-left">



										<h1>Manage Levy Tracker</h1>



									</div>



									<div class="pull-right">



										<ol class="breadcrumb">



											<li><a href="#">Home</a></li>



											<li class="active"><span>Manage Levy Tracker</span></li>



										</ol>



									</div>



								</div>



							</div>



						</div>



						<!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->



						<div class="col-lg-12">



							<!-- Single Delete -->



							<div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">



								<div class="modal-dialog">



									<div class="modal-content">



										<div class="modal-header">



											<i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>



										</div>



										<div class="modal-title">Are you sure you want to delete the selected user?</div>



										<div class="modal-body"></div>



										<div class="modal-footer">



											<button id="confirm-delete-button" onclick="delete_single_user_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>



											<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>



											<input type="hidden" value="" id="bookedid" name="bookedid">



											<button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>



										</div>



									</div> <!-- / .modal-content -->



								</div> <!-- / .modal-dialog -->



							</div> <!-- / .modal -->



							<!-- / Single Delete -->



							<!-- Multipal Delete -->



							<div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">



								<div class="modal-dialog">



									<div class="modal-content">



										<div class="modal-header">



											<i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>



										</div>



										<div class="modal-title">Are you sure you want to delete selected user?</div>



										<div class="modal-body"></div>



										<div class="modal-footer">



											<button onclick="delete_user()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>



											<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>



											<button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>



										</div>



									</div> <!-- / .modal-content -->



								</div> <!-- / .modal-dialog -->



							</div> <!-- / .modal -->



							<!-- / Multipal Delete -->



						</div>



						<!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->



						<div class="row">



							<div class="col-lg-12">



								<div class="main-box clearfix">



									<div class="panel">



										<div class="panel-body">



										 <div class="panel-body">

                                         <form action="<?php echo base_url() ?>admin/shiftrereport">

                                                      

                                                <div class="row">

                                              

                                               

                                                

                                                 <div style="" class="form-group col-md-4">

                                                         <input id="cid" type="text" value="<?php if($_GET['cid']) { echo $_GET['cid']; } ?>" placeholder="Client ID" name ="cid" class="form-control">

                                                 

                                                </div>

                                                <div style="" class="form-group col-md-4">

                                                         <input id="cars" type="text" value="<?php if($_GET['cab']) { echo $_GET['cab']; } ?>" placeholder="Cab No" name ="cab" class="form-control">

                                                 

                                                </div>

                                                 

                                                

                                                 <div style="" class="form-group col-md-4">

                                                         <input id="tags" type="text" value="<?php if($_GET['username']) { echo $_GET['username']; } ?>" placeholder="Driver ID" name ="username" class="form-control">

                                                 

                                                </div>

                                                <div style="" class="form-group col-md-4">

                                                         <select id="category" type="text"  name ="category" class="form-control">

                                                             <option value="0">Cab Model</option>

                                                              <option value="0">All</option>

                                                             <?php

                                                                foreach($cabData as $val)

                                                                {

                                                                    ?>

                                                                    <option value="<?php echo $val['cab_id'] ?>" ><?php echo $val['cartype'] ?></option>

                                                                    <?php

                                                                } 

                                                             ?>

                                                         </select>

                                                 

                                                </div>

                                                 <div style="" class="form-group col-md-4">

                                                         <input id="dname" type="text" value="<?php if($_GET['name']) { echo $_GET['name']; } ?>" placeholder="Driver Name" name ="name" class="form-control">

                                                 

                                                </div>

                                                

                                                   <div style="" class="form-group col-md-4">

                                                         <input id="tags" type="text" value="<?php if($_GET['phone']) { echo $_GET['phone']; } ?>" placeholder="Driver Mobile No" name ="phone" class="form-control">

                                                 

                                                </div>

                                                 

                                                 

                                                <div style="" class="form-group col-md-4">

                                                    <input type="text" autocomplete="off" value="<?php if($_GET['sdate']) { echo $_GET['sdate']; } ?>" placeholder="From Date" name ="sdate" class="form-control date">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div>

                                                

                                                <div style="" class="form-group col-md-4">

                                                    <input type="text" autocomplete="off" value="<?php if($_GET['eedate']) { echo $_GET['eedate']; } ?>" placeholder="To Date" name ="eedate" class="form-control date">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div>

                                                

                                               

                                                </div>

                                                       <div class="row resh">

                                                <a href="<?php echo base_url() ?>admin/shiftrereport" class="btn btn-primary ">

                                                    <i class="fa fa-refresh fa-lg"></i> Reset

                                                </a>

                                                

                                                

                                                <button type="submit" class="btn btn-primary " href="javascript:void(0)">

                                                    <i class="fa fa-search fa-lg"></i> Search

                                                </button>



                                                <a   class="btn btn-primary <?php if(empty($result)) { echo 'disabled'; } ?>" href="<?php echo base_url(); ?>admin/exportshift?query=<?php echo $queryData; ?>">

                                                    <!-- <i class="fa fa-envelope fa-lg"></i> --> Export

                                                </a>

                                                

                                                 <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary " href="javascript:void(0)">

                                                   <!--  <i class="fa fa-envelope fa-lg"></i> --> Send To Mail

                                                </button>

                                            

                                                 

                                               

                                                <span>&nbsp;</span>

                                                </div> 

                                            </form>  

                                    

                                </div>



										</div>



									</div>



									<div class="main-box-body clearfix">



										<div class="table-responsive">



											<table id="example" class="table table-hover table-bordered user-list datata">



												<thead>



												<tr>



													<th style="vertical-align: middle;min-width: 90px;"><a href="javascript:void(0);">Client ID</a></th>



													<th style="vertical-align: middle;min-width: 90px;"><a href="javascript:void(0);">Shift ID</a></th>

													<th style="vertical-align: middle;min-width: 80px;"><a href="javascript:void(0);">Cab No</a></th>

													<th style="vertical-align: middle;min-width: 110px;"><a href="javascript:void(0);">Meter Sr No</a></th>

														<th style="vertical-align: middle;min-width: 120px;"><a href="javascript:void(0);">Driver ID</a></th>

													<th style="vertical-align: middle;min-width: 120px;"><a href="javascript:void(0);">Driver Name</a></th>

												

													<th style="vertical-align: middle;min-width: 175px;"><a href="javascript:void(0);">Shift Start Date/Time</a></th>

														<th style="vertical-align: middle;min-width: 165px;"><a href="javascript:void(0);">Shift End Date/Time</a></th>

															<th style="vertical-align: middle;min-width: 90px;"><a href="javascript:void(0);">Total KM</a></th>

															<th style="vertical-align: middle;min-width: 105px;"><a href="javascript:void(0);">Total Trips</a></th>

																<th style="vertical-align: middle;min-width: 126px;"><a href="javascript:void(0);"> Levy Amount</a></th>

																	<th style="vertical-align: middle;min-width: 130px;"><a href="javascript:void(0);">Shift Earning</a></th>

																	<th style="vertical-align: middle;"><a href="javascript:void(0);">Action</a></th>

													

													

												

												</tr>

												<tbody>

													<?php

													$i=1;

													foreach ($result as $key => $value) {

													$totalbooking=$this->db->query("SELECT count(id) as TotalBooking,sum(amount) as totalerning from bookingdetails WHERE shiftId=".$value['shiftId']."")->row_array();

													

														$totalbookingkm=$this->db->query("SELECT count(id) as TotalBooking,sum(distance) as totalkm from bookingdetails WHERE shiftId=".$value['shiftId']."")->row_array();

													

													?>

													<tr>



														

                                            <td><?php echo $value['clientId']; ?></td>

                                            <td><a class="badge" href="view_trip?id=<?php echo $value['shiftId'] ?>&client=<?php echo $value['clientId']; ?>&Start=<?php echo $value['loginAt']; ?>&end=<?php echo $value['logoutAt']; ?>&cab=<?php echo $value['car_number']; ?>"  style="color:#fff;"><?php echo $value['shiftId'] ?></a></td>

                                            <td><?php echo $value['car_number']; ?></td>

                                             <td><?php echo 'N/A'; ?></td>

                                               <td><?php echo $value['user_name']; ?></td>

                                            <td><?php echo $value['name'] ?></td>

                                          

                                            <td><?php $dateData=explode(" ",$value['loginAt']);  ; echo date('d/m/Y', strtotime($dateData[0])).' '.$dateData[1] ?> </td>

                                            

                                             <td><?php if(!$value['logoutAt']){ echo 'N/A'; } else { $dateData=explode(" ",$value['logoutAt']); $d=date($dateData[0]); echo date('d/m/Y', strtotime($d)).' '.$dateData[1]; } ?></td>

                                            

                                            <td><?php echo $totalbookingkm['totalkm']; ?></td>

                                            <td><?php echo $totalbooking['TotalBooking']; ?></td>

                                            <td>$<?php echo $totalbooking['TotalBooking']*1.10; ?></td>

                                            <td>$<?php echo $totalbooking['totalerning']; ?></td>

                                           

                                           <td class="dram"><a href="view_trip?id=<?php echo $value['shiftId'] ?>&client=<?php echo $value['clientId']; ?>&Start=<?php echo $value['loginAt']; ?>&end=<?php echo $value['logoutAt']; ?>&cab=<?php echo $value['car_number']; ?>" class="table-link">

                <span class="fa-stack">

                    <i class="fa fa-square fa-stack-2x"></i>

                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>

                </span>

            </a>

		</td>

													

														

												

														

													</tr>

													<?php $i++; } ?>

												</tbody>

												</thead>



											</table>



										</div>





										<!--<ul class="pagination pull-right">



                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>



                                            <li><a href="javascript:void(0);">1</a></li>



                                            <li><a href="javascript:void(0);">2</a></li>



                                            <li><a href="javascript:void(0);">3</a></li>



                                            <li><a href="javascript:void(0);">4</a></li>



                                            <li><a href="javascript:void(0);">5</a></li>



                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>



                                        </ul>-->



									</div>



								</div>



							</div>



						</div>



					</div>



				</div>







				<?php include "includes/admin-footer.php"?>



				<input type="hidden" name="filter_col" id="filter_col" value="<?php echo $query; ?>"/>



			</div>



		</div>



	</div>



</div>







<div id="config-tool" class="closed" style="display:none;">



	<a id="config-tool-cog">



		<i class="fa fa-cog"></i>



	</a>







	<div id="config-tool-options">



		<h4>Layout Options</h4>



		<ul>



			<li>



				<div class="checkbox-nice">



					<input type="checkbox" id="config-fixed-header" checked />



					<label for="config-fixed-header">



						Fixed Header



					</label>



				</div>



			</li>



			<li>



				<div class="checkbox-nice">



					<input type="checkbox" id="config-fixed-sidebar" checked />



					<label for="config-fixed-sidebar">



						Fixed Left Menu



					</label>



				</div>



			</li>



			<li>



				<div class="checkbox-nice">



					<input type="checkbox" id="config-fixed-footer" checked />



					<label for="config-fixed-footer">



						Fixed Footer



					</label>



				</div>



			</li>



			<li>



				<div class="checkbox-nice">



					<input type="checkbox" id="config-boxed-layout" />



					<label for="config-boxed-layout">



						Boxed Layout



					</label>



				</div>



			</li>



			<li>



				<div class="checkbox-nice">



					<input type="checkbox" id="config-rtl-layout" />



					<label for="config-rtl-layout">



						Right-to-Left



					</label>



				</div>



			</li>



		</ul>



		<br/>



		<h4>Skin Color</h4>



		<ul id="skin-colors" class="clearfix">



			<li>



				<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">



				</a>



			</li>



			<li>



				<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">



				</a>



			</li>



			<li>



				<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">



				</a>



			</li>



			<li>



				<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">



				</a>



			</li>



			<li>



				<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">



				</a>



			</li>



			<li>



				<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">



				</a>



			</li>



			<li>



				<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">



				</a>



			</li>



			<li>



				<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">



				</a>



			</li>



		</ul>



	</div>



</div>



<!-- Trigger the modal with a button -->





<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Send To Mail</h4>

      </div>

      <div class="modal-body">

       <form action="<?php echo base_url(); ?>admin/sendshiftreport" id="sendmail" method="post">

                                                      

                                                <div class="row">

                                              <input type="hidden" name="query" value="<?php echo $queryData; ?>">

                                       

                                                 <div style="" class="form-group col-md-12">

                                                         <input type="email" required placeholder="Email id" name="email" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

                                                  <div style="" class="form-group col-md-12">

                                                         <textarea placeholder="Message" name="msg" class="form-control" autocomplete="off"></textarea>

                                                 

                                                </div>

                                                

                                               

                                                </div>

                                                       <div class="row">

                                             

                                                <button type="submit" id="send" class="btn btn-primary " href="javascript:void(0)">

                                                    <!--<i class="fa fa-search fa-lg"></i>--> Send Data

                                                </button>

                                                

                                               

                                                <span>&nbsp;</span>

                                                </div> 

                                                    

                                                </form>

      </div>

      <div class="modal-footer">

     

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>



<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>



<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>



<!-- global scripts -->



<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->







<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>



<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>



<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>







<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->







<!-- this page specific scripts -->



<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>







<!-- theme scripts -->



<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>



<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>







<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />



<!-- this page specific inline scripts -->



<script type="text/javascript">

$('.datata').dataTable({bFilter: false, bInfo: false,'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
    }]});

 $( function() {

    var availableTags = [

     <?php

     foreach($driverData as $val)

     {

         ?>

          "<?php echo $val['user_name']; ?>",

         <?php

     } ?>

     

     

    ];

    $( "#tags" ).autocomplete({

      source: availableTags

    });

     $( "#ur" ).autocomplete({

      source: availableTags

    });

    

     var availableTagss = [

     <?php

     foreach($carData as $val)

     {

         ?>

          "<?php echo $val['car_number']; ?>",

         <?php

     } ?>

     

     

    ];

    $( "#cars" ).autocomplete({

      source: availableTagss

    });

    

    

     var availableTagssssss = [

     <?php

     foreach($driverData as $val)

     {

         ?>

          "<?php echo $val['user_name']; ?>",

         <?php

     } ?>

     

     

    ];

    $( "#dname" ).autocomplete({

      source: availableTagssssss

    });

    

    



    

    

       var availableTagssss = [

     <?php

     foreach($clientData as $val)

     {

         ?>

          "<?php echo $val['ClientID']; ?>",

         <?php

     } ?>

     

     

    ];

    $( "#cid" ).autocomplete({

      source: availableTagssss

    });

  } );



$(function() {

  $('.date').datepicker({

  

      dateFormat:'dd/mm/yy',

    

  });

});

	$(document).ready(function() {



		//CHARTS



		function gd(year, day, month) {



			return new Date(year, month - 1, day).getTime();



		}



	});



</script>



<script type="text/javascript" language="javascript" >



	$(window).load(function() {



		$(".cover").fadeOut(2000);



	});



	



</script>



</body> 



</html>