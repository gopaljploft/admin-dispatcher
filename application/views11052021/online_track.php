

<!DOCTYPE html>

<html>

<head>

<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



<title>Live Tracking - Infinite Cab</title>



<!-- bootstrap -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



<!-- RTL support - for demo only -->

<script src="js/demo-rtl.js"></script>

<!--

If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

And add "rtl" class to <body> element - e.g. <body class="rtl">

-->



<!-- libraries -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



<!-- global styles -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



<!-- this page specific styles -->

<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



<!-- Favicon -->

<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



<!-- google font libraries -->

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



<!--[if lt IE 9]>

<script src="js/html5shiv.js"></script>

<script src="js/respond.min.js"></script>

<![endif]-->



<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}

table {
    border-color: #e7ebee;
    line-height: 40px;
}

tr.rampro {
    background-color: #f8a41b;
    color: white;
    line-height: 40px;
}



/*
.filter-block  input#carss  {
    height: 30px !important;
}*/

.filter-block.dri-del .form-group {
    height: 0;
}

</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

<?php

include"includes/admin_header.php";

?>

<div id="page-wrapper" class="container">

<div class="row">

<?php

include"includes/admin_sidebar.php";

?>

<div id="content-wrapper">

<div class="row" style="opacity: 1;">

<div class="col-lg-12">

<div class="row">

<div class="col-lg-12">

<div id="content-header" class="clearfix">

<div class="pull-left">

<h1>Live Tracking</h1>

</div>

<div class="pull-right">

<ol class="breadcrumb">

<li><a href="#">Home</a></li>

<li class="active"><span>Live Tracking</span></li>

</ol>

</div>

</div>

</div>

</div>



<div class="row">

<div class="col-lg-12">

<div class="main-box clearfix">

<div class="panel">

<div class="panel-body">

<ul class="nav nav-tabs">

<li class="active"><a data-toggle="tab" href="#home">Live Tracking</a></li>


</ul>



<div class="filter-block dri-del">

<form action="<?php echo base_url() ?>admin/online_track">


<div style="margin:0px !important;" class="form-group col-md-3">

<select name="status" style="" class="form-control">

<option value="0">Select All</option>

<option value="booked">ON Duty</option>

<option value="free">Off Duty</option>


</select>

</div>


<div style="margin:0px !important;" class="form-group col-md-3">

<div style="" class="form-group">

<input id="carss" type="text" value="<?php if($_GET['car']) { echo $_GET['car']; } ?>" placeholder="Cab Number" name ="car" class="form-control">


</div>

</div>


<div style="margin:0px !important;" class="form-group col-md-3">

<div style="" class="form-group">

<input id="carss" type="text" value="<?php if($_GET['did']) { echo $_GET['did']; } ?>" placeholder="Driver ID" name ="did" class="form-control">


</div>

</div>
<div class="col-md-3">
<a style="padding: 5px 15px;" href="<?php echo base_url() ?>admin/online_track" class="btn btn-primary">

<i class="fa fa-refresh fa-lg"></i> Reset

</a>


<button style="padding: 5px 15px;" type="submit" class="btn btn-primary" href="javascript:void(0)">

<i class="fa fa-search fa-lg"></i> Search

</button>
</div>

</form>

</div>

</div>


 
</div>


<style>

#mapCanvas {

width: 100%;

height: 600px;

margin: 0 auto;

}

#mapLegend {

background: #fdfdfd;

color: #3c4750;

padding: 0 10px 0 10px;

margin: 10px;

font-weight: bold;

filter: alpha(opacity=80);

opacity: 0.8;

border: 2px solid #000;

}

#mapLegend div {

height: 40px;

line-height: 25px;

font-size: 1.2em;

}

#mapLegend div img {

float: left;

margin-right: 10px;

}

#mapLegend h2 {

text-align: center

}

</style>

<div class="main-box-body clearfix">

<div class="tab-content">

<div id="home" class="tab-pane fade in active">

<div id="mapContainer">

<?php

//  print_r($query2);die;

if($query2[0]['longlatitude'])

{

?>

<div id="mapCanvas"></div>

<table align="center" id="ex" border="1" style="width:100%;text-align:center;margin-top: 41px;">

    <thead>
<tr class="rampro">
<th style="text-align:center;font-size: 12px;">Driver ID</th>
<th style="text-align:center;font-size: 12px;">Driver First Name</th>
<th style="text-align:center;font-size: 12px;">Driver Last Name</th>
<th style="text-align:center;font-size: 12px;">Driver Mobile No </th>

<th style="text-align:center;font-size: 12px;">Cab No</th>

<th style="text-align:center;font-size: 12px;">Driver Status</th>

<th style="text-align:center;font-size: 12px;">Current Zone </th>

<th style="text-align:center;font-size: 12px;">Zone Queue Position Time</th>

 <th style="text-align:center;font-size: 12px;">Lat </th>
  <th style="text-align:center;font-size: 12px;">Lng </th> 

</tr>    </thead>
        <tbody>                                     <?php

foreach ($query2 as $key => $value) {
?>

<tr>   
<td><?php echo $value['user_name'] ?></td>
<td><?php echo $value['first_name'] ?></td>
<td><?php echo $value['last_name'] ?></td>
<td><?php echo $value['phone'] ?></td>
<td><?php echo $value['car_number'] ?></td>
<td><?php echo ucwords($value['driver_booking_status']); ?></td>
<td>All India</td>
<td>N/A</td>
<td><?php echo $value['latitude'] ?></td>
<td><?php echo $value['longlatitude'] ?></td>

</tr>   

<?php } ?>

                                         </tbody></table>
<?php }else{ ?>

<div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=sydney+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe></div>

<?php } ?>

</div>

</div>

<div id="menu1" class="tab-pane fade">

<table class="table table-striped">

<thead>

<tr>

<th>Sr.No</th>

<th>Lat</th>

<th>lng</th>

<th>Date/Time</th>

</tr>

</thead>

<tbody>

<?php

foreach ($query5 as $key => $value) {

# code...



?>

<tr>

<td><?php echo $value['id'] ?></td>

<td><?php echo $value['lat'] ?></td>

<td><?php echo $value['lng'] ?></td>

<td><?php echo $value['date'].'-'.$value['time'] ?></td>

</tr>

<?php } ?>

</tbody>

</table>

</div>



</div>



<!-- jQuery CDN -->





</div>

</div>

</div>

</div>

</div>

</div>

<?php include "includes/admin-footer.php"?>

</div>

</div>

</div>

</div>



<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">



<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<!-- this page specific inline scripts -->





<!-- this page specific inline scripts -->

<script type="text/javascript">
$('#ex').DataTable();
$(function() {

$('.date').daterangepicker({

timePicker: true,

startDate: moment().startOf('hour'),

endDate: moment().startOf('hour').add(32, 'hour'),

locale: {

format: 'YYYY.MM.DD HH:mm'

}

});

});

$( function() {



var availableTagss = [

<?php

foreach($query4 as $val)

{

?>

"<?php echo $val['car_number']; ?>",

<?php

} ?>





];

$( "#carss" ).autocomplete({

source: availableTagss

});

} );



$(window).load(function() {

$(".cover").fadeOut(2000);

});

$(document).ready(function() {

//CHARTS

function gd(year, day, month) {

return new Date(year, month - 1, day).getTime();

}

});

</script>

<script type="text/javascript">

function initMap() {

var map;

var bounds = new google.maps.LatLngBounds();

var mapOptions = {

mapTypeId: 'roadmap'

};



// Display a map on the web page

map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);

map.setTilt(50);



// Multiple markers location, latitude, and longitude



var markers = [

<?php



foreach ($query2 as $key => $value) {



?>

['<?php echo $value['car_number'] ?>', <?php echo $value['latitude'] ?>, <?php echo $value['longlatitude'] ?>],

<?php } ?>

];



console.log(markers);         

// Info window content

var infoWindowContent = [

<?php

foreach ($query2 as $key => $value) {



?>

['<div class="info_content">' +

'<h3><?php echo $value['name'] ?>,<?php echo $value['car_no'] ?></h3>' +

'<p><strong>Driver ID : <?php echo $value['user_name'] ?></strong></p>'+'<p><strong>Driver First Name : <?php echo $value['first_name'] ?></strong></p>'+' <p><strong>Driver Last Name : <?php echo $value['last_name'] ?></strong></p>'+' <p><strong>Driver Mobile No : <?php echo $value['phone'] ?></strong></p>'+' <p><strong>Cab No : <?php echo $value['car_number'] ?></strong></p>'+' <p><strong>Driver Status  : <?php echo ucwords($value['driver_booking_status']); ?></strong></p>'+' <p><strong>Current Zone  : All India</strong></p>'+' <p><strong>Zone Queue Position Time   :N/A</strong></p>'+' <p><strong>Lat : <?php echo $value['latitude'] ?></strong></p>'+' <p><strong>Lng : <?php echo $value['longlatitude'] ?></strong></p> '+ '</div>'],

<?php } ?>

];



// Add multiple markers to map

var infoWindow = new google.maps.InfoWindow(), marker, i;



// Place each marker on the map  

for( i = 0; i < markers.length; i++ ) {

console.log(markers[0][1]);

var position = new google.maps.LatLng(markers[i][1], markers[i][2]);

bounds.extend(position);

marker = new google.maps.Marker({

position: position,

map: map,

title: markers[i][0],

icon:"http://admin.infinitecabs.com.au/upload/car.png"

});



// Add info window to marker    

google.maps.event.addListener(marker, 'click', (function(marker, i) {

return function() {

infoWindow.setContent(infoWindowContent[i][0]);

infoWindow.open(map, marker);

}

})(marker, i));



// Center the map to fit all markers on the screen

map.fitBounds(bounds);

}



// Set zoom level

var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {

this.setZoom(12);

google.maps.event.removeListener(boundsListener);

});



}

// Load initialize function

//google.maps.event.addDomListener(window, 'load', initMap);



</script> 



<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">

</script>

<!--  <script>-->

<!--    function validate() {-->

<!--      var x = document.forms["add_user"]["cartype"].value;-->

<!--      var car_rate = document.forms["add_user"]["carrate"].value;-->

<!--      var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->

<!--      var filename=document.getElementById('uploadImageFile').value;-->

<!--      var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--      var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--      //alert(extension);-->

<!--     if(image=='')-->

<!--      {-->

<!--        alert('car Image must be filled out');-->

<!--        return false;-->

<!--      }-->

<!--//     else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->

<!--//        return true;-->

<!--//      }-->

<!--//-->

<!--//     else-->

<!--//     {-->

<!--//       alert('Not Allowed Extension!');-->

<!--//       return false;-->

<!--//     }-->

<!--    else if (x == null || x == "") {-->

<!--        alert("Car Type must be filled out");-->

<!--        return false;-->

<!--      }-->

<!--       else if (car_rate == null || car_rate == "") {-->

<!--       alert("car rate must be filled out");-->

<!--       return false;-->

<!--      }-->

<!--     else if (seating_capacity == null || seating_capacity == "") {-->

<!--       alert("seating capacity must be filled out");-->

<!--       return false;-->

<!--     }-->

<!---->

<!--    }-->

<!--  </script>-->

</body>

</html>