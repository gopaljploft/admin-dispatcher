

<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Add New Cab - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>

    <script src="js/html5shiv.js"></script>

    <script src="js/respond.min.js"></script>

    <![endif]-->



    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/admin_header.php";

    ?>

    <div id="page-wrapper" class="container">

        <div class="row">

            <?php

            include"includes/admin_sidebar.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Cabs</h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Add New Cab </span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                            <h2>Add New Cab</h2>

                                        </div>

                                    </div>

                                      <?php if($this->session->flashdata('error_msg')) {
                                            ?>
                                             <div class="alert alert-success">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                <?php
                                            echo $this->session->flashdata('error_msg');
                                            ?>
                                        </div>
                                            <?php

                                            }

                                        ?>


                                    <div class="main-box-body clearfix">

                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->

                                        <?php echo form_open_multipart('admin/insert_cars',array('id' => 'formAddUser','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>

                                      
                                     

                                        

                                        

                                         

                                     

                                        <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">CID</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" required  placeholder="Enter CID" name="cid" id="inputname" class="form-control" required>

                                                </div>

                                            </div>

                                     

                                     

                                        <div class="form-group">

                                        <label class="col-lg-2 control-label" for="carrate">Operator ID</label>

                                        <div id="cartype1" class="col-lg-10">

                                       

                                        <select name="opt" class="form-control"  required id="select3">

                                        <option value="">Select Operator</option>

                                        <?php foreach ($operators as $value) { ?>

                                        <option value="<?php echo $value['ClientID'] ?>"><?php echo $value['ClientID'] ?></option>

                                        <?php 

                                        }

                                        ?>

                                        </select>

                                        </div>

                                        </div>    

                                            

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">Cab VIN</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" required  placeholder="Enter Cab VIN" name="sid" id="inputname" class="form-control" required>

                                                </div>

                                            </div>

                                        

                                    

                                           <div class="form-group">

                                        <label class="col-lg-2 control-label" for="carrate">Cab Model</label>

                                        <div id="cartype1" class="col-lg-10">

                                      

                                        <select name="car_model" class="form-control"  required id="select2">

                                        

                                        <?php foreach ($allCarType1 as $value) { ?>

                                        <option <?php if($value['cartype']== $value['cartype']) { echo 'selected'; } ?> value="<?php echo $value['cartype'] ?>"><?php echo $value['cartype'] ?></option>

                                        <?php 

                                        }

                                        ?>

                                        </select>

                                        </div>

                                        </div>

                                              <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Cab Make</label>

                                                <div id="make" class="col-lg-10">

                                                    <input type="text" required placeholder="Cab Make" name="care_make" id="make" class="form-control" required>

                                                </div>

                                            </div>

                                         

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Cab Number</label>

                                                <div id="carmodel" class="col-lg-10">

                                                    <input type="text" required placeholder="Cab Number" name="car_number" id="carmodel" class="form-control" required>

                                                </div>

                                            </div>

                                            

                                            

                                       <div class="form-group">

                                        <label class="col-lg-2 control-label" for="carrate">Cab Type</label>

                                        <div id="cartype1" class="col-lg-10">

                                      

                                        <select name="cabtt" class="form-control"  required id="select2">

                                        

                                        <?php foreach ($cabTypes as $value) { ?>

                                        <option value="<?php echo $value['type'] ?>"><?php echo $value['type'] ?></option>

                                        <?php 

                                        }

                                        ?>

                                        </select>

                                        </div>

                                        </div>

                                        <div class="form-group">

                                        <label class="col-lg-2 control-label" for="carrate">Cab Categories/Seating Capacity</label>

                                        <div id="cartype1" class="col-lg-10">

                                      

                                        <select name="car_type_id" class="form-control"  required id="select2">

                                        

                                        <?php foreach ($allCarType as $value) { ?>

                                        <option <?php if($value['cartype']=='First Available') { echo 'selected'; } ?> value="<?php echo $value['cab_id'] ?>"><?php echo $value['cartype'] ?>(1-<?php echo $value['seat_capacity']; ?>)</option>

                                        <?php 

                                        }

                                        ?>

                                        </select>

                                        </div>

                                        </div>

                                        <div class="form-group">

                                        <label class="col-lg-2 control-label" for="carrate">Wheelchair Accessible</label>

                                        <div id="cartype1" class="col-lg-10">

                                       

                                        <select name="wheelchair" class="form-control"  required id="select3">

                                        <option value="yes">Yes</option>

                                        <option value="no">No</option>

                                        

                                        </select>

                                        </div>

                                        </div>  



                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Year Make</label>

                                                <div id="colour" class="col-lg-10">

                                                    <input type="text" placeholder="Year Make" required name="color" class="form-control dd" style="    width: 100%;">

                                                </div>

                                            </div>

                                            

                                             <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Meter S.No</label>

                                                <div id="colour" class="col-lg-10">

                                                    <input type="text"  placeholder="Meter S.No" name="mid" id="mid" class="form-control" >

                                                </div>

                                            </div>

                                            

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Tablet ID</label>

                                                <div id="colour" class="col-lg-10">

                                                    <input type="text"  placeholder="Tablet ID" name="tid" id="tid" class="form-control" >

                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Meter Id</label>

                                                <div id="colour" class="col-lg-10">

                                                    <input type="text"  placeholder="Meter Id" name="meterId" id="meterId" class="form-control" >

                                                </div>

                                            </div>

                                         

                                         <!-- <div class="form-group">

                                                <label class="col-lg-2 control-label" for="inputImgCar">Upload CTP </label>

                                                <div id="inputregistration" class="col-lg-10">

                                                    <input type="file"  name="ctp" id="inputregistration" class="form-control">

                                                </div>

                                            </div>



                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="inputImgCar">Upload Registration </label>

                                                <div id="inputregistration" class="col-lg-10">

                                                    <input type="file"  name="registarion_image" id="inputregistration" class="form-control">

                                                </div>

                                            </div>



                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="inputImgCar">Upload Insurance </label>

                                                <div id="insurance" class="col-lg-10">

                                                    <input type="file"  name="insurance_image" id="insurance" class="form-control">

                                                </div>

                                            </div>-->



                                            <div class="form-group">

                                                <div class="col-lg-offset-2 col-lg-1">

                                                    <button style="display:block;" class="btn btn-success" name="save" id="notification-trigger-bouncyflip" type="submit">

                                                        <span id="category_button" class="content">SUBMIT</span>

                                                    </button>

                                                </div>

                                            </div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php include "includes/admin-footer.php"?>

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- this page specific inline scripts -->

<script type="text/javascript">

$( function() {

    $( ".dd" ).datepicker({

            changeMonth: true,

            changeYear: true,

            yearRange:  '1964:2050',

            dateFormat: 'dd/mm/yy',
            maxDate: new Date()

            

        });

  } );

            

$("#select2").select2({



});

$("#select3").select2({



});



    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

</script>

<!--	<script>-->

<!--		function validate() {-->

<!--			var x = document.forms["add_user"]["cartype"].value;-->

<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->

<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->

<!--			var filename=document.getElementById('uploadImageFile').value;-->

<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			//alert(extension);-->

<!--		 if(image=='')-->

<!--			{-->

<!--				alert('car Image must be filled out');-->

<!--				return false;-->

<!--			}-->

<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->

<!--//				return true;-->

<!--//			}-->

<!--//-->

<!--//		 else-->

<!--//		 {-->

<!--//			 alert('Not Allowed Extension!');-->

<!--//			 return false;-->

<!--//		 }-->

<!--		else if (x == null || x == "") {-->

<!--				alert("Car Type must be filled out");-->

<!--				return false;-->

<!--			}-->

<!--			 else if (car_rate == null || car_rate == "") {-->

<!--			 alert("car rate must be filled out");-->

<!--			 return false;-->

<!--		 	}-->

<!--		 else if (seating_capacity == null || seating_capacity == "") {-->

<!--			 alert("seating capacity must be filled out");-->

<!--			 return false;-->

<!--		 }-->

<!---->

<!--		}-->

<!--	</script>-->

</body>

</html>