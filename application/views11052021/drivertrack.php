

  <?php

     $get_bookingLocation=array();

     $this->load->model('Model_admin','home');

     $get_bookingLocation = $this->home->get_bookingLocation($_GET['id']);



?>

<!DOCTYPE html>

<html>

<head>

<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



<title>Tracking History - Infinite Cab</title>



<!-- bootstrap -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



<!-- RTL support - for demo only -->

<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

<!--

If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

And add "rtl" class to <body> element - e.g. <body class="rtl">

-->



<!-- libraries -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



<!-- global styles -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



<!-- this page specific styles -->

<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



<!-- Favicon -->

<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



<!-- google font libraries -->

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



<!--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>-->

<link href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" rel="stylesheet" type="text/css"/>

<!--[if lt IE 9]>

<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

<![endif]-->



<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>

<style>
  .dt-buttons {
    margin-top: 10px;
    margin-bottom: 10px;
}

    .myclass{

            margin-bottom: 0px !important;

    }



.row.proparttwo .form-group.col-md-4 {

    margin-bottom: 15px;

}



.form-group.two-sec.col-md-4 { 

    margin-top: -20px !important; 

    padding-bottom: 15px !important; 

}

</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

<?php

include"includes/admin_header.php";

?>

<div id="page-wrapper" class="container">

<div class="row">

    <?php

    include"includes/admin_sidebar.php";

    ?>

    <div id="content-wrapper">

        <div class="row" style="opacity: 1;">

            <div class="col-lg-12">

                <div class="row">

                    <div class="col-lg-12">

                        <div id="content-header" class="clearfix">

                            <div class="pull-left">

                                <h1>Tracking History</h1>

                            </div>

                            <div class="pull-right">

                                <ol class="breadcrumb">

                                    <li><a href="#">Home</a></li>

                                    <li class="active"><span>Tracking History</span></li>

                                </ol>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-12">

                        <div class="main-box clearfix">

                            <div class="panel" style="margin-bottom:0px;">

                                <div class="panel-body">

                                         <form action="<?php echo base_url() ?>admin/driverTrack">

                                                      

                                                <div class="row proparttwo">

                                              

                                               

                                                

                                                 <div style="" class="form-group col-md-4">

                                                         <input id="tags" type="text" value="<?php if($_GET['username']) { echo $_GET['username']; } ?>" placeholder="Driver ID" name ="username" class="form-control">

                                                 

                                                </div>

                                                 <div style="" class="form-group col-md-4">

                                                         <input id="cars" type="text" value="<?php if($_GET['car']) { echo $_GET['car']; } ?>" placeholder="Cab No" name ="car" class="form-control">

                                                 

                                                </div>

                                                

                                                <div style="" class="form-group col-md-4">

                                                         <input id="id" type="text" value="<?php if($_GET['id']) { echo $_GET['id']; } ?>" placeholder="Trip ID" name ="id" class="form-control">

                                                 

                                                </div>

                                                

                                                 <div style="" class="form-group col-md-4">

                                                         <input id="id" type="text" value="<?php if($_GET['tid']) { echo $_GET['tid']; } ?>" placeholder="Transaction ID" name ="tid" class="form-control">

                                                 

                                                </div>

                                                

                                                

                                                

                                                <div style="" class="form-group col-md-4">

                                                    <input type="text" autocomplete="off" value="<?php if($_GET['sdate']) { echo $_GET['sdate']; } ?>" placeholder="Start Date" name ="sdate" class="form-control date">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div>

                                                

                                                 <div style="" class="form-group col-md-4">

                                                    <input type="text" autocomplete="off" value="<?php if($_GET['edate']) { echo $_GET['edate']; } ?>" placeholder="End Date" name ="edate" class="form-control date">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div>



                                                 <div style="" class="form-group two-sec col-md-4">

                                                         <input id="id" type="text" value="<?php if($_GET['sid']) { echo $_GET['sid']; } ?>" placeholder="Shift ID" name ="sid" class="form-control">

                                                 

                                                </div>     

                                                

                                               

                                                </div> 

                                                       <div class="row resh">

                                                <a href="<?php echo base_url() ?>admin/driverTrack" class="btn btn-primary ">

                                                    <i class="fa fa-refresh fa-lg"></i> Reset 

                                                </a>

                                                

                                                

                                                <button type="submit" class="btn btn-primary " href="javascript:void(0)">

                                                    <i class="fa fa-search fa-lg"></i> Search

                                                </button>

                                                <!-- <button class="dt-button buttons-csv buttons-html5 btn btn-primary myclass sec-part" tabindex="0" aria-controls="DataTables_Table_1" type="button"><span>Export</span></button> -->

                                                

                                                 <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary " href="javascript:void(0)">

                                                    <!--<i class="fa fa-envelope fa-lg"></i>--> Send To Mail

                                                </button> 

                                                <span>&nbsp;</span>  

                                                </div> 

                                                    

                                                </form>  

                                    

                                </div>

                            </div>

                            

<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Send Data</h4>

      </div>

      <div class="modal-body">

       <form action="<?php echo base_url() ?>admin/sendmail" id="sendmail" method="post">

                                                      

                                                <div class="row"> 

                                              

                                                <div style="" class="form-group col-md-12">

                                                    <input type="text" autocomplete="off" value="<?php if($_GET['sdate']) { echo $_GET['sdate']; } ?>" placeholder="Time Range" name ="sdate" class="form-control date">

                                                    <!-- <i class="fa fa-clock search-icon"></i> -->

                                                </div>

                                                

                                               

                                                

                                                 <div style="" class="form-group col-md-12">

                                                         <input id="ur" type="text" placeholder="Driver ID" name ="driver" class="form-control">

                                                 

                                                </div>

                                               <!--  <span style="margin-left: 45%;font-size: 30px;">OR</span> -->

                                                

                                                 <div style="" class="form-group col-md-12">

                                                         <input id="ur" type="text" placeholder="Trip ID" name ="bid" class="form-control">

                                                 

                                                </div>

                                                 <div style="" class="form-group col-md-12">

                                                         <input  type="email"  placeholder="Email id" name ="email" class="form-control">

                                                 

                                                </div>

                                                

                                                  <div style="" class="form-group col-md-12">

                                                         <textarea   placeholder="Massage" name ="msg" class="form-control"></textarea>

                                                 

                                                </div>

                                                

                                               

                                                </div>

                                                       <div class="row">

                                             

                                                <button type="button" id="send" class="btn btn-primary " href="javascript:void(0)">

                                                    <!--<i class="fa fa-search fa-lg"></i>--> Send Data

                                                </button>

                                                  

                                               

                                                <span>&nbsp;</span>

                                                </div> 

                                                    

                                                </form>

                                    

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>

                            <div class="main-box-body clearfix">

                    

                                            <ul class="nav nav-tabs">

   <!-- <li class="active"><a  data-toggle="tab" href="#booking">Booking History</a></li>-->

   <!-- <li><a data-toggle="tab" href="#payment">Payment History</a></li>

    <li><a data-toggle="tab" href="#cur">Current Booking</a></li>-->

    <li class="active"><a data-toggle="tab" href="#time">Time Log</a></li>

    <li ><a data-toggle="tab" href="#menu1">Route in Lat, Lng</a></li>

    <?php if($_GET['id']) { ?>

    <li ><a data-toggle="tab" href="#home">History On Map</a></li>

   <?php } ?>

  </ul>





  <div class="tab-content">

       <?php if($_GET['id']) { ?>

    <div id="home" class="tab-pane fade">

    

       <div class="form-group">

        <div id="dvMap" class="col-lg-7" style="height: 500px;width: 100%;"></div>

        

        <!--<div id="dvDistance"></div>-->

    </div>

    </div>

    <?php } ?>

   

    <div id="menu1" class="tab-pane fade">

       <table class="table table-striped example">

    <thead>

      <tr>

        

        <th>Driver ID</th>

        <th>Driver Name</th>
        <th>Cab No</th>

        <th>Lat</th>

        <th>Lng</th>

        <th>Date & Time</th>

      </tr>

    </thead>

    <tbody>

         <?php

      foreach ($get_bookingLocations as $key => $value) {

        # code...

    

    ?>

      <tr>

        <td><?php echo $value['user_name'] ?></td>

        <td><?php echo $value['name'] ?></td>

        <td><?php echo $value['car_number'] ?></td>

        <td><?php echo $value['lat'] ?></td>

        <td><?php echo $value['lng'] ?></td>

        <td><?php $dateData=explode(" ",$value['date']); echo date('d/m/Y', strtotime($dateData['0'])).'-'.$value['time'] ?></td>

      </tr>

    <?php } ?>

    </tbody>

  </table>

    </div>

    

     <div id="time" class="tab-pane fade in active">

        

            <table id="" class="table table-hover table-bordered user-list example">

                                                <thead>

                                                <tr>

                                                    <th>Sr.No</th>

                                                    <th><a href="javascript:void(0);">Shift ID</a></th>

                                                    <th>Driver ID</th>

                                                    <th><a href="javascript:void(0);">Driver Name</a></th>

                                                    <th><a href="javascript:void(0);">Cab No</a></th>

                                                    <th><a href="javascript:void(0);">Login Date & Time</a></th>

                                                     <th><a href="javascript:void(0);">Log Out Date & Time</a></th>

                                                      <th><a href="javascript:void(0);">Hours Worked</a></th>

                                                       <th><a href="javascript:void(0);">Total Trips</a></th>

                                                   

                                                </tr>

                                                </thead>

                                                <tbody>

                                                    <?php

                                                    $i=1;

                                                   // print_r($loginHistory);die;

                                                    foreach ($loginHistory as $key => $value) {

                                                      

                                                    $countTotalTrip=$this->db->query("SELECT * FROM bookingdetails WHERE shiftId=".$value['id']."")->result_array();


                                                    $lastLogotTime=$this->db->query("SELECT * FROM driverLoginsLog WHERE driverId=".$value['driverId']." and status='logout' order by id desc")->row_array();
                                                     $firsloginTime=$this->db->query("SELECT * FROM driverLoginsLog WHERE driverId=".$value['driverId']." and status='login' order by id desc")->row_array();

                                                    ?>

                              

                                                    <tr>

                                                        <td><?php echo $i; ?></td>

                                                        <td><?php echo $value['id']; ?></td>

                                                        <td><?php echo $value['user_name'] ?></td>

                                                        <td><?php echo $value['name']; ?></td>

                                                        <td><?php echo $value['car_number']; ?></td>

                                                        <td><?php  echo date("d/m/Y H:i:s", strtotime($value['loginAt']));  ?></td>

                                                       <td><?php if($value['logoutAt']) {  echo date("d/m/Y H:i:s", strtotime($value['logoutAt'])); } else{ echo 'N/A'; };  ?></td>

                                                          <td>00:00</td>

                                                           <td><?php echo count($countTotalTrip); ?></td>

                                                           

                                                       

                                                        

                                                    </tr>

                                                    <?php $i++; } ?>

                                                </tbody>

                                            </table>

    </div>

    

    <div id="booking" class="tab-pane fade">

       <table id="" class="table table-hover table-bordered user-list">

                                                <thead>

                                                <tr>

                                                    <th>Sr.No</th>

                                                    <th><a href="javascript:void(0);">Passenger Name</a></th>

                                                    <th><a href="javascript:void(0);">Passenger ID</a></th>

                                                    <th><a href="javascript:void(0);">Booking ID</a></th>

                                                    <th><a href="javascript:void(0);">Taxi Type</a></th>

                                                      <th><a href="javascript:void(0);">Cab Number</a></th>

                                                    <th class="text-center"><a href="javascript:void(0);">From</a></th>

                                                    <th class="text-center"><a href="javascript:void(0);">To</a></th>

                                                    <th class="text-center"><a href="#" class="desc">Date</a></th>

                                                    <th class="text-center">Status</th>

                                                     <th class="text-center">Job Type</th>

                                               

                                                </tr>

                                                </thead>

                                                <tbody>

                                                    <?php

                                                    $i=1;//print_r($booking);

                                                    foreach ($bookings as $key => $value) {

                                                      

                                                    

                                                    ?>

                              

                                                    <tr>

                                                        <td><?php echo $i; ?></td>

                                                        <td><?php echo $value['username']; ?></td>

                                                        <td><?php echo $value['user_id']; ?></td>

                                                        <td><?php echo $value['id']; ?></td>

                                                        <td><?php echo $value['taxi_type']; ?></td>

                                                        <td><?php echo $value['car_number']; ?></td>

                                                        <td><?php echo $value['pick_address']; ?></td>

                                                        <td><?php echo $value['drop_address']; ?></td>

                                                        <td><?php echo $value['bookingdateandtime']; ?></td>

                                                        <td><?php echo $value['status_code']; ?></td>

                                                        <td><?php echo $value['isdevice']; ?></td>

                                                       

                                                        

                                                    </tr>

                                                    <?php $i++; } ?>

                                                </tbody>

                                            </table>

                                        </div>

    

                                <div id="payment" class="tab-pane fade">

                                   <table id="" class="table table-hover table-bordered user-list">

                                                <thead>

                                                <tr>

                                                    <th>Sr.No</th>

                                                    <th><a href="javascript:void(0);">Transaction ID</a></th>

                                                   

                                                    <th><a href="javascript:void(0);">Booking ID</a></th>

                                                    <th><a href="javascript:void(0);">Driver ID</a></th>

                                                       <th><a href="javascript:void(0);">Cab Number</a></th>

                                                    

                                                   

                                                    <th><a href="javascript:void(0);">Amount</a></th>

                                                     <th><a href="javascript:void(0);">Payment Type</a></th>

                                                   

                                                    <th class="text-center"><a href="#" class="desc">Date</a></th>

                                                    <th class="text-center">Status</th>

                                                </tr>

                                                </thead>

                                                <tbody>

                                                    <?php

                                                    $i=1;

                                                    foreach ($payments as $key => $value) {

                                                      

                                                    if($value['payment_type']!='Cash')

                                                    {

                                                    ?>

                              

                                                    <tr>

                                                        <td><?php echo $i; ?></td>

                                                        <td>#000<?php echo $value['bookingId']; ?></td>

                                                        

                                                        <td><?php echo $value['id']; ?></td>

                                                        <td><?php echo $value['name']; ?></td>

                                                         <td><?php echo $value['car_number']; ?></td>

                                                        

                                                        

                                                      

                                                        <td>$<?php echo $value['amount']; ?></td>

                                                         <td><?php echo $value['paymentMode']; ?></td>

                                                        

                                                         <td><?php $dateData=explode(" ",$value['date']); echo $dateData[0]; ?></td>

                                                        <td>Done</td>

                                                         <?php } ?>

                                                    </tr>

                                                    <?php $i++; } ?>

                                                </tbody>

                                            </table>

    </div>

  <div id="cur" class="tab-pane fade">

       <table id="" class="table table-hover table-bordered user-list">

                                                <thead>

                                                <tr>

                                                    <th>Sr.No</th>

                                                    <th><a href="javascript:void(0);">Passenger Name</a></th>

                                                    <th><a href="javascript:void(0);">Passenger ID</a></th>

                                                    <th><a href="javascript:void(0);">Booking ID</a></th>

                                                    <th><a href="javascript:void(0);">Taxi Type</a></th>

                                                    <th class="text-center"><a href="javascript:void(0);">From</a></th>

                                                    <th class="text-center"><a href="javascript:void(0);">To</a></th>

                                                    <th class="text-center"><a href="#" class="desc">Date</a></th>

                                                    <th class="text-center">Status</th>

                                                     <th class="text-center">Job TYpe</th>

                                               

                                                </tr>

                                                </thead>

                                                <tbody>

                                                    <?php

                                                    $i=1;//print_r($booking);

                                                    foreach ($bookings as $key => $value) {

                                                      

                                                        if($value['status_code']=='on-trip')

                                                        {

                                                    ?>

                              

                                                    <tr>

                                                        <td><?php echo $i; ?></td>

                                                        <td><?php echo $value['username']; ?></td>

                                                        <td><?php echo $value['user_id']; ?></td>

                                                        <td><?php echo $value['id']; ?></td>

                                                        <td><?php echo $value['taxi_type']; ?></td>

                                                        <td><?php echo $value['pick_address']; ?></td>

                                                        <td><?php echo $value['drop_address']; ?></td>

                                                          <td><?php $dateData=explode(" ",$value['bookingdateandtime']); echo $dateData[0]; ?></td>

                                                       

                                                        <td><?php echo $value['status_code']; ?></td>

                                                        <td><?php echo $value['isdevice']; ?></td>

                                                       

                                                        

                                                    </tr>

                                                    <?php } ?>

                                                    <?php $i++; } ?>

                                                </tbody>

                                            </table>

                                        </div>

  </div>

                          

                                 

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



        <footer class="row" id="footer-bar" style="opacity: 1;">

            <p id="footer-copyright" class="col-xs-12">

                Powered by Infinite Cab.

            </p>

        </footer>

    </div>

</div>

</div>

</div>



<div id="config-tool" class="closed" style="display:none;">

<a id="config-tool-cog">

<i class="fa fa-cog"></i>

</a>



<div id="config-tool-options">

<h4>Layout Options</h4>

<ul>

    <li>

        <div class="checkbox-nice">

            <input type="checkbox" id="config-fixed-header" checked />

            <label for="config-fixed-header">

                Fixed Header

            </label>

        </div>

    </li>

    <li>

        <div class="checkbox-nice">

            <input type="checkbox" id="config-fixed-sidebar" checked />

            <label for="config-fixed-sidebar">

                Fixed Left Menu

            </label>

        </div>

    </li>

    <li>

        <div class="checkbox-nice">

            <input type="checkbox" id="config-fixed-footer" checked />

            <label for="config-fixed-footer">

                Fixed Footer

            </label>

        </div>

    </li>

    <li>

        <div class="checkbox-nice">

            <input type="checkbox" id="config-boxed-layout" />

            <label for="config-boxed-layout">

                Boxed Layout

            </label>

        </div>

    </li>

    <li>

        <div class="checkbox-nice">

            <input type="checkbox" id="config-rtl-layout" />

            <label for="config-rtl-layout">

                Right-to-Left

            </label>

        </div>

    </li>

</ul>

<br/>

<h4>Skin Color</h4>

<ul id="skin-colors" class="clearfix">

    <li>

        <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

        </a>

    </li>

    <li>

        <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

        </a>

    </li>

    <li>

        <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

        </a>

    </li>

    <li>

        <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

        </a>

    </li>

    <li>

        <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

        </a>

    </li>

    <li>

        <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

        </a>

    </li>

    <li>

        <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

        </a>

    </li>

    <li>

        <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

        </a>

    </li>

</ul>

</div>

</div>

<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>



<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">



<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<!-- this page specific inline scripts -->

<script type="text/javascript">

   $('.table').DataTable( {
        bFilter: false, bInfo: false,
        dom: 'Bfrtip',
       
       buttons: [

        { extend: 'csv', text: 'Export',className: 'btn btn-primary myclass sec-part <?php if(empty($get_bookingLocations)) { ?> disabled <?php } ?>' }

    ] 

    } );

$('#send').click(function(){

    $.ajax({

                type: "POST",

                url: "<?php echo base_url(); ?>admin/sendmail",

                data: $('#sendmail').serialize(),

                success: function (result) {

                  alert("Mail request is submited successfully! it will be take some time.");

                  location.reload();

                },

                async: false

            }); 

});

 $( function() {

    var availableTags = [

     <?php

     foreach($driverData as $val)

     {

         ?>

          "<?php echo $val['user_name']; ?>",

         <?php

     } ?>

     

     

    ];

    $( "#tags" ).autocomplete({

      source: availableTags

    });

     $( "#ur" ).autocomplete({

      source: availableTags

    });

    

     var availableTagss = [

     <?php 

     foreach($carData as $val)

     {

         ?>

          "<?php echo $val['car_number']; ?>",

         <?php

     } ?>

     

     

    ];

    $( "#cars" ).autocomplete({

      source: availableTagss

    });

  } );



$(function() {

  $('.date').datepicker({

   dateFormat: 'dd/mm/yy'

  });

});

$(document).ready(function() {

    

    

//CHARTS

function gd(year, day, month) {

    return new Date(year, month - 1, day).getTime();

}

$('.table-responsive').css('display','none');

});

</script>

<script type="text/javascript" language="javascript">

$(window).load(function() {

$(".cover").fadeOut(2000);

});

$(document).ready(function() {

     // get selected car type data

     if($("#select-car option:selected").val()!=''){

        $('#car-calculate').show();

        $.ajax({

            type: "POST",

            url: "<?php echo base_url(); ?>admin/get_cartype_data",

            data: {cab_id:$("#select-car option:selected").val()},

            success: function (result) {

            var json_arr=JSON.parse(result);

            console.log(json_arr);

            $('#car-calculate').html('<table class="cartype-details" align="center" border="1" style="width:100%;"><tr><td>Car Type:</td><td>'+$("#select-car option:selected").text()+'</td></tr><tr><td>First 5 km:</td><td>'+json_arr.car_rate+'$ /km</td></tr><tr><td>After 5 km:</td><td>'+json_arr.fromintailrate+'$ /km</td></tr><tr><td>Per Minute</td><td>'+json_arr.ride_time_rate+' $/min</td></tr><tr><td>Approx Cost:</td><td id="approx-cost"><span></span>$'+json_arr.car_rate+'</td></tr></table>');

            }

        });

}

    // submit form on submit

$('#formAddBooking').submit(function(){

    $('#example tr').each(function() {

            if($(this).hasClass('selected'))

            {

                var sel_id=$(this).find('td:nth-child(2)').html();

                if(sel_id=='' || sel_id==null){

                    sel_id=null;

                }

            }

            else

            {

                sel_id=null; 

            } 

            if($('#select-car').val()!=''){

                var car_type=$("#select-car option:selected").text();

                var approx_amt=$('#approx-cost span').html();

            }

            else{

                alert('Please select atlease one car type');

                return false;

                var car_type=null;

            }

            $.ajax({

                type: "POST",

                url: "<?php echo base_url(); ?>admin/update_booking_data",

                data: {id:'<?php echo $query->id; ?>',data_id: sel_id,taxi_type: car_type,amount:approx_amt},

                success: function (result) {

                    if(result == 0){

                        $(".taxi").html('<p class="error">Error</p>');

                        setTimeout(function(){$(".taxi").hide(); }, 3000);

                    }

                    else{

                        location.reload();

                        //$(".taxi").html('<p class="success">Booking Details Saved Successfully</p>');

                        //setTimeout(function(){$(".taxi").hide(); }, 1500);

                    }

                },

                async: false

            });

    });

});



if($('#edit-driver-link').length>0)

{

    $('.table-responsive').css('display','none');

}

$('#edit-driver-link').click(function(e)

{

    e.preventDefault();

    $( ".table-responsive" ).toggle();

});

var dataTable = $('#example').DataTable({

    "processing": true,

    "serverSide": true,

    "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],

    "columnDefs": [

        {

        orderable: false,

        className: 'select-checkbox',

        targets:   0

        },

        {

            "targets": [ 1 ],

            "visible": true,

            "searchable": true,

            "width": "10%",

            "sortable" :true

        },

        {

            "targets": [ 2 ],

            "visible": true,

            "searchable": true,

            "sortable" :true

        },

        {

            "targets": [ 3 ],

            "visible": true,

            "searchable": true,

            "sortable" :true

        },

        {

            "targets": [ 4 ],

            "visible": true,

            "searchable": true,

            "width": "20%",

            "sortable" :true

        },

        {

            "targets": [ 5 ],

            "visible": true,

            "searchable": true,

            "width": "20%",

            "sortable" :true

        },

        {

            "targets": [ 6 ],

            "visible": true,

            "searchable": true,

            "width": "20%",

            "sortable" :true

        },

        {

            "targets": [ 7 ],

            "visible": true,

            "searchable": false,

            "width": "10%",

            "sortable" :false

        }

    ],

    select: {

        style:    'os',

        selector: 'td:first-child'

    },

    "ajax":{

        url : '<?php echo base_url(); ?>admin/get_select_driver_data', // json datasource

        type: "post",  // method  , by default get

        data: {booking_id:'<?php echo $query->id; ?>'},

        error: function(){  // error handling

            $(".booking-grid-error").html("");

            $("#example").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

            $("#booking-grid_processing").css("display","none");

        }

    }

});

});



$('#select-car').on('change',function(){

if($(this).val()!=''){

$.ajax({

        type: "POST",

        url: "<?php echo base_url(); ?>admin/get_cartype_data",

        data: {cab_id:$(this).val()},

        success: function (result) {

            GetRoute();

            $('#car-calculate').show();

            var json_arr=JSON.parse(result);

           $('#car-calculate').html('<table class="cartype-details" align="center" border="1" style="width:100%;"><tr><td>Car Type:</td><td>'+$("#select-car option:selected").text()+'</td></tr><tr><td>First 5 km:</td><td>'+json_arr.car_rate+'ريال /km</td></tr><tr><td>After 5 km:</td><td>'+json_arr.fromintailrate+'ريال /km</td></tr><tr><td>Per Minute</td><td>'+json_arr.ride_time_rate+'ريال /min</td></tr><tr><td>Approx Distance:</td><td id="approx-distance"></td></tr><tr><td>Approx Cost:</td><td id="approx-cost"><span></span>ريال</td></tr></table>');

        }

    });

}

else{

    $('#car-calculate').hide();

    alert('Please select atleast one car type');

}

});

</script>

<script async defer

    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">

    </script>

<script type="text/javascript">

  <?php if($_GET['id']) { ?>

  function initMap() {

        var map = new google.maps.Map(document.getElementById('dvMap'), {

          zoom: 15,

          center: {lat: 26.9059, lng: 75.7727},

          mapTypeId: 'terrain'

        });



        var flightPlanCoordinates = [

      <?php

      

      foreach ($get_bookingLocation as $key => $value) {

        # code...

    

    ?>

          {lat: <?php echo $value['lat'] ?>, lng: <?php echo $value['lng'] ?>},

      <?php } ?>

        ];

        var flightPath = new google.maps.Polyline({

          path: flightPlanCoordinates,

          geodesic: true,

          strokeColor: '#FF0000',

          strokeOpacity: 1.0,

          strokeWeight: 2

        });



        flightPath.setMap(map);

      }

      <?php } ?>

</script>

<script>

/*function initMap() {

var bounds = new google.maps.LatLngBounds;

var markersArray = [];



var origin1 = {lat: <?php echo $query->pickup_lat ?>, lng: <?php echo $query->pickup_long ?>};

var destinationA = {lat: <?php echo $query->drop_lat ?>, lng: <?php echo $query->drop_long ?>};



var destinationIcon = 'https://chart.googleapis.com/chart?' +

    'chst=d_map_pin_letter&chld=D|FF0000|000000';

var originIcon = 'https://chart.googleapis.com/chart?' +

    'chst=d_map_pin_letter&chld=O|FFFF00|000000';

var map = new google.maps.Map(document.getElementById('dvMap'), {

    center: {lat: 20.1868076, lng: 64.4295101},

    zoom: 10,

    scrollwheel: false,

    navigationControl: false,

    mapTypeControl: false,

    scaleControl: false,

    mapTypeId: google.maps.MapTypeId.ROADMAP

});

var geocoder = new google.maps.Geocoder;



var service = new google.maps.DistanceMatrixService;

service.getDistanceMatrix({

    origins: [origin1],

    destinations: [destinationA],

    travelMode: google.maps.TravelMode.DRIVING,

    unitSystem: google.maps.UnitSystem.METRIC,

    avoidHighways: false,

    avoidTolls: false

}, function(response, status) {

    if (status !== google.maps.DistanceMatrixStatus.OK) {

        alert('Error was: ' + status);

    } else {

        var originList = response.originAddresses;

        var destinationList = response.destinationAddresses;

        var outputDiv = document.getElementById('dvPanel');

        outputDiv.innerHTML = '';

        var infoWindow = new google.maps.InfoWindow({ maxWidth: 150 });

        deleteMarkers(markersArray);



        var showGeocodedAddressOnMap = function(asDestination) {

            var icon = asDestination ? destinationIcon : originIcon;

            return function(results, status) {

                if (status === google.maps.GeocoderStatus.OK) {

                    map.fitBounds(bounds.extend(results[0].geometry.location));

                    var marker = new google.maps.Marker({

                        position: results[0].geometry.location,

                        map:map,

                        icon: icon,

                        info: results[0].formatted_address

                    });



                    // Allow each marker to have an info window

                    google.maps.event.addListener( marker, 'click', function() {

                        infoWindow.setContent( this.info );

                        infoWindow.open( map, this );

                    });



                    markersArray.push(marker);

                } else {

                    alert('Geocode was not successful due to: ' + status);

                }

            };

        };



        for (var i = 0; i < originList.length; i++) {

            var results = response.rows[i].elements;

            geocoder.geocode({'address': originList[i]},

                showGeocodedAddressOnMap(false));

            for (var j = 0; j < results.length; j++) {

                geocoder.geocode({'address': destinationList[j]},

                    showGeocodedAddressOnMap(true));

            }

        }

    }

});

}



function deleteMarkers(markersArray) {

for (var i = 0; i < markersArray.length; i++) {

    markersArray[i].setMap(null);

}

markersArray = [];

}*/

</script>

<!--<script async defer

src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCr5WgfHn67qGhlT_qAZOBiU5zMXz67qhE&callback=initMap">

</script>-->

</body>

</html>    

 