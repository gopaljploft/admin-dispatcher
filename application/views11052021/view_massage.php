<!DOCTYPE html>

<html>

<head>

	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



	<title>view Message - Infinite Cab</title>



	<!-- bootstrap -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



	<!-- RTL support - for demo only -->

	<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

	<!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



	<!-- libraries -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



	<!-- global styles -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



	<!-- this page specific styles -->

	<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

	<link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">



	<!-- Favicon -->

	<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />



	<!-- google font libraries -->

	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



	<!--[if lt IE 9]>

	<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

	<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

	<![endif]-->



	<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

	<?php

	include"includes/admin_header.php";

	?>

	<div id="page-wrapper" class="container">

		<div class="row">

			<?php

			include"includes/admin_sidebar.php";

			?>

			<div id="content-wrapper">

				<div class="row" style="opacity: 1;">

					<div class="col-lg-12">

						<div class="row">

							<div class="col-lg-12">

								<div id="content-header" class="clearfix">

									<div class="pull-left">

										<h1>View Message</h1>

									</div>

									<div class="pull-right">

										<ol class="breadcrumb">

											<li><a href="#">Home</a></li>

											<li class="active"><span>View Message</span></li>

										</ol>

									</div>

								</div>

							</div>

						</div>

						<!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

						<div class="col-lg-12">

							<!-- Single Delete -->

							<div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-header">

											<i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>

										</div>

										<div class="modal-title">Are you sure you want to delete the selected user?</div>

										<div class="modal-body"></div>

										<div class="modal-footer">

											<button id="confirm-delete-button" onclick="delete_single_user_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>

											<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

											<input type="hidden" value="" id="bookedid" name="bookedid">

											<button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>

										</div>

									</div> <!-- / .modal-content -->

								</div> <!-- / .modal-dialog -->

							</div> <!-- / .modal -->

							<!-- / Single Delete -->

							<!-- Multipal Delete -->

							<div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-header">

											<i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>

										</div>

										<div class="modal-title">Are you sure you want to delete selected user?</div>

										<div class="modal-body"></div>

										<div class="modal-footer">

											<button onclick="delete_user()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>

											<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

											<button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>

										</div>

									</div> <!-- / .modal-content -->

								</div> <!-- / .modal-dialog -->

							</div> <!-- / .modal -->

							<!-- / Multipal Delete -->

						</div>

						<!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

						<div class="row">

							<div class="col-lg-12">

								<div class="main-box clearfix">

									<div class="panel">

										<div class="panel-body">

											<h2 class="pull-left">View Message </h2>

											<div class="filter-block pull-right">

												



												<span>&nbsp;</span>

												

											</div>

										</div>

									</div>

									<div class="main-box-body clearfix">

										<div class="table-responsive">

											<table id="example" class="table table-hover table-bordered user-list">

												<thead>

												<tr>

													<th>Sr.No</th>
                                                <th><a href="javascript:void(0);">Message Id</a></th>
                                                <th><a href="javascript:void(0);">Message</a></th>
                                                
                                                <th><a href="javascript:void(0);">Action</a></th>

													

												</tr>
												<tbody>
													<?php
													$i=1;
													$msg=$this->db->query("SELECT * FROM seduledMassags WHERE id=".$_GET['id']."")->row_array();
													foreach ($resultData as $key => $value) {
													
													
													?>
													<tr>
														<td><?php echo $i; ?></td>
														
														<td><?php echo $value['msgId']; ?></td>
														<td><?php echo substr($msg['massage'], 0, 10); ?></td>

													
														<td>
		
            <a  href="<?php echo base_url(); ?>admin/deleteMassage?id=<?php echo $value['id'] ?>" onclick="return confirm('Are you sure?')">
				<span class="fa-stack">
					<i class="fa fa-square fa-stack-2x"></i>
					<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
				</span>
			</a>
		
													
														
													</tr>
													<?php $i++; } ?>
												</tbody>
												</thead>

											</table>

										</div>


										<!--<ul class="pagination pull-right">

                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>

                                            <li><a href="javascript:void(0);">1</a></li>

                                            <li><a href="javascript:void(0);">2</a></li>

                                            <li><a href="javascript:void(0);">3</a></li>

                                            <li><a href="javascript:void(0);">4</a></li>

                                            <li><a href="javascript:void(0);">5</a></li>

                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>

                                        </ul>-->

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<?php include "includes/admin-footer.php"?>

				<input type="hidden" name="filter_col" id="filter_col" value="<?php echo $query; ?>"/>

			</div>

		</div>

	</div>

</div>



<div id="config-tool" class="closed" style="display:none;">

	<a id="config-tool-cog">

		<i class="fa fa-cog"></i>

	</a>



	<div id="config-tool-options">

		<h4>Layout Options</h4>

		<ul>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-fixed-header" checked />

					<label for="config-fixed-header">

						Fixed Header

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-fixed-sidebar" checked />

					<label for="config-fixed-sidebar">

						Fixed Left Menu

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-fixed-footer" checked />

					<label for="config-fixed-footer">

						Fixed Footer

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-boxed-layout" />

					<label for="config-boxed-layout">

						Boxed Layout

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-rtl-layout" />

					<label for="config-rtl-layout">

						Right-to-Left

					</label>

				</div>

			</li>

		</ul>

		<br/>

		<h4>Skin Color</h4>

		<ul id="skin-colors" class="clearfix">

			<li>

				<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

				</a>

			</li>

			<li>

				<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

				</a>

			</li>

		</ul>

	</div>

</div>

<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Car Fare</h4>
      </div>
      <div class="modal-body">
        <form>
        	<div class="row">
        		<div class="form-group">
        			<table class="table table-hover">
        				<tr>
        					<th>Type</th>
        					<th>Amount</th>
        				</tr>
        				<tr>
        					<td>
        						Par KM
        					</td>
        					<td>
        						<input class="form-control" type="number" placeholder="Amount" name="amount[]">
        					</td>
        				</tr>

        				<tr>
        					<td>
        						Night Charges
        					</td>
        					<td>
        						<input class="form-control" type="number" placeholder="Amount" name="amount[]">
        					</td>
        				</tr>

        				<tr>
        					<td>
        						Day Charges
        					</td>
        					<td>
        						<input class="form-control" type="number" placeholder="Amount" name="amount[]">
        					</td>
        				</tr>
        			</table>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>

<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- this page specific inline scripts -->

<script type="text/javascript">
    $('.status').change(function(){
        
        var id=$(this).attr('id');
        var status=$(this).val();
        $.ajax({
        url: "<?php echo base_url() ?>admin/update_status_promo",
        type: "post",
        data: {id:id,status:status} ,
        success: function (response) {
            alert("Status Updated Successfully");
            location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });        
    });
	$(document).ready(function() {

		//CHARTS

		function gd(year, day, month) {

			return new Date(year, month - 1, day).getTime();

		}

	});

</script>

<script type="text/javascript" language="javascript" >

	$(window).load(function() {

		$(".cover").fadeOut(2000);

	});

	

</script>

</body>

</html>