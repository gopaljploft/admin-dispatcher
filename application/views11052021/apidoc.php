<style>
  /*    
    Side Navigation Menu V2, RWD
    ===================
    License:
    https://goo.gl/EaUPrt
    ===================
    Author: @PableraShow

 */

  @charset "UTF-8";
  @import url(https://fonts.googleapis.com/css?family=Open+Sans:300,400,700);

  body {
    font-family: 'Open Sans', sans-serif;
    font-weight: 300;
    line-height: 1.42em;
    color: #A7A1AE;
    background-color: #fff;
  }

  h1 {
    font-size: 3em;
    font-weight: 300;
    line-height: 1em;
    text-align: center;
    color: #4DC3FA;
  }

  h2 {
    font-size: 1em;
    font-weight: 300;
    text-align: center;
    display: block;
    line-height: 1em;
    padding-bottom: 2em;
    color: #FB667A;
  }

  h2 a {
    font-weight: 700;
    text-transform: uppercase;
    color: #FB667A;
    text-decoration: none;
  }

  .blue {
    color: #185875;
  }

  .yellow {
    color: #FFF842;
  }

  .container th h1 {
    font-weight: bold;
    font-size: 1em;
    text-align: left;
    color: #185875;
  }

  .container td {
    font-weight: normal;
    font-size: 1em;
    -webkit-box-shadow: 0 2px 2px -2px #0E1119;
    -moz-box-shadow: 0 2px 2px -2px #0E1119;
    box-shadow: 0 2px 2px -2px #0E1119;
  }

  .container {
    text-align: left;
    overflow: hidden;
    width: 90%;
    margin: 0 auto;
    display: table;
    padding: 0 0 8em 0;
  }

  .container td,
  .container th {
    padding-bottom: 2%;
    padding-top: 2%;
    padding-left: 2%;
  }

  /* Background-color of the odd rows */
  .container tr:nth-child(odd) {
    background-color: #323C50;
  }

  /* Background-color of the even rows */
  .container tr:nth-child(even) {
    background-color: #2C3446;
  }

  .container th {
    background-color: #1F2739;
  }

  .container td:first-child {
    color: #FB667A;
  }

  .container tr:hover {
    background-color: #464A52;
    -webkit-box-shadow: 0 6px 6px -6px #0E1119;
    -moz-box-shadow: 0 6px 6px -6px #0E1119;
    box-shadow: 0 6px 6px -6px #0E1119;
  }

  .container td:hover {
    background-color: #FFF842;
    color: #403E10;
    font-weight: bold;

    box-shadow: #7F7C21 -1px 1px, #7F7C21 -2px 2px, #7F7C21 -3px 3px, #7F7C21 -4px 4px, #7F7C21 -5px 5px, #7F7C21 -6px 6px;
    transform: translate3d(6px, -6px, 0);

    transition-delay: 0s;
    transition-duration: 0.4s;
    transition-property: all;
    transition-timing-function: line;
  }

  @media (max-width: 800px) {

    .container td:nth-child(4),
    .container th:nth-child(4) {
      display: none;
    }
  }
</style>
<h1>API Doc</h1>


<table class="container">
  <thead>
    <tr>
      <th>
        <h1>Screens</h1>
      </th>
      <th>
        <h1>Url</h1>
      </th>
      <th>
        <h1>Method</h1>
      </th>
      <th>
        <h1>Parameter</h1>
      </th>
      <th>
        <h1>Response</h1>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>2</d>
      <td>http://admin.infinitecabs.com.au/web_service/sign_up</td>
      <td>Post</td>
      <td>
        <pre>
        first_name:raju<br>
        last_name:sharma<br>
        email:raju123@mailinator.com<br>
        mobile:1236523580<br>
        password:123456<br>
        isdevice:ww<br>
         deviceToken:wdddddddw<br>
        address:de<br>
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "Successfully Signed up",
            "Mail": "mail Send Sucessfully",
            "data": [
                {
                    "id": "137",
                    "image": null,
                    "first_name": "raju",
                    "last_name": "sharma",
                    "name": null,
                    "username": null,
                    "mobile": "1236523580",
                    "email": "raju123@mailinator.com",
                    "gender": null,
                    "dob": null,
                    "address": "ww",
                    "password": "e10adc3949ba59abbe56e057f20f883e",
                    "pickupadd": null,
                    "user_status": "Active",
                    "wallet_amount": null,
                    "device_id": null,
                    "type": null,
                    "facebook_id": null,
                    "twitter_id": null,
                    "isdevice": null,
                    "device_token": null,
                    "flag": "no",
                    "is_deleted": "0"
                }
            ]
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>3</td>
     <td>http://admin.infinitecabs.com.au/web_service/login</td>
      <td>Post</td>
      <td>
        <pre>
      email:kamal@gmail.com
      password:123456
      deviceToken:ssssss
      </pre>
      </td>
      <td>
        <pre>
         {
          {
          "statusCode": "200",
          "message": "Successfully Logged in",
          "data": {
              "id": "119",
              "first_name": "kamal",
              "last_name": "sharama",
              "email": "kamal@gmail.com",
              "mobile": "7844741478",
              "device_id": "",
              "user_status": "Active"
          }
      }
      </pre>
      </td>
    </tr>
    <tr>
      <td>6</td>
     <td>http://admin.infinitecabs.com.au/web_service/serchUserTexi</td>
      <td>Post</td>
      <td>
        <pre>
      lat:-33<br>
      lng:151<br>
      </pre>
      </td>
      <td>
        <pre>
         {
    "statusCode": "200",
    "message": "success",
    "data": [
        {
            "driver_id": "2",
            "latitude": "-33.729752",
            "longlatitude": "150.836090",
            "distance": 82.565185958534,
            "cab_id": "1",
            "cartype": "Suv",
            "min_rate": "50.00",
            "max_rate": 60,
            "seat_capacity": "8"
        }
       
    ]
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>5</td>
      <td>https://stageofproject.com/texiapp-new/web_service/forgot_password</td>
      <td>Post</td>
      <td>
        <pre>
       email:kkkk147@gmail.com
      </pre>
      </td>
      <td>
        <pre>
        {
        "status": "success",
        "message": "Please check your email, including index."
       }
      </pre>
      </td>
    </tr>

    <tr>
      <td>6</td>
      <td>https://stageofproject.com/texiapp-new/web_service/getAllCab</td>
      <td>Post</td>
      <td>
        <pre>
       <!-- email:kkkk147@gmail.com -->
        lat:-33<br>
        lng:151<br>
      </pre>
      </td>
      <td>
        <pre>
               {
    "statusCode": "200",
    "message": "success",
    "data": [
        {
            "driver_id": "55",
            "latitude": "-33.737885",
            "longlatitude": "151.235260",
            "distance": 84.91303242851,
            "cabData": [
                {
                    "cab_id": "2",
                    "cartype": "Sedan",
                    "car_rate": "45.00",
                    "seat_capacity": "6"
                }
            ]
        },
        {
            "driver_id": "72",
            "latitude": "-33.729752",
            "longlatitude": "150.836090",
            "distance": 82.565185958534,
            "cabData": [
                {
                    "cab_id": "1",
                    "cartype": "Suv",
                    "car_rate": "50.00",
                    "seat_capacity": "8"
                }
            ]
        },
        {
            "driver_id": "78",
            "latitude": "-33.949448",
            "longlatitude": "151.008591",
            "distance": 105.58331840447,
            "cabData": [
                {
                    "cab_id": "3",
                    "cartype": "Hatchback ",
                    "car_rate": "40.00",
                    "seat_capacity": "8"
                }
            ]
        },
        {
            "driver_id": "59",
            "latitude": "-33.944489",
            "longlatitude": "150.854706",
            "distance": 105.88999860797,
            "cabData": [
                {
                    "cab_id": "1",
                    "cartype": "Suv",
                    "car_rate": "50.00",
                    "seat_capacity": "8"
                }
            ]
        },
        {
            "driver_id": "60",
            "latitude": "-33.812222",
            "longlatitude": "151.143707",
            "distance": 91.30036437715,
            "cabData": [
                {
                    "cab_id": "2",
                    "cartype": "Sedan",
                    "car_rate": "45.00",
                    "seat_capacity": "6"
                }
            ]
        },
        {
            "driver_id": "61",
            "latitude": "-33.903557",
            "longlatitude": "151.237732",
            "distance": 102.86956931883,
            "cabData": [
                {
                    "cab_id": "4",
                    "cartype": "MPV",
                    "car_rate": "45.00",
                    "seat_capacity": "10"
                }
            ]
        },
        {
            "driver_id": "62",
            "latitude": "-33.815521",
            "longlatitude": "151.026642",
            "distance": 90.721104800964,
            "cabData": [
                {
                    "cab_id": "3",
                    "cartype": "Hatchback ",
                    "car_rate": "40.00",
                    "seat_capacity": "8"
                }
            ]
        },
        {
            "driver_id": "63",
            "latitude": "-33.829525",
            "longlatitude": "150.873764",
            "distance": 92.985845451627,
            "cabData": [
                {
                    "cab_id": "2",
                    "cartype": "Sedan",
                    "car_rate": "45.00",
                    "seat_capacity": "6"
                }
            ]
        },
        {
            "driver_id": "6",
            "latitude": "-33.25588999",
            "longlatitude": "151.36958214",
            "distance": 44.657459067483,
            "cabData": [
                {
                    "cab_id": "4",
                    "cartype": "MPV",
                    "car_rate": "45.00",
                    "seat_capacity": "10"
                }
            ]
        }
    ]
}
      </pre>
      </td>
    </tr>

    <tr>
      <td>9 - 10</td>
      <td>https://stageofproject.com/texiapp-new/web_service/serchUserTexiAmount</td>
      <td>Post</td>
      <td>
        <pre>
        user_pickup_latitude:-33.737885<br>
        user_pickup_longlatitude:151.235260<br>
        user_drop_latitude:-33.729752<br>
        user_drop_longlatitude:150.836090<br>
      </pre>
      </td>
      <td>
        <pre>
           {
              "statusCode": "200",
              "message": "success",
              "data": [
                  {
                      "cab_id": "60",
                      "cartype": "Suv",
                      "car_rate": "50.00",
                      "min_rate": 1605,
                      "max_rate": 1615,
                      "seat_capacity": "8"
                  },
                  {
                      "cab_id": "61",
                      "cartype": "Sedan",
                      "car_rate": "45.00",
                      "min_rate": 1444.5,
                      "max_rate": 1454.5,
                      "seat_capacity": "6"
                  },
                  {
                      "cab_id": "62",
                      "cartype": "Hatchback ",
                      "car_rate": "40.00",
                      "min_rate": 1284,
                      "max_rate": 1294,
                      "seat_capacity": "8"
                  },
                  {
                      "cab_id": "63",
                      "cartype": "Truck",
                      "car_rate": "45.00",
                      "min_rate": 1444.5,
                      "max_rate": 1454.5,
                      "seat_capacity": "10"
                  },
                  {
                      "cab_id": "64",
                      "cartype": "Van",
                      "car_rate": "30.00",
                      "min_rate": 963,
                      "max_rate": 973,
                      "seat_capacity": "12"
                  },
                  {
                      "cab_id": "65",
                      "cartype": "zeep",
                      "car_rate": "25.00",
                      "min_rate": 802.5,
                      "max_rate": 812.5,
                      "seat_capacity": "8"
                  }
              ]
          }
      </pre>
      </td>
    </tr>

    <tr>
      <td>7</td>
      <td>https://stageofproject.com/texiapp-new/web_service/insertUserAddress</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:45
        place_postion:ppp147<br>
        place_name:fff<br>
        place_address:gggg<br>
        lat:33<br>
        lng:55<br>


      </pre>
      </td>
      <td>
        <pre>
        {
          "statusCode": "200",
          "message": "Address Insert Successfully",
          "data": {
              "user_id": "45",
              "place_postion": "ppp147",
              "place_name": "fff",
              "place_address": "fff",
              "lat": "33",
              "lng": "55"
          }
      }

        {
            "statusCode": "200",
            "message": "Address Update Successfully",
            "data": {
                "place_postion": "ppp147",
                "place_name": "fff",
                "place_address": "gggg",
                "lat": "33",
                "lng": "55"
            }
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>7</td>
      <td>http://admin.infinitecabs.com.au/web_service/getUseAddress?user_id=5</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:5
      </pre>
      </td>
      <td>
        <pre>
        {
          "statusCode": "200",
          "message": "success",
          "data": [
              {
                  "user_id": "5",
                  "place_postion": "Home",
                  "lat": "26.862471",
                  "lng": "75.762413",
                  "address": "54/212, Saryu Marg, Ward 27, Mansarovar Sector 5, Mansarovar, Jaipur, Rajasthan 302020, India"
              },
              {
                  "user_id": "5",
                  "place_postion": "Work",
                  "lat": "26.855450",
                  "lng": "75.820374",
                  "address": "A-90, Shivanand Marg, Sector 1, Malviya Nagar, Jaipur, Rajasthan 302017, India"
              },
              {
                  "user_id": "5",
                  "place_postion": "Other",
                  "lat": "26.920980",
                  "lng": "75.794220",
                  "address": "11, MI Road, Panch Batti, Kanti Nagar, Bani Park, Jaipur, Rajasthan 302001, India"
              }
          ]
      }
              </pre>
              </td>
            </tr>

            <tr>
              <td>https://stageofproject.com/texiapp-new/web_service/emailExists</td>
              <td>Post</td>
              <td>
                <pre>
               email:test@gmail.com
              </pre>
              </td>
              <td>
                <pre>
                 {
            "statusCode": "200",
            "message": "email exists",
            "data": []
        }
      </pre>
      </td>
    </tr>
    <tr>
     <td>21</td>
      <td>https://stageofproject.com/texiapp-new/web_service/profile_edit</td>
      <td>Post</td>
      <td>
        <pre>
        uid:5<br/>
        first_name:pankaj<br/>
        last_name:kumar<br/>
        email:47855@gmail.com<br/>
        mobile:1237414444<br/>
        isdevice:<br/>
      </pre>
      </td>
      <td>
        <pre>
            {
            "statusCode": "200",
            "message": "Your profile details have been successfully updated.",
            "data": {
                "id": "5",
                "first_name": "raviu",
                "last_name": "kumar",
                "email": "test@gmail.com",
                "mobile": "1237414444",
                "isdevice": ""
            }
        }
      </pre>
      </td>
    </tr>

    <tr>
       <td>21</td>
      <td>http://admin.infinitecabs.com.au/web_service/getUserProfile/</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:5
      </pre>
      </td>
      <td>
        <pre>
            {
            "statusCode": "200",
            "message": "success",
            "data": {
                "id": "5",
                "first_name": "pankaj",
                "last_name": "kumar",
                "email": "47855@gmail.com",
                "mobile": "1237414444",
                "status": "Active"
            }
        }
      </pre>
      </td>
    </tr>
    
    

        
     
     <!-- <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driverMessage</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:5
        message:test
      </pre>
      </td>
      <td>
        <pre>
            {
            "statusCode": "200",
            "message": "Message Sent Successfully",
            "data": {
                "user_id": "5",
                "message": "test"
            }
        }
      </pre>
      </td>
    </tr> -->

    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/InsertDriverLiveCation</td>
      <td>Post</td>
      <td>
        <pre>
        driver_id:5
        cab_id:6
        lat:11
        lng:122
      </pre>
      </td>
      <td>
        <pre>
            {
            "statusCode": "200",
            "message": "Driver location Successfully Insert",
            "data": {
                "driver_id": "5",
                "cab_id": "6",
                "latitude": "11",
                "longlatitude": "122"
            }
        }
      </pre>
      </td>
    </tr>

    <tr>
       <td>15</td>
      <td>https://stageofproject.com/texiapp-new/web_service/driverFeedback</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:7<br>
        driver_id:8<br>
        rating:22<br>
        message:dfdfdf<br>
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "Message Sent Successfully",
            "data": {
                "user_id": "7",
                "driver_id": "8",
                "rating": "22",
                "message": "dfdfdf"
            }
        }
      </pre>
      </td>
    </tr>



      <tr>
      <td>11</td>
      <td>https://stageofproject.com/texiapp-new/web_service/boookingCab</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:45<br>
        cab_id:89<br>
        pickup_lat:-33<br>
        pickup_long:151<br>
        drop_lat:-33<br>
        drop_long:150<br>
        isdevice:1<br>
        payment_type:cash<br>
        message:hiii<br>
        booking_type<br>
        book_create_date_time<br>
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "waiting search driver",
            "data": {
                "booking_id": 1677
            }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/getBookingDetails</td>
      <td>Post</td>
      <td>
        <pre>
        <br>
      </pre>
      </td>
      <td>
        <pre>
              {
              "statusCode": "200",
              "message": "success",
              "data": [
                  {
                      "id": "1624",
                      "user_id": "11",
                      "cabtype": "Suv",
                      "pickup_lat": "25",
                      "pickup_long": "78",
                      "drop_lat": "78",
                      "drop_long": "45"
                  },
                  {
                      "id": "1625",
                      "user_id": "8",
                      "cabtype": "Suv",
                      "pickup_lat": "30",
                      "pickup_long": "22",
                      "drop_lat": "78",
                      "drop_long": "45"
                  },
                  {
                      "id": "1626",
                      "user_id": "9",
                      "cabtype": "Suv",
                      "pickup_lat": "35",
                      "pickup_long": "36",
                      "drop_lat": "78",
                      "drop_long": "45"
                  },
                  {
                      "id": "1633",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1634",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1635",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1636",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1637",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1638",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1639",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1640",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1641",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1642",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1643",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1644",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1645",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1646",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1647",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1648",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1649",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1650",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1651",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1652",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1653",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1654",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1655",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1656",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1657",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1658",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1659",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1660",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1661",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1662",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1663",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1666",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1667",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  },
                  {
                      "id": "1668",
                      "user_id": "45",
                      "cabtype": "",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150"
                  }
              ]
          }
      </pre>
      </td>
    </tr>

    <tr>
      <td>13</td>
      <td>https://stageofproject.com/texiapp-new/web_service/searchDriver</td>
      <td>Post</td>
      <td>
        <pre>
        user_latitude:-33<br>
        user_longlatitude:151<br>
        booking_id:1665<br>
        cab_id:60<br>
      </pre>
      </td>
      <td>
        <pre>
           {
                "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
                "statusCode": 200,
                "message": "Driver Accepted Your booking Request",
                "data": [],
                "data1": {
                    "id": "55",
                    "name": "rk",
                    "phone": "0000000000",
                    "car_type": "Sedan",
                    "car_no": "1213",
                    "Car_Model": "2014",
                    "image": "avatar5.png",
                    "driver_latutude": "-33.737885",
                    "driver_long_latutude": "151.235260"
                }
            }

                      {
              "statusCode": 200,
              "message": "waiting for response",
              "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
              "data": {
                  "id": "1",
                  "driver_id": "55",
                  "cab": [
                      {
                          "cartype": "Sedan",
                          "car_rate": "45.00",
                          "seat_capacity": "6"
                      }
                  ],
                  "status": "online",
                  "driver_booking_status": "free",
                  "driver_latutude": "-33.737885",
                  "driver_long_latutude": "151.235260",
                  "distance": 84.91303242851
              },
              "data1": []
          }
      </pre>
      </td>
    </tr>

      <tr>
        <td>13</td>
      <td>https://stageofproject.com/texiapp-new/web_service/user_driver_latlong</td>
      <td>post</td>
      <td>
        <pre>
      booking_id:200<br>
      user_id:12<br>
      lat:33.58855<br>
      lng:151.666<br>
      </pre>
      </td>
      <td>
        <pre>
               {
              "statusCode": "200",
              "message": "success",
              "data": {
                  "driver_id": "12",
                  "booking_id": "200",
                  "latitude": "33.58855",
                  "longlatitude": "151.666",
                  "status": "user"
              }
          }
      </pre>
      </td>
    </tr>

    <tr>
      <td>13</td>
      <td>https://stageofproject.com/texiapp-new/web_service/driverRequestUpdate</td>
      <td>Post</td>
      <td>
        <pre>
        driver_id:5<br>
        booking_id:1671<br>
        driver_id:72<br>
        status:yes<br>
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "Driver  Request Successfully Accepting",
            "data": {
                "driver_details": {
                    "id": "72",
                    "name": "sumna",
                    "phone": "9509360088",
                    "car_no": "1213",
                    "car_name": "",
                    "latitude": "-33.729752",
                    "longlatitude": "150.836090",
                    "status": "online",
                    "driver_booking_status": "free",
                    "image_url": "https://stageofproject.com/texiapp-new/driverimages/"
                }
            }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>14</td>
      <td>https://stageofproject.com/texiapp-new/web_service/pickupUser</td>
      <td>Post</td>
      <td>
        <pre>
       status:yes<br>
            booking_id:1665<br>
            driver_id:6<br>
            cab_id:60<br>
            driver_lat:33<br>
            driver_lat_long:50<br>
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "Driver pickup user",
            "data": {
                "status_code": "on-trip",
                "status": 8,
                "pickup_date_time": "2019-11-25 11:58:27",
                "driver_pickup_details": {
                    "driver_id": "6",
                    "cab_id": "60",
                    "latitude": "33",
                    "longlatitude": "50",
                    "status": "online",
                    "driver_booking_status": "booked"
                }
            }
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/dropUsertime</td>
      <td>Post</td>
      <td>
        <pre>
       status:yes
      booking_id:1665
      driver_id:6
      cab_id:2
      user_pickup_latitude:-33.737885
      user_pickup_longlatitude:151.235260
      user_drop_latitude:-33.729752
      user_drop_longlatitude:150.836090
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "driver drop user",
            "data": {
                "bookingdetails": {
                    "drop_date_time": "2019-11-26 18:26:20",
                    "status_code": "completed",
                    "distance": 51.6599424,
                    "car_rate": "45.00",
                    "amount": 2324.697408,
                    "status": 9
                },
                "driver_result": {
                    "latitude": "-33.729752",
                    "longlatitude": "150.836090",
                    "status": "online",
                    "driver_booking_status": "free"
                }
            }
        }
      </pre>
      </td>
    </tr>
       <tr>
      <td>20</td>
      <td>https://stageofproject.com/texiapp-new/web_service/userCompleteRide</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:5
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "success",
            "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
            "data": [
                {
                    "booking_id": "396",
                    "pickup_area": "10, Kalawad Road, Vaishali Nagar, Rajkot, India",
                    "drop_area": "Race Course, Rajkot, Gujarat, India",
                    "amount": "55",
                    "driver_name": "test",
                    "driver_image": "Hydrangeas1.jpg",
                    "driver_car_number": "1213",
                    "car_type": "Suv",
                    "user_pickup_latitude": "22.295326",
                    "user_pickup_long_latitude": "70.784362",
                    "user_drop_latitude": "22.303588",
                    "user_drop_long_latitude": "70.787773",
                    "pickup_address": "10, Kalavad Rd, Vaishali Nagar, Rajkot, Gujarat 360007, India",
                    "drop_address": "RaceCource Plaza, Race Course Rd, Race Course, Sadar, Rajkot, Gujarat 360001, India",
                    "book_create_date_time": "2016-08-13 09:14:13"
                },
                {
                    "booking_id": "402",
                    "pickup_area": "Rajkot, Gujarat, India",
                    "drop_area": "Bhuj, Gujarat, India",
                    "amount": "3741",
                    "driver_name": "sultan",
                    "driver_image": "avatar2.png",
                    "driver_car_number": "1213",
                    "car_type": "Sedan",
                    "user_pickup_latitude": "22.303887",
                    "user_pickup_long_latitude": "70.802139",
                    "user_drop_latitude": "23.241996",
                    "user_drop_long_latitude": "69.666935",
                    "pickup_address": "Hospital Chowk, Sadar, Rajkot, Gujarat 360001, India",
                    "drop_address": "Spa Point Opp Jubely Grund Near Allahabad Bank Bhuj Deep Kruti Billding, Bhuj, Gujarat 370001, India",
                    "book_create_date_time": "2016-08-13 04:32:57"
                }
            ]
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>20</td>
      <td>https://stageofproject.com/texiapp-new/web_service/userCancelRide</td>
      <td>Post</td>
      <td>
        <pre>
       user_id:5
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "success",
            "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
            "data": [
                {
                    "booking_id": "403",
                    "pickup_area": "Rajkot, Gujarat, India",
                    "drop_area": "Bhuj, Gujarat, India",
                    "amount": "3741",
                    "driver_name": "rk",
                    "driver_image": "avatar5.png",
                    "driver_car_number": "1213",
                    "car_type": "dsvds",
                    "user_pickup_latitude": "22.303887",
                    "user_pickup_long_latitude": "70.802139",
                    "user_drop_latitude": "23.241996",
                    "user_drop_long_latitude": "69.666935"
                }
            ]
        }
      </pre>
      </td>
    </tr>

    

    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driver_login</td>
      <td>Post</td>
      <td>
        <pre>
        username:refgw<br>
        password:123456<br>
      </pre>
      </td>
      <td>
        <pre>
           {
            "statusCode": "200",
            "message": "Successfully Logged in",
            "image_path": "http://admin.infinitecabs.com.au/driverimages/",
            "data": {
                "id": "77",
                "name": "wewwerr",
                "user_name": "refgw",
                "phone": "1478522214",
                "address": "asdadqweqwq",
                "email": "14785@gmail.com",
                "license_no": "rfwewqw",
                "car_type": "asdasqwewqe",
                "car_no": "asdasqweqw",
                "gender": "male",
                "dob": "10.10.2012",
                "Lieasence_Expiry_Date": "20.10.2023",
                "Insurance": "sads",
                "license_plate": "asdsd",
                "Seating_Capacity": "4",
                "Car_Model": "ewrqwer",
                "image": "",
                "status": "Active"
            }
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>18</td>
      <td>https://stageofproject.com/texiapp-new/web_service/insertCreditCard</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:2<br>
      card_holder_name:ak<br>
      car_dnumber:1234567890<br>
      expairy:20/10/201<br>
      csv:895<br>
      </pre>
      </td>
      <td>
        <pre>
           echo 1{
                "statusCode": "200",
                "message": "success",
                "data": {
                    "user_id": "3",
                    "card_holder_name": "ak",
                    "card_number": "1234567890",
                    "expairy_date": "20/10/201",
                    "csv_no": "895"
                }
            }
      </pre>
      </td>
    </tr>

  

    <tr>
       <td>18</td>
      <td>https://stageofproject.com/texiapp-new/web_service/getCreditcardDetails</td>
      <td>Post</td>
      <td>
        <pre>
        user_id:3<br>
      </pre>
      </td>
      <td>
        <pre>
          {
              "statusCode": "200",
              "message": "success",
              "data": [
                  {
                      "user_id": "3",
                      "card_holder_name": "bk",
                      "card_number": "1234567890",
                      "expairy_date": "20/10/201",
                      "csv_no": "895"
                  },
                  {
                      "user_id": "3",
                      "card_holder_name": "mn",
                      "card_number": "1234567890",
                      "expairy_date": "20/10/201",
                      "csv_no": "895"
                  }
              ]
          }
      </pre>
      </td>
    </tr>

   
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driverDetails</td>
      <td>Post</td>
      <td>
        <pre>
      user_id:72
      </pre>
      </td>
      <td>
        <pre>
              {
              "statusCode": "200",
              "message": "success",
              "image_path": "https://stageofproject.com/texiapp-new/driverimages/avatar.png",
              "data": {
                  "name": "sumna",
                  "user_name": "suan",
                  "phone": "9509360088",
                  "address": "645-A DEVI NAGAR NEW SANGANER ROAD\nSODALA",
                  "email": "su@gmail.com",
                  "license_no": "123",
                  "car_type": "Suv",
                  "gender": "male",
                  "dob": "10-10-2013",
                  "user_status": "active",
                  "Lieasence_Expiry_Date": "31-8-2016",
                  "license_plate": "12345",
                  "Insurance": "da",
                  "Car_Model": "dsfs",
                  "image": "avatar.png",
                  "status": "Active"
              }
          }
      </pre>
      </td>
    </tr>
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driverCompeteRide</td>
      <td>get</td>
      <td>
        <pre>
      user_id:5
      </pre>
      </td>
      <td>
        <pre>
        {
          "statusCode": "200",
          "message": "success",
          "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
          "data": [
              {
                  "booking_id": "",
                  "amount": "3741",
                  "driver_name": "test",
                  "driver_image": "Hydrangeas1.jpg",
                  "driver_car_number": "1213",
                  "pickup_area": "Rajkot, Gujarat, India",
                  "car_type": "Suv",
                  "user_pickup_latitude": "22.303887",
                  "user_pickup_long_latitude": "70.802139",
                  "user_drop_latitude": "23.241996",
                  "user_drop_long_latitude": "69.666935",
                  "pickup_address": "Hospital Chowk, Sadar, Rajkot, Gujarat 360001, India",
                  "drop_address": "Spa Point Opp Jubely Grund Near Allahabad Bank Bhuj Deep Kruti Billding, Bhuj, Gujarat 370001, India",
                  "book_create_date_time": "2016-08-13 04:32:57"
              },
              {
                  "booking_id": "",
                  "amount": "1087",
                  "driver_name": "test",
                  "driver_image": "Hydrangeas1.jpg",
                  "driver_car_number": "1213",
                  "pickup_area": "Silwar Stand Road 2,360003,Rajkot,Gujarat,India",
                  "car_type": "Suv",
                  "user_pickup_latitude": "22.300260",
                  "user_pickup_long_latitude": "70.808202",
                  "user_drop_latitude": "21.761218",
                  "user_drop_long_latitude": "70.626797",
                  "pickup_address": "Shamaseth St, Soni Bazar, Old walled City, Rajkot, Gujarat 360001, India",
                  "drop_address": "Unnamed Road, Jai Shree Krishna Housing Society, Jetpur, Gujarat 360370, India",
                  "book_create_date_time": "2016-08-13 11:54:15"
              }
          ]
      }
      </pre>
      </td>
    </tr>
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driverConceledRide</td>
      <td>get</td>
      <td>
        <pre>
      user_id:5
      </pre>
      </td>
      <td>
        <pre>
                {
                  "statusCode": "200",
                  "message": "success",
                  "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
                  "data": [
                      {
                          "booking_id": "",
                          "amount": "1895",
                          "driver_name": "test",
                          "driver_image": "Hydrangeas1.jpg",
                          "driver_car_number": "1213",
                          "pickup_area": "Ring Road,360003,Rajkot,Gujarat,India",
                          "car_type": "Suv",
                          "user_pickup_latitude": "22.339199",
                          "user_pickup_long_latitude": "70.807649",
                          "user_drop_latitude": "21.761218",
                          "user_drop_long_latitude": "70.626797",
                          "pickup_address": "Ring road, Rajkot, Gujarat 360003, India",
                          "drop_address": "Unnamed Road, Jai Shree Krishna Housing Society, Jetpur, Gujarat 360370, India",
                          "book_create_date_time": "2016-08-13 11:56:41"
                      }
                  ]
              }
      </pre>
      </td>
    </tr>
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/updateDriverStatus</td>
      <td>post</td>
      <td>
        <pre>
      driver_id:55<br>
      status:offline<br>
      </pre>
      </td>
      <td>
        <pre>
                {
                "statusCode": "200",
                "message": "offline",
                "data": []
            }
            {
              "statusCode": "200",
              "message": "online",
              "data": []
          }
      </pre>
      </td>
    </tr>
    
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/contactus</td>
      <td>Post</td>
      <td>
        <pre>
        userId:2<br>
        message:ddsddf<br>
        email:test@gmail.com<br>
      </pre>
      </td>
      <td>
        <pre>
          {
    "statusCode": "200",
    "message": "Mail Sent Successfully",
    "data": []
}
      </pre>
      </td>
    </tr>
<tr>
  <td>

  </td>
  <td>
    
  </td>
  <td>
    
  </td>
</tr>

  <tr>
    <td>http://admin.infinitecabs.com.au/web_service/notification_update</td>
    <td>Post</td>
    <td>
      <pre>
      userId:124<br>
      notification:2<br>
      </pre>
    </td>
    <td>
      <pre>
      {
        "statusCode":"200",
        "message":"Notification Update Successfully",
        "data":{"notification":"2"}
      }
      </pre>
    </td>
  </tr>

  <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/get_user_driver_latlong</td>
      <td>Post</td>
      <td>
        <pre>
      booking_id:200<br>
      user_id:2<br>
      </pre>
      </td>
      <td>
        <pre>
        if send booking_id Get (diver_live_location according to booking_id)
        {
            "statusCode": "200",
            "message": "success",
            "data": {
                "user_id": "12",
                "booking_id": "200",
                "latitude": "33.58855",
                "longlatitude": "151.666"
            }
        }
        if send user_id Get (diver_live_location according to user_id)
        {
          "statusCode":"200",
          "message":"success",
          "data":{
              "user_id":"2",
              "latitude":"33",
              "longlatitude":"55"
          }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://admin.infinitecabs.com.au/web_service/change_password</td>
      <td>Post</td>
      <td>
        <pre>
      password:123456<br>
      uid:124<br>
      </pre>
      </td>
      <td>
        <pre>
        {
          "statusCode":"200",
          "message":"Password Change Successfully"
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://admin.infinitecabs.com.au/web_service/InsertDriverlivelatlong</td>
      <td>Post</td>
      <td>
        <pre>
      driver_id:3
      booking_id:1709
      lat:33.58855
      lng:151.666
      </pre>
      </td>
      <td>
        <pre>
        {
            "statusCode": "200",
            "message": "Update Successfully"
        }
      </pre>
      </td>
    </tr>
    <!-- <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/get_user_driver_latlong</td>
      <td>get</td>
      <td>
        <pre>
     booking_id:200
      </pre>
      </td>
      <td>
        <pre>
              {
            "statusCode": "200",
            "message": "success",
            "data": {
                "user_id": "12",
                "booking_id": "200",
                "latitude": "33.58855",
                "longlatitude": "151.666"
            }
        }
      </pre>
      </td>
    </tr> -->
    <!-- <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driver_status</td>
      <td>get</td>
      <td>
        <pre>
      <br>
     
    
      </pre>
      </td>
      <td>
        <pre>
        {
    "statusCode": "200",
    "message": "success",
    "data": [
        {
            "cab_id": "60",
            "cartype": "Suv",
            "car_rate": "50.00",
            "transfertype": "Point to Point Transfer",
            "intialkm": "5.00",
            "intailrate": "0.00",
            "standardrate": "0.00",
            "fromintialkm": "5.00",
            "fromintailrate": "25.00",
            "fromstandardrate": "0.00",
            "night_fromintialkm": "0.00",
            "night_fromintailrate": "22.00",
            "extrahour": "0",
            "extrakm": "0",
            "timetype": "day",
            "package": "",
            "night_package": "",
            "icon": "Type-SUV.png",
            "description": "Mitsubishi Montero , Mercedes Benz GLC Class , Mahindra NuvoSport",
            "ride_time_rate": "6.00",
            "night_ride_time_rate": "7.00",
            "daystarttime": "",
            "day_end_time": "",
            "night_start_time": "",
            "night_end_time": "",
            "night_intailrate": "45.00",
            "night_standardrate": "0.00",
            "seat_capacity": "8"
        },
        {
            "cab_id": "61",
            "cartype": "Sedan",
            "car_rate": "45.00",
            "transfertype": "Point to Point Transfer",
            "intialkm": "5.00",
            "intailrate": "0.00",
            "standardrate": "0.00",
            "fromintialkm": "5.00",
            "fromintailrate": "20.00",
            "fromstandardrate": "0.00",
            "night_fromintialkm": "0.00",
            "night_fromintailrate": "15.00",
            "extrahour": "0",
            "extrakm": "0",
            "timetype": "day",
            "package": "",
            "night_package": "",
            "icon": "Type-Sedans.png",
            "description": "Maruti Dzire , Tata indigo , Toyoto Etios , Toyota altis",
            "ride_time_rate": "5.00",
            "night_ride_time_rate": "6.00",
            "daystarttime": "",
            "day_end_time": "",
            "night_start_time": "",
            "night_end_time": "",
            "night_intailrate": "50.00",
            "night_standardrate": "0.00",
            "seat_capacity": "6"
        },
        {
            "cab_id": "62",
            "cartype": "Hatchback ",
            "car_rate": "40.00",
            "transfertype": "Point to Point Transfer",
            "intialkm": "5.00",
            "intailrate": "0.00",
            "standardrate": "0.00",
            "fromintialkm": "5.00",
            "fromintailrate": "12.00",
            "fromstandardrate": "0.00",
            "night_fromintialkm": "0.00",
            "night_fromintailrate": "13.00",
            "extrahour": "0",
            "extrakm": "0",
            "timetype": "day",
            "package": "",
            "night_package": "",
            "icon": "Type-Hatchback.png",
            "description": "Renault Kwid , Maruti Swift Dzire ,Mahindra XUV500",
            "ride_time_rate": "3.00",
            "night_ride_time_rate": "5.00",
            "daystarttime": "",
            "day_end_time": "",
            "night_start_time": "",
            "night_end_time": "",
            "night_intailrate": "55.00",
            "night_standardrate": "0.00",
            "seat_capacity": "8"
        },
        {
            "cab_id": "63",
            "cartype": "Truck",
            "car_rate": "45.00",
            "transfertype": "Point to Point Transfer",
            "intialkm": "5.00",
            "intailrate": "0.00",
            "standardrate": "0.00",
            "fromintialkm": "1.00",
            "fromintailrate": "15.00",
            "fromstandardrate": "0.00",
            "night_fromintialkm": "0.00",
            "night_fromintailrate": "18.00",
            "extrahour": "0",
            "extrakm": "0",
            "timetype": "day",
            "package": "",
            "night_package": "",
            "icon": "Type-Truck1.png",
            "description": "Small trucks , Light trucks , Medium trucks , Heavy trucks",
            "ride_time_rate": "5.00",
            "night_ride_time_rate": "7.00",
            "daystarttime": "",
            "day_end_time": "",
            "night_start_time": "",
            "night_end_time": "",
            "night_intailrate": "50.00",
            "night_standardrate": "0.00",
            "seat_capacity": "10"
        },
        {
            "cab_id": "64",
            "cartype": "Van",
            "car_rate": "30.00",
            "transfertype": "Point to Point Transfer",
            "intialkm": "5.00",
            "intailrate": "0.00",
            "standardrate": "0.00",
            "fromintialkm": "1.00",
            "fromintailrate": "10.00",
            "fromstandardrate": "0.00",
            "night_fromintialkm": "0.00",
            "night_fromintailrate": "9.00",
            "extrahour": "0",
            "extrakm": "0",
            "timetype": "day",
            "package": "",
            "night_package": "",
            "icon": "Type-Van1.png",
            "description": "Tata Venture , Tata Winger , Maruti Suzuki Omni",
            "ride_time_rate": "2.00",
            "night_ride_time_rate": "3.00",
            "daystarttime": "",
            "day_end_time": "",
            "night_start_time": "",
            "night_end_time": "",
            "night_intailrate": "35.00",
            "night_standardrate": "0.00",
            "seat_capacity": "12"
        },
        {
            "cab_id": "65",
            "cartype": "zeep",
            "car_rate": "25.00",
            "transfertype": "Point to Point Transfer",
            "intialkm": "5.00",
            "intailrate": "0.00",
            "standardrate": "0.00",
            "fromintialkm": "1.00",
            "fromintailrate": "6.00",
            "fromstandardrate": "0.00",
            "night_fromintialkm": "0.00",
            "night_fromintailrate": "7.00",
            "extrahour": "0",
            "extrakm": "0",
            "timetype": "day",
            "package": "",
            "night_package": "",
            "icon": "Type-Zeep1.png",
            "description": "Jeep Renegade BU , Jeep Wrangler , jeep Grand Cherokee , jeep Compass",
            "ride_time_rate": "2.00",
            "night_ride_time_rate": "3.00",
            "daystarttime": "",
            "day_end_time": "",
            "night_start_time": "",
            "night_end_time": "",
            "night_intailrate": "30.00",
            "night_standardrate": "0.00",
            "seat_capacity": "8"
        }
    ]
}
      </pre>
      </td>
    </tr> -->
    
    <!-- <tr>
      <td>http://stageofproject.com/celebra/Webservice/driver_arrived_trip</td>
      <td>Post</td>
      <td>
        <pre>
       booking_id:402<br/>
       driver_id:72<br/>
     
    
      </pre>
      </td>
      <td>
        <pre>
        {
    "status": "success",
    "Isactive": "Active",
    "trip_detail": [
        {
            "id": "402",
            "username": "hiral",
            "user_id": "5",
            "purpose": "Point to Point Transfer",
            "pickup_area": "Rajkot, Gujarat, India",
            "drop_area": "Bhuj, Gujarat, India",
            "pickup_date_time": "2016-08-13 17:00:59",
            "taxi_type": "Truck",
            "taxi_id": "0",
            "departure_date_time": "0000-00-00 00:00:00",
            "package": "",
            "status": "7",
            "status_code": "driver-arrived",
            "promo_code": "",
            "book_create_date_time": "2016-08-13 04:32:57",
            "distance": "0",
            "isdevice": "0",
            "approx_time": "3 hours 57 mins",
            "amount": "3741",
            "final_amount": "0",
            "pickup_address": "",
            "transfer": "",
            "assigned_for": "0",
            "person": "8",
            "payment_type": "cash",
            "transaction_id": "",
            "km": "235",
            "timetype": "day",
            "comment": "",
            "driver_status": "",
            "pickup_lat": "22.303887",
            "pickup_long": "70.802139",
            "drop_lat": "23.241996",
            "drop_long": "69.666935",
            "flag": "0",
            "reason": "0"
        }
    ]
}
      </pre>
      </td>
    </tr> -->
    <!-- <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driver_on_trip</td>
      <td>Post</td>
      <td>
        <pre>
            booking_id:404<br/>
           driver_id:72<br/>
        </pre>
      </td>
      <td>
        <pre>
        {
    "status": "success",
    "Isactive": "Active",
    "trip_detail": [
        {
            "id": "404",
            "username": "ketan",
            "user_id": "46",
            "purpose": "Point to Point Transfer",
            "pickup_area": "150 Feet Ring Road,Gulab Vatika,Rajkot, Gujarat 360001,Rajkot,360001,India",
            "drop_area": "Jamnagar, Gujarat, India",
            "pickup_date_time": "2016-08-13 17:01:00",
            "taxi_type": "Truck with fridge (for meat)",
            "taxi_id": "0",
            "departure_date_time": "2016-08-13 14:41:51",
            "package": "",
            "status": "8",
            "status_code": "on-trip",
            "promo_code": "",
            "book_create_date_time": "2016-08-13 11:33:18",
            "distance": "0",
            "isdevice": "1",
            "approx_time": "1 hour 35 mins",
            "amount": "2794",
            "final_amount": "0",
            "pickup_address": "",
            "transfer": "",
            "assigned_for": "0",
            "person": "1",
            "payment_type": "Cash",
            "transaction_id": "",
            "km": "92",
            "timetype": "day",
            "comment": "",
            "driver_status": "",
            "pickup_lat": "22.2815827",
            "pickup_long": "70.7762019",
            "drop_lat": "22.4707019",
            "drop_long": "70.05773",
            "flag": "0",
            "reason": "0"
        }
    ]
}
      </pre>
      </td>
    </tr> -->
    <!-- <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driver_completed_trip</td>
      <td>Post</td>
      <td>
        booking_id:396<br>
        driver_id:72<br>
        final_amount
        delay_reason
      </td>
      <td>
        <pre>
        {
    "status": "success",
    "Isactive": "Active",
    "trip_detail": [
        {
            "id": "396",
            "username": "hiral",
            "user_id": "5",
            "purpose": "Point to Point Transfer",
            "pickup_area": "10, Kalawad Road, Vaishali Nagar, Rajkot, India",
            "drop_area": "Race Course, Rajkot, Gujarat, India",
            "pickup_date_time": "2016-08-13 14:41:51",
            "taxi_type": "Truck",
            "taxi_id": "0",
            "departure_date_time": "2016-08-13 14:41:51",
            "package": "",
            "status": "9",
            "status_code": "completed",
            "promo_code": "",
            "book_create_date_time": "2016-08-13 09:14:13",
            "distance": "0",
            "isdevice": "0",
            "approx_time": "5 mins",
            "amount": "55",
            "final_amount": "0",
            "pickup_address": "",
            "transfer": "",
            "assigned_for": "5",
            "person": "8",
            "payment_type": "cash",
            "transaction_id": "",
            "km": "1",
            "timetype": "day",
            "comment": "",
            "driver_status": "",
            "pickup_lat": "22.295326",
            "pickup_long": "70.784362",
            "drop_lat": "22.303588",
            "drop_long": "70.787773",
            "flag": "0",
            "reason": "0"
        }
    ]
}
      </pre>
      </td>
    </tr> -->
    <!-- <tr>
      <td>http://stageofproject.com/celebra/Webservice/slider/</td>
      <td>Post</td>
      <td>

      </td>
      <td>
        <pre>
        {
    "status": "success",
    "trip_detail": [
        {
            "id": "405",
            "username": "harshil",
            "user_id": "62",
            "purpose": "Point to Point Transfer",
            "pickup_area": "Silwar Stand Road 3,360002,Rajkot,Gujarat,India",
            "drop_area": "Chotila Maa Temple, Chotila, Gujarat, India",
            "pickup_date_time": "2016-08-13 17:03:07",
            "taxi_type": "Truck",
            "taxi_id": "0",
            "departure_date_time": "2016-08-13 14:41:51",
            "package": "",
            "status": "5",
            "status_code": "driver-unavailable",
            "promo_code": "",
            "book_create_date_time": "2016-08-13 11:33:19",
            "distance": "0",
            "isdevice": "0",
            "approx_time": "53 mins",
            "amount": "745",
            "final_amount": "0",
            "pickup_address": "",
            "transfer": "",
            "assigned_for": "0",
            "person": "8",
            "payment_type": "cash",
            "transaction_id": "",
            "km": "47",
            "timetype": "day",
            "comment": "",
            "driver_status": "",
            "pickup_lat": "22.300260",
            "pickup_long": "70.808202",
            "drop_lat": "22.423705",
            "drop_long": "71.194608",
            "flag": "0",
            "reason": "0"
        }
    ]
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/driver_unavailable_cancelled_book</td>
      <td>get</td>
      <td>
        booking_id:405
      </td>
      <td>
        <pre>
            {
        "status": "success",
        "trip_detail": [
            {
                "id": "402",
                "username": "hiral",
                "user_id": "5",
                "purpose": "Point to Point Transfer",
                "pickup_area": "Rajkot, Gujarat, India",
                "drop_area": "Bhuj, Gujarat, India",
                "pickup_date_time": "2016-08-13 17:00:59",
                "taxi_type": "Truck",
                "taxi_id": "0",
                "departure_date_time": "0000-00-00 00:00:00",
                "package": "",
                "status": "6",
                "status_code": "driver-unavailable",
                "promo_code": "",
                "book_create_date_time": "2016-08-13 04:32:57",
                "distance": "0",
                "isdevice": "0",
                "approx_time": "3 hours 57 mins",
                "amount": "3741",
                "final_amount": "0",
                "pickup_address": "",
                "transfer": "",
                "assigned_for": "0",
                "person": "8",
                "payment_type": "cash",
                "transaction_id": "",
                "km": "235",
                "timetype": "day",
                "comment": "",
                "driver_status": "",
                "pickup_lat": "22.303887",
                "pickup_long": "70.802139",
                "drop_lat": "23.241996",
                "drop_long": "69.666935",
                "flag": "0",
                "reason": "0"
            }
        ]
    }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/celeList/</td>
      <td>Post</td>
      <td>
        userId=8
      </td>
      <td>
        <pre>
        {
            "statusCode": 200,
            "message": "Celebrity List",
            "data": {
                "celeList": [
                    {
                        "name": "chetan",
                        "userId": "8",
                        "profileImage": "",
                        "stars": "4",
                        "like": "",
                        "dislike": ""
                    }
                ]
            }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/celeDetail/</td>
      <td>Post</td>
      <td>
        celeId : 8 <br>
        userId : 3 <br>
      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Celebrity List",
    "data": {
        "celeList": [
            {
                "name": "chetan",
                "userId": "8",
                "profileImage": "http://localhost/celebra/assets/profile_image/1565954725.jpg",
                "stars": 5,
                "like": 2,
                "dislike": 1,
                "followers": "",
                "totalVideo": 1,
                "bio": "",
                "favourite": 0,
                "email": "",
                "location": "jaipur",
                "price": "",
                "follow": "Pending",
                "type": "Actor"
            }
        ],
        "category": [
            {
                "name": "Birthday",
                "icon": "http://localhost/celebra/assets/upload/b.png",
                "status": "Pending",
                "id": "1",
                "date": "2019-09-06 13:31:03"
            }
        ],
        "videos": [
            {
                "name": "test birthday video",
                "image": "http://localhost/celebra/assets/upload/s.jpg",
                "video": "http://localhost/celebra/assets/upload/vm.mp4",
                "id": "1",
                "date": "2019-09-05 10:29:51"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/categoryList/</td>
      <td>GET</td>
      <td>

      </td>
      <td>
        <pre>
                  {
              "statusCode": 200,
              "message": "Category List ",
              "data": {
                  "category": [
                      {
                          "name": "Birthday",
                          "id": "1"
                      },
                      {
                          "name": "Graduation",
                          "id": "2"
                      }
                  ]
              }
          }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/insertRequest/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        firstName:f <br>
        lastName:s <br>
        email:asdf@gasdf <br>
        celeId:8 <br>
        categoryId:1 <br>
        price:12 <br>
        msg:sdfsdf
      </td>
      <td>
        <pre>
        {
          "statusCode": 200,
          "message": "Request Submitted Successfully",
          "data": {
              "requestid": 4
          }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/editRequest/</td>
      <td>Post</td>
      <td>
        requestId:4 <br>

      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Request Data",
    "data": {
        "requestData": [
            {
                "firstName": "f",
                "lastName": "s",
                "email": "asdf@gasdf",
                "celeId": "8",
                "categoryId": "1",
                "price": "12",
                "msg": "sdfsdf",
                "id": "4"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/updateRequest/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        firstName:f <br>
        lastName:sssssssssssssssss <br>
        email:asdf@gasdf <br>
        celeId:8 <br>
        categoryId:1 <br>
        price:12 <br>
        msg:sdfsdf <br>
        id:3

      </td>
      <td>
        <pre>
        {
            "statusCode": 200,
            "message": "Request Updated Successfully",
            "data": {
                "requestid": "3"
            }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/search/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        searchTxt:ch

      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Celebrity List",
    "data": {
        "celeList": [
            {
                "name": "chetan",
                "userId": "8",
                "profileImage": "",
                "stars": "4",
                "like": "",
                "dislike": "",
                "favourite": 0
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/addFav/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        celeId:2

      </td>
      <td>
        <pre>
        {
          "statusCode": 200,
          "message": "Add  Successfully in favourite",
          "data": {
              "userId": "2"
          }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/myVideo/</td>
      <td>Post</td>
      <td>
        userId:3 <br>


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Celebrity List",
    "data": {
        "celeList": [
            {
                "name": "chetan",
                "userId": "1",
                "thumbnail": "http://localhost/celebra/assets/upload/s.jpg",
                "videoname": "test birthday video",
                "createdDate": "2019-09-05 10:29:51",
                "profileImage": "http://localhost/celebra/assets/upload/1565954725.jpg",
                "videoUrl": "http://localhost/celebra/assets/upload/vm.mp4",
                "videoId": "1",
                "celeId": "8",
                "videoDescription": "",
                "likes": 2,
                "dislikes": 1,
                "rating": 5,
                "category": "Birthday"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/likes/</td>
      <td>Post</td>
      <td>
        celeId:3 <br>
        userId:3 <br>
        videoId:2 <br>
        like:Like/Dislike <br>
        rating:5


      </td>
      <td>
        <pre>
        {
          "statusCode": 200,
          "message": "Rating Is Updated Successfully",
          "data": {
              "userId": "3"
          }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/videoDetail/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        videoId:3 <br>


      </td>
      <td>
        <pre>
        {
          "statusCode": 200,
          "message": "Video Detail",
          "data": {
              "videoDetail": [
                  {
                      "name": "chetan",
                      "userId": "1",
                      "thumbnail": "http://localhost/celebra/assets/upload/s.jpg",
                      "videoname": "test birthday video",
                      "createdDate": "2019-09-05 10:29:51",
                      "profileImage": "http://localhost/celebra/assets/upload/1565954725.jpg",
                      "videoUrl": "http://localhost/celebra/assets/upload/vm.mp4",
                      "videoId": "1",
                      "celeId": "8",
                      "videoDescription": "",
                      "likes": 2,
                      "dislikes": 1,
                      "rating": 5,
                      "category": "Birthday",
                      "price": 0
                  }
              ]
          }
      }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/addFollower/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        celeId:3 <br>


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Add  Successfully in Followers",
    "data": {
        "userId": "3"
    }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/sendMessage/</td>
      <td>Post</td>
      <td>
        senderId:3 <br>
        receiverId:3 <br>
        msg:"Test sdsf"<br>


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Message Send Successfully",
    "data": {
        "userId": "2"
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/messageUserList/</td>
      <td>Post</td>
      <td>
        userId:3 <br>



      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Message Send Successfully",
    "data": {
        "userId": "3",
        "userList": [
            {
                "name": "chetan",
                "id": "2",
                "msg": "testtts",
                "profileImage": "http://localhost/celebra/assets/profile_image/1565954725.jpg"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/chatmsg/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        celeId:2


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Message Send Successfully",
    "data": {
        "userId": "3",
        "userList": [
            {
                "senderName": "chetan",
                "receiverName": "sss",
                "senderId": "2",
                "receiverId": "3",
                "createdDate": "2019-09-16 11:57:46",
                "id": "1",
                "msg": "testtts",
                "senderimage": "http://localhost/celebra/assets/profile_image/1565954725.jpg",
                "receiverimage": ""
            },
            {
                "senderName": "chetan",
                "receiverName": "sss",
                "senderId": "2",
                "receiverId": "3",
                "createdDate": "2019-09-16 11:57:46",
                "id": "2",
                "msg": "msg 2",
                "senderimage": "http://localhost/celebra/assets/profile_image/1565954725.jpg",
                "receiverimage": ""
            },
          
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getFollowRequest/</td>
      <td>Post</td>
      <td>
        userId:3 <br>



      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Follow Request List",
    "data": {
        "userId": "8",
        "requestList": [
            {
                "name": "sss",
                "profileImage": "",
                "location": "",
                "requestId": "1"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/updateFollowRequest/</td>
      <td>Post</td>
      <td>
        requestId:3 <br>
        status:1 for Accept and 2 for reject



      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Follow Request Status Updated Sucessfully",
    "data": {
        "requestId": "1"
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getfollower/</td>
      <td>Post</td>
      <td>
        userId:3 <br>




      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Celebrity List",
    "data": {
        "celeList": [
            {
                "name": "chetan",
                "userId": "8",
                "profileImage": "http://localhost/celebra/assets/profile_image/1565954725.jpg",
                "stars": 5,
                "like": 2,
                "dislike": 1,
                "favourite": 1
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/searchVideo/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        search:test<br>




      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Search video list",
    "data": {
        "videoList": [
            {
                "name": "chetan",
                "userId": "1",
                "thumbnail": "http://localhost/celebra/assets/upload/s.jpg",
                "videoname": "test birthday video",
                "createdDate": "2019-09-05 10:29:51",
                "profileImage": "http://localhost/celebra/assets/upload/1565954725.jpg",
                "videoUrl": "http://localhost/celebra/assets/upload/vm.mp4",
                "videoId": "1",
                "celeId": "8",
                "videoDescription": "",
                "likes": 2,
                "dislikes": 1,
                "rating": 5,
                "category": "Birthday",
                "price": "0"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getnotification/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        search:test<br>

      </td>
      <td>
        <pre>
        {
            "statusCode": 200,
            "message": "Notification List",
            "data": {
                "notificationList": [
                    {
                        "name": "chetan",
                        "msg": "test sdtes ",
                        "profileImage": "http://localhost/celebra/assets/profile_image/1565954725.jpg",
                        "id": "1"
                    }
                ]
              }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/uploadVideo/</td>
      <td>Post</td>
      <td>
        userId:3 <br>
        requestId:1 <br>
        video:base64<br>
        name:s<br>
        description:df<br>
        category:3<br>
        type:1 for private and 0 for public<br>

      </td>
      <td>
        <pre>
        {
            "statusCode": 200,
            "message": "Video Uploaded Successfully",
            "data": {
                "videoId": [
                    {
                        "videoId": "1",
                     
                    }
                ]
              }
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getfollowerCele/</td>
      <td>Post</td>
      <td>
        userId:8 <br>


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Celebrity List",
    "data": {
        "celeList": [
            {
                "name": "sss",
                "userId": "3",
                "email": "8946950488",
                "location": "",
                "profileImage": ""
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getRequestCele/</td>
      <td>Post</td>
      <td>
        userId:8 <br>


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Request List",
    "data": {
        "Pending": [
            {
                "name": "Birthday",
                "icon": "http://localhost/celebra/assets/upload/b.png",
                "status": "Pending",
                "id": "1",
                "date": "2019-09-06 13:31:03"
            }
        ],
        "Viewed": [
            {
                "name": "Birthday",
                "icon": "http://localhost/celebra/assets/upload/b.png",
                "status": "Pending",
                "id": "2",
                "date": "2019-09-06 13:31:03"
            }
        ],
        "Completed": [
            {
                "name": "Birthday",
                "icon": "http://localhost/celebra/assets/upload/b.png",
                "status": "Pending",
                "id": "3",
                "date": "2019-09-06 13:31:03"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/updateRequestStatus/</td>
      <td>Post</td>
      <td>
        userId:8 <br>
        id:1<br>
        status:1<br>


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Request Status Updated SuccessFully",
    "data": {
        "requestId": "1"
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getVideo/</td>
      <td>Post</td>
      <td>
        userId:8 <br>
        type:1<br>
     


      </td>
      <td>
        <pre>
        {
    "statusCode": 200,
    "message": "Video List",
    "data": {
        "videoList": [
            {
                "name": "test birthday video",
                "image": "http://localhost/celebra/assets/upload/s.jpg",
                "video": "http://localhost/celebra/assets/upload/vm.mp4",
                "id": "1",
                "date": "2019-09-05 10:29:51"
            }
        ]
    }
}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/blockfan/</td>
      <td>Post</td>
      <td>
        userId:8 <br>
        fanId:1<br>
     


      </td>
      <td>
        <pre>
        {"statusCode":200,"message":"Fan Block Successfully ","data":{"userId":"8"}}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/insertvidoenotification/</td>
      <td>Post</td>
      <td>
      userId:8 <br>
      videoId:1<br>
     


      </td>
      <td>
        <pre>
        {"statusCode":200,"message":"Fan Block Successfully ","data":{"userId":"8"}}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/hideprofile/</td>
      <td>Post</td>
      <td>
      userId:8 <br>
    
      </td>
      <td>
        <pre>
        {"statusCode":200,"message":"Profile Is Display Successfully","data":[]}
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/getcelelistagent/</td>
      <td>Post</td>
      <td>
      userId:8 <br>
    
      </td>
      <td>
        <pre>
       same as celelist
      </pre>
      </td>
    </tr>
    <tr>
      <td>http://stageofproject.com/celebra/Webservice/updateVideo/</td>
      <td>Post</td>
      <td>
      id:123
    name:test
        description:test sdfjlt sdfljtes
    category: 1
        type:1

      </td>
      <td>
        <pre>
        {"statusCode":200,"message":"Video Updated Successfully","data":{"videoId":"123"}}
      </pre>
      </td>
    </tr> -->

  </tbody>
</table>