<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Create New Zone - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->

 

    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

    <link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>  



    <!--[if lt IE 9]>

    <script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

    <script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

    <![endif]-->



    <!--<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>-->



<style>  

    

body.theme-red.fixed-header.fixed-footer.fixed-leftmenu.pace-done.pace-done .modal-dialog {

    position: fixed !important;

    right: 0 !important;

    top: 20px !important;

}  



.main-box.clearfix.up-zn {

    height: 270px;

}



#content-wrapper{

    height: 93vh !important;

}





  

</style>



</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/dispatch_header.php";

    ?>

    <div id="page-wrapper" class="container  nav-small">

        <div class="row">

            <?php

            include"includes/dispatch_side.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Create New Zone</h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Create New Zone</span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

                        <div class="col-lg-12"> 

                            <!-- Single Delete -->

                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>

                                        </div>

                                        <div class="modal-title">Are you sure you want to delete the selected reason?</div>

                                        <div class="modal-body"></div>

                                        <div class="modal-footer">

                                            <button id="confirm-delete-button" onclick="delete_single_reason_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>

                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 

                                            <input type="hidden" value="" id="bookedid" name="bookedid">

                                            <button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>

                                        </div>

                                    </div> <!-- / .modal-content --> 

                                </div> <!-- / .modal-dialog -->

                            </div> <!-- / .modal -->

                            <!-- / Single Delete -->

                            <!-- Multipal Delete --> 

                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>

                                        </div>

                                        <div class="modal-title">Are you sure you want to delete selected reason?</div>

                                        <div class="modal-body"></div>

                                        <div class="modal-footer">

                                            <button onclick="delete_reason()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>

                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                            <button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>

                                        </div>

                                    </div> <!-- / .modal-content -->

                                </div> <!-- / .modal-dialog -->

                            </div> <!-- / .modal -->

                            <!-- / Multipal Delete -->

                        </div>

                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

                         <div class="row">

                            <div class="col-lg-6">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                           

                                        </div>

                                    </div>

                                    <div class="main-box-body clearfix">

                                         <form action="<?php echo base_url(); ?>zone/saveZone" id="sendmail" method="post">

                                                      

                                                <div class="row">

                                              <input type="hidden" name="json" id="json" value="">

                                       

                                                 <div style="" class="form-group">

                                                     <lable>Zone Name</lable>

                                                         <input type="text" name="title" id="title" placeholder="Zone Name" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

                                                 <div style="" class="form-group">

                                                     <lable>Post Code</lable>

                                                         <input type="text" id="zipcode" name="zipcode" placeholder="Post Code" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

                                                

                                                <div style="" class="form-group">

                                                    <lable>Dispatch Time</lable>

                                                         <select id="time" name="time" class="form-control">
                                                             <option value="10">10 Min</option>
                                                             <option value="20">20 Min</option>
                                                             <option value="30">30 Min</option>
                                                         </select>

                                                 

                                                </div>

                                                

                                                 

                                                

                                               

                                                </div>

                                                       <div class="row">

                                             

                                                <button type="button" id="send" class="btn btn-primary " href="javascript:void(0)" style="margin-left: 43%;">

                                                    Submit

                                                </button>

                                                

                                               

                                                <span>&nbsp;</span>

                                                </div> 

                                                    

                                                </form>

                                    </div>

                                </div>

                            </div>

                             <div class="col-lg-6">

                                <div class="main-box clearfix up-zn">

                                    <div class="panel">

                                        <div class="panel-body">

                                           

                                        </div>

                                    </div>

                                    <div class="main-box-body clearfix">

                                         <form enctype="multipart/form-data" action="<?php echo base_url(); ?>dispatcher/uploadcsv" id="sendmail" method="post">

                                                      

                                                <div class="row">

                                              

                                       

                                                 <div style="" class="form-group">

                                                     <lable>Upload Zone(<a href="http://admin.infinitecabs.com.au/zonelist.csv" download>Download sample file</a>)</lable>

                                                         <input type="file" required name="csv" id="title" placeholder="Zone Name" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

                                                

                                               

                                                </div>

                                                       <div class="row">

                                             

                                                <button type="submit" id="send" class="btn btn-primary " href="javascript:void(0)" style="margin-left: 43%;">

                                                    Submit

                                                </button>

                                                

                                               

                                                <span>&nbsp;</span>

                                                </div> 

                                                    

                                                </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Save Zone</h4>

      </div>

      <div class="modal-body">

       <form action="<?php echo base_url(); ?>dispatcher/saveZone" id="sendmail" method="post">

                                                      

                                                <div class="row">

                                              <input type="hidden" name="json" id="json" value="">

                                       

                                                 <div style="" class="form-group col-md-12">

                                                         <input type="text" name="title" placeholder="Zone Name" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

                                                

                                                <div style="" class="form-group col-md-12">

                                                         <input type="number" name="time" placeholder="Job Dispatch Time In Minutes" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

                                                 

                                                

                                               

                                                </div>

                                                       <div class="row">

                                             

                                                <button type="submit" id="send" class="btn btn-primary " href="javascript:void(0)" style="margin-left: 43%;">

                                                    Submit

                                                </button>

                                                

                                               

                                                <span>&nbsp;</span>

                                                </div> 

                                                    

                                                </form>

      </div>

      <div class="modal-footer">

     

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>

                <?php include "includes/dispatch_footer.php"?>

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>  

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



  



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>



<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>



<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>







<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->







<!-- this page specific scripts -->



<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>







<!-- theme scripts -->



<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>



<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>  







<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>



<!-- this page specific inline scripts -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARycrYqBtU2Eb69rG0hIMxa32DTD28S4o&libraries=drawing,places"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.js"></script>



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- this page specific inline scripts -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>

    $('.select2').select2();

</script>

<script>

$('#send').click(function(){

   

   

        var title=$('#title').val();

        var time=$('#time').val();

        var zipcode=$('#zipcode').val();

        if(title=='')

        {

            alert("Please Enter Zone Name");

        }else  if(zipcode=='')

        {

            alert("Please Enter Post Code");

        }else{

        $.ajax({



            type: "POST",



            url: "<?php echo base_url(); ?>dispatcher/saveZone",



            data: {title: title,time: time,zipcode: zipcode},



            success: function (result) {

                if(result==1)

                {

                    alert("Post Code Already in use.");

                }else{

            alert("Added Successfully");

                }

             location.href='<?php echo base_url(); ?>dispatcher/indexzone';



            }

        }); 

        }

});

$('.date').click(function(){

    //alert();

    $(this).datepicker({ dateFormat: 'dd/mm/yy' }).focus();

});

( function() {

    $('.date').datepicker();

 //   $( ".date" ).datepicker({ dateFormat: 'dd/mm/yy' });

  } );

 var options = {

	map: "#map",

	country: 'in',

	mapOptions: {

		streetViewControl: false,

		//mapTypeId : google.maps.MapTypeId.HYBRID

	},

	markerOptions: {

		draggable: true

	}

 }

$("#pac-input").geocomplete().bind("geocode:result", function(event, latLng){

  console.log(latLng);

  console.log(latLng.geometry.location.lat());

   console.log(latLng.geometry.location.lng());

   

	initMap(latLng.geometry.location.lat(),latLng.geometry.location.lng());

});





      // This example requires the Drawing library. Include the libraries=drawing

      // parameter when you first load the API. For example:

      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">



  function initMap(lat,lng) {

  var map = new google.maps.Map(document.getElementById('map'), {

    center: {

      lat: lat,

      lng: lng

    },

    zoom: 12

  });



  var drawingManager = new google.maps.drawing.DrawingManager({

    drawingMode: google.maps.drawing.OverlayType.POLYGON,

    drawingControl: true,

    drawingControlOptions: {

      position: google.maps.ControlPosition.TOP_CENTER,

      drawingModes: [

        google.maps.drawing.OverlayType.POLYGON,

       

      ]

    },

    markerOptions: {

      icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'

    },

    circleOptions: {

      fillColor: '#ffff00',

      fillOpacity: 1,

      strokeWeight: 5,

      clickable: false,

      editable: true,

      zIndex: 1

    }

  });

    function update_polygon_closure(polygon, i){

        return function(event){

           polygon.getPath().setAt(i, event.latLng); 

        }

    }

  

   <?php

  $arrayMap=array();

  $i=1;

    foreach ($zone as $key => $value) {

        ?>

    var flightPlanCoordinates<?php echo $i; ?> = [

       

       <?php

       if(!empty($value['poligunData']))

       {

       echo $value['poligunData'];

       }

        ?>

        

       

    ];

       



<?php 

        $arrayMap[]="flightPlanCoordinates$i";

       $i++; 

    } 

   $string= str_replace('"', '', json_encode($arrayMap));



    ?>





 var arrayOfFlightPlans = <?php echo $string; ?>;

  var marker_options = {

        map: map,

      

        flat: true,

        draggable: true,

        raiseOnDrag: false

    };

    

    //Loops through all polyline paths and draws each on the map.

    for (let i = 0; i < arrayOfFlightPlans.length; i++) {

        var flightPath = new google.maps.Polygon({

        path: arrayOfFlightPlans[i],

        geodesic: true,

        strokeColor: 'red',

        strokeOpacity: 1.0,

        strokeWeight: 1,

        });

         marker_options.position = arrayOfFlightPlans[i];

        var point = new google.maps.Marker(marker_options);

 google.maps.event.addListener(point, "drag", update_polygon_closure(flightPath, i));

        flightPath.setMap(map);

    }

  

  drawingManager.setMap(map);

  var dataLayer = new google.maps.Data();

  // from https://stackoverflow.com/questions/25072069/export-geojson-data-from-google-maps

  // from http://jsfiddle.net/doktormolle/5F88D/

  google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {

    switch (event.type) {

      case google.maps.drawing.OverlayType.MARKER:





        dataLayer.add(new google.maps.Data.Feature({

          geometry: new google.maps.Data.Point(event.overlay.getPosition())

        }));

        break;

      case google.maps.drawing.OverlayType.RECTANGLE:

        var b = event.overlay.getBounds(),

          p = [b.getSouthWest(), {

              lat: b.getSouthWest().lat(),

              lng: b.getNorthEast().lng()

            },

            b.getNorthEast(), {

              lng: b.getSouthWest().lng(),

              lat: b.getNorthEast().lat()

            }

          ]

        dataLayer.add(new google.maps.Data.Feature({

          geometry: new google.maps.Data.Polygon([p])

        }));

        break;

      case google.maps.drawing.OverlayType.POLYGON:

        dataLayer.add(new google.maps.Data.Feature({

          geometry: new google.maps.Data.Polygon([event.overlay.getPath().getArray()])

        }));

        break;

      case google.maps.drawing.OverlayType.POLYLINE:

        dataLayer.add(new google.maps.Data.Feature({

          geometry: new google.maps.Data.LineString(event.overlay.getPath().getArray())

        }));

        break;

      case google.maps.drawing.OverlayType.CIRCLE:

        dataLayer.add(new google.maps.Data.Feature({

          properties: {

            radius: event.overlay.getRadius()

          },

          geometry: new google.maps.Data.Point(event.overlay.getCenter())

        }));

        break;

    }

  });

  google.maps.event.addDomListener(document.getElementById('save'), 'click', function() {

    dataLayer.toGeoJson(function(obj) {

      //document.getElementById('geojson').innerHTML = JSON.stringify(obj);

       document.getElementById('json').value = JSON.stringify(obj);

    });

  })

}



function deleteLine()

{

    initMap(-33.8281,148.6779);

    

}

initMap(-33.8281,148.6779);

    </script>

 

 

 

    

<script type="text/javascript">

    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

</script>

<script type="text/javascript" language="javascript" >





    $(document).ready(function() {

        var dataTable = $('#example').DataTable( {

            "processing": true,

            "serverSide": true,

            "order": [[ 3, "desc" ]],

            "columnDefs": [

                {

                    "targets": [ 0 ],

                    "visible": true,

                    "searchable": false,

                    "sortable" :false



                },

                {

                    "targets": [ 1 ],

                    "visible": true,

                    "searchable": true,

                    "sortable" :true



                },

                {

                    "targets": [ 2 ],

                    "visible": true,

                    "searchable": true,

                    "sortable" :true



                },

                {

                    "targets": [ 3 ],

                    "visible": true,

                    "type": "numeric",

                    "searchable": true,

                    "sortable" :false



                },

                {

                    "targets": [ 4 ],

                    "visible": true,

                    "searchable": false,

                    "sortable" :false



                }

            ],

            "ajax":{

                url : '<?php echo base_url(); ?>admin/get_reason_data', // json datasource

                type: "post",  // method  , by default get

                error: function(){  // error handling

                    $(".booking-grid-error").html("");

                    $("#example").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

                    $("#booking-grid_processing").css("display","none");

                }

            }

        } );





        $("#allcheck").on('click',function() { // bulk checked

            var status = this.checked;

            $(".deleteRow").each( function() {

                $(this).prop("checked",status);

            });

        });

    } );

    function delete_reason(){

        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked

            var ids = [];

            $('.deleteRow').each(function(){

                if($(this).is(':checked')) {

                    ids.push($(this).val());

                }

            });

            var ids_string = ids.toString();  // array to string conversion

            $.ajax({

                type: "POST",

                url: "<?php echo base_url(); ?>admin/delete_reason_data",

                data: {data_ids:ids_string},

                success: function(result) {

                    var oTable1 = $('#example').DataTable();

                    oTable1.ajax.reload(null, false);

                },

                async:false

            });

        }

    }

    function delete_single_reason(single_id){

        $('#bookedid').val(single_id);

    }



    function delete_single_reason_action()

    {

        var single_id = $('#bookedid').val();

        $.ajax({

            type: "POST",

            url: "<?php echo base_url(); ?>admin/delete_single_reason_data",

            data: {data_id: single_id},

            success: function (result) {

                var oTable1 = $('#example').DataTable();

                oTable1.ajax.reload(null, false);

            },

            async: false

        });

    }

</script>

</body> 

</html>