<!DOCTYPE html>

<html>

<head>

<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



<title>Manage Zone - Infinite Cab</title>



<!-- bootstrap -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



<!-- RTL support - for demo only -->

<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

<!--

If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

And add "rtl" class to <body> element - e.g. <body class="rtl">

-->



<!-- libraries -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



<!-- global styles -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



<!-- this page specific styles -->

<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

<link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">



<!-- Favicon -->

<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />



<!-- google font libraries -->

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



<!--[if lt IE 9]>

<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

<![endif]-->



<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}
.container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 60%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}
#footer-bar{
  position: fixed;
}


/*.main-box.clearfix.her-part .table-responsive {
    height: 65vh;
}

#mapCanvas {
    height: 100vh !important;}
*/

</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

<?php

include"includes/admin_header.php";

?>

<div id="page-wrapper" class="container">

<div class="row">

<?php

include"includes/admin_sidebar.php";

?>

<div id="content-wrapper">

  <div class="row" style="opacity: 1;">

      <div class="col-lg-12">

          <div class="row">

              <div class="col-lg-12">

                  <div id="content-header" class="clearfix">

                      <div class="pull-left">

                          <h1>Manage Zone</h1>

                      </div>

                      <div class="pull-right">

                          <ol class="breadcrumb">

                              <li><a href="#">Home</a></li>

                              <li class="active"><span>Manage Zone</span></li>

                          </ol>

                      </div>

                  </div>

              </div>

          </div>

        

          <div class="row">

              <div class="col-lg-12">

                  <div class="main-box clearfix">

                      <div class="panel">

                          <div class="panel-body">

                              <h2 class="pull-left">Manage Zone</h2>

                              <div class="filter-block pull-right">

                                  <!--

                                  <a href="add-company.html" class="btn btn-primary pull-right">

                                    <i class="fa fa-plus-circle fa-lg"></i> Add Company

                                  </a>

                                  -->



                                  <a class="btn btn-primary pull-right" href="javascript:void(0)" onclick="window.location.href='createzone'">

                                      <i class="fa fa-plus-circle fa-lg"></i> Create New Zone

                                  </a>

                                  <span>&nbsp;</span>

                                  <!--                                                <div style="margin:0px !important;" class="form-group pull-right">-->

                                  <!--                                                    <input type="text" placeholder="Search..." class="form-control">-->

                                  <!--                                                    <i class="fa fa-search search-icon"></i>-->

                                  <!--                                                </div>-->

                              </div>

                          </div>

                      </div>

                      <div class="main-box-body clearfix">

                        <div class="container">
<h3 class=" text-center">Messaging</h3>
<div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Recent</h4>
            </div>
            <div class="srch_bar">
              <div class="stylish-input-group">
                <input type="text" class="search-bar"  placeholder="Search" >
                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
            </div>
          </div>
          <div class="inbox_chat">
            <div class="chat_list active_chat">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                  <p>Test, which is a new approach to have all solutions 
                    astrology under one roof.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="mesgs">
          <div class="msg_history">
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Test which is a new approach to have all
                    solutions</p>
                  <span class="time_date"> 11:01 AM    |    June 9</span></div>
              </div>
            </div>
            <div class="outgoing_msg">
              <div class="sent_msg">
                <p>Test which is a new approach to have all
                  solutions</p>
                <span class="time_date"> 11:01 AM    |    June 9</span> </div>
            </div>
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Test, which is a new approach to have</p>
                  <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
              </div>
            </div>
            <div class="outgoing_msg">
              <div class="sent_msg">
                <p>Apollo University, Delhi, India Test</p>
                <span class="time_date"> 11:01 AM    |    Today</span> </div>
            </div>
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>We work directly with our designers and suppliers,
                    and sell direct to you, which means quality, exclusive
                    products, at a price anyone can afford.</p>
                  <span class="time_date"> 11:01 AM    |    Today</span></div>
              </div>
            </div>
          </div>
          <div class="type_msg">
            <div class="input_msg_write">
              <input type="text" class="write_msg" placeholder="Type a message" />
              <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
      </div>
      
      
      <p class="text-center top_spac"> Design by <a target="_blank" href="#">Sunil Rajput</a></p>
      
    </div></div>

                          <button style="margin:6px 0px;" class="btn btn-primary pull-left" data-toggle="modal" data-target="#uidemo-modals-alerts-delete-multipaluser" id="multi">Multiple Delete</button>

                          <!--<ul class="pagination pull-right">

                              <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>

                              <li><a href="javascript:void(0);">1</a></li>

                              <li><a href="javascript:void(0);">2</a></li>

                              <li><a href="javascript:void(0);">3</a></li>

                              <li><a href="javascript:void(0);">4</a></li>

                              <li><a href="javascript:void(0);">5</a></li>

                              <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>

                          </ul>-->

                      </div>

                  </div>

              </div>

          </div>

      </div>

  </div>



  <?php include "includes/admin-footer.php"?>

</div>

</div>

</div>

</div>




<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>

<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- this page specific inline scripts -->

<script type="text/javascript">

$(window).load(function() {

$(".cover").fadeOut(2000);

});

$(document).ready(function() {

//CHARTS

function gd(year, day, month) {

return new Date(year, month - 1, day).getTime();

}

});

</script>

<script type="text/javascript" language="javascript" >

$(document).ready(function() {

var dataTable = $('#example').DataTable( {

"processing": true,

"serverSide": true,

"order": [[ 3, "desc" ]],

"columnDefs": [

  {

      "targets": [ 0 ],

      "visible": true,

      "searchable": false,

      "sortable" :false



  },

  {

      "targets": [ 1 ],

      "visible": true,

      "searchable": false,

      "sortable" :false



  },

  {

      "targets": [ 2 ],

      "visible": true,

      "searchable": true,

      "sortable" :true



  },

  {

      "targets": [ 3 ],

      "visible": true,

      "type": "numeric",

      "searchable": true,

      "sortable" :true



  },

  {

      "targets": [ 4 ],

      "visible": true,

      "searchable": true,

      "sortable" :true



  },

  {

      "targets": [ 5 ],

      "visible": true,

      "searchable": false,

      "sortable" :false



  }

],

"ajax":{

  url : '<?php echo base_url(); ?>zone/get_car_data', // json datasource

  type: "post",  // method  , by default get

  error: function(){  // error handling

      $(".booking-grid-error").html("");

      $("#example").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

      $("#booking-grid_processing").css("display","none");

  }

}

} );





$("#allcheck").on('click',function() { // bulk checked

var status = this.checked;

$(".deleteRow").each( function() {

  $(this).prop("checked",status);

});

});

} );

function delete_user(){

if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked

var ids = [];

$('.deleteRow').each(function(){

  if($(this).is(':checked')) {

      ids.push($(this).val());

  }

});

var ids_string = ids.toString();  // array to string conversion

$.ajax({

  type: "POST",

  url: "<?php echo base_url(); ?>zone/delete_car_data",

  data: {data_ids:ids_string},

  success: function(result) {

      var oTable1 = $('#example').DataTable();

      oTable1.ajax.reload(null, false);

  },

  async:false

});

}

}

function delete_zone(single_id){

$('#bookedid').val(single_id);

}

function delete_single_user_action()

{

var single_id = $('#bookedid').val();

$.ajax({

type: "POST",

url: "<?php echo base_url(); ?>zone/delete_zone",

data: {data_id: single_id},

success: function (result) {

  var oTable1 = $('#example').DataTable();

  oTable1.ajax.reload(null, false);

},

async: false

});

}

</script>

</body>

</html>