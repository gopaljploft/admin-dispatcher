<!DOCTYPE html>

<html>

<head>

<meta charset="UTF-8" />

<meta name="google-translate-customization" content="e6d13f48b4352bb5-f08d3373b31c17a6-g7407ad622769509b-12"></meta>

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



<title>Dashboard - Infinite Cab</title>



<!-- bootstrap -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



<!-- RTL support - for demo only -->

<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

<!-- 

If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

And add "rtl" class to <body> element - e.g. <body class="rtl"> 

-->



<!-- libraries -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



<!-- global styles -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



<!-- this page specific styles -->

<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/timeline.css">



<!-- Favicon -->

<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />



<!-- google font libraries -->

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>





<!--[if lt IE 9]>

<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

<![endif]-->

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

<?php

include"includes/dispatch_header.php";

?>

<div id="page-wrapper" class="container">

<div class="row">

<?php

include"includes/dispatch_sidebar.php";

?>

<div id="content-wrapper">

<div class="row">

<div class="col-lg-12">

<div class="row">

<div class="col-lg-12">

<div id="content-header" class="clearfix">

<div class="pull-left">

<ol class="breadcrumb">

<li><a href="#">Home</a></li>

<li class="active"><span>Dashboard</span></li>

</ol>

<h1>Dashboard</h1>

</div>

<?php



$queryCab = $this->db->query("Select count(id) as cars From `cars`")->row_array();

$usersRow = $this->db->query("Select count(id) as total From `userdetails`")->row_array();

$driverRow = $this->db->query("Select count(id) as total From `driver_details`")->row_array();



$bookingRow = $this->db->query("Select count(id) as total From `bookingdetails`")->row_array();



$queryadminlogin = $this->db->query("SELECT SUM(CASE WHEN role = 'dispatch' THEN 1 ELSE 0 END) AS dispatch, SUM(CASE WHEN role = 'bookkeeping' THEN 1 ELSE 0 END) AS bookkeeping FROM adminlogin")->row_array();

$summery='';

$summery="{label: Total Passengers, value:".$usersRow["total"]."},{label: Total Drivers, value:".$driverRow["total"]."},{label: Total Dispatch Users, value:".$queryadminlogin["dispatch"]."},{label: Total BookKeeping Users, value:".$queryadminlogin["bookkeeping"]."},{label: Total Cab, value:".$queryCab["cars"]."}, ";

 $summery = substr($summery, 0, -2);



$bookingJobHistory=$this->db->query("SELECT SUM(CASE WHEN isdevice = 'online' THEN 1 ELSE 0 END) AS online,SUM(CASE WHEN isdevice = 'offload' THEN 1 ELSE 0 END) AS offload, SUM(CASE WHEN isdevice = 'dispatcher' THEN 1 ELSE 0 END) AS dispatcher,SUM(CASE WHEN isdevice = 'street ' THEN 1 ELSE 0 END) AS street , count(id) as total FROM bookingdetails

")->row_array();





$bookingStatus=$this->db->query("SELECT SUM(CASE WHEN status_code = 'completed' THEN 1 ELSE 0 END) AS completed,SUM(CASE WHEN status_code = 'pending' THEN 1 ELSE 0 END) AS pending,SUM(CASE WHEN status_code = 'accepted' THEN 1 ELSE 0 END) AS accepted,SUM(CASE WHEN status_code = 'on-trip' THEN 1 ELSE 0 END) AS running, SUM(CASE WHEN status_code = 'user-cancelled' THEN 1 ELSE 0 END) AS usercancal,SUM(CASE WHEN status_code = 'driver-cancelled' THEN 1 ELSE 0 END) AS drivercancal,SUM(CASE WHEN status_code = 'Drver Not Found' THEN 1 ELSE 0 END) AS noshow,SUM(CASE WHEN booking_status = 'booklater' THEN 1 ELSE 0 END) AS upcom, count(id) as total FROM bookingdetails

")->row_array();





$cabHistory=$this->db->query("SELECT SUM(CASE WHEN driver_booking_status = 'offline' THEN 1 ELSE 0 END) AS offline,SUM(CASE WHEN driver_booking_status = 'booked' THEN 1 ELSE 0 END) AS booked, SUM(CASE WHEN driver_booking_status = 'free' THEN 1 ELSE 0 END) AS free, count(id) as total FROM diver_live_location

")->row_array();

$booking_data='';







$payments=$this->db->query("SELECT date, CASE WHEN paymentMode = 'case' THEN sum(amount) ELSE 0 END AS Cash, CASE WHEN paymentMode = 'Online' THEN sum(amount) ELSE 0 END AS online, sum(amount) AS Total FROM payment_logs GROUP BY MONTH(date) order by date asc")->result_array();

//print_r($payments);die;

$chart_data = '';

foreach($payments as $val)

{

   // echo $val["date"];die;

 $chart_data .= "{ year:'".date("Y-M", strtotime($val["date"]))."', Cash:".$val["Cash"].", Online:".$val["online"].", Total:".$val["Total"]."}, ";

}

$chart_data = substr($chart_data, 0, -2);

//print_r($chart_data);

$query = $this->db->query("Select count(id) as count From `bookingdetails` where status=9");

$row = $query->row('bookigndetails');

?>

<!--<div class="pull-right hidden-xs">

<div class="xs-graph pull-left">

<div class="graph-label">



<b><i class="fa fa-car"></i> <?php echo $row->count; ?></b> Rides

</div>

<div class="graph-content spark-orders"></div>

</div>



<div class="xs-graph pull-left mrg-l-lg mrg-r-sm">

<div class="graph-label">

<?php

$query = $this->db->query("SELECT SUM(final_amount) as sum FROM `bookingdetails`");

$row = $query->row('bookingdetails');

if($row->sum) {

?>

 <b>&dollar; <?php echo $row->sum; ?></b> Earnings

<?php

}else{

?>

<b>&dollar; 0</b> Earnings 

<?php

}

?>

</div>

<div class="graph-content spark-revenues"></div>

</div>

</div>-->

</div>

</div>

</div>



<div class="row">



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="glyphicon glyphicon-user prple"></i>

<?php

$query = $this->db->query("Select count(id) as count From `driver_details`  ");

$row = $query->row('driver_details');

?>

<span class="headline">Total Drivers</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>

<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="glyphicon glyphicon-user rede"></i>

<?php





$querye = $this->db->query("SELECT `driver_details`.`first_name`, `driver_details`.`last_name`, `driver_details`.`rating`, `driver_details`.`dtype`, `driver_details`.`user_name`, `driver_details`.`id` as did, `driver_details`.`status` as dstatus, `driver_details`.`wallet_amount`, `driver_details`.`debit_amount`, `driver_details`.`credit_amount`, `driver_details`.`due_amount`, `driver_details`.`name`, `driver_details`.`car_no`, `driver_details`.`phone`, `driver_details`.`address`, `driver_details`.`license_no`, `diver_live_location`.*

FROM (`driver_details`)

LEFT JOIN `diver_live_location` ON `diver_live_location`.`driver_id` = `driver_details`.`id`

HAVING (diver_live_location.driver_booking_status ='free' or diver_live_location.driver_booking_status='booked')

ORDER BY `driver_details`.`id` DESC")->result_array();

?>

<span class="headline" style="    font-size: 13px;">Online Driver</span>

<span class="value"><?php echo count($querye);?></span>

</a>

</div>

<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="glyphicon glyphicon-user gren"></i>

<?php

$querye = $this->db->query("SELECT `driver_details`.`first_name`, `driver_details`.`last_name`, `driver_details`.`rating`, `driver_details`.`dtype`, `driver_details`.`user_name`, `driver_details`.`id` as did, `driver_details`.`status` as dstatus, `driver_details`.`wallet_amount`, `driver_details`.`debit_amount`, `driver_details`.`credit_amount`, `driver_details`.`due_amount`, `driver_details`.`name`, `driver_details`.`car_no`, `driver_details`.`phone`, `driver_details`.`address`, `driver_details`.`license_no`, `diver_live_location`.*

FROM (`driver_details`)

LEFT JOIN `diver_live_location` ON `diver_live_location`.`driver_id` = `driver_details`.`id`

HAVING (diver_live_location.driver_booking_status ='offline' or diver_live_location.driver_booking_status is null)

ORDER BY `driver_details`.`id` DESC")->result_array();



?>

<span class="headline" style="    font-size: 13px;">Offline Driver</span>

<span class="value"><?php echo count($querye);?></span>

</a>

</div>





<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="glyphicon glyphicon-user yelow"></i>

<?php







$query = $this->db->query("Select count(id) as count From `driver_details` where status='Active'");

$row = $query->row('driver_details');

?>

<span class="headline" style="    font-size: 13px;">Active Driver</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>

<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="glyphicon glyphicon-user prple"></i>

<?php

$querye = $this->db->query("Select count(id) as count From `driver_details` where status='Deactive'");

$rows = $querye->row('driver_details');

?>

<span class="headline" style="    font-size: 13px;">Suspended Drivers</span>

<span class="value"><?php echo $rows->count;?></span>

</a>

</div>







<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="glyphicon glyphicon-user rede"></i>

<?php

$query = $this->db->query("SELECT driver_id, AVG(rating) from driver_feedback  GROUP BY driver_id HAVING AVG(rating) <= 3.5");

$row = $query->row('driver_details');

?>

<span class="headline">Driver Rating < 3.5</span>

<span class="value"><?php echo count($row); ?></span>

</a>

</div>

<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-car gren"></i>



<span class="headline">Total Cabs</span>

<span class="value"><?php echo $queryCab['cars'];?></span>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-car yelow"></i>

<?php







$query = $this->db->query("Select count(id) as count From `cars` where status =1");

$row = $query->row('cars');

?>

<span class="headline" style="    font-size: 13px;">Active Cabs</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>

<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-car prple"></i>

<?php

$queryed = $this->db->query("Select count(id) as count From `cars` where status='0'");

$rows = $queryed->row('cars');

?>

<span class="headline" style="    font-size: 13px;">Suspended Cabs</span>

<span class="value"><?php echo $rows->count;?></span>

</a>

</div>





<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-ticket rede"></i>

<?php

$query = $this->db->query("Select count(id) as count From `bookingdetails` where status_code='completed'");

$row = $query->row('bookingdetails');

?>

<span class="headline">Completed Bookings</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-ticket gren"></i>

<?php

$query = $this->db->query("Select count(id) as count From `bookingdetails` WHERE status_code='accepted'");

$row = $query->row('bookingdetails');

?>

<span class="headline">Running Bookings</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-ticket yelow"></i>

<?php

$query = $this->db->query("Select count(id) as count From `bookingdetails` where booking_status='latter'");

$row = $query->row('bookingdetails');

?>

<span class="headline">Upcoming Bookings</span>

<span class="value"><?php echo $row->count; ?></span>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-ticket prple"></i>

<?php

$query = $this->db->query("Select count(id) as count From `bookingdetails` where status_code='driver-cancelled' or status_code='user-cancelled'");

$row = $query->row('bookingdetails');

?>

<span class="headline">Cancelled Bookings </span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>





<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-user rede"></i>

<?php

$query = $this->db->query("Select count(id) as count From `userdetails` where is_deleted=0");

$row = $query->row('userdetails');

?>

<span class="headline">Total App Users</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-user gren"></i>

<?php

$query = $this->db->query("Select count(id) as count From `userdetails` where user_status='Active'");

$row = $query->row('userdetails');

?>

<span class="headline">Active App Users</span>

<span class="value"><?php echo $row->count;?></span>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-user yelow"></i>

<?php

$queryss = $this->db->query("Select count(id) as count From `userdetails` where user_status='Inactive' and is_deleted=0");

$rowss = $queryss->row('userdetails');

?>

<span class="headline">Blacklisted App Users</span>

<span class="value"><?php echo $rowss->count;?></span>

</a>

</div>





<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-money prple"></i>

<?php

$query = $this->db->query("SELECT SUM(amount) as sum FROM `bookingdetails`");

$row = $query->row('bookingdetails');

?>

<span class="headline">Total Earning</span>

<?php if($row->sum) { ?>

<span class="value">$<?php echo round($row->sum,2);?></span>

<?php } else { ?>

<span class="value">0</span>

<?php } ?>

</a>

</div>



<div class="col-lg-4 col-sm-6 col-xs-12">

<a class="main-box infographic-box colored">

<i class="fa fa-money rede"></i>

<?php

$query = $this->db->query("SELECT SUM(adminCommissions) as sum FROM `payment_logs` where adminPay=1");

$row = $query->row('bookingdetails');

?>

<span class="headline">This Month Earning</span>

<?php if($row->sum) { ?>

<span class="value">$<?php echo $row->sum;?></span>

<?php } else { ?>

<span class="value">0</span>

<?php } ?>

</a>

</div>



</div>

</div>

</div>

                   

             

              <div class="row">

                            

                            <div class="col-lg-6">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                           <h2>Current Jobs Summary</h2>

                                            <div class="col-md-12">

                                                 <canvas id="bookingstatus2"></canvas>

                                            </div>

                                            





            </div>

            </div>

            </div>

            </div>

                            <div class="col-lg-6">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                           <h2>Current Cabs Status</h2>

                                            <div class="col-md-12">

                                                 <canvas id="bookingstatus"></canvas>

                                            </div>

                                            





            </div>

            </div>

            </div>

            </div>

            <div class="col-lg-6">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                           <h2>Current Booking Summary</h2>

                                            <div class="col-md-12">

                                                <canvas id="bookingstatus1"></canvas>

                                                

                                            </div>

                                            





            </div>

            </div>

            </div>

            </div>

            

             </div>

             

            

<?php include "includes/admin-footer.php"?>

</div>	

</div>

</div>

</div>



<div id="config-tool" class="closed" style="display:none;">

<a id="config-tool-cog">

<i class="fa fa-cog"></i>

</a>



<div id="config-tool-options">

<h4>Layout Options</h4>

<ul>

<li>

<div class="checkbox-nice">

<input type="checkbox" id="config-fixed-header" checked />

<label for="config-fixed-header">

Fixed Header

</label>

</div>

</li>

<li>

<div class="checkbox-nice">

<input type="checkbox" id="config-fixed-sidebar" checked />

<label for="config-fixed-sidebar">

Fixed Left Menu

</label>

</div>

</li>

<li>

<div class="checkbox-nice">

<input type="checkbox" id="config-fixed-footer" checked />

<label for="config-fixed-footer">

Fixed Footer

</label>

</div>

</li>

<li>

<div class="checkbox-nice">

<input type="checkbox" id="config-boxed-layout" />

<label for="config-boxed-layout">

Boxed Layout

</label>

</div>

</li>

<li>

<div class="checkbox-nice">

<input type="checkbox" id="config-rtl-layout" />

<label for="config-rtl-layout">

Right-to-Left

</label>

</div>

</li>

</ul>

<br/>

<h4>Skin Color</h4>

<ul id="skin-colors" class="clearfix">

<li>

<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

</a>

</li>

<li>

<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

</a>

</li>

<li>

<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

</a>

</li>

<li>

<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

</a>

</li>

<li>

<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

</a>

</li>

<li>

<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

</a>

</li>

<li>

<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

</a>

</li>

<li>

<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

</a>

</li>

</ul>

</div>

</div>

<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.sparkline.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/modernizr.js"></script>

<script src="<?php echo base_url();?>application/views/js/timeline.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<script type="text/javascript" src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>

<script type="text/javascript" src="https://www.chartjs.org/samples/latest/utils.js"></script>

<?php// print_r($bookingStatus);die; ?>

<!-- this page specific inline scripts -->

<script type="text/javascript">

		var config = {

			type: 'pie',

			data: {

				datasets: [{

					data: [

						<?php echo $bookingStatus['upcom'] ?>,

						<?php echo $bookingStatus['pending'] ?>,

						<?php echo $bookingStatus['accepted'] ?>,

						<?php echo $bookingStatus['running'] ?>,

					<?php echo $bookingStatus['completed'] ?>,

					<?php echo $bookingStatus['usercancal'] ?>,

						<?php echo $bookingStatus['drivercancal'] ?>,

					<?php echo $bookingStatus['noshow'] ?>,

					],

					backgroundColor: [

						window.chartColors.purple,

						window.chartColors.orange,

						window.chartColors.yellow,

						window.chartColors.green,

						window.chartColors.blue,

						window.chartColors.pink,

						window.chartColors.brown,

						window.chartColors.red,

					],

					label: 'Dataset 1'

				}],

				labels: [

					'Upcoming',

					'Pending',

					'Accepted',

					'Running',

					'Completed',

					'User Canceled',

                    'Driver Canceled',

                    'No Showed'

				]

			},

			options: {

				responsive: true

			}

		};

		

		var config1 = {

			type: 'pie',

			data: {

				datasets: [{

					data: [

						<?php echo $cabHistory['booked'] ?>,

						<?php echo $cabHistory['free'] ?>,

						<?php echo $cabHistory['offline'] ?>,

						

				

					],

					backgroundColor: [

						window.chartColors.red,

						window.chartColors.orange,

						window.chartColors.yellow,

					

					

					],

					label: 'Dataset 1'

				}],

				labels: [

					'OnTrip',

					'Vacant',

					'Offline',

					

					

				]

			},

			options: {

				responsive: true

			}

		};

		

		

			var config2 = {

			type: 'pie',

			data: {

				datasets: [{

					data: [

						<?php echo $bookingJobHistory['dispatcher'] ?>,

						<?php echo $bookingJobHistory['online'] ?>,

						<?php echo $bookingJobHistory['offload'] ?>,

						<?php echo $bookingJobHistory['street'] ?>,

				

					],

					backgroundColor: [

						window.chartColors.red,

						window.chartColors.orange,

						window.chartColors.yellow,

						window.chartColors.green,

					

					],

					label: 'Dataset 1'

				}],

				labels: [

					'Dispatcher',

					'Online',

					'Driver OFL',

					'Street Hail',

					

				]

			},

			options: {

				responsive: true

			}

		};



		window.onload = function() {

			var ctx = document.getElementById('bookingstatus1').getContext('2d');

			var ctx1 = document.getElementById('bookingstatus').getContext('2d');

				var ctx2 = document.getElementById('bookingstatus2').getContext('2d');

			window.myPie = new Chart(ctx, config);

				window.myPie = new Chart(ctx1, config1);

					window.myPie = new Chart(ctx2, config2);

		};





$(window).load(function() {

$(".cover").fadeOut(2000);

});

$(document).ready(function() {

//CHARTS

function gd(year, day, month) {

return new Date(year, month - 1, day).getTime();

}

/* SPARKLINE - graph in header */

//var orderValues = [10,8,5,7,4,4,3,8,0,7,10,6];

var orderValues = [<?php echo $push_ride_per_month; ?>];

$('.spark-orders').sparkline(orderValues, {

type: 'bar', 

barColor: '#ced9e2',

height: 25,

barWidth: 6

});



//var revenuesValues = [8,3,2,6,4,9,1,10,8,2,5,8];

var revenuesValues = [<?php echo $push_earning_per_month; ?>];

$('.spark-revenues').sparkline(revenuesValues, {

type: 'bar', 

barColor: '#ced9e2',

height: 25,

barWidth: 6

});



});

</script>

 

</body>

</html>