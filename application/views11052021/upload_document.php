<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Add Car - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>

    <script src="js/html5shiv.js"></script>

    <script src="js/respond.min.js"></script>

    <![endif]-->



    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/admin_header.php";

    ?>

    <div id="page-wrapper" class="container">

        <div class="row">

            <?php

            include"includes/admin_sidebar.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Upload Document</h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Add Car Type</span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                            <h2>Upload Document</h2>

                                        </div>

                                    </div>



                                    <div class="main-box-body clearfix">

                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->

                                        <div class="card">

                            <div class="card-block">



                                <div class="card-block">

                          <form method="post" action="<?php echo base_url(); ?>admin/save_document" name="myForm" id="frmEnquiry" enctype="multipart/form-data">

                                    <div class="form-section">

                                        <input type="hidden"  name="id" value="<?php echo $_GET['id']; ?>">

                                        <div class="Create">

                                          

                                            

                                            <div id="second_formData" style="">

                                                

                                                 <div class="row License">

                                               

                                               <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label class="control-label">Original registration certificate</label>

                                                        <input type="file" id="myFile" required="" accept="image/x-png,image/gif,image/jpeg,application/pdf" name="attachment1" value="" class="form-control">

                                                        <?php



                                                        if(!empty($documentData))

                                                        { 

                                                            

                                                        ?>

                                                        <p><a href="http://admin.infinitecabs.com.au/car_image/<?php echo $documentData['registration_certificate']; ?>" download>Download Document</a></p>

                                                        <?php } ?>

                                                    </div>

                                                </div>

                                                 <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label class="control-label">Valid insurance copy</label>

                                                        <input type="file" id="myFile2" accept="image/x-png,image/gif,image/jpeg,application/pdf" required="" name="attachment2" value="" class="form-control">

                                                        <?php

                                                        if(!empty($documentData))

                                                        {

                                                        ?>

                                                        <p><a href="http://admin.infinitecabs.com.au/car_image/<?php echo $documentData['insurance']; ?>" download>Download Document</a></p>

                                                        <?php } ?>

                                                    </div>

                                                </div>

                                                </div>

                                            

                                              <div class="row">     

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                      <label class="control-label">PUC certificate</label>

                                                      <input type="file" id="myFile1" accept="image/x-png,image/gif,image/jpeg,application/pdf" required="" name="attachment3" value="" class="form-control">

                                                     <?php

                                                        if(!empty($documentData))

                                                        {

                                                        ?>

                                                        <p><a href="http://admin.infinitecabs.com.au/car_image/<?php echo $documentData['puc']; ?>" download>Download Document</a></p>

                                                        <?php } ?>

                                                    </div>

                                                </div>



                                                </div> 

                                              

                                            </div>



                                            <div class="submitting">

                                                <input required="" accept="image/x-png,image/gif,image/jpeg,application/pdf" type="checkbox" id="selectcbox"><span><b>Read &amp; Accept Infinite Cabs Code of Conduct </b></span>

                                              </div>



                                            <div class=" form-actions">

                                            <input type="submit" class="btn btn-success" name="submit" id="subdatasec" value="Submit"> 

                                             <img src="Wait.gif" id="loader-icon" style="text-align: center;margin: auto;width: 13%;display: none;">

                                            </div>

                                                

                                                

                                            

                                            </div>

                                            

                                            



                                        </div>

                                    </div>

                                    </form>

                                </div>

                            </div>

                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php include "includes/admin-footer.php"?>

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<!-- this page specific inline scripts -->

<script type="text/javascript">
    $('#myFile').bind('change', function() {

  //this.files[0].size gets the size of your file.
    //because this is single file upload I use only first index
        var f = this.files[0]

        //here I CHECK if the FILE SIZE is bigger than 8 MB (numbers below are in bytes)
        if (f.size > 8388608 || f.fileSize > 8388608)
        {
           //show an alert to the user
           alert("Allowed file size exceeded. (Max. 8 MB)")

           //reset file upload control
           this.value = null;
        }

});
    $('#myFile1').bind('change', function() {

  //this.files[0].size gets the size of your file.
    //because this is single file upload I use only first index
        var f = this.files[0]

        //here I CHECK if the FILE SIZE is bigger than 8 MB (numbers below are in bytes)
        if (f.size > 8388608 || f.fileSize > 8388608)
        {
           //show an alert to the user
           alert("Allowed file size exceeded. (Max. 8 MB)")

           //reset file upload control
           this.value = null;
        }

});
    $('#myFile2').bind('change', function() {

  //this.files[0].size gets the size of your file.
    //because this is single file upload I use only first index
        var f = this.files[0]

        //here I CHECK if the FILE SIZE is bigger than 8 MB (numbers below are in bytes)
        if (f.size > 8388608 || f.fileSize > 8388608)
        {
           //show an alert to the user
           alert("Allowed file size exceeded. (Max. 8 MB)")

           //reset file upload control
           this.value = null;
        }

});


    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

</script>

<!--	<script>-->

<!--		function validate() {-->

<!--			var x = document.forms["add_user"]["cartype"].value;-->

<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->

<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->

<!--			var filename=document.getElementById('uploadImageFile').value;-->

<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			//alert(extension);-->

<!--		 if(image=='')-->

<!--			{-->

<!--				alert('car Image must be filled out');-->

<!--				return false;-->

<!--			}-->

<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->

<!--//				return true;-->

<!--//			}-->

<!--//-->

<!--//		 else-->

<!--//		 {-->

<!--//			 alert('Not Allowed Extension!');-->

<!--//			 return false;-->

<!--//		 }-->

<!--		else if (x == null || x == "") {-->

<!--				alert("Car Type must be filled out");-->

<!--				return false;-->

<!--			}-->

<!--			 else if (car_rate == null || car_rate == "") {-->

<!--			 alert("car rate must be filled out");-->

<!--			 return false;-->

<!--		 	}-->

<!--		 else if (seating_capacity == null || seating_capacity == "") {-->

<!--			 alert("seating capacity must be filled out");-->

<!--			 return false;-->

<!--		 }-->

<!---->

<!--		}-->

<!--	</script>-->

</body>

</html>