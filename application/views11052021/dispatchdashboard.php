<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="google-translate-customization" content="e6d13f48b4352bb5-f08d3373b31c17a6-g7407ad622769509b-12"></meta>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Dashboard - Infinite Cab</title>

<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />

<!-- RTL support - for demo only -->
<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>
<!-- 
If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />
And add "rtl" class to <body> element - e.g. <body class="rtl"> 
-->

<!-- libraries -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />

<!-- this page specific styles -->
<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/timeline.css">

<!-- Favicon -->
<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />

<!-- google font libraries -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>
<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div class="cover"></div>
<div id="theme-wrapper">
<?php
include"includes/admin_header.php";
?>
<div id="page-wrapper" class="container">
<div class="row">
<?php
include"includes/admin_sidebar.php";
?>
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<div id="content-header" class="clearfix">
<div class="pull-left">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Dashboard</span></li>
</ol>
<h1>Dashboard</h1>
</div>

<div class="pull-right hidden-xs">
<div class="xs-graph pull-left">
<div class="graph-label">
<?php
$query = $this->db->query("Select count(id) as count From `bookingdetails` where status=9");
$row = $query->row('bookigndetails');
?>
<b><i class="fa fa-car"></i> <?php echo $row->count; ?></b> Rides
</div>
<div class="graph-content spark-orders"></div>
</div>

<div class="xs-graph pull-left mrg-l-lg mrg-r-sm">
<div class="graph-label">
<?php
$query = $this->db->query("SELECT SUM(final_amount) as sum FROM `bookingdetails`");
$row = $query->row('bookingdetails');
if($row->sum) {
?>
 <b>&dollar; <?php echo $row->sum; ?></b> Earnings<!--Revenues-->
<?php
}else{
?>
<b>&dollar; 0</b> Earnings <!--Revenues-->
<?php
}
?>
</div>
</div>
</div>
</div>
</div>
</div>


</div>
</div>
 <div class="row">
                             <div class="col-lg-6">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h2>Booking System</h2>
                                        </div>
                                        </div>
                                       <div class="main-box-body clearfix">
                                      <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->
                                        <?php echo form_open_multipart('admin/add_booking',array('id' => 'fromId','class' => '','role' => 'from', 'onsubmit' => 'return validate()')); ?>
                                        <?php if($this->session->flashdata('error_msg')) {
                                            echo $this->session->flashdata('error_msg');
                                            }
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                            <div class="form-group">
                                               
                                                    <input type="text" required  placeholder="Enter Passenger First Name" name="pname" id="inputname" class="form-control" required>
                                                

                                            </div>
                                            </div>
                                             <div class="col-md-6">
                                            <div class="form-group">
                                                
                                                    <input type="text" required  placeholder="Enter Passenger Last Name" name="lname" id="inputname" class="form-control" required>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-md-12 pass-ed">
                                            <div class="form-group">
                                                
                                                    <input type="text" required  placeholder="Enter Passenger Contact" name="contact" id="inputname" class="form-control" required>
                                                </div>
                                            </div>
                                             <div class="row">
                                            <div class="col-md-6">
                                            <div class="form-group">
                                              
                                                    <input type="text" required  placeholder="Enter pickup location" name="plocation" id="pick" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" required  placeholder="Enter Drop Off" name="dlocation" id="drop" class="form-control" required>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-md-12">
                                                <div id="form-group">
                                                    <select class="form-control" id="btype" name="btype">
                                                        <option name="Now">Now</option>
                                                        <option name="Latter">Latter</option>
                                                    </select>
                                                </div>
                                            </div>
                                            </div>
                                            
                                             <div class="row" id="lat" style="display:none;margin-top: 23px;">
                                             <div class="form-group col-md-6">
                                                <div id="inputname" >
                                                     <input type="date"  placeholder="Date" name="date"  class="form-control">
                                                </div>
                                                 </div>
                                                 <div class="form-group col-md-6">
                                                <div id="inputname" >
                                                 <input type="time"  placeholder="Time" name="time"  class="form-control">

                                                </div>
                                                 </div>
                                            </div>
                                            <div class="row" style="    margin-top: 19px;">
                                                 <div class="col-md-12"> 
                                            <div class="form-group">
                                                <div id="cartype1" class="">
                                                    <!-- <input type="text" required placeholder="Enter Car Type" name="car_type_id" id="cartype1" class="form-control"> -->
                                                    <select name="car_type_id" id="cartype" class="form-control" required>
                                                        <option>Select Type</option>
                                                      <?php foreach ($allCarType as $value) { ?>
                                                         <option value="<?php echo $value['cartype'] ?>"><?php echo $value['cartype'] ?>(<?php echo $value['seat_capacity'] ?>)</option>
                                                          <?php 
                                                      }
                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                              </div>
                                              </div> 
                                            <!--  <div class="row">
                                                  <div class="col-md-12">
                                             <div class="form-group">
                                                 <ul class="list-group resp">
                                                </ul>
                                                </div>
                                                 </div>
                                                  </div> -->
                                            

                                        <div class="row">
                                                  <div class="col-md-12">
                                             <div class="form-group">
                                                <div id="inputname" class="">
                                                    <select class="form-control" id="ptype" name="ptype">
                                                        <option value="Cash">Cash</option>
                                                        <option value="online">Online</option>
                                                    </select>
                                                </div>
                                            </div>
                                             </div>
                                            </div>
                             <div class="row" id="card" style="display:none;">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Payment Details
                    </h3>
                    <div class="checkbox pull-right">
                        <label>
                            <input type="checkbox" />
                            Remember
                        </label>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="cardNumber">
                            CARD NUMBER</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="cnumber" id="cardNumber" placeholder="Valid Card Number"
                                required autofocus />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7 col-md-7">
                            <div class="form-group">
                                <label for="expityMonth">
                                    EXPIRY DATE</label>
                                <div class="col-xs-6 col-lg-6 pl-ziro">
                                    <input type="text" class="form-control" name="edate" id="expityMonth" placeholder="MM" required style="    margin-top: 26px;"/>
                                </div>
                                <div class="col-xs-6 col-lg-6 pl-ziro">
                                    <input type="text" class="form-control" name="xyear" id="expityYear" placeholder="YY" required /></div>
                            </div>
                        </div>
                        <div class="col-xs-5 col-md-5 pull-right">
                            <div class="form-group">
                                <label for="cvCode">
                                    CV CODE</label>
                                <input type="password" class="form-control" name="xcv" id="cvCode" placeholder="CV" required />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
           
        </div>
    </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                            <div class="form-group">
                                                <div id="carmodel" class="">
                                                    <textarea class="form-control" name="note"></textarea>
                                                    </div>
                                                </div>
                                                </div>
                                                </div>

                                           
                                                <input type="hidden" name="plat" id="plat">
                                                <input type="hidden" name="plng" id="plng">
                                                <input type="hidden" name="dlat" id="dlat">
                                                <input type="hidden" name="dlng" id="dlng">
                                                <input type="hidden" name="pincode" id="pincode">
                                               
                                               
                                                
                                            <div class="form-group">
                                                <div class="col-lg-offset-1 col-lg-10">
                                                    <button style="display:block;" class="btn btn-block btn-success" name="save" id="notification-trigger-bouncyflip" type="button">
                                                        <span id="category_button" class="content">Confirm Booking</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!--<ul class="pagination pull-right">
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>
                                            <li><a href="javascript:void(0);">1</a></li>
                                            <li><a href="javascript:void(0);">2</a></li>
                                            <li><a href="javascript:void(0);">3</a></li>
                                            <li><a href="javascript:void(0);">4</a></li>
                                            <li><a href="javascript:void(0);">5</a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>
                                        </ul>-->
                                    </div>
                                </div>
                            </div>
                        
                         <div class="col-lg-6">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h2>Offload Bookings</h2>
                                        </div>
                                        </div>
                                       <div class="main-box-body clearfix">
                                        <div class="table-responsive">
                                            <table id="" class="table table-hover table-bordered user-list">
                                                <thead>
                                                <tr>
                                                    <th><a href="javascript:void(0);">User Name</a></th>
                                                    
                                                    <th class="text-center"><a href="javascript:void(0);">From</a></th>
                                                    <th class="text-center"><a href="javascript:void(0);">To</a></th>
                                                    <th class="text-center"><a href="#" class="desc">Date</a></th>
                                                    <th class="text-center">Status</th>   
                                                
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i=1;
                                                    foreach ($result3 as $key => $value) {
                                                      
                                                    
                                                    ?>
                              
                                                    <tr>
                                                        <td><?php echo $value['username']; ?></td>
                                                        
                                                        <td><?php echo $value['pick_address']; ?></td>
                                                        <td><?php echo $value['drop_address']; ?></td>
                                                        <td><?php echo $value['bookingdateandtime']; ?></td>
                                                        <td><?php echo $value['status_code']; ?></td>
                                                      
                                                    </tr>
                                                    <?php $i++; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--<ul class="pagination pull-right">
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>
                                            <li><a href="javascript:void(0);">1</a></li>
                                            <li><a href="javascript:void(0);">2</a></li>
                                            <li><a href="javascript:void(0);">3</a></li>
                                            <li><a href="javascript:void(0);">4</a></li>
                                            <li><a href="javascript:void(0);">5</a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>
                                        </ul>-->
                                    </div>
                                </div>
                            </div>
                        </div>

             <div class="row">
                            <div class="col-lg-12">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h2>Online Driver</h2>
                            <span class="label label-default">Active : <?php echo $driverData['active'] ?></span>
                            <span class="label label-primary">Booked : <?php echo $driverData['booked'] ?></span>
                            <span class="label label-success">Blocked :  <?php echo $driverData['blocked'] ?></span>
                                        </div>
                                        </div>
                                        <div class="main-box-body clearfix">
                                        
                                        <div id="mapContainer">
                                        <div id="mapCanvas"></div>
                                        
                                        </div>
        <!-- jQuery CDN -->
       

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                            <div class="main-box clearfix">
                                 <div class="panel">
                                        <div class="panel-body">
                                              <h2>Current Booking Status</h2>
                                            </div>
                                            
                            </div>
                             <div class="main-box-body clearfix">
                                        <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">On Trip</a></li>
    <li><a data-toggle="tab" href="#menu1">Cancelled Trip</a></li>
    <li><a data-toggle="tab" href="#menu2">Accepted Trip</a></li>
    
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <div class="table-responsive">
  <table id="" class="table table-hover table-bordered user-list">
  <thead>
  <tr>
  <th><a href="javascript:void(0);">User</a></th>
  <th><a href="javascript:void(0);">Booking ID</a></th>
<th class="text-center"><a href="javascript:void(0);">From</a></th>
<th class="text-center"><a href="javascript:void(0);">To</a></th>
  <th><a href="javascript:void(0);">Driver</a></th>
  <th class="text-center">Action</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($result as $val){ ?>
  <tr>
  <td><?php echo $val['username'] ?></td>
  <td><?php echo $val['booking_id'] ?></td>
   <td><?php echo $val['pick_address'] ?></td>
    <td><?php echo $val['drop_address'] ?></td>
  
  <td><?php echo $val['user_name'] ?></td>
  
  <td>

  <a onclick="window.location.href='view_booking_details?id=<?php echo $val['booking_id'] ?>'" href="javascript:void(0);" class="table-link">
  <span class="fa-stack">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
  </span>
  </a>
  </td>
  </tr>
<?php } ?>
                  </tbody>
  </table>
  </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="table-responsive">
  <table id="" class="table table-hover table-bordered user-list">
  <thead>
  <tr>
  <th><a href="javascript:void(0);">User</a></th>
  <th><a href="javascript:void(0);">Booking ID</a></th>
  <th><a href="javascript:void(0);">From</a></th>
    <th><a href="javascript:void(0);">To</a></th>
  <th><a href="javascript:void(0);">Driver</a></th>
  <th class="text-center">Action</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($result1 as $val2){ ?>
  <tr>
  <td><?php echo $val2['username'] ?></td>
  <td><?php echo $val2['booking_id'] ?></td>
    <td><?php echo $val2['pick_address'] ?></td>
    <td><?php echo $val2['drop_address'] ?></td>
  <td><?php echo $val2['user_name'] ?></td>
  
  <td>

  <a onclick="window.location.href='view_booking_details?id=<?php echo $val2['booking_id'] ?>'" href="javascript:void(0);" class="table-link">
  <span class="fa-stack">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
  </span>
  </a>
  </td>
  </tr>
<?php } ?>
                  </tbody>
  </table>
  </div>
    </div>
    <div id="menu2" class="tab-pane fade">
      <div class="table-responsive">
  <table id="" class="table table-hover table-bordered user-list">
  <thead>
  <tr>
  <th><a href="javascript:void(0);">User</a></th>
  <th><a href="javascript:void(0);">Booking ID</a></th>
    <th><a href="javascript:void(0);">From</a></th>
      <th><a href="javascript:void(0);">To</a></th>
  <th><a href="javascript:void(0);">Driver</a></th>
  <th class="text-center">Action</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($result2 as $val1){ ?>
  <tr>
  <td><?php echo $val1['username'] ?></td>
  <td><?php echo $val1['booking_id'] ?></td>
   <td><?php echo $val1['pick_address'] ?></td>
    <td><?php echo $val1['drop_address'] ?></td>
  <td><?php echo $val1['user_name'] ?></td>
  
  <td>

  <a onclick="window.location.href='view_booking_details?id=<?php echo $val1['booking_id'] ?>'" href="javascript:void(0);" class="table-link">
  <span class="fa-stack">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
  </span>
  </a>
  </td>
  </tr>
<?php } ?>
                  </tbody>
  </table></div>
    </div>
   
  </div>
</div>
      
                            </div>
                            </div>
                           
                        </div>
                        
                        <div class="row">
                             <div class="col-lg-12">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h2>Last Completed Bookings</h2>
                                        </div>
                                        </div>
                                       <div class="main-box-body clearfix">
                                        <div class="table-responsive">
                                            <table id="" class="table table-hover table-bordered user-list">
                                                <thead>
                                                <tr>
                                                    <th><input type="checkbox" name="allcheck" id="allcheck"></th>
                                                    <th><a href="javascript:void(0);">User Name</a></th>
                                                    <th><a href="javascript:void(0);">User ID</a></th>
                                                    <th><a href="javascript:void(0);">Booking ID</a></th>
                                                    <th><a href="javascript:void(0);">Taxi Type</a></th>
                                                    <th class="text-center"><a href="javascript:void(0);">From</a></th>
                                                    <th class="text-center"><a href="javascript:void(0);">To</a></th>
                                                    <th class="text-center"><a href="#" class="desc">Date</a></th>
                                                    <th class="text-center">Status</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i=1;
                                                    foreach ($booking as $key => $value) {
                                                      
                                                    
                                                    ?>
                              
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $value['username']; ?></td>
                                                        <td><?php echo $value['user_id']; ?></td>
                                                        <td><?php echo $value['id']; ?></td>
                                                        <td><?php echo $value['taxi_type']; ?></td>
                                                        <td><?php echo $value['pick_address']; ?></td>
                                                        <td><?php echo $value['drop_address']; ?></td>
                                                        <td><?php echo $value['bookingdateandtime']; ?></td>
                                                        <td><?php echo $value['status_code']; ?></td>
                                                        <td>

            <a onclick="window.location.href='view_booking_details?id=<?php echo $value['id']; ?>'" href="javascript:void(0);" class="table-link">
                <span class="fa-stack">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <!-- <a data-target="#uidemo-modals-alerts-delete-user" data-toggle="modal" class="table-link danger" href="javascript:void(0);" onclick="delete_single_user(1)">
                <span class="fa-stack">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                </span>
            </a> --></td>
                                                    </tr>
                                                    <?php $i++; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <button style="margin:6px 0px;" class="btn btn-primary pull-left" data-toggle="modal" data-target="#uidemo-modals-alerts-delete-multipaluser" id="multi">Multiple Delete</button>
                                        <!--<ul class="pagination pull-right">
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>
                                            <li><a href="javascript:void(0);">1</a></li>
                                            <li><a href="javascript:void(0);">2</a></li>
                                            <li><a href="javascript:void(0);">3</a></li>
                                            <li><a href="javascript:void(0);">4</a></li>
                                            <li><a href="javascript:void(0);">5</a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>
                                        </ul>-->
                                    </div>
                                </div>
                            </div>
                        </div>
<?php include "includes/admin-footer.php"?>
</div>	
</div>
</div>
</div>
<style>
#mapCanvas {
  width: 100%;
  height: 600px;
  margin: 0 auto;
}
#mapLegend {
  background: #fdfdfd;
  color: #3c4750;
  padding: 0 10px 0 10px;
  margin: 10px;
  font-weight: bold;
  filter: alpha(opacity=80);
  opacity: 0.8;
  border: 2px solid #000;
}
#mapLegend div {
  height: 40px;
  line-height: 25px;
  font-size: 1.2em;
}
#mapLegend div img {
  float: left;
  margin-right: 10px;
}
#mapLegend h2 {
  text-align: center
}
</style>
          
<div id="config-tool" class="closed" style="display:none;">
<a id="config-tool-cog">
<i class="fa fa-cog"></i>
</a>

<div id="config-tool-options">
<h4>Layout Options</h4>
<ul>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-header" checked />
<label for="config-fixed-header">
Fixed Header
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-sidebar" checked />
<label for="config-fixed-sidebar">
Fixed Left Menu
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-footer" checked />
<label for="config-fixed-footer">
Fixed Footer
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-boxed-layout" />
<label for="config-boxed-layout">
Boxed Layout
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-rtl-layout" />
<label for="config-rtl-layout">
Right-to-Left
</label>
</div>
</li>
</ul>
<br/>
<h4>Skin Color</h4>
<ul id="skin-colors" class="clearfix">
<li>
<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
</a>
</li>
<li>
<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
</a>
</li>
</ul>
</div>
</div>
<!-- global scripts -->
<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->

<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>
<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->

<!-- this page specific scripts -->
<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>
<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>application/views/js/modernizr.js"></script>
<script src="<?php echo base_url();?>application/views/js/timeline.js"></script>

<!-- theme scripts -->
<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>
<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<?php
$query = $this->db->query("SELECT YEAR(book_create_date_time) AS y, MONTH(book_create_date_time) AS m, count(id) AS count FROM `bookingdetails` WHERE status=9 AND YEAR(book_create_date_time) = YEAR(CURDATE()) GROUP BY y,m");
if($query){
$ride_per_month = array();
foreach($query->result() as $row)
{
for($i=1;$i<=12;$i++){
if($row->m==$i)
{
if($row->count!=0 || $row->count!=NULL){
$ride_per_month[$i]=$row->count;
}
else{
$ride_per_month[$i]=0;
}
}
else{
$ride_per_month[$i]=0;
}
}
}
}
$push_ride_per_month=implode(',',$ride_per_month);

$query1 = $this->db->query("SELECT YEAR(book_create_date_time) AS y, MONTH(book_create_date_time) AS m, sum(final_amount) AS sum FROM `bookingdetails` WHERE status=9 AND YEAR(book_create_date_time) = YEAR(CURDATE()) GROUP BY y,m");
if($query1){
$earning_per_month = array();
foreach($query1->result() as $row1)
{
for($i=1;$i<=12;$i++){
if($row1->m==$i)
{
if($row1->sum!=0 || $row1->sum!=NULL){
$earning_per_month[$i]=$row1->sum;
}
else{
$earning_per_month[$i]=0;
}
}
else{
if(!isset($earning_per_month[$i]))
{
$earning_per_month[$i]=0;
}
}
}
}
}
$push_earning_per_month=implode(',',$earning_per_month);
?>
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyARycrYqBtU2Eb69rG0hIMxa32DTD28S4o"></script>

 <script src="https://w3schools.in/demo/geocomplete/js/jquery.geocomplete.min.js"></script>
<!-- this page specific inline scripts -->
<script type="text/javascript">
$('#ptype').change(function(){
   if($(this).val()=='online')
   {
       $('#card').show();
   }else{
       $('#card').hide();
   }
});
$('#btype').change(function(){
    
   if($(this).val()!='Now')
   {
       $('#lat').show();
   }else{
       $('#lat').hide();
   }
});
$('#cartype').change(function(){
    var car=$(this).val();
    var user_pickup_latitude=$('#plat').val();
    var user_pickup_longlatitude=$('#plng').val();
    var user_drop_latitude=$('#dlat').val();
    var user_drop_longlatitude=$('#dlng').val();
    $.ajax({
        url: "<?php echo base_url() ?>admin/serchUserTexiAmount",
        type: "post",
        data: {car:car,user_pickup_latitude:user_pickup_latitude,user_pickup_longlatitude:user_pickup_longlatitude,user_drop_latitude:user_drop_latitude,user_drop_longlatitude:user_drop_longlatitude} ,
        success: function (obj) {
            console.log(obj);
          
             $(".resp").html(obj); 
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    }); 
});
          $("#pick").geocomplete({

            country: 'AUS',

            details: ".geo-details",

            detailsAttribute: "data-geo"

         

         }).bind("geocode:result", function (event, result) {            
            var parsedResult=$(result.adr_address);
            var postal_code=parsedResult.filter('.postal-code').text();
            console.log(result.geometry.location.lat());
          $("#plat").val(result.geometry.location.lat());
          $("#plng").val(result.geometry.location.lng());
          $('#pincode').val(postal_code);

         });

         $("#drop").geocomplete({

         country: 'AUS',

         details: ".geo-details",

         detailsAttribute: "data-geo"

         

         }).bind("geocode:result", function (event, result) {            

         $("#dlat").val(result.geometry.location.lat());
          $("#dlng").val(result.geometry.location.lng());

         });

         $('#drop').focusout(function(){

         

                     GetRoute();

         });
    $('#notification-trigger-bouncyflip').click(function(){
        var formdata=$('#fromId').serialize();
        $.ajax({
        url: "<?php echo base_url() ?>admin/add_booking",
        type: "post",
        data: formdata ,
        success: function (response) {

          if(response=='')
          {
            alert("Something Went Wrong");
          }else{
            $('#bookingId').val(response);
            $('#bookinmodal').modal('show');;
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
        });

    var refInterval = window.setInterval('update()', 10000); // 30 seconds

var update = function() {
    var bookingId=$('#bookingId').val();
    if(bookingId!='')
    {
    $.ajax({
        type : 'POST',
        url : '<?php echo base_url() ?>web_service/searchDriver',
        data:{ booking_id:bookingId },
        success : function(data){
            var datanew = JSON.parse(data);
            if(datanew.statusCode==201)
            {
                 $('#search').show();
            }
            else if(datanew.statusCode==200)
            {
            $('#search').hide();
            $('#bid').text(datanew.bookingData.bookingId);
            $('#dname').text(datanew.bookingData.driverName);
            $('#padd').text(datanew.bookingData.pickupLocation);
            $('#dadd').text(datanew.bookingData.dropLocation);
            $('#cnum').text(datanew.bookingData.drivercar_no);
            $('#bookingdata').show();
        }
        else if(datanew.statusCode==202)
            {
                alert(datanew.message);
            }
           console.log(datanew.bookingData.bookingId);
        },
    });
}
};
update();

$(window).load(function() {
$(".cover").fadeOut(2000);
});
$(document).ready(function() {
//CHARTS
function gd(year, day, month) {
return new Date(year, month - 1, day).getTime();
}
/* SPARKLINE - graph in header */
//var orderValues = [10,8,5,7,4,4,3,8,0,7,10,6];
var orderValues = [<?php echo $push_ride_per_month; ?>];
$('.spark-orders').sparkline(orderValues, {
type: 'bar', 
barColor: '#ced9e2',
height: 25,
barWidth: 6
});

//var revenuesValues = [8,3,2,6,4,9,1,10,8,2,5,8];
var revenuesValues = [<?php echo $push_earning_per_month; ?>];
$('.spark-revenues').sparkline(revenuesValues, {
type: 'bar', 
barColor: '#ced9e2',
height: 25,
barWidth: 6
});

});
</script>
 <script type="text/javascript">
 function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
    map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
   
    var markers = [
        <?php
    foreach ($query5 as $key => $value) {
       
        ?>
        ['<?php echo $value['name'] ?>', <?php echo $value['latitude'] ?>, <?php echo $value['longlatitude'] ?>],
        <?php } ?>
    ];
                        
    // Info window content
    var infoWindowContent = [
        <?php
    foreach ($query5 as $key => $value) {
       
        ?>
        ['<div class="info_content">' +
        '<h3><?php echo $value['name'] ?>,<?php echo $value['car_no'] ?></h3>' +
        '<p><strong>Contact NO : <?php echo $value['phone'] ?></strong></p>'+'<p><strong>Car No : <?php echo $value['car_no'] ?></strong></p>'+' <p><strong>Driver Status : <?php echo $value['driver_booking_status'] ?></strong></p>'+' <p><strong>Car Type : <?php echo $value['car_type'] ?></strong></p>'+' <p><strong>License No : <?php echo $value['license_no'] ?></strong></p>'+' <p><strong>Lat : <?php echo $value['latitude'] ?></strong></p>'+' <p><strong>Lng : <?php echo $value['longlatitude'] ?></strong></p> '+ '</div>'],
       <?php } ?>
    ];
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            icon:"http://admin.infinitecabs.com.au/upload/car.png"
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(6);
        google.maps.event.removeListener(boundsListener);
    });
 }

// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);

</script> 

        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">
        </script>
</body>
</html> 