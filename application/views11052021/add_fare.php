
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Add New Tariff - Infinite Cab</title>

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />

    <!-- RTL support - for demo only -->
    <script src="js/demo-rtl.js"></script>
    <!--
    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />
    And add "rtl" class to <body> element - e.g. <body class="rtl">
    -->

    <!-- libraries -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />

    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />
        <link rel="stylesheet" href="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css" type="text/css" />

    <!-- Favicon -->
    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />

    <!-- google font libraries -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">\
    .checkbox {
  padding-left: 20px; }
  .checkbox label {
    display: inline-block;
    position: relative;
    padding-left: 5px; }
    .checkbox label::before {
      content: "";
      display: inline-block;
      position: absolute;
      width: 17px;
      height: 17px;
      left: 0;
      margin-left: -20px;
      border: 1px solid #cccccc;
      border-radius: 3px;
      background-color: #fff;
      -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
      -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
      transition: border 0.15s ease-in-out, color 0.15s ease-in-out; }
    .checkbox label::after {
      display: inline-block;
      position: absolute;
      width: 16px;
      height: 16px;
      left: 0;
      top: 0;
      margin-left: -20px;
      padding-left: 3px;
      padding-top: 1px;
      font-size: 11px;
      color: #555555; }
  .checkbox input[type="checkbox"] {
    opacity: 0; }
    .checkbox input[type="checkbox"]:focus + label::before {
      outline: thin dotted;
      outline: 5px auto -webkit-focus-ring-color;
      outline-offset: -2px; }
    .checkbox input[type="checkbox"]:checked + label::after {
      font-family: 'FontAwesome';
      content: "\f00c"; }
    .checkbox input[type="checkbox"]:disabled + label {
      opacity: 0.65; }
      .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed; }
  .checkbox.checkbox-circle label::before {
    border-radius: 50%; }
  .checkbox.checkbox-inline {
    margin-top: 0; }

.checkbox-primary input[type="checkbox"]:checked + label::before {
  background-color: #428bca;
  border-color: #428bca; }
.checkbox-primary input[type="checkbox"]:checked + label::after {
  color: #fff; }

.checkbox-danger input[type="checkbox"]:checked + label::before {
  background-color: #d9534f;
  border-color: #d9534f; }
.checkbox-danger input[type="checkbox"]:checked + label::after {
  color: #fff; }

.checkbox-info input[type="checkbox"]:checked + label::before {
  background-color: #5bc0de;
  border-color: #5bc0de; }
.checkbox-info input[type="checkbox"]:checked + label::after {
  color: #fff; }

.checkbox-warning input[type="checkbox"]:checked + label::before {
  background-color: #f0ad4e;
  border-color: #f0ad4e; }
.checkbox-warning input[type="checkbox"]:checked + label::after {
  color: #fff; }

.checkbox-success input[type="checkbox"]:checked + label::before {
  background-color: #5cb85c;
  border-color: #5cb85c; }
.checkbox-success input[type="checkbox"]:checked + label::after {
  color: #fff; }
    .modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}



  </style> 
</head>  
<body>
<div class="cover"></div> 
<div id="theme-wrapper">    
    <?php
    include"includes/admin_header.php";
    ?>
    <div id="page-wrapper" class="container">
        <div class="row">
            <?php
            include"includes/admin_sidebar.php";
            ?>
            <div id="content-wrapper">
                <div class="row" style="opacity: 1;">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="content-header" class="clearfix">
                                    <div class="pull-left">
                                        <h1>Add New Tariff</h1>
                                    </div>
                                    <div class="pull-right">
                                        <ol class="breadcrumb">
                                            <li><a href="#">Home</a></li>
                                            <li class="active"><span>Add New Tariff </span></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h2>Add New Tariff</h2>
                                        </div>
                                    </div>

                                    <div class="main-box-body clearfix">
                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->
                                        <?php echo form_open_multipart('admin/save_fare',array('id' => 'formAddUser','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>
                                        <?php if($this->session->flashdata('error_msg')) {
                                            echo $this->session->flashdata('error_msg');
                                            }
                                        ?>
                                       <!-- <div class="form-group">
                                                <label class="col-lg-2 control-label" for="carrate">Select Cab Type</label>
                                                <div id="cartype1" class="col-lg-10">
                                                    <select name="car_type_id" class="form-control" required>
                                                        <option>Select Type</option>
                                                      <?php foreach ($allCarType as $value) { ?>
                                                         <option <?php if($fareData['cabId']==$value['cab_id']){ echo 'selected'; } ?> value="<?php echo $value['cab_id'] ?>"><?php echo $value['cartype'] ?></option>
                                                          <?php 
                                                      }
                                                      ?>
                                                    </select>
                                                </div>
                                            </div>-->
                                            
                                         <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                                          
                                    
                                       <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">Tariff Title</label>
                                                <div id="carmodel" class="col-lg-10">
                                                    <input autocomplete="off" type="text" value="<?php echo $fareData['title']; ?>" required placeholder="Tariff Title" name="title" id="title" class="form-control" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">Start Time</label>
                                                <div id="carmodel" class="col-lg-10">
                                                    <input autocomplete="off" type="text" value="<?php echo $fareData['startTime']; ?>" required placeholder="Start Time" name="startTimt" id="stime" class="form-control" required>
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">End Time</label>
                                                <div id="make" class="col-lg-10">
                                                    <input type="text" autocomplete="off" value="<?php echo $fareData['endTime']; ?>" required placeholder="End Time" name="endtime" id="etime" class="form-control" required>
                                                </div>
                                            </div>
                                         <div class="form-group">
                                                <label class="col-lg-2 control-label" for="carrate">Seating Capacity</label>
                                                <div id="cartype1" class="col-lg-10">
                                                    <select name="seat" class="form-control" required>
                                                        <option value="0">--Select--</option>
                                                      
                                                         <option <?php if($fareData['capacity']==4){ echo 'selected';} ?> value="4">1-4</option>
                                                         <option <?php if($fareData['capacity']==7){ echo 'selected';} ?> value="7">1-7</option>
                                                         <option <?php if($fareData['capacity']==11){ echo 'selected';} ?> value="11">1-11</option>
                                                         
                                                    </select>
                                                </div>
                                            </div>
                                        
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Flagfall($)</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <input type="text" value="<?php echo $fareData['Flagfall']; ?>"  required  placeholder="Flagfall" name="flagfall" id="flagfall" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                           
                                    
                                           

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">Distance Charge/Km($)</label>
                                                <div id="carmodel" class="col-lg-10">
                                                    <input type="text" value="<?php echo $fareData['DistanceChargeKm']; ?>" required placeholder="Distance Charge/Km" name="distance" id="distance" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">Waiting Charge($)</label>
                                                <div id="carmodel" class="col-lg-10">
                                                    <input type="text" value="<?php echo $fareData['wait']; ?>" required placeholder="Waiting Charge" name="wait" id="distance" class="form-control" required>
                                                </div>
                                            </div>

                            

                                           <!-- <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">Time Charge/Km($)</label>
                                                <div id="colour" class="col-lg-10">
                                                    <input type="text" value="<?php echo $fareData['TimeChargeKm']; ?>" required placeholder="Time Charge/Km" name="time" id="time" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="seatingcapecity">Tolls/Booking($)</label>
                                                <div id="colour" class="col-lg-10">
                                                    <input type="text" value="<?php echo $fareData['TollsBooking']; ?>" required placeholder="Tolls/Booking" name="tolls" id="tolls" class="form-control" required>
                                                </div>
                                            </div>-->
                                            
                                             

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="carrate">If Public Holiday Move To</label>
                                                <div id="cartype1" class="col-lg-10">
                                                    <select name="move_to" class="form-control" required>
                                                        <option value="0">--Select--</option>
                                                      <?php foreach ($allCarType as $value) { ?>
                                                         <option <?php if($fareData['move_to']==$value['id']){ echo 'selected'; } ?> value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                                                          <?php 
                                                      }
                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php
                                                $days=explode(",",$fareData['days']);
                                            ?>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="carrate">Select Days</label>
                                                <div id="cartype1" class="col-lg-10">
                                                   
                                                        <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="1" name="days[]" <?php if(in_array('1',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox2">Monday</label>
                                                        </div>
                                                         <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="2" name="days[]" <?php if(in_array('2',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox3">Tuesday</label>
                                                        </div>
                                                         <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox4" value="3" name="days[]" <?php if(in_array('3',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox4">Wednesday</label>
                                                        </div>
                                                         <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox5" value="4" name="days[]" <?php if(in_array('4',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox5">Thursday</label>
                                                        </div>
                                                         <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox6" value="5" name="days[]" <?php if(in_array('5',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox6">Friday</label>
                                                        </div>
                                                         <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox7" value="6" name="days[]" <?php if(in_array('6',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox7">Saturday</label>
                                                        </div>
                                                         <div class="checkbox checkbox-success checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="7" name="days[]" <?php if(in_array('7',$days)){ echo 'checked' ;} ?>>
                                                            <label for="inlineCheckbox1">Sunday</label>
                                                        </div>
                
                                                    </div>
                                            </div>
                                        

                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-1">
                                                    <button style="display:block;" class="btn btn-success" name="save" id="btn_add" type="button">
                                                        <span id="category_button" class="content">SUBMIT</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include "includes/admin-footer.php"?>
            </div>
        </div>
    </div>
</div>

<div id="config-tool" class="closed" style="display:none;">
    <a id="config-tool-cog">
        <i class="fa fa-cog"></i>
    </a>

    <div id="config-tool-options">
        <h4>Layout Options</h4>
        <ul>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-header" checked />
                    <label for="config-fixed-header">
                        Fixed Header
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-sidebar" checked />
                    <label for="config-fixed-sidebar">
                        Fixed Left Menu
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-footer" checked />
                    <label for="config-fixed-footer">
                        Fixed Footer
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-boxed-layout" />
                    <label for="config-boxed-layout">
                        Boxed Layout
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-rtl-layout" />
                    <label for="config-rtl-layout">
                        Right-to-Left
                    </label>
                </div>
            </li>
        </ul>
        <br/>
        <h4>Skin Color</h4>
        <ul id="skin-colors" class="clearfix">
            <li>
                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
                </a>
            </li>
            <li>
                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- global scripts -->
<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->

<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>
<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->

<!-- this page specific scripts -->
<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>
<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>

<!-- theme scripts -->
<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>
<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>
<script src="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>

<!-- this page specific inline scripts -->
<script type="text/javascript">
$('#stime').clockpicker({
    autoclose: true
});
$('#etime').clockpicker({
    autoclose: true
});
    $(window).load(function() {
        $(".cover").fadeOut(2000);
    });
    $(document).ready(function() {
     
      
        //CHARTS
        function gd(year, day, month) {
            return new Date(year, month - 1, day).getTime();
        }
    });
       $('#high').change(function(){
           
           if($(this).val()==1)
           {
               $('.feebox').show();
           }
           else
           {
               $('.feebox').hide();
           }
       });
        $('#btn_add').click(function(){
            
            var cab=$('#car_type_id').val();
            var high=$('#high').val();
            var stime=$('#stime').val();
            var etime=$('#etime').val();
            var flagfall=$('#flagfall').val();
            //var distance=$('#distance').val();
            var tolls=$('#tolls').val();
            var fee=$('#fee').val();
            
            
            if(cab=='')
            {
                alert("Please Select Cab");
            }
            else if(stime=='')
            {
                alert("Please Select Start Time");
            }
            else if(etime=='')
            {
                alert("Please Enter End Time");
            }
            else if(flagfall=='')
            {
                alert("Please Enter Flagfall");
            }else{
            
            $.ajax({
            url: "<?php echo base_url(); ?>admin/save_fare",
            type: "post",
            data: $('#formAddUser').serialize(),
            success: function (response) {

           if(response==1)
           {
               alert("Please select different time.this time is already in Records!");
           }else{
              location.href="fare_manage";
           }
        }
    }); 
            }
        });
</script>
<!--	<script>-->
<!--		function validate() {-->
<!--			var x = document.forms["add_user"]["cartype"].value;-->
<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->
<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->
<!--			var filename=document.getElementById('uploadImageFile').value;-->
<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->
<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->
<!--			//alert(extension);-->
<!--		 if(image=='')-->
<!--			{-->
<!--				alert('car Image must be filled out');-->
<!--				return false;-->
<!--			}-->
<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->
<!--//				return true;-->
<!--//			}-->
<!--//-->
<!--//		 else-->
<!--//		 {-->
<!--//			 alert('Not Allowed Extension!');-->
<!--//			 return false;-->
<!--//		 }-->
<!--		else if (x == null || x == "") {-->
<!--				alert("Car Type must be filled out");-->
<!--				return false;-->
<!--			}-->
<!--			 else if (car_rate == null || car_rate == "") {-->
<!--			 alert("car rate must be filled out");-->
<!--			 return false;-->
<!--		 	}-->
<!--		 else if (seating_capacity == null || seating_capacity == "") {-->
<!--			 alert("seating capacity must be filled out");-->
<!--			 return false;-->
<!--		 }-->
<!---->
<!--		}-->
<!--	</script>-->
</body>
</html>