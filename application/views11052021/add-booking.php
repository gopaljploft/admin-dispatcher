
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Add Car - Infinite Cab</title>

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />

    <!-- RTL support - for demo only -->
    <script src="js/demo-rtl.js"></script>
    <!--
    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />
    And add "rtl" class to <body> element - e.g. <body class="rtl">
    -->

    <!-- libraries -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />

    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

    <!-- Favicon -->
    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />

    <!-- google font libraries -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;} .panel-title {display: inline;font-weight: bold;}
.checkbox.pull-right { margin: 0; }
.pl-ziro { padding-left: 0px; }</style>
</head>
<body>
<div class="cover"></div>
<div id="theme-wrapper">
    <?php
    include"includes/admin_header.php";
    ?>
    <div id="page-wrapper" class="container">
        <div class="row">
            <?php
            include"includes/admin_sidebar.php";
            ?>
            <div id="content-wrapper">
                <div class="row" style="opacity: 1;">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="content-header" class="clearfix">
                                    <div class="pull-left">
                                        <h1>Add Booking </h1>
                                    </div>
                                    <div class="pull-right">
                                        <ol class="breadcrumb">
                                            <li><a href="#">Home</a></li>
                                            <li class="active"><span>Add Booking </span></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h2>Add Booking </h2>
                                        </div>
                                    </div>

                                    <div class="main-box-body clearfix">
                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->
                                        <?php echo form_open_multipart('admin/add_booking',array('id' => 'fromId','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>
                                        <?php if($this->session->flashdata('error_msg')) {
                                            echo $this->session->flashdata('error_msg');
                                            }
                                        ?>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Passenger First Name</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <input type="text" required  placeholder="Enter Passenger First Name" name="pname" id="inputname" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Passenger Last Name</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <input type="text" required  placeholder="Enter Passenger Last Name" name="lname" id="inputname" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Passenger Contact</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <input type="text" required  placeholder="Enter Passenger Contact" name="contact" id="inputname" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Enter pickup location</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <input type="text" required  placeholder="Enter pickup location" name="plocation" id="pick" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Enter Drop Off</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <input type="text" required  placeholder="Enter Drop Off" name="dlocation" id="drop" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Booking Type</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <select class="form-control" id="btype" name="btype">
                                                        <option name="Now">Now</option>
                                                        <option name="Latter">Latter</option>
                                                    </select>
                                                </div>
                                            </div>
                                             <div class="row" id="lat" style="display:none">
                                             <div class="form-group col-md-12">
                                                <label class="col-lg-2 control-label" for="cartype">Booking Date</label>
                                                <div id="inputname" class="col-lg-10">
                                                     <input type="date"  placeholder="Date" name="date"  class="form-control">
                                                </div>
                                                 </div>
                                                 <div class="form-group col-md-12">
                                                <label class="col-lg-2 control-label" for="cartype">Booking Time</label>
                                                <div id="inputname" class="col-lg-10">
                                                 <input type="time"  placeholder="Time" name="time"  class="form-control">

                                                </div>
                                                 </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="carrate"> Car Type</label>
                                                <div id="cartype1" class="col-lg-10">
                                                    <!-- <input type="text" required placeholder="Enter Car Type" name="car_type_id" id="cartype1" class="form-control"> -->
                                                    <select name="car_type_id" id="cartype" class="form-control" required>
                                                        <option>Select Type</option>
                                                      <?php foreach ($allCarType as $value) { ?>
                                                         <option value="<?php echo $value['cartype'] ?>"><?php echo $value['cartype'] ?>(<?php echo $value['seat_capacity'] ?>)</option>
                                                          <?php 
                                                      }
                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                  <label class="col-lg-2 control-label" for="cartype">Esteemed Fare</label>
                                                <div id="inputname" class="col-lg-10">
                                                 <ul class="list-group resp">
                                                </ul>
                                                </div>
                                                 </div>-->
                                            
                                       <div class="form-group">
                                                <label class="col-lg-2 control-label" for="cartype">Payment Type</label>
                                                <div id="inputname" class="col-lg-10">
                                                    <select class="form-control" id="ptype" name="ptype">
                                                        <option value="Cash">Cash</option>
                                                        <option value="online">Online</option>
                                                    </select>
                                                </div>
                                            </div>
                             <div class="row" id="card" style="display:none;">
                                <div class="col-md-2">
                                </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Payment Details
                    </h3>
                    <div class="checkbox pull-right">
                        <label>
                            <input type="checkbox" />
                            Remember
                        </label>
                    </div>
                </div>
                <div class="panel-body">
                    <form role="form">
                    <div class="form-group">
                        <label for="cardNumber">
                            CARD NUMBER</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="cnumber" id="cardNumber" placeholder="Valid Card Number"
                                required autofocus />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7 col-md-7">
                            <div class="form-group">
                                <label for="expityMonth">
                                    EXPIRY DATE</label>
                                <div class="col-xs-6 col-lg-6 pl-ziro">
                                    <label class="sds" for="expityMonth">
                                    EXPIRY DATE</label>
                                    <input type="text" class="form-control" name="edate" id="expityMonth" placeholder="MM" required />
                                </div>
                                <div class="col-xs-6 col-lg-6 pl-ziro">
                                    <input type="text" class="form-control" name="xyear" id="expityYear" placeholder="YY" required /></div>
                            </div>
                        </div>
                        <div class="col-xs-5 col-md-5 pull-right">
                            <div class="form-group">
                                <label for="cvCode">
                                    CV CODE</label>
                                <input type="password" class="form-control" name="xcv" id="cvCode" placeholder="CV" required />
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
           
        </div>
    </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label dn" for="seatingcapecity">Driver Note</label>
                                                <div id="carmodel" class="col-lg-10">
                                                    <textarea class="form-control" name="note"></textarea>
                                                    </div>
                                                </div>

                                           
                                                <input type="hidden" name="plat" id="plat">
                                                <input type="hidden" name="plng" id="plng">
                                                <input type="hidden" name="dlat" id="dlat">
                                                <input type="hidden" name="dlng" id="dlng">
                                                <input type="hidden" name="pincode" id="pincode">
                                               
                                               
                                                
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-1">
                                                    <button style="display:block;" class="btn btn-success" name="save" id="notification-trigger-bouncyflip" type="button">
                                                        <span id="category_button" class="content">SUBMIT</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include "includes/admin-footer.php"?>
            </div>
        </div>
    </div>
</div>

<div id="config-tool" class="closed" style="display:none;">
    <a id="config-tool-cog">
        <i class="fa fa-cog"></i>
    </a>

    <div id="config-tool-options">
        <h4>Layout Options</h4>
        <ul>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-header" checked />
                    <label for="config-fixed-header">
                        Fixed Header
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-sidebar" checked />
                    <label for="config-fixed-sidebar">
                        Fixed Left Menu
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-footer" checked />
                    <label for="config-fixed-footer">
                        Fixed Footer
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-boxed-layout" />
                    <label for="config-boxed-layout">
                        Boxed Layout
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-rtl-layout" />
                    <label for="config-rtl-layout">
                        Right-to-Left
                    </label>
                </div>
            </li>
        </ul>
        <br/>
        <h4>Skin Color</h4>
        <ul id="skin-colors" class="clearfix">
            <li>
                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
                </a>
            </li>
            <li>
                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- global scripts -->
<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->

<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>
<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->

<!-- this page specific scripts -->
<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>
<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>

<!-- theme scripts -->
<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>
<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<!-- Modal -->
<input type="hidden" id="bookingId" name="">
<div id="bookinmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Searching Driver</h4>
      </div>
      <div class="modal-body">
        <div id="search">
            <img src="https://i.gifer.com/YCZH.gif" style="text-align: center;width: 100%;">
        </div>
        <div id="bookingdata" style="display: none;">
            <div class="container">
  <h2 style="text-align: center;">Booking Details</h2>
  <ul class="list-group">
    <li class="list-group-item">Booking Id <span class="badge" id="bid">12</span></li>
    <li class="list-group-item">Driver Name <span class="badge" id="dname">5</span></li>
    <li class="list-group-item">Car Number <span class="badge" id="cnum">3</span></li>
     <li class="list-group-item">PickUp Address <span class="badge" id="padd">3</span></li>
      <li class="list-group-item">Drop Address <span class="badge" id="dadd">3</span></li>
     
  </ul>
</div>
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyARycrYqBtU2Eb69rG0hIMxa32DTD28S4o"></script>

 <script src="https://w3schools.in/demo/geocomplete/js/jquery.geocomplete.min.js"></script>
<!-- this page specific inline scripts -->
<script type="text/javascript">
$('#ptype').change(function(){
   if($(this).val()=='online')
   {
       $('#card').show();
   }else{
       $('#card').hide();
   }
});
$('#btype').change(function(){
    
   if($(this).val()!='Now')
   {
       $('#lat').show();
   }else{
       $('#lat').hide();
   }
});
$('#cartype').change(function(){
    var car=$(this).val();
    var user_pickup_latitude=$('#plat').val();
    var user_pickup_longlatitude=$('#plng').val();
    var user_drop_latitude=$('#dlat').val();
    var user_drop_longlatitude=$('#dlng').val();
    $.ajax({
        url: "<?php echo base_url() ?>admin/serchUserTexiAmount",
        type: "post",
        data: {car:car,user_pickup_latitude:user_pickup_latitude,user_pickup_longlatitude:user_pickup_longlatitude,user_drop_latitude:user_drop_latitude,user_drop_longlatitude:user_drop_longlatitude} ,
        success: function (obj) {
            console.log(obj);
          
             $(".resp").html(obj); 
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    }); 
});
          $("#pick").geocomplete({

            country: 'AUS',

            details: ".geo-details",

            detailsAttribute: "data-geo"

         

         }).bind("geocode:result", function (event, result) {            
            var parsedResult=$(result.adr_address);
            var postal_code=parsedResult.filter('.postal-code').text();
            console.log(result.geometry.location.lat());
          $("#plat").val(result.geometry.location.lat());
          $("#plng").val(result.geometry.location.lng());
          $('#pincode').val(postal_code);

         });

         $("#drop").geocomplete({

         country: 'AUS',

         details: ".geo-details",

         detailsAttribute: "data-geo"

         

         }).bind("geocode:result", function (event, result) {            

         $("#dlat").val(result.geometry.location.lat());
          $("#dlng").val(result.geometry.location.lng());

         });

         $('#drop').focusout(function(){

         

                     GetRoute();

         });
    $('#notification-trigger-bouncyflip').click(function(){
        var formdata=$('#fromId').serialize();
        $.ajax({
        url: "<?php echo base_url() ?>admin/add_booking",
        type: "post",
        data: formdata ,
        success: function (response) {

          if(response=='')
          {
            alert("Something Went Wrong");
          }else{
            $('#bookingId').val(response);
            $('#bookinmodal').modal('show');;
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
        });

    var refInterval = window.setInterval('update()', 10000); // 30 seconds

var update = function() {
    var bookingId=$('#bookingId').val();
    if(bookingId!='')
    {
    $.ajax({
        type : 'POST',
        url : '<?php echo base_url() ?>web_service/searchDriver',
        data:{ booking_id:bookingId },
        success : function(data){
            var datanew = JSON.parse(data);
            if(datanew.statusCode==201)
            {
                 $('#search').show();
            }
            else if(datanew.statusCode==200)
            {
            $('#search').hide();
            $('#bid').text(datanew.bookingData.bookingId);
            $('#dname').text(datanew.bookingData.driverName);
            $('#padd').text(datanew.bookingData.pickupLocation);
            $('#dadd').text(datanew.bookingData.dropLocation);
            $('#cnum').text(datanew.bookingData.drivercar_no);
            $('#bookingdata').show();
        }
        else if(datanew.statusCode==202)
            {
                alert(datanew.message);
            }
           console.log(datanew.bookingData.bookingId);
        },
    });
}
};
update();

    $(window).load(function() {
        $(".cover").fadeOut(2000);
    });
    $(document).ready(function() {
        //CHARTS
        function gd(year, day, month) {
            return new Date(year, month - 1, day).getTime();
        }
    });
</script>
<!--	<script>-->
<!--		function validate() {-->
<!--			var x = document.forms["add_user"]["cartype"].value;-->
<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->
<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->
<!--			var filename=document.getElementById('uploadImageFile').value;-->
<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->
<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->
<!--			//alert(extension);-->
<!--		 if(image=='')-->
<!--			{-->
<!--				alert('car Image must be filled out');-->
<!--				return false;-->
<!--			}-->
<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->
<!--//				return true;-->
<!--//			}-->
<!--//-->
<!--//		 else-->
<!--//		 {-->
<!--//			 alert('Not Allowed Extension!');-->
<!--//			 return false;-->
<!--//		 }-->
<!--		else if (x == null || x == "") {-->
<!--				alert("Car Type must be filled out");-->
<!--				return false;-->
<!--			}-->
<!--			 else if (car_rate == null || car_rate == "") {-->
<!--			 alert("car rate must be filled out");-->
<!--			 return false;-->
<!--		 	}-->
<!--		 else if (seating_capacity == null || seating_capacity == "") {-->
<!--			 alert("seating capacity must be filled out");-->
<!--			 return false;-->
<!--		 }-->
<!---->
<!--		}-->
<!--	</script>-->
</body>
</html>