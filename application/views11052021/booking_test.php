
  <?php
     $this->load->model('Model_admin','home');
     $get_bookingLocation = $this->home->get_bookingLocation($_GET['id']);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Booking Details - Infinite Cab</title>

<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />

<!-- RTL support - for demo only -->
<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>
<!--
If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />
And add "rtl" class to <body> element - e.g. <body class="rtl">
-->

<!-- libraries -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />

<!-- this page specific styles -->
<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

<!-- Favicon -->
<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />

<!-- google font libraries -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

<!--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>-->
<link href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>
<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>
<![endif]-->

<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>
</head>
<body>
<div class="cover"></div>
<div id="theme-wrapper">
<?php
include"includes/admin_header.php";
?>
<div id="page-wrapper" class="container">
<div class="row">
    <?php
    include"includes/admin_sidebar.php";
    ?>
    <div id="content-wrapper">
        <div class="row" style="opacity: 1;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="content-header" class="clearfix">
                            <div class="pull-left">
                                <h1>Driver Details</h1>
                            </div>
                            <div class="pull-right">
                                <ol class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active"><span>Driver Details</span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-box clearfix">
                            <div class="panel" style="margin-bottom:0px;">
                                
                            </div>
                            

                            <div class="main-box-body clearfix">
                    
                           <div id="dvMap" class="col-lg-7" style="height: 500px;width: 100%;"></div>


 
                          
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="row" id="footer-bar" style="opacity: 1;">
            <p id="footer-copyright" class="col-xs-12">
                Powered by Infinite Cab.
            </p>
        </footer>
    </div>
</div>
</div>
</div>

<div id="config-tool" class="closed" style="display:none;">
<a id="config-tool-cog">
<i class="fa fa-cog"></i>
</a>

<div id="config-tool-options">
<h4>Layout Options</h4>
<ul>
    <li>
        <div class="checkbox-nice">
            <input type="checkbox" id="config-fixed-header" checked />
            <label for="config-fixed-header">
                Fixed Header
            </label>
        </div>
    </li>
    <li>
        <div class="checkbox-nice">
            <input type="checkbox" id="config-fixed-sidebar" checked />
            <label for="config-fixed-sidebar">
                Fixed Left Menu
            </label>
        </div>
    </li>
    <li>
        <div class="checkbox-nice">
            <input type="checkbox" id="config-fixed-footer" checked />
            <label for="config-fixed-footer">
                Fixed Footer
            </label>
        </div>
    </li>
    <li>
        <div class="checkbox-nice">
            <input type="checkbox" id="config-boxed-layout" />
            <label for="config-boxed-layout">
                Boxed Layout
            </label>
        </div>
    </li>
    <li>
        <div class="checkbox-nice">
            <input type="checkbox" id="config-rtl-layout" />
            <label for="config-rtl-layout">
                Right-to-Left
            </label>
        </div>
    </li>
</ul>
<br/>
<h4>Skin Color</h4>
<ul id="skin-colors" class="clearfix">
    <li>
        <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
        </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
        </a>
    </li>
    <li>
        <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
        </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
        </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
        </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
        </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
        </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
        </a>
    </li>
</ul>
</div>
</div>
<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>

<!-- global scripts -->
<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->

<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>
<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->

<!-- this page specific scripts -->
<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>
<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>

<!-- theme scripts -->
<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>
<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- this page specific inline scripts -->
<script type="text/javascript">

$(document).ready(function() {
    
    
//CHARTS
function gd(year, day, month) {
    return new Date(year, month - 1, day).getTime();
}
$('.table-responsive').css('display','none');
});
</script>
<script type="text/javascript" language="javascript">
$(window).load(function() {
$(".cover").fadeOut(2000);
});
$(document).ready(function() {
     // get selected car type data
     if($("#select-car option:selected").val()!=''){
        $('#car-calculate').show();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/get_cartype_data",
            data: {cab_id:$("#select-car option:selected").val()},
            success: function (result) {
            var json_arr=JSON.parse(result);
            console.log(json_arr);
            $('#car-calculate').html('<table class="cartype-details" align="center" border="1" style="width:100%;"><tr><td>Car Type:</td><td>'+$("#select-car option:selected").text()+'</td></tr><tr><td>First 5 km:</td><td>'+json_arr.car_rate+'$ /km</td></tr><tr><td>After 5 km:</td><td>'+json_arr.fromintailrate+'$ /km</td></tr><tr><td>Per Minute</td><td>'+json_arr.ride_time_rate+' $/min</td></tr><tr><td>Approx Cost:</td><td id="approx-cost"><span></span>$'+json_arr.car_rate+'</td></tr></table>');
            }
        });
}
    // submit form on submit


if($('#edit-driver-link').length>0)
{
    $('.table-responsive').css('display','none');
}
$('#edit-driver-link').click(function(e)
{
    e.preventDefault();
    $( ".table-responsive" ).toggle();
});
var dataTable = $('#example').DataTable({
    "processing": true,
    "serverSide": true,
    "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
    "columnDefs": [
        {
        orderable: false,
        className: 'select-checkbox',
        targets:   0
        },
        {
            "targets": [ 1 ],
            "visible": true,
            "searchable": true,
            "width": "10%",
            "sortable" :true
        },
        {
            "targets": [ 2 ],
            "visible": true,
            "searchable": true,
            "sortable" :true
        },
        {
            "targets": [ 3 ],
            "visible": true,
            "searchable": true,
            "sortable" :true
        },
        {
            "targets": [ 4 ],
            "visible": true,
            "searchable": true,
            "width": "20%",
            "sortable" :true
        },
        {
            "targets": [ 5 ],
            "visible": true,
            "searchable": true,
            "width": "20%",
            "sortable" :true
        },
        {
            "targets": [ 6 ],
            "visible": true,
            "searchable": true,
            "width": "20%",
            "sortable" :true
        },
        {
            "targets": [ 7 ],
            "visible": true,
            "searchable": false,
            "width": "10%",
            "sortable" :false
        }
    ],
    select: {
        style:    'os',
        selector: 'td:first-child'
    },
    "ajax":{
        url : '<?php echo base_url(); ?>admin/get_select_driver_data', // json datasource
        type: "post",  // method  , by default get
        data: {booking_id:'<?php echo $query->id; ?>'},
        error: function(){  // error handling
            $(".booking-grid-error").html("");
            $("#example").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
            $("#booking-grid_processing").css("display","none");
        }
    }
});
});

$('#select-car').on('change',function(){
if($(this).val()!=''){
$.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>admin/get_cartype_data",
        data: {cab_id:$(this).val()},
        success: function (result) {
            GetRoute();
            $('#car-calculate').show();
            var json_arr=JSON.parse(result);
           $('#car-calculate').html('<table class="cartype-details" align="center" border="1" style="width:100%;"><tr><td>Car Type:</td><td>'+$("#select-car option:selected").text()+'</td></tr><tr><td>First 5 km:</td><td>'+json_arr.car_rate+'ريال /km</td></tr><tr><td>After 5 km:</td><td>'+json_arr.fromintailrate+'ريال /km</td></tr><tr><td>Per Minute</td><td>'+json_arr.ride_time_rate+'ريال /min</td></tr><tr><td>Approx Distance:</td><td id="approx-distance"></td></tr><tr><td>Approx Cost:</td><td id="approx-cost"><span></span>ريال</td></tr></table>');
        }
    });
}
else{
    $('#car-calculate').hide();
    alert('Please select atleast one car type');
}
});
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">
    </script>
<script type="text/javascript">
  
  function initMap() {
        var map = new google.maps.Map(document.getElementById('dvMap'), {
          zoom: 20,
          center: {lat: 26.9580626, lng: 75.7142001},
          mapTypeId: 'terrain'
        });

        var flightPlanCoordinates = [
      <?php
      foreach ($location as $key => $value) {
        # code...
    
    ?>
          {lat: <?php echo $value['lat'] ?>, lng: <?php echo $value['lng'] ?>},
      <?php } ?>
        ];
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        flightPath.setMap(map);
      }
</script>
<script>
/*function initMap() {
var bounds = new google.maps.LatLngBounds;
var markersArray = [];

var origin1 = {lat: <?php echo $query->pickup_lat ?>, lng: <?php echo $query->pickup_long ?>};
var destinationA = {lat: <?php echo $query->drop_lat ?>, lng: <?php echo $query->drop_long ?>};

var destinationIcon = 'https://chart.googleapis.com/chart?' +
    'chst=d_map_pin_letter&chld=D|FF0000|000000';
var originIcon = 'https://chart.googleapis.com/chart?' +
    'chst=d_map_pin_letter&chld=O|FFFF00|000000';
var map = new google.maps.Map(document.getElementById('dvMap'), {
    center: {lat: 20.1868076, lng: 64.4295101},
    zoom: 10,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
});
var geocoder = new google.maps.Geocoder;

var service = new google.maps.DistanceMatrixService;
service.getDistanceMatrix({
    origins: [origin1],
    destinations: [destinationA],
    travelMode: google.maps.TravelMode.DRIVING,
    unitSystem: google.maps.UnitSystem.METRIC,
    avoidHighways: false,
    avoidTolls: false
}, function(response, status) {
    if (status !== google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
    } else {
        var originList = response.originAddresses;
        var destinationList = response.destinationAddresses;
        var outputDiv = document.getElementById('dvPanel');
        outputDiv.innerHTML = '';
        var infoWindow = new google.maps.InfoWindow({ maxWidth: 150 });
        deleteMarkers(markersArray);

        var showGeocodedAddressOnMap = function(asDestination) {
            var icon = asDestination ? destinationIcon : originIcon;
            return function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.fitBounds(bounds.extend(results[0].geometry.location));
                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map:map,
                        icon: icon,
                        info: results[0].formatted_address
                    });

                    // Allow each marker to have an info window
                    google.maps.event.addListener( marker, 'click', function() {
                        infoWindow.setContent( this.info );
                        infoWindow.open( map, this );
                    });

                    markersArray.push(marker);
                } else {
                    alert('Geocode was not successful due to: ' + status);
                }
            };
        };

        for (var i = 0; i < originList.length; i++) {
            var results = response.rows[i].elements;
            geocoder.geocode({'address': originList[i]},
                showGeocodedAddressOnMap(false));
            for (var j = 0; j < results.length; j++) {
                geocoder.geocode({'address': destinationList[j]},
                    showGeocodedAddressOnMap(true));
            }
        }
    }
});
}

function deleteMarkers(markersArray) {
for (var i = 0; i < markersArray.length; i++) {
    markersArray[i].setMap(null);
}
markersArray = [];
}*/
</script>
<!--<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCr5WgfHn67qGhlT_qAZOBiU5zMXz67qhE&callback=initMap">
</script>-->
</body>
</html>