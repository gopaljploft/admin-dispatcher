<!DOCTYPE html>

<html>

<head>

	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



	<title>Edit Driver - Infinite Cab</title>



	<!-- bootstrap -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



	<!-- RTL support - for demo only -->

	<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

	<!--

	If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

	And add "rtl" class to <body> element - e.g. <body class="rtl">

	-->



	<!-- libraries -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



	<!-- global styles -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



	<!-- this page specific styles -->

	<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



	<!-- Favicon -->

	<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



	<!-- google font libraries -->

	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



	<!--[if lt IE 9]>

	<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

	<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

	<![endif]-->



	<style type="text/css">.modal-open .modal{ background:url(img/transpharant.png) top left repeat;} input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

	<?php

	include"includes/admin_header.php";

	?>

	<div id="page-wrapper" class="container">

		<div class="row">

			<?php

			include"includes/admin_sidebar.php";

			?>

			<div id="content-wrapper">

				<div class="row" style="opacity: 1;">

					<div class="col-lg-12">

						<div class="row">

							<div class="col-lg-12">

								<div id="content-header" class="clearfix">

									<div class="pull-left">

										<h1>Edit Driver</h1>

									</div>

									<div class="pull-right">

										<ol class="breadcrumb">

											<li><a href="#">Home</a></li>

											<li class="active"><span>Edit Driver</span></li>

										</ol>

									</div>

								</div>

							</div>

						</div>

						<div class="row">

							<div class="col-lg-12">

								<div class="main-box clearfix">

									<div class="panel" style="margin-bottom:0px;">

										<div class="panel-body">

											<h2>Edit Driver</h2>

										</div>

									</div>

									<div class="main-box-body clearfix">

										<!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form">-->

										<?php echo form_open_multipart('admin/update_driver',array('id' => 'formAddUser','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>

										<?php if($this->session->flashdata('error_msg')) {

											echo $this->session->flashdata('error_msg');

										}

										?>

										
                                             <!--<div class="form-group">

                        <label class="col-lg-2 control-label" for="drivername">Select Car</label>

                        <div id="inputDriverName" class="col-lg-10">

                          <select  name="carId" class="form-control" >
                              <option value="0">Select car</option>
                               <?php
                                foreach($cars as $val)
                                {
                                    ?>
                                    <option <?php if($driver['carId']== $val['id']) { echo 'selected'; } ?> value="<?php echo $val['id'] ?>"><?php echo $val['car_name'] ?> (<?php echo $val['car_number'] ?>)</option>
                                    <?php
                                }
                               ?>
                          </select>

                        </div>

                      </div-->
                       <div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">Driver Type</label>

												<div id="inputDriverName" class="col-lg-10 ">

													<select  name="type" id="type" class="form-control" required>
													    <option <?php if($driver['dtype']== 'Standard') { echo 'selected'; } ?> value="Standard">Standard</option>
													     <option <?php if($driver['dtype']== 'Platinum') { echo 'selected'; } ?> value="Platinum">Platinum</option>
													  </select>

												</div>

											</div>
												<div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">Preferred Name</label>

												<div id="inputDriverName" class="col-lg-10 ">

													<input type="text"  value="<?php echo $driver['pname'] ?>" placeholder="Enter Preferred Name" name="pname" id="pName" class="form-control" required>

												</div>

											</div>
											<div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">First Name</label>

												<div id="inputDriverName" class="col-lg-10 ">

													<input type="text" value="<?php echo $driver['first_name'] ?>"  placeholder="Enter First Name" name="driverName" id="driverName" class="form-control" required>

												</div>

											</div>
												<div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">Last Name</label>

												<div id="inputDriverName" class="col-lg-10 ">

													<input type="text" value="<?php echo $driver['last_name'] ?>"  placeholder="Enter Last Name" name="lName" id="lName" class="form-control" required>

												</div>

											</div>
										        	<div class="form-group">

												<label class="col-lg-2 control-label" for="driverphone">Phone No</label>

												<div id="inputDriverPhone" class="col-lg-10">

													<div class="input-group">

														<span class="input-group-addon">+61</span>

														<input value="<?php echo $driver['phone'] ?>" type="tel" onkeypress="return isNumberKey(event)" minlength="5" maxlength="15" title="Mobile number less than 14 digit." name="driverPhone" id="driverPhone" class="form-control" placeholder="Enter phone number" required>

													</div>

												</div>

											</div>

											<div class="form-group">

												<label class="col-lg-2 control-label" for="driveremail">Email Address</label>

												<div id="inputDriverEmail" class="col-lg-10">

													<input type="email" value="<?php echo $driver['email'] ?>"  placeholder="Enter email address" name="email" id="email" class="form-control" required>

												</div>

											</div>	
													<div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">Date Of Birth</label>

												<div id="inputDriverName" class="col-lg-10">

													<input type="text"  value="<?php echo $driver['dob'] ?>"  placeholder="Enter Date Of Birth" name="dob" id="dob	" class="form-control datepicker" required>

												</div>

											</div>

											<div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">Gender</label>

												<div id="inputDriverName" class="col-lg-10">

													<select name="gender" id="gender" class="form-control" required>

														<option value="male">Male</option>

														<option value="female">Female</option>

													</select>

												</div>

											</div>

											<div class="form-group">

												<label class="col-lg-2 control-label" for="driveraddress">Address</label>

												<div id="inputDriverAddress" class="col-lg-10">

													<textarea rows="3" name="driverAddress" id="driverAddress" class="form-control" placeholder="Enter address" required><?php echo $driver['address'] ?></textarea>

												</div>

											</div>

										
											
										
                                    

											<div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">UserName/DriverId</label>

												<div id="inputDriverName" class="col-lg-10">

													<input type="text" readonly  value="<?php echo $driver['user_name'] ?>"  placeholder="Enter Username" name="username" id="username" class="form-control"  required>

												</div>

											</div>

									
												<div class="form-group">

												<label class="col-lg-2 control-label" for="driveremail">Password</label>

												<div id="inputDriverEmail" class="col-lg-10">

													<input type="text" value="<?php echo $driver['password'] ?>" placeholder="Enter Password" name="password" id="email" class="form-control" required>

												</div>

											</div>
											
											 <div class="form-group">

												<label class="col-lg-2 control-label" for="drivername"> Emergency Person Name</label>

												<div id="inputDriverName" class="col-lg-10 ">

													<input type="text"  placeholder=" Emergency Person Name" name="ename" id="lName" class="form-control" required  value="<?php echo $driver['ename'] ?>">

												</div>

											</div>
                                    
                                    <div class="form-group">

												<label class="col-lg-2 control-label" for="driverphone">Emergency Contact No</label>

												<div id="inputDriverPhone" class="col-lg-10">

													<div class="input-group">

														<span class="input-group-addon"><i class="fa fa-phone	"></i></span>

														<input type="tel" onkeypress="return isNumberKey(event)" minlength="5" maxlength="15" title="Mobile number less than 14 digit." name="ephone" id="driverPhone" class="form-control" placeholder="Emergency Contact No" required  value="<?php echo $driver['ephone'] ?>">

													</div>

												</div>

											</div>
                            <div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Driver ABN No</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="text" placeholder="Enter Driver ABN No" name="abn" id="licenseno" class="form-control" value="<?php echo $driver['abm'] ?>" required>

												</div>

											</div>
											<div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">License NO</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="text" value="<?php echo $driver['license_no'] ?>" placeholder="Enter license number" name="licenseno" id="licenseno" class="form-control" required>

												</div>

											</div>

											<div class="form-group">
												

												<label class="col-lg-2 control-label" for="driverlicenseno">License Expiry Date</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="text" value="<?php echo $driver['Lieasence_Expiry_Date'] ?>"  placeholder="Enter License Expiry Date" name="licennex" id="licennex" class="form-control datepicker" required>

												</div>

											</div>

                                        <div class="form-group">

												<label class="col-lg-2 control-label" for="drivername">License issue state</label>

												<div id="inputDriverName" class="col-lg-10 ">

                                                        <select name="state" class="form-control">
                                                        <option  <?php if($driver['lstate']== 'ACT') { echo 'selected'; } ?> value="ACT">Australian Capital Territory</option>
                                                        <option  <?php if($driver['lstate']== 'NSW') { echo 'selected'; } ?> value="NSW">New South Wales</option>
                                                        <option  <?php if($driver['lstate']== 'NT') { echo 'selected'; } ?> value="NT ">Northern Territory</option>
                                                        <option  <?php if($driver['lstate']== 'QLD') { echo 'selected'; } ?> value="QLD">Queensland</option>
                                                        <option  <?php if($driver['lstate']== 'SA') { echo 'selected'; } ?> value="SA ">South Australia</option>
                                                        <option  <?php if($driver['lstate']== 'TAS') { echo 'selected'; } ?> value="TAS">Tasmania</option>
                                                        <option  <?php if($driver['lstate']== 'VIC') { echo 'selected'; } ?> value="VIC">Victoria</option>
                                                        <option  <?php if($driver['lstate']== 'WA') { echo 'selected'; } ?> value="WA ">Western Australia</option>
                                                        </select>


												</div>

											</div>
											
										<input type="hidden" value="<?php echo $driver['id'] ?>" name="did">
										<input type="hidden" value="<?php echo $driver['image'] ?>" name="himg">



										<!--	<div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Upload Document 1</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="file" name="doc1" id="doc1" class="form-control" required>

												</div>

											</div>

											<div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Upload Document 2</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="file" name="doc2" id="doc2" class="form-control" required>

												</div>

											</div>

											<div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Upload Document 3</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="file" name="doc3" id="doc3" class="form-control" required>

												</div>

											</div>
-->
											
	                                                <div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Offload?</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<select  name="offload" id="driverimage" class="form-control">
													    <option <?php if($driver['offload']==1) { echo 'selected'; } ?> value="1">Yes</option>
													    <option <?php if($driver['offload']==0) { echo 'selected'; } ?> value="0">No</option>
													</select>

												</div>

											</div>
											
											<div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Status</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<select  name="status" id="driverimage" class="form-control">
													    <option <?php if($driver['status']=='Active') { echo 'selected'; } ?> value="Active">Active</option>
													    <option <?php if($driver['status']=='Deactive') { echo 'selected'; } ?> value="Deactive">Inactive</option>
													</select>

												</div>

											</div>
												<div class="form-group">

												<label class="col-lg-2 control-label" for="driverlicenseno">Profile Photo</label>

												<div id="inputDriverLicenseNo" class="col-lg-10">

													<input type="file" accept="image/x-png,image/gif,image/jpeg" name="profile" id="profile" class="form-control">

												</div>
												<img class="img-responsive" src="<?php echo base_url(); ?>/<?php echo $driver['image'] ?>" style="width: 100px;">
											</div>

											<div class="form-group">

												<div class="col-lg-offset-2 col-lg-1">

													<button style="display:block;" class="btn btn-success" name="save" id="notification-trigger-bouncyflip" type="submit" >

														<span id="category_button" class="content">SUBMIT</span>

													</button>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<footer class="row" id="footer-bar" style="opacity: 1;">

					<p id="footer-copyright" class="col-xs-12">

						Powered by Infinite Cab.

					</p>

				</footer>

			</div>

		</div>

	</div>

</div>



<div id="config-tool" class="closed" style="display:none;">

	<a id="config-tool-cog">

		<i class="fa fa-cog"></i>

	</a>



	<div id="config-tool-options">

		<h4>Layout Options</h4>

		<ul>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-fixed-header" checked />

					<label for="config-fixed-header">

						Fixed Header

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-fixed-sidebar" checked />

					<label for="config-fixed-sidebar">

						Fixed Left Menu

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-fixed-footer" checked />

					<label for="config-fixed-footer">

						Fixed Footer

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-boxed-layout" />

					<label for="config-boxed-layout">

						Boxed Layout

					</label>

				</div>

			</li>

			<li>

				<div class="checkbox-nice">

					<input type="checkbox" id="config-rtl-layout" />

					<label for="config-rtl-layout">

						Right-to-Left

					</label>

				</div>

			</li>

		</ul>

		<br/>

		<h4>Skin Color</h4>

		<ul id="skin-colors" class="clearfix">

			<li>

				<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

				</a>

			</li>

			<li>

				<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

				</a>

			</li>

			<li>

				<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

				</a>

			</li>

		</ul>

	</div>

</div>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<!-- this page specific inline scripts -->

<script type="text/javascript">
	function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
$( function() {
     $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange:  '1964:2050',
            dateFormat: 'dd/mm/yy',
            maxDate: new Date(),
            
        });
   
  } );
	$(window).load(function() {

		$(".cover").fadeOut(2000);

	});

	$(document).ready(function() {

		//CHARTS

		function gd(year, day, month) {

			return new Date(year, month - 1, day).getTime();

		}

	});

</script>



</body>

</html>