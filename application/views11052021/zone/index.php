<!DOCTYPE html>



<html>



<head>



    <meta charset="UTF-8" />



    <meta name="viewport" content="width=device-width, initial-scale=1.0" />



    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />







    <title>Zone Overview - Infinite Cab</title>







    <!-- bootstrap -->



    <link rel="stylesheet" type="text/css" href="https://admin.infinitecabs.com.au/application/views/css/bootstrap/bootstrap.min.css" />







    <!-- RTL support - for demo only -->



    <script src="https://admin.infinitecabs.com.au/application/views/js/demo-rtl.js"></script>



    <!--



    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />



    And add "rtl" class to <body> element - e.g. <body class="rtl">



    -->







    <!-- libraries -->



    <link rel="stylesheet" type="text/css" href="https://admin.infinitecabs.com.au/application/views/css/libs/font-awesome.css" />



    <link rel="stylesheet" type="text/css" href="https://admin.infinitecabs.com.au/application/views/css/libs/nanoscroller.css" />







    <!-- global styles -->



    <link rel="stylesheet" type="text/css" href="https://admin.infinitecabs.com.au/application/views/css/compiled/theme_styles.css" />







    <!-- this page specific styles -->



    <link rel="stylesheet" href="https://admin.infinitecabs.com.au/application/views/css/libs/daterangepicker.css" type="text/css" />



    <link href="https://admin.infinitecabs.com.au/application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">







    <!-- Favicon -->



    <link type="image/x-icon" href="https://admin.infinitecabs.com.au/upload/favicon.ico" rel="shortcut icon" />







    <!-- google font libraries -->



    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>







    <!--[if lt IE 9]>



    <script src="https://admin.infinitecabs.com.au/application/views/js/html5shiv.js"></script>



    <script src="https://admin.infinitecabs.com.au/application/views/js/respond.min.js"></script>



    <![endif]-->







    <style type="text/css">.modal-open .modal{ background:url(https://admin.infinitecabs.com.au/application/views/img/transpharant.png) top left repeat;}

.form-group.col-md-4.jspart {

    margin-bottom: 0px;

}

.main-box.clearfix.zo-prt div#map {

    height: 750px;

}

ul.pagination {

    font-size: 12px;

}



.main-box.clearfix.zo-prt {

    height: 860px;

}

/*.main-box.clearfix.del-rest {

    height: 860px;

}*/





/*span.select2.select2-container.select2-container--default.select2-container--below {

    width: 100% !important;

}

*//*span.select2.select2-container.select2-container--default.select2-container--focus {

    width: 100% !important;

}*/



.main-box.clearfix.del-rest .table-responsive {

    height: 815px;

}







.main-box.clearfix.del-rest .table-responsive {

    height: 85vh !important;

}

.main-box.clearfix.zo-prt {

    height: 91vh;

}

.main-box.clearfix.zo-prt div#map {

    height: 75vh !important;

}









</style>



</head>



<body>



<div class="cover"></div>



<div id="theme-wrapper">



    <?php



    include"includes/admin_header.php";



    ?>



    <div id="page-wrapper" class="container">



        <div class="row">



            <?php



            include"includes/admin_sidebar.php";



            ?>



            <div id="content-wrapper">



                <div class="row" style="opacity: 1;">



                    <div class="col-lg-12">



                        <div class="row">



                            <div class="col-lg-12">



                                <div id="content-header" class="clearfix">



                                    <div class="pull-left">



                                        <h1>Zone Overview</h1>



                                    </div>



                                    <div class="pull-right">



                                        <ol class="breadcrumb">



                                            <li><a href="#">Home</a></li>



                                            <li class="active"><span>Zone Overview</span></li>



                                        </ol>



                                    </div>



                                </div>



                            </div>



                        </div>



                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->



                        <div class="col-lg-12">



                            <!-- Single Delete -->



                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">



                                <div class="modal-dialog">



                                    <div class="modal-content">



                                        <div class="modal-header">



                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>



                                        </div>



                                        <div class="modal-title">Are you sure you want to delete the selected user?</div>



                                        <div class="modal-body"></div>



                                        <div class="modal-footer">



                                            <button id="confirm-delete-button" onclick="delete_single_user_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>



                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>



                                            <input type="hidden" value="" id="bookedid" name="bookedid">



                                            <button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>



                                        </div>



                                    </div> <!-- / .modal-content -->



                                </div> <!-- / .modal-dialog -->



                            </div> <!-- / .modal -->



                            <!-- / Single Delete -->



                            <!-- Multipal Delete -->



                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">



                                <div class="modal-dialog">



                                    <div class="modal-content">



                                        <div class="modal-header">



                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>



                                        </div>



                                        <div class="modal-title">Are you sure you want to delete selected user?</div>



                                        <div class="modal-body"></div>



                                        <div class="modal-footer">



                                            <button onclick="delete_user()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>



                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>



                                            <button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>



                                        </div>



                                    </div> <!-- / .modal-content -->



                                </div> <!-- / .modal-dialog -->



                            </div> <!-- / .modal -->



                            <!-- / Multipal Delete -->



                        </div>



                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

<div class="panel">



                                        <div class="panel-body">



                                            



                                            <div class="filter-block">

                                                  <div class="row">

                                                <form action="https://admin.infinitecabs.com.au/zone">

                                                    

                                                <div style="margin-bottom:10px;" class="form-group col-md-4 jspart">

                                                    <input type="text" placeholder="Zone Name"  value="<?php echo @$_GET['name']; ?>" name="name" class="form-control">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div>

                                                

                                                

                                                <div style="margin-bottom:10px;" class="form-group col-md-4">  

                                                     <select name="cab" class="form-control" style="width: 100% !important;height: 37px !important;"  id="select2">

                                        <option value="">Cab No</option>

                                        <?php foreach ($cars as $value) { ?>

                                        <option <?php if($value['id']== @$_GET['cab']) { echo 'selected'; } ?> value="<?php echo $value['id'] ?>"><?php echo $value['car_number'] ?></option>

                                        <?php 

                                        }

                                        ?>

                                        </select>

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div>

                                                

                                             <div style="margin-bottom:10px;text-align:;" class="form-group col-md-12">

                                                     

                                                     <a href="http://admin.infinitecabs.com.au/zone" class="btn btn-primary">

                                                    <i class="fa fa-refresh fa-lg"></i> Reset

                                                </a>

                                                

                                                <button type="submit" class="btn btn-primary" href="javascript:void(0)">

                                                    <i class="fa fa-search fa-lg"></i> Search

                                                </button>

                                               

                                          </div>



                                            </div>

 </form>

 </div>

                                        </div>



                                    </div>

                        <div class="row">



                            <div class="col-md-6">



                                <div class="main-box clearfix del-rest">



                                    <div class="panel">



                                        



                                    </div>



                                    <div class="main-box-body clearfix">



                                        <div class="table-responsive">



                                            <table id="" class="table table-hover table-bordered user-list zones">



                                                <thead>



                                                <tr>



                                                <!--    <th>Sr.No</th>-->



                                                    <th style="min-width: 90px; font-size: 12px;"><a href="javascript:void(0);">Zone Name</a></th>

                                                    <th style="min-width: 110px; font-size: 12px;"><a href="javascript:void(0);">Post Code</a></th>



                                                    <th style="min-width: 110px;font-size: 12px;"><a href="javascript:void(0);">Total Cabs </a></th>

                                                    

                                                      <th style="min-width: 90px;font-size: 12px;"><a href="javascript:void(0);">Trips</a></th>



                                                   



                                                    <th style="width: 150px;font-size: 12px;"><a href="javascript:void(1);">Action</a></th>



 







                                                </tr>



                                                </thead>

                                                <?php

                                                $i=1;

                                                foreach ($zone as $key => $value) {

                                                    # code...

                                               

                                                    $totalDriversww=$this->db->query("SELECT count(id) as totalDriver FROM diver_live_location WHERE zoneId=".$value['id']."  and (driver_booking_status='free' or driver_booking_status='booked') ")->row_array();
                                                   

                                                 $totaltrip=$this->db->query("SELECT count(id) as totaltrip FROM bookingdetails WHERE zoneId=".$value['id']."")->row_array();



                                                ?>

                                                <tr>



                                           <!-- <td><?php echo $i; ?></td>-->



                                            <td><a href="<?php echo base_url() ?>zone/zoneDetails/<?php echo $value['id'] ?>?zipcode=<?php echo $value['zipcode'] ?>"><?php echo $value['zone_title'] ?></a></td>

                                            <td><?php echo $value['zipcode'] ?></td>



                                            <td>

                                                <?php

                                                $query=$this->db->query("SELECT COUNT(*) as subrab FROM zone_details where zoneId=".$value['id']."")->row_array();

                                                

                                                //echo $this->db->last_query();

                                               

                                                ?>

                                                <a href="javascript:void(0);"><?php echo $totalDriversww['totalDriver'] ?></a></td>

                                            <td>

                                              <?php echo $totaltrip['totaltrip'] ?>  

                                            </td>

                                           



                                           <td>



                                            <a class="table-link" href="<?php echo base_url() ?>zone/zoneDetails/<?php echo $value['id'] ?>?zipcode=<?php echo $value['zipcode'] ?>">

                <span class="fa-stack">

                    <i class="fa fa-square fa-stack-2x"></i>

                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>

                </span>

            </a>

                                               <a class="table-link edit" id="<?php echo $value['id'] ?>"  data-toggle="modal" data-target="#myModal<?php echo $value['id'] ?>" href="javascript:">

                <span class="fa-stack">

                    <i class="fa fa-square fa-stack-2x"></i>

                    <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>

                </span>

            </a>

            

                                               

          

            <a data-target="#uidemo-modals-alerts-delete-user" data-toggle="modal" class="table-link danger" href="javascript:void(0);" onclick="delete_zone(<?php echo $value['id'] ?>)">

                <span class="fa-stack">

                    <i class="fa fa-square fa-stack-2x"></i>

                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>

                </span>

            </a></td>











                                                </tr>

                                                

                                                

<!-- Modal -->

<div id="myModal<?php echo $value['id'] ?>" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Edit Zone</h4>

      </div>

      <div class="modal-body">

       <form action="/action_page.php">

  <div class="form-group">

    <label for="email">Zone Name:</label>

    <input type="texi" value="<?php echo $value['zone_title'] ?>" class="form-control" id="email<?php echo $value['id'] ?>">

  </div>

   <div style="" class="form-group">

                                                     <lable>Post Code</lable>

                                                         <input type="text" id="zipcode<?php echo $value['id'] ?>" value="<?php echo $value['zipcode'] ?>" name="zipcode" placeholder="Post Code" class="form-control" autocomplete="off">

                                                 

                                                </div>

                                                

  <div class="form-group">

    <label for="pwd">Dispatch Time:</label>

    <input type="texi" value="<?php echo $value['time'] ?>"  class="form-control" id="pwd<?php echo $value['id'] ?>">

  </div>

 

  <button type="button" id="<?php echo $value['id'] ?>" class="btn btn-primary  update">Submit</button>

</form>

      </div>

      <div class="modal-footer">

        <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>

                                            <?php $i++; } ?>

                                            </table>



                                        </div>





                                        <!--<ul class="pagination pull-right">



                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>



                                            <li><a href="javascript:void(0);">1</a></li>



                                            <li><a href="javascript:void(0);">2</a></li>



                                            <li><a href="javascript:void(0);">3</a></li>



                                            <li><a href="javascript:void(0);">4</a></li>



                                            <li><a href="javascript:void(0);">5</a></li>



                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>



                                        </ul>-->



                                    </div>



                                </div>



                            </div>

                            

                            

                             <div class="col-md-6">



                                <div class="main-box clearfix zo-prt">



                                    <div class="panel">



                                        <div class="panel-body">



                                            <h2 class="pull-left">Manage Zone</h2>



                                            <div class="filter-block pull-right">



                                              

                                                    <a class="btn btn-primary pull-right" href="javascript:void(0)" onclick="window.location.href='<?php echo base_url() ?>admin/geofancing'">



                                                    <i class="fa fa-plus-circle fa-lg"></i> Create New Zone



                                                </a>

                                            </div>



                                        </div>



                                    </div>



                                    <div class="main-box-body clearfix">



                                        



                                        <style>

                                                #map {

                                                width: 100%;

                                                height: 600px;

                                                margin: 0 auto;

                                                }

                                        </style>

                                       <div id="map"></div>



                                    </div>



                                </div>



                            </div>



                        </div>



                    </div>



                </div>







                <?php include "includes/admin-footer.php"?>



            </div>



        </div>



    </div>



</div>







<div id="config-tool" class="closed" style="display:none;">



    <a id="config-tool-cog">



        <i class="fa fa-cog"></i>



    </a>







    <div id="config-tool-options">



        <h4>Layout Options</h4>



        <ul>



            <li>



                <div class="checkbox-nice">



                    <input type="checkbox" id="config-fixed-header" checked />



                    <label for="config-fixed-header">



                        Fixed Header



                    </label>



                </div>



            </li>



            <li>



                <div class="checkbox-nice">



                    <input type="checkbox" id="config-fixed-sidebar" checked />



                    <label for="config-fixed-sidebar">



                        Fixed Left Menu



                    </label>



                </div>



            </li>



            <li>



                <div class="checkbox-nice">



                    <input type="checkbox" id="config-fixed-footer" checked />



                    <label for="config-fixed-footer">



                        Fixed Footer



                    </label>



                </div>



            </li>



            <li>



                <div class="checkbox-nice">



                    <input type="checkbox" id="config-boxed-layout" />



                    <label for="config-boxed-layout">



                        Boxed Layout



                    </label>



                </div>



            </li>



            <li>



                <div class="checkbox-nice">



                    <input type="checkbox" id="config-rtl-layout" />



                    <label for="config-rtl-layout">



                        Right-to-Left



                    </label>



                </div>



            </li>



        </ul>



        <br/>



        <h4>Skin Color</h4>



        <ul id="skin-colors" class="clearfix">



            <li>



                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">



                </a>



            </li>



            <li>



                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">



                </a>



            </li>



            <li>



                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">



                </a>



            </li>



            <li>



                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">



                </a>



            </li>



            <li>



                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">



                </a>



            </li>



            <li>



                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">



                </a>



            </li>



            <li>



                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">



                </a>



            </li>



            <li>



                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">



                </a>



            </li>



        </ul>



    </div>



</div>



<script src="https://admin.infinitecabs.com.au/application/views/js/jquery-1.12.3.js"></script>



<script src="https://admin.infinitecabs.com.au/application/views/js/jquery.dataTables.js"></script>



<!-- global scripts -->



<script src="https://admin.infinitecabs.com.au/application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->







<script src="https://admin.infinitecabs.com.au/application/views/js/jquery.js"></script>



<script src="https://admin.infinitecabs.com.au/application/views/js/bootstrap.js"></script>



<script src="https://admin.infinitecabs.com.au/application/views/js/jquery.nanoscroller.min.js"></script>







<script src="https://admin.infinitecabs.com.au/application/views/js/demo.js"></script> <!-- only for demo -->







<!-- this page specific scripts -->



<script src="https://admin.infinitecabs.com.au/application/views/js/moment.min.js"></script>



<script src="https://admin.infinitecabs.com.au/application/views/js/gdp-data.js"></script>







<!-- theme scripts -->



<script src="https://admin.infinitecabs.com.au/application/views/js/scripts.js"></script>



<script src="https://admin.infinitecabs.com.au/application/views/js/pace.min.js"></script>







<script src="https://admin.infinitecabs.com.au/assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>



<script src="https://admin.infinitecabs.com.au/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>



<!-- this page specific inline scripts -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>



<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">





</script>

<script src="https://admin.infinitecabs.com.au/assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>



<script src="https://admin.infinitecabs.com.au/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- this page specific inline scripts -->

<script type="text/javascript">

$('.zones').DataTable( {bFilter: false, bInfo: false,'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
    }]});

   $('.update').click(function(){

       

 var single_id= $(this).attr('id');

 var title=$('#email'+single_id).val();

  var time=$('#pwd'+single_id).val();

   var zipcode=$('#zipcode'+single_id).val();



      



        $.ajax({



            type: "POST",



            url: "<?php echo base_url(); ?>zone/updateZone",



            data: {data_id: single_id,title: title,time: time,zipcode: zipcode},



            success: function (result) {

            alert("Update Successfully");

             location.reload();



            }

        });



   })

   

$("#select2").select2({



});

var map;

<?php

//print_r($query2);die;

if($query2)

{

?>

function initMap() {



var map;



var bounds = new google.maps.LatLngBounds();



var mapOptions = {



mapTypeId: 'roadmap'



};







// Display a map on the web page



map = new google.maps.Map(document.getElementById("map"), mapOptions);



map.setTilt(50);







// Multiple markers location, latitude, and longitude







var markers = [



<?php







foreach ($query2 as $key => $value) {







?>



['<?php echo $value['car_number'] ?>', <?php echo $value['latitude'] ?>, <?php echo $value['longlatitude'] ?>],



<?php } ?>



];







console.log(markers);         



// Info window content



var infoWindowContent = [



<?php



foreach ($query2 as $key => $value) {







?>



['<div class="info_content">' +



'<h3><?php echo $value['name'] ?>,<?php echo $value['car_no'] ?></h3>' +



'<p><strong>Driver ID : <?php echo $value['user_name'] ?></strong></p>'+'<p><strong>Driver First Name : <?php echo $value['first_name'] ?></strong></p>'+' <p><strong>Driver Last Name : <?php echo $value['last_name'] ?></strong></p>'+' <p><strong>Driver Mobile No : <?php echo $value['phone'] ?></strong></p>'+' <p><strong>Cab No : <?php echo $value['car_number'] ?></strong></p>'+' <p><strong>Driver Status  : <?php echo ucwords($value['driver_booking_status']); ?></strong></p>'+' <p><strong>Current Zone  :<?php echo ucwords($value['zone_title']); ?></strong></p>'+'  <p><strong>Lat : <?php echo $value['latitude'] ?></strong></p>'+' <p><strong>Lng : <?php echo $value['longlatitude'] ?></strong></p> '+ '</div>'],



<?php } ?>



];







// Add multiple markers to map



var infoWindow = new google.maps.InfoWindow(), marker, i;







// Place each marker on the map  



for( i = 0; i < markers.length; i++ ) {



console.log(markers[0][1]);



var position = new google.maps.LatLng(<?php echo $value['latitude'] ?>,<?php echo $value['longlatitude'] ?>);



bounds.extend(position);



marker = new google.maps.Marker({



position: position,



map: map,



title: markers[i][0],



icon:"http://admin.infinitecabs.com.au/upload/car.png"



});







// Add info window to marker    



google.maps.event.addListener(marker, 'click', (function(marker, i) {



return function() {



infoWindow.setContent(infoWindowContent[i][0]);



infoWindow.open(map, marker);



}



})(marker, i));







// Center the map to fit all markers on the screen



map.fitBounds(bounds);



}







// Set zoom level



var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {



this.setZoom(7);



google.maps.event.removeListener(boundsListener);



});







}

<?php } else {
//echo "string";die;
    ?>

    function initMap() {

    var mapOptions = {

  center: new google.maps.LatLng(52.640143,1.28685),

      zoom: 15,

      mapTypeId: google.maps.MapTypeId.ROADMAP

    };

    var map = new google.maps.Map(document.getElementById("map"),

        mapOptions);



var marker = new google.maps.Marker({

    position: new google.maps.LatLng(52.640143,1.28685),

    map: map,

    title: "Mark On Map"

});

google.maps.event.addDomListener(window, 'load', initialize);
  }

  

    <?php

} ?>

    $(window).load(function() {



        $(".cover").fadeOut(2000);



    });



    $(document).ready(function() {



        //CHARTS



        function gd(year, day, month) {



            return new Date(year, month - 1, day).getTime();



        }



    });



</script>



<script type="text/javascript" language="javascript" >



 



    function delete_user(){



        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked



            var ids = [];



            $('.deleteRow').each(function(){



                if($(this).is(':checked')) {



                    ids.push($(this).val());



                }



            });



            var ids_string = ids.toString();  // array to string conversion



            $.ajax({



                type: "POST",



                url: "<?php echo base_url(); ?>zone/delete_car_data",



                data: {data_ids:ids_string},



                success: function(result) {



                    var oTable1 = $('#example').DataTable();



                    oTable1.ajax.reload(null, false);



                },



                async:false



            });



        }



    }



    function delete_zone(single_id){



        $('#bookedid').val(single_id);



    }



    function delete_single_user_action()



    {



        var single_id = $('#bookedid').val();



        $.ajax({



            type: "POST",



            url: "<?php echo base_url(); ?>zone/delete_zone",



            data: {data_id: single_id},



            success: function (result) {



             location.reload();



            },



            async: false



        });



    }



</script>



</body>



</html>