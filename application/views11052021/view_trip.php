<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Manage Booking - Infinite Cab</title>

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />

    <!-- RTL support - for demo only -->
    <script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>
    <!--
    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />
    And add "rtl" class to <body> element - e.g. <body class="rtl">
    -->

    <!-- libraries -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />

    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />
    <link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />

    <!-- google font libraries -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}
 .panel-body.prod-pt h2 {
    font-size: 16px;
    font-weight: 600;
}
</style>
</head>
<body>
<div class="cover"></div>
<div id="theme-wrapper">
    <?php
    include"includes/admin_header.php";
    ?>
    <div id="page-wrapper" class="container">
        <div class="row">
            <?php
            include"includes/admin_sidebar.php";
            ?>
            <div id="content-wrapper">
                <div class="row" style="opacity: 1;">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="content-header" class="clearfix">
                                    <div class="pull-left">
                                        <h1>Manage Booking</h1>
                                    </div>
                                    <div class="pull-right">
                                        <ol class="breadcrumb">
                                            <li><a href="#">Home</a></li>
                                            <li class="active"><span>Manage Booking</span></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->
                        <div class="col-lg-12">
                            <!-- Single Delete -->
                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>
                                        </div>
                                        <div class="modal-title">Are you sure you want to delete the selected booking?</div>
                                        <div class="modal-body"></div>
                                        <div class="modal-footer">
                                            <button id="confirm-delete-button" onclick="delete_single_booking_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <input type="hidden" value="" id="bookedid" name="bookedid">
                                            <button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>
                                        </div>
                                    </div> <!-- / .modal-content -->
                                </div> <!-- / .modal-dialog -->
                            </div> <!-- / .modal -->
                            <!-- / Single Delete -->
                            <!-- Multipal Delete -->
                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>
                                        </div>
                                        <div class="modal-title">Are you sure you want to delete selected booking?</div>
                                        <div class="modal-body"></div>
                                        <div class="modal-footer">
                                            <button onclick="delete_booking()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>
                                        </div>
                                    </div> <!-- / .modal-content -->
                                </div> <!-- / .modal-dialog -->
                            </div> <!-- / .modal -->
                            <!-- / Multipal Delete -->
                        </div>
                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-box clearfix">
                                    <div class="panel">
                                        <div class="panel-body prod-pt">
                                            <h2>Client Number : <?php if( $_GET['client']) { echo $_GET['client']; } else { echo 'N/A'; }  ?> , Shift ID : <?php if( $_GET['id']) { echo $_GET['id']; } else { echo 'N/A'; }  ?> <br> Shift Start Date & Time  : <?php if( $_GET['Start']) { echo $_GET['Start']; } else { echo 'N/A'; }  ?> , Shift End Date & Time  : <?php if( $_GET['end']) { echo $_GET['end']; } else { echo 'N/A'; }  ?> , <br> Cab Number  : <?php if( $_GET['cab']) { echo $_GET['cab']; } else { echo 'N/A'; }  ?> ,  Cab Meter S.No : N/A ,</h2>
                                            <div class="filter-block">
                                            
                                                 <a   class="btn btn-primary " href="<?php echo base_url(); ?>admin/shifttripexport?query=<?php echo $queryData; ?>">
                                                    <!-- <i class="fa fa-envelope fa-lg"></i> --> Export
                                                </a>


                                                 <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary " href="javascript:void(0)">
                                                    <!--  <i class="fa fa-envelope fa-lg"> </i> --> Send To Mail
                                                </button> 
                                            
                                               
                                               
                                                <span>&nbsp;</span>
                                                </div> 
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-box-body clearfix">
                                        <div class="table-responsive">
                                            <table id="" class="table table-hover table-bordered user-list">
                                                <thead>
                                                <tr>
                                                    <th>Seq  No.</th>
                                                    <th>Cab Number</th>
                                                    <th><a href="javascript:void(0);">Driver Name</a></th>
                                                    <th><a href="javascript:void(0);">Driver ID</a></th>
                                                    <th><a href="javascript:void(0);">Job/Booking ID</a></th>
                                                    <th><a href="javascript:void(0);">Trip Duration</a></th>
                                                    <th class="text-center"><a href="javascript:void(0);">Start Trip Location</a></th>
                                                    <th class="text-center"><a href="javascript:void(0);">End Trip Location</a></th>
                                                    <th class="text-center"><a href="#" class="desc">Date</a></th>
                                                    <th class="text-center">Status</th>
                                                     <th class="text-center">Booking From</th>
                                                    <th>Trip Km</th>
                                                    <!-- <th>Trip  Duration</th> -->
                                                    
                                                    <th>Trip Fare</th>
                                                    <th>Trip Levy Amount</th>
                                                    <th>Trip Tolls/Extra</th>
                                                    <th>Trip Total Fare</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i=1;//print_r($booking);
                                                    foreach ($bookings as $key => $value) {
                                                      
                                                    
                                                    ?>
                              
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $_GET['cab']; ?></td>
                                                        <td><?php echo $value['drivername']; ?></td>
                                                        <td><?php echo $value['did']; ?></td>
                                                        <td><?php echo $value['id']; ?></td>
                                                        <td><?php echo $value['approx_time']; ?></td>
                                                        <td><?php echo $value['pick_address']; ?></td>
                                                        <td><?php echo $value['drop_address']; ?></td>
                                                        <td><?php echo $value['bookingdateandtime']; ?></td>
                                                        <td><?php echo $value['status_code']; ?></td>
                                                        <td><?php echo $value['isdevice']; ?></td>
                                                        <td><?php echo $value['distance']; ?></td>
                                                         <!-- <td>N/A</td> -->
                                                         <td>$<?php echo $value['amount']; ?></td>
                                                          <td>$1.10</td>
                                                           <td>$0</td>
                                                           <td>$<?php echo $value['amount']; ?></td>
                                                        <td>

            <a onclick="window.location.href='view_booking_details?id=<?php echo $value['id']; ?>'" href="javascript:void(0);" class="table-link">
                <span class="fa-stack">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <!-- <a data-target="#uidemo-modals-alerts-delete-user" data-toggle="modal" class="table-link danger" href="javascript:void(0);" onclick="delete_single_user(1)">
                <span class="fa-stack">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                </span>
            </a> --></td>
                                                    </tr>
                                                    <?php $i++; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send To Mail</h4>
      </div>
      <div class="modal-body">
       <form action="<?php echo base_url(); ?>admin/sendtripReportTomail" id="sendmail" method="post">
                                                      
                                                <div class="row">
                                              <input type="hidden" name="query" value="<?php echo $queryData; ?>">
                                       
                                                 <div style="" class="form-group col-md-12">
                                                         <input type="email" placeholder="Email id" name="email" class="form-control" autocomplete="off">
                                                 
                                                </div>
                                                
                                                  <div style="" class="form-group col-md-12">
                                                         <textarea placeholder="Massage" name="msg" class="form-control" autocomplete="off"></textarea>
                                                 
                                                </div>
                                                
                                               
                                                </div>
                                                       <div class="row">
                                             
                                                <button type="submit" id="send" class="btn btn-primary " href="javascript:void(0)">
                                                    <!--<i class="fa fa-search fa-lg"></i>--> Send Data
                                                </button> 
                                                
                                               
                                                <span>&nbsp;</span>
                                                </div> 
                                                    
                                                </form>
      </div>
      <div class="modal-footer">
     
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
                                        <!--<ul class="pagination pull-right">
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>
                                            <li><a href="javascript:void(0);">1</a></li>
                                            <li><a href="javascript:void(0);">2</a></li>
                                            <li><a href="javascript:void(0);">3</a></li>
                                            <li><a href="javascript:void(0);">4</a></li>
                                            <li><a href="javascript:void(0);">5</a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>
                                        </ul>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php include "includes/admin-footer.php"?>
               <!--  <input type="hidden" name="filter_col" id="filter_col" value="<?php echo $query; ?>"/> -->
            </div>
        </div>
    </div>
</div>

<div id="config-tool" class="closed" style="display:none;">
    <a id="config-tool-cog">
        <i class="fa fa-cog"></i>
    </a>

    <div id="config-tool-options">
        <h4>Layout Options</h4>
        <ul>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-header" checked />
                    <label for="config-fixed-header">
                        Fixed Header
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-sidebar" checked />
                    <label for="config-fixed-sidebar">
                        Fixed Left Menu
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-fixed-footer" checked />
                    <label for="config-fixed-footer">
                        Fixed Footer
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-boxed-layout" />
                    <label for="config-boxed-layout">
                        Boxed Layout
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox-nice">
                    <input type="checkbox" id="config-rtl-layout" />
                    <label for="config-rtl-layout">
                        Right-to-Left
                    </label>
                </div>
            </li>
        </ul>
        <br/>
        <h4>Skin Color</h4>
        <ul id="skin-colors" class="clearfix">
            <li>
                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
                </a>
            </li>
            <li>
                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
                </a>
            </li>
            <li>
                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
                </a>
            </li>
        </ul>
    </div>
</div>
<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>
<!-- global scripts -->
<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->

<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>
<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->

<!-- this page specific scripts -->
<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>
<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>

<!-- theme scripts -->
<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>
<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- this page specific inline scripts -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
<!-- this page specific inline scripts -->
<script type="text/javascript">
 $('.status').change(function(){
        
        var id=$(this).attr('id');
        var status=$(this).val();
        $.ajax({
        url: "<?php echo base_url() ?>admin/update_booking_status",
        type: "post",
        data: {id:id,status:status} ,
        success: function (response) {
            alert("Status Updated Successfully");
            location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });        
    });
$( function() {
    $( ".date" ).datepicker();
  } );
    $(window).load(function() {
        $(".cover").fadeOut(2000);
    });
    $(document).ready(function() {
        //CHARTS
        function gd(year, day, month) {
            return new Date(year, month - 1, day).getTime();
        }
    });
</script>

</body> 
</html>  