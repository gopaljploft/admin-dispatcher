<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Trips Overview - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

    <link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>

    <script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>

    <script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>

    <![endif]-->



    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}

    .form-group.col-md-3 {

    width: 32%;

}



.filter-block.jackpart .form-group {

    margin-bottom: 0px !important;

}

    </style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/admin_header.php";

    ?>

    <div id="page-wrapper" class="container">

        <div class="row">

            <?php

            include"includes/admin_sidebar.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Trips Overview</h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Trips Overview</span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

                        <div class="col-lg-12">

                            <!-- Single Delete -->

                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>

                                        </div>

                                        <div class="modal-title">Are you sure you want to delete the selected booking?</div>

                                        <div class="modal-body"></div>

                                        <div class="modal-footer">

                                            <button id="confirm-delete-button" onclick="delete_single_booking_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>

                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                            <input type="hidden" value="" id="bookedid" name="bookedid">

                                            <button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>

                                        </div>

                                    </div> <!-- / .modal-content -->

                                </div> <!-- / .modal-dialog -->

                            </div> <!-- / .modal -->

                            <!-- / Single Delete -->

                            <!-- Multipal Delete -->

                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>

                                        </div>

                                        <div class="modal-title">Are you sure you want to delete selected booking?</div>

                                        <div class="modal-body"></div>

                                        <div class="modal-footer">

                                            <button onclick="delete_booking()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>

                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                            <button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>

                                        </div>

                                    </div> <!-- / .modal-content -->

                                </div> <!-- / .modal-dialog -->

                            </div> <!-- / .modal -->

                            <!-- / Multipal Delete -->

                        </div>

                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                            

                                            <div class="filter-block jackpart">



                                                  <form action="<?php echo base_url() ?>admin/manage_booking">

                                                      

                                                <div class="row">

                                                <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['id']) { echo $_GET['id']; } ?>" placeholder="Trip ID" name ="id" class="form-control">

                                                 

                                                </div>

                                                

                                                  <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['phone']) { echo $_GET['phone']; } ?>" placeholder="Mobile No" name ="phone" class="form-control">

                                                 

                                                </div>

                                                

                                                 <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['username']) { echo $_GET['username']; } ?>" placeholder="Passenger Name" name ="username" class="form-control">

                                                 

                                                </div>

                                                

                                                

                                                

                                                 <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['pick']) { echo $_GET['pick']; } ?>" placeholder="Pick up" name ="pick" class="form-control">

                                                 

                                                </div>

                                                

                                                 <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['drop']) { echo $_GET['drop']; } ?>" placeholder="Drop off" name ="drop" class="form-control">

                                                 

                                                </div>

                                                <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['did']) { echo $_GET['did']; } ?>" placeholder="Driver ID" name ="did" class="form-control">

                                                 

                                                </div> 

                                                 <div style="" class="form-group col-md-3">

                                                         <select type="text" name ="status_code" class="form-control">

                                                             <option <?php if($_GET['status_code']==0) { echo 'selected'; } ?> value="0">Status</option>

                                                            <option <?php if($_GET['status_code']=='on-trip') { echo 'selected'; } ?> value="on-trip">Running</option>

                                                            <option <?php if($_GET['status_code']=='pending') { echo 'selected'; } ?> value="pending">Pending</option>

                                                            <option <?php if($_GET['status_code']=='completed') { echo 'selected'; } ?> value="completed">Completed</option>

                                                            <option <?php if($_GET['status_code']=='upcoming') { echo 'selected'; } ?> value="upcoming">Upcoming</option>

                                                           <!-- <option <?php if($_GET['status_code']=='accepted') { echo 'selected'; } ?> value="accepted">Assigned</option>-->

                                                           <!--  <option <?php if($_GET['status_code']=='can') { echo 'selected'; } ?> value="can">Cancelled</option> -->

                                                            <option <?php if($_GET['status_code']=='Drver Not Found') { echo 'selected'; } ?> value="Drver Not Found">No Show</option>

                                                           

                                                         </select> 

                                                 

                                                </div>  

                                                 

                                                <div style="" class="form-group col-md-3">

                                                         <select type="text" name ="ca" class="form-control">

                                                             <option value="0">Cancelled By</option>

                                                          

                                                             <option <?php if($_GET['ca']=='user-cancelled') { echo 'selected'; } ?> value="user-cancelled">Passenger Cancelled</option>

                                                            <option <?php if($_GET['ca']=='driver-cancelled') { echo 'selected'; } ?> value="driver-cancelled">Driver Cancelled</option>

                                                            <option <?php if($_GET['ca']=='dispatcher-cancelled') { echo 'selected'; } ?> value="dispatcher-cancelled">Dispatcher Cancelled</option>

                                                         </select>   

                                                 

                                                </div>  



                                                  <div style="" class="form-group col-md-3">

                                                         <input type="text" value="<?php if($_GET['cab']) { echo $_GET['cab']; } ?>" placeholder="Cab No" name ="cab" class="form-control">

                                                 

                                                </div>

                                           

                                                 

                                                <div style="" class="form-group col-md-3">

                                                    <input type="text" value="<?php if($_GET['sdate']) { echo $_GET['sdate']; } ?>" autocomplete="off" placeholder="Start Date" name ="sdate" class="form-control date">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div> 

                                                 <div style="" class="form-group col-md-3">

                                                    <input type="text" placeholder="End Date" value="<?php if($_GET['edate']) { echo $_GET['edate']; } ?>" name="edate" autocomplete="off" class="form-control date">

                                                    <i class="fa fa-clock search-icon"></i>

                                                </div> 

                                               

                                                

                                                 

                                                

                                                

                                                 

                                                </div> 

                                                

                                                       <div class="row">

                                                <a href="<?php echo base_url() ?>admin/manage_booking" class="btn btn-primary">

                                                    <i class="fa fa-refresh fa-lg"></i> Reset

                                                </a>

                                                

                                                

                                                <button type="submit" class="btn btn-primary" href="javascript:void(0)">

                                                    <i class="fa fa-search fa-lg"></i> Search

                                                </button> 

                                                <span>&nbsp;</span> 

                                                </div> 

                                                    

                                                </form>     

                                            </div>

                                        </div> 

                                    </div>

                                    <div class="main-box-body clearfix">

                                        <div class="table-responsive">

                                            <table id="example" class="table table-hover table-bordered user-list">

                                                <thead>

                                                <tr>

                                                    

                                                    <th style="vertical-align: middle;min-width: 80px;">Trip ID</th>

                                                   

                                                    <th style="vertical-align: middle;min-width: 105px;"><a href="javascript:void(0);">Date & Time</a></th>

                                                    <th style="vertical-align: middle;min-width: 145px;"><a href="javascript:void(0);">Passenger Name</a></th>

                                                    <th style="vertical-align: middle;min-width: 115px;"><a href="javascript:void(0);">Passenger ID</a></th>

                                                    <th style="vertical-align: middle;min-width: 100px;"><a href="javascript:void(0);">Phone No</a></th>

                                                    <th style="vertical-align: middle;min-width: 75px;" class="text-center"><a href="javascript:void(0);">Cab No</a></th>

                                                    <th style="vertical-align: middle;min-width: 90px;" class="text-center"><a href="javascript:void(0);">Driver ID</a></th>

                                                      <th style="vertical-align: middle;min-width: 155px;" class="text-center">Pick up Date/Time</th>

                                                    <th style="vertical-align: middle;min-width: 80px;" class="text-center"><a href="#" class="desc">Pick up </a></th>

                                                    <th style="vertical-align: middle;min-width: 105px;" class="text-center">	Drop off</th>

                                                    

                                                     <th style="vertical-align: middle;min-width: 135px;" class="text-center">Start Date/Time</th>

                                                    <th style="vertical-align: middle;min-width: 126px;">End Date/Time</th>

                                        <th style="vertical-align: middle;min-width: 90px;" class="text-center">Trip Type</th>

                                        <th style="vertical-align: middle;" class="text-center">Status</th>

                                        <th style="vertical-align: middle;min-width: 110px;" class="text-center">Pay Method</th>

                                        <th style="vertical-align: middle;min-width: 110px;" class="text-center">Total Fare</th>

                                        <th style="vertical-align: middle;min-width: 130px;" class="text-center">Change Status</th>

                                        <th style="vertical-align: middle;" class="text-center">View</th>

                                        

                                                </tr>

                                                </thead>

                                                <tbody>

                                                    <?php

                                                    $i=1;//print_r($booking);

                                                    $date='';

                                                    foreach ($booking as $key => $value) {

                                                   if(!$value['taxi_id'])

                                                   {

                                                       $cabID=0;

                                                   }else{

                                                       $cabID=$value['taxi_id'];

                                                       

                                                   }

                                                   

                                                   if(!$value['driverId'])

                                                   {

                                                       $driver_id=0;

                                                   }else{

                                                       $driver_id=$value['driverId'];

                                                       

                                                   }

                                                   

                                                     $driverDetails=$this->db->query("SELECT * FROM driver_details where id=".$driver_id."")->row_array();

                                                //     echo $this->db->last_query();

                                                    $car=$this->db->query("SELECT * FROM cars where id=".$cabID."")->row_array();

                                                 //   echo $this->db->last_query();

                                                    ?>

                              

                                                    <tr>

                                                        <td><?php echo $value['id']; ?></td>

                                                        <td><?php if (strpos($value['bookingCurrentTime'], '/') !== false) {  $date = str_replace('/', '-', $value['bookingCurrentTime']); echo date('d/m/Y H:i', strtotime($date)); } else{echo  date("d/m/Y H:i", strtotime($value['bookingCurrentTime'])); };  ?></td>

                                                        <td><?php echo $value['username']; ?></td>

                                                        <td><?php echo $value['user_id']; ?></td>

                                                        <td><?php echo $value['phone']; ?></td>

                                                        <td><?php echo $car['car_number']; ?></td>

                                                        <td><?php echo $driverDetails['user_name']; ?></td>

                                                        <td><?php if (strpos($value['bookingdateandtime'], '/') !== false) {  $date = str_replace('/', '-', $value['bookingdateandtime']); echo date('d/m/Y H:i', strtotime($date)); } else{echo  date("d/m/Y H:i", strtotime($value['bookingdateandtime'])); };  ?></td>

                                                        <td><?php echo $value['pick_address']; ?></td>

                                                        <td><?php echo $value['drop_address']; ?></td>

                                                        

                                                    

                                                     <td><?php if (!empty($value['pickup_date_time']) && strpos($value['pickup_date_time'], '/') !== false) { $date = str_replace('/', '-', $value['pickup_date_time']); echo date('d/m/Y H:i', strtotime($date)); } else{  if($value['pickup_date_time']){ echo date("d/m/Y H:i", strtotime($value['pickup_date_time'] )); } };  ?></td>



                                                     

                                                     <td><?php if (!empty($value['drop_date_time']) && strpos($value['drop_date_time'], '/') !== false) { $date = str_replace('/', '-', $value['drop_date_time']); echo date('d/m/Y H:i', strtotime($date)); } else{  if($value['drop_date_time']){ echo date("d/m/Y H:i", strtotime($value['drop_date_time'] )); } };  ?></td>



                                                         

                                                           <td><?php echo ucwords($value['isdevice']); ?></td>

                                                            <td><?php if($value['status_code']=='Drver Not Found') { echo 'No Show'; } else if($value['booking_status']=='latter') { echo 'Upcoming'; } else{ echo ucwords($value['status_code']); } ?></td>

                                                             <td><?php  echo ucwords($value['payment_type']); ?></td>

                                                              <td>$<?php echo $value['amount']; ?></td>

                                                        <?php

                                                              if($value['status_code']=='Drver Not Found' || $value['status_code']=='completed' || $value['status_code']=='dispatcher-cancelled' ||  $value['status_code']=='admin-cancelled' || $value['status_code']=='user-cancelled' || $value['status_code']=='driver-cancelled'){

                                                                  ?>

                                                                   <td><select disabled class="form-controlm status" id="<?php echo $value['id'] ?>">

                                                            <option value="0">---Select---</option>

                                                            <option <?php if($value['status_code']=='Drver Not Found'){ echo 'selected'; } ?> value="Drver Not Found">No Show</option>

                                                               

                                                            </select></td>

                                                                  <?php

                                                              }else if($value['booking_status']=='latter') {

                                                              ?>

                                                       <td><select  class="form-controlm status" id="<?php echo $value['id'] ?>">

                                                            <option  value="0">---Select---</option>

                                                            <option <?php if($value['status_code']=='dispatcher-cancelled'){ echo 'selected'; } ?> value="admin-cancelled">Cancelled</option>

                                                               

                                                            </select></td>

                                                            <?php } else {

                                                              ?>

                                                       <td><select  class="form-controlm status" id="<?php echo $value['id'] ?>">

                                                            <option  value="0">---Select---</option>

                                                            <option <?php if($value['status_code']=='dispatcher-cancelled'){ echo 'selected'; } ?> value="admin-cancelled">Cancelled</option>

                                                               <option value="Drver Not Found">No Show</option> 

                                                            </select></td>

                                                            <?php } ?>

                                                        <td>



            <a onclick="window.location.href='view_booking_details?id=<?php echo $value['id']; ?>'" href="javascript:void(0);" class="table-link">

                <span class="fa-stack">

                    <i class="fa fa-square fa-stack-2x"></i>

                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>

                </span>

            </a>

            <!-- <a data-target="#uidemo-modals-alerts-delete-user" data-toggle="modal" class="table-link danger" href="javascript:void(0);" onclick="delete_single_user(1)">

                <span class="fa-stack">

                    <i class="fa fa-square fa-stack-2x"></i>

                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>

                </span>

            </a> --></td>

                                                    </tr>

                                                    <?php $i++; } ?>

                                                </tbody>

                                            </table>

                                        </div>

                                        <!--<ul class="pagination pull-right">

                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>

                                            <li><a href="javascript:void(0);">1</a></li>

                                            <li><a href="javascript:void(0);">2</a></li>

                                            <li><a href="javascript:void(0);">3</a></li>

                                            <li><a href="javascript:void(0);">4</a></li>

                                            <li><a href="javascript:void(0);">5</a></li>

                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>

                                        </ul>-->

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



                <?php include "includes/admin-footer.php"?>

               <!--  <input type="hidden" name="filter_col" id="filter_col" value="<?php echo $query; ?>"/> -->

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>

<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>

<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- this page specific inline scripts -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

   

<!-- this page specific inline scripts -->

<script type="text/javascript">

    $(function() {

  $('.date').datepicker({

  

      dateFormat:'dd/mm/yy',

    

  });

});
$('.table-responsive').on('change', '.status', function() {

        

        var id=$(this).attr('id');

        var status=$(this).val();

        if(status!=0)

        {

        $.ajax({

        url: "http://admin.infinitecabs.com.au/dispatcher/update_booking_status",

        type: "post",

        data: {id:id,status:status} ,

        success: function (response) {

            alert("Status Updated Successfully");

           // location.reload();

        },

        error: function(jqXHR, textStatus, errorThrown) {

           console.log(textStatus, errorThrown);

        }

    });  

        }

    });
$(window).load(function() {

        $(".cover").fadeOut(2000);

    });

 

</script>



</body>

</html>  