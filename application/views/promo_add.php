

<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Add Promo - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>

    <script src="js/html5shiv.js"></script>

    <script src="js/respond.min.js"></script>

    <![endif]-->



    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/admin_header.php";

    ?>

    <div id="page-wrapper" class="container">

        <div class="row">

            <?php

            include"includes/admin_sidebar.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Add Promo </h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Add Promo </span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                            <h2>Add Promo </h2>

                                        </div>

                                    </div>



                                    <div class="main-box-body clearfix">

                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->

                                        <?php echo form_open_multipart('admin/save_parmo',array('id' => 'fromId','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>

                                        <?php if($this->session->flashdata('msg')) {
                                            ?><div class = "alert alert-success"><?php

                                            echo $this->session->flashdata('msg');
                                            ?></div><?php

                                            }

                                        ?>

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">Promo Code</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" value="<?php echo @$promodetais['promocode'] ?>" required  placeholder="Enter Promo Code" name="pname" id="inputname" class="form-control" required>

                                                </div>

                                            </div>
                                               
                                    <div class="form-group">

                                    <label class="col-lg-2 control-label" for="type">Promo Type</label>

                                    <div id="inputname" class="col-lg-10">

                                        <select class="form-control" id="ptypes" name="ptypes" required="">
                                            <?php if(!$_GET['id']){  ?>
                                            <option value="">Select Type</option><?php } ?>

                                            <option value="amount" <?php if($promodetais['ptypes']=='amount'){ echo 'selected'; } ?>>Fixed Amount</option>

                                            <option value="percentage" <?php if($promodetais['ptypes']=='percentage'){ echo 'selected="true"'; } ?>>Percentage</option>

                                        </select>

                                    </div>

                                    </div>

                                   

                                            <div class="form-group amount" style="<?php if($_GET['id'] && $promodetais['ptypes']=='amount') { echo 'display:block'; } else { echo 'display:none;'; } ?>">

                                                <label class="col-lg-2 control-label" for="cartype">Amount</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" <?php if($_GET['id'] && $promodetais['ptypes']=='amount') { echo 'required'; } ?>   value="<?php echo @$promodetais['amount'] ?>"   placeholder="Amount" name="discount" id="amount" class="form-control">

                                                </div>

                                            </div>

                                             <div class="form-group per" style="<?php if($_GET['id'] && $promodetais['ptypes']=='percentage') { echo 'display:block'; } else { echo 'display:none;'; } ?>">

                                                <label class="col-lg-2 control-label" for="cartype">Percentage</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" <?php if($_GET['id'] && $promodetais['ptypes']=='percentage') { echo 'required'; } ?>  value="<?php echo @$promodetais['percentage'] ?>"   placeholder="Percentage" name="percentage" id="par" class="form-control" >

                                                </div>

                                            </div>

                                            

                                        

                                            

                                             <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">Start Date</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" value="<?php echo @$promodetais['startdate'] ?>" required  placeholder="Start Date" name="sdate" id="sdate" class="form-control date" required>

                                                </div>

                                            </div>

                                             <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">End Date</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" value="<?php echo @$promodetais['enddate'] ?>" required  placeholder="End Date" name="edate" id="edate" class="form-control date" required>

                                                </div>

                                            </div>

                                             <div class="form-group">

                                                <label class="col-lg-2 control-label" for="type">Promo Type</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <select class="form-control" id="ptype" name="ptype">

                                                        <option value="">--Select--</option>

                                                        <option value="all" <?php if($promodetais['type']=='all'){ echo 'selected'; } ?>>All Passengers</option>

                                                        <option value="fix" <?php if($promodetais['type']=='fix'){ echo 'selected'; } ?>>Fixed Passengers</option>

                                                    </select>

                                                </div>

                                            </div>

                                            <div class="form-group" id="user"  style="<?php if($promodetais['type']=='all'){ echo 'display:none;'; } ?>">

                                                <label class="col-lg-2 control-label" for="users"> Passengers</label>

                                                <div id="cartype1" class="col-lg-10">

                                                    <!-- <input type="text" required placeholder="Enter Car Type" name="car_type_id" id="cartype1" class="form-control"> -->

                                                    <select name="car_type_id[]" multiple class="form-control" id="select2" style="width:100% !important">

                                                        <option>Select Passengers</option>

                                                        <?php

                                                            $users=explode(",",$promodetais['users']);

                                                        ?>

                                                      <?php foreach ($userList as $value) { ?>

                                                         <option <?php if(in_array($value['id'], $users)){ echo 'selected'; } ?> value="<?php echo $value['id'] ?>"><?php echo $value['first_name'] ?></option>

                                                          <?php 

                                                      }

                                                      ?>

                                                    </select>

                                                </div>

                                            </div>

                                    

                                    

                                    <div class="form-group">

                                                <label class="col-lg-2 control-label" for="type">Used Count </label>

                                                <div id="inputname" class="col-lg-10">

                                                    <select class="form-control" id="count" name="count">

                                                        

                                                        <option value="one" <?php if($promodetais['count']=='one'){ echo 'selected'; } ?>>One Time</option>

                                                        <option value="unlimited" <?php if($promodetais['count']=='unlimited'){ echo 'selected'; } ?>>Unlimited Time</option>\

                                                        <option value="specific" <?php if($promodetais['count']=='specific'){ echo 'selected'; } ?>>Specific Time</option>



                                                    </select>

                                                </div>

                                            </div>

                                            

                                             <div class="form-group used" style="<?php if($promodetais['count']=='specific'){ echo 'display:block;'; } else { echo 'display:none;'; } ?>">

                                                <label class="col-lg-2 control-label" for="cartype">Used Count Number</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" value="<?php echo @$promodetais['number'] ?>"  placeholder="Used Count Number" name="number" id="inputname" class="form-control" >

                                                </div>

                                            </div>

                                            

                                            

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Description</label>

                                                <div id="carmodel" class="col-lg-10">

                                                    <textarea class="form-control"  name="note"><?php echo @$promodetais['description'] ?></textarea>

                                                    </div>

                                                </div>

                                                

                                                

                                                 <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Status</label>

                                                <div id="carmodel" class="col-lg-10">

                                                    <select class="form-control status" id="<?php echo $value['id'] ?>" name="status">

														        <option <?php if($promodetais['status']==1){ echo 'selected'; } ?> value="1">Active</option>

														       <option <?php if($promodetais['status']==0){ echo 'selected'; } ?> value="0">Inactive</option>      

														    </select>

                                                    </div>

                                                </div>



                                            <input type="hidden" value="<?php echo @$_GET['id'] ?>" name="id">



                                            <div class="form-group">

                                                <div class="col-lg-offset-2 col-lg-1">

                                                    <button style="display:block;margin:0 auto;" class="btn btn-success" name="save" id="" type="submit">

                                                        <span id="category_button" class="content">SUBMIT</span>

                                                    </button>

                                                </div>

                                            </div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php include "includes/admin-footer.php"?>

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<!-- Modal -->

<input type="hidden" id="bookingId" name="">

<div id="bookinmodal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Searching Driver</h4>

      </div>

      <div class="modal-body">

        <div id="search">

            <img src="https://i.gifer.com/YCZH.gif" style="text-align: center;width: 100%;">

        </div>

        <div id="bookingdata" style="display: none;">

            <div class="container">

  <h2 style="text-align: center;">Booking Details</h2>

  <ul class="list-group">

    <li class="list-group-item">Booking Id <span class="badge" id="bid">12</span></li>

    <li class="list-group-item">Driver Name <span class="badge" id="dname">5</span></li>

    <li class="list-group-item">Car Number <span class="badge" id="cnum">3</span></li>

     <li class="list-group-item">PickUp Address <span class="badge" id="padd">3</span></li>

      <li class="list-group-item">Drop Address <span class="badge" id="dadd">3</span></li>

     

  </ul>

</div>

            

        </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>

<!-- this page specific inline scripts -->

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript">

$('#sdate').datepicker({

      changeMonth: true,

            changeYear: true,

            yearRange:  '1964:2050',

            dateFormat: 'dd/mm/yy',
            minDate: new Date(),
     

});

$('#edate').datepicker({

      changeMonth: true,

            changeYear: true,

            yearRange:  '1964:2050',

            dateFormat: 'dd/mm/yy',
            minDate: new Date(),
      

});

$("#select2").select2({

placeholder: "Passengers",

});

$('#ptype').change(function(){

   var thisvalue=$(this).val();

   if(thisvalue=='all')

   {

      $('#user').hide();

   }else{

        

        

         $('#user').show();

   }

});



$('#count').change(function(){

   var thisvalue=$(this).val();

   if(thisvalue=='specific')

   {

     

      $('.used').show();

   }else{

        

         $('.used').hide();

         

   }

});





$('#ptypes').change(function(){

   var thisvalue=$(this).val();

   if(thisvalue=='percentage')

   {

     

      $('.per').show();

       $('.amount').hide();
       $('#par').attr('required','true')
       $('#amount').removeAttr('required')

   }else if(thisvalue=='amount')

   {
     $('#amount').attr('required','true')
       $('#par').removeAttr('required')
      $('.per').hide();

      $('.amount').show();

   }{

        

         $('.used').hide();

         

   }

});





    $('#notification-trigger-bouncyflip').click(function(){

        var formdata=$('#fromId').serialize();

        $.ajax({

        url: "<?php echo base_url() ?>admin/add_booking",

        type: "post",

        data: formdata ,

        success: function (response) {



          if(response=='')

          {

            alert("Something Went Wrong");

          }else{

            $('#bookingId').val(response);

            $('#bookinmodal').modal('show');;

          }

        },

        error: function(jqXHR, textStatus, errorThrown) {

           console.log(textStatus, errorThrown);

        }

    });

        });



    var refInterval = window.setInterval('update()', 10000); // 30 seconds



var update = function() {

    var bookingId=$('#bookingId').val();

    if(bookingId!='')

    {

    $.ajax({

        type : 'POST',

        url : '<?php echo base_url() ?>web_service/searchDriver',

        data:{ booking_id:bookingId },

        success : function(data){

            var datanew = JSON.parse(data);

            if(datanew.bookingId!='')

            {

            $('#search').hide();

            $('#bid').text(datanew.bookingData.bookingId);

            $('#dname').text(datanew.bookingData.driverName);

            $('#padd').text(datanew.bookingData.pickupLocation);

            $('#dadd').text(datanew.bookingData.dropLocation);

            $('#cnum').text(datanew.bookingData.drivercar_no);

            $('#bookingdata').show();

        }

           console.log(datanew.bookingData.bookingId);

        },

    });

}

};

update();



    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

</script>

<!--	<script>-->

<!--		function validate() {-->

<!--			var x = document.forms["add_user"]["cartype"].value;-->

<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->

<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->

<!--			var filename=document.getElementById('uploadImageFile').value;-->

<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			//alert(extension);-->

<!--		 if(image=='')-->

<!--			{-->

<!--				alert('car Image must be filled out');-->

<!--				return false;-->

<!--			}-->

<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->

<!--//				return true;-->

<!--//			}-->

<!--//-->

<!--//		 else-->

<!--//		 {-->

<!--//			 alert('Not Allowed Extension!');-->

<!--//			 return false;-->

<!--//		 }-->

<!--		else if (x == null || x == "") {-->

<!--				alert("Car Type must be filled out");-->

<!--				return false;-->

<!--			}-->

<!--			 else if (car_rate == null || car_rate == "") {-->

<!--			 alert("car rate must be filled out");-->

<!--			 return false;-->

<!--		 	}-->

<!--		 else if (seating_capacity == null || seating_capacity == "") {-->

<!--			 alert("seating capacity must be filled out");-->

<!--			 return false;-->

<!--		 }-->

<!---->

<!--		}-->

<!--	</script>-->

</body>

</html>