<!DOCTYPE html>







<html>







<head>







	<meta charset="UTF-8" />







	<meta name="viewport" content="width=device-width, initial-scale=1.0" />







	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />















	<title>Manage Driver - Infinite Cab</title>















	<!-- bootstrap -->







	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />















	<!-- RTL support - for demo only -->







	<script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>







	<!--







    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />







    And add "rtl" class to <body> element - e.g. <body class="rtl">







    -->















	<!-- libraries -->







	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />







	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />















	<!-- global styles -->







	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />















	<!-- this page specific styles -->







	<link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />







	<link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">















	<!-- Favicon -->







	<link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />















	<!-- google font libraries -->







	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>















	<!--[if lt IE 9]>







	<script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>







	<script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>







	<![endif]-->















	<style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}



	

.re-pt.filter-block .form-group .form-control {

    height: 34px;

    margin-bottom: 0px;

}



	.profile 



{



    min-height: 355px;



    display: inline-block;



    }



figcaption.ratings



{



    margin-top:20px;



    }



figcaption.ratings a



{



    color:#f1c40f;



    font-size:11px;



    }



figcaption.ratings a:hover



{



    color:#f39c12;



    text-decoration:none;



    }



.divider 



{



    border-top:1px solid rgba(0,0,0,0.1);



    }



.emphasis 



{



    border-top: 4px solid transparent;



    }



.emphasis:hover 



{



    border-top: 4px solid #1abc9c;



    }



.emphasis h2



{



    margin-bottom:0;



    }



span.tags 



{



    background: #1abc9c;



    border-radius: 2px;



    color: #f5f5f5;



    font-weight: bold;



    padding: 2px 4px;



    }



.dropdown-menu 



{



    background-color: #34495e;    



    box-shadow: none;



    -webkit-box-shadow: none;



    width: 250px;



    margin-left: -125px;



    left: 50%;



    }



.dropdown-menu .divider 



{



    background:none;    



    }



.dropdown-menu>li>a



{



    color:#f5f5f5;



    }



.dropup .dropdown-menu 



{



    margin-bottom:10px;



    }



.dropup .dropdown-menu:before 



{



    content: "";



    border-top: 10px solid #34495e;



    border-right: 10px solid transparent;



    border-left: 10px solid transparent;



    position: absolute;



    bottom: -10px;



    left: 50%;



    margin-left: -10px;



    z-index: 10;



    }



    td.act-pt {

    width: 110px !important;

}



	</style>







</head>







<body>







<div class="cover"></div>







<div id="theme-wrapper">







	<?php







	include"includes/admin_header.php";







	?>







	<div id="page-wrapper" class="container">







		<div class="row">







			<?php







			include"includes/admin_sidebar.php";







			?>







			<div id="content-wrapper">







				<div class="row" style="opacity: 1;">







					<div class="col-lg-12">







						<div class="row">







							<div class="col-lg-12">







								<div id="content-header" class="clearfix">







									<div class="pull-left">







										<h1>Manage Driver</h1>







									</div>







									<div class="pull-right">







										<ol class="breadcrumb">







											<li><a href="#">Home</a></li>







											<li class="active"><span>Manage Driver</span></li>







										</ol>







									</div>







								</div>







							</div>







						</div>







						<!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->







						<div class="col-lg-12">







							<!-- Single Delete -->







							<div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">







								<div class="modal-dialog">







									<div class="modal-content">







										<div class="modal-header">







											<i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>







										</div>







										<div class="modal-title">Are you sure you want to delete the selected user?</div>







										<div class="modal-body"></div>







										<div class="modal-footer">







											<button id="confirm-delete-button" onclick="delete_single_user_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>







											<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>







											<input type="hidden" value="" id="bookedid" name="bookedid">







											<button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>







										</div>







									</div> <!-- / .modal-content -->







								</div> <!-- / .modal-dialog -->







							</div> <!-- / .modal -->







							<!-- / Single Delete -->







							<!-- Multipal Delete -->







							<div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">







								<div class="modal-dialog">







									<div class="modal-content">







										<div class="modal-header">







											<i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>







										</div>







										<div class="modal-title">Are you sure you want to delete selected user?</div>







										<div class="modal-body"></div>







										<div class="modal-footer">







											<button onclick="delete_user()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>







											<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>







											<button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>







										</div>







									</div> <!-- / .modal-content -->







								</div> <!-- / .modal-dialog -->







							</div> <!-- / .modal -->







							<!-- / Multipal Delete -->







						</div>







						<!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->







						<div class="row">







							<div class="col-lg-12">







								<div class="main-box clearfix">







									<div class="panel">







										<div class="panel-body">







											







											<div class="filter-block re-pt">



                                                <form action="<?php echo base_url() ?>admin/manage_driver">



                                                



                                               <div class="row">



                                             



                                                    



                                                <div style="" class="form-group col-md-4">



                                                    <input type="text" placeholder="Licence No" value="<?php echo $_GET['car'] ?>" name="car" class="form-control">



                                                    <i class="fa fa-clock search-icon"></i>



                                                </div>



                                                



                                                



                                                <div style="" class="form-group col-md-4">



                                                    <input type="text" value="<?php echo $_GET['username'] ?>" placeholder="Cab No" name="username" class="form-control">



                                                    <i class="fa fa-clock search-icon"></i>



                                                </div>



                                                



                                                 <div style="" class="form-group col-md-4">



                                                    <input type="text" value="<?php echo $_GET['did'] ?>" placeholder="Driver ID" name="did" class="form-control">



                                                    <i class="fa fa-clock search-icon"></i>



                                                </div>



                                                <div style=";" class="form-group col-md-4">



                                                    <input type="text" value="<?php echo $_GET['phone'] ?>" placeholder="Mobile No" name="phone" class="form-control">



                                                    <i class="fa fa-clock search-icon"></i>



                                                </div>



                                                



                                                



                                               



                                                    



                                                <div style="" class="form-group col-md-4">



                                                    <select name="status" class="form-control">



                                                                    <option value="0">Status</option>

                                                                     <option value="0">All</option>



                                                 



                                                  <option value="booked" <?php if($_GET['status']=='booked'){ echo 'selected'; } ?>>On duty</option>



                                                  



                                                                    <option value="offline" <?php if($_GET['status']=='offline'){ echo 'selected'; } ?>>Off duty </option>



                                                                    <option value="active" <?php if($_GET['Active']=='Active'){ echo 'selected'; } ?>>Active</option>



                                                                    <option value="Inactive" <?php if($_GET['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>



                                                                   



                                                                                  </select>



                                                    </div>



                                                    



                                                     </div>



                                                     <a href="<?php echo base_url() ?>admin/manage_driver" class="btn btn-primary">



                                                    <i class="fa fa-refresh fa-lg"></i> Reset



                                                </a>



                                                



                                                <button type="submit" class="btn btn-primary" href="javascript:void(0)">



                                                    <i class="fa fa-search fa-lg"></i> Search



                                                </button>



                                                </form>



												<!--







                                                <a href="add-company.html" class="btn btn-primary pull-right">







                                                  <i class="fa fa-plus-circle fa-lg"></i> Add Company







                                                </a>







                                                -->



















												<span>&nbsp;</span>







												<!--												<div style="margin:0px !important;" class="form-group pull-right">-->







												<!--													<input type="text" placeholder="Search..." class="form-control">-->







												<!--													<i class="fa fa-search search-icon"></i>-->







												<!--												</div>-->







											</div>







										</div>







									</div>







									<div class="main-box-body clearfix">







										<div class="table-responsive">







											<table id="example" class="table table-hover table-bordered user-list">







	<thead>

       <tr>



         <th style="vertical-align: middle;">Rating</th>



			 <th style="vertical-align: middle;"><a href="javascript:void(0);">Driver Type</a></th>

			  <th style="vertical-align: middle;"><a href="javascript:void(0);">Driver Id</a></th>



				<th style="vertical-align: middle;"><a href="javascript:void(0);">First Name</a></th>



					<th style="vertical-align: middle;"><a href="javascript:void(0);">Last Name</a></th>



						<th style="vertical-align: middle;"><a href="javascript:void(0);">Mobile No</a></th>



							<th style="vertical-align: middle;"><a href="javascript:void(0);">License No</a></th>

 

							<th style="vertical-align: middle;"><a href="javascript:void(0);">ADDRESS</a></th>



			            <th style="vertical-align: middle;"><a href="javascript:void(0);">Logged in Cab</a></th>

 

													 	 		 			

                   <th style="vertical-align: middle;" class="text-center">Status</th> 



                                           <th style="vertical-align: middle;" class="text-center"> Login Status</th> 

 

                                                    <th style="vertical-align: middle;" class="text-center">Action</th>



 



												</tr>



												<tbody>



													<?php



													$i=1;



													foreach ($dataDriver as $key => $value) {



                                                                ?>



													<tr>



													   



														 <td><i class="fa fa-star"></i><a href="<?php echo base_url(); ?>admin/view_rating?id=<?php echo $value['did']; ?>"><span class="badge"><?php echo round($value['rating'],2); ?></span></a></td>



														

														 <td><?php echo $value['dtype'] ?></td>	

 <td><?php echo $value['user_name'] ?></td>	

                                                        	<td><?php echo $value['first_name'] ?></td>	



                                                        		



														<td><?php echo $value['last_name'] ?></td>



														<td><?php echo $value['phone'] ?></td>



														<td><?php echo $value['license_no'] ?></td>



														<td><?php echo $value['address'] ?></td>



														<td><?php echo $value['car_no'] ?></td>



														



														



														<td><?php if($value['driver_booking_status']=='free'|| $value['driver_booking_status']=='booked') { echo 'Online';  } else if($value['driver_booking_status']=='offline' || $value['driver_booking_status']==''){ echo 'Offline'; } else { echo ucwords($value['driver_booking_status']); } ?></td>



													<td>



													   <?php if($value['dstatus']=='Active') {



													       ?>



													       <span class=""><a class="updatestatus ab<?php echo $value['did'] ?> label label-success " id="<?php echo $value['did'] ?>"  table="driver_details" href="javascript:void(0)" onclick="status(265)" style="color: white;">Active</a></span>



													       <?php }else{ ?><span class=""><a class="updatestatus ab<?php echo $value['did'] ?> label label-default " id="<?php echo $value['did'] ?>"  table="driver_details" href="javascript:void(0)" onclick="status(265)" style="color: white;">Inactive</a></span> <?php } ?>



													       



													      



													   



													</td>



														<td class="act-pt">



															<a class="table-link" href="view_driver_details?id=<?php echo $value['did'] ?>">



                                                            <span class="fa-stack">



                                                            <i class="fa fa-square fa-stack-2x"></i>



                                                            <i class="fa fa-eye fa-stack-1x fa-inverse"></i>



                                                            </span>



                                                            </a>



                                                            <a class="table-link" href="view_driver_details?id=<?php echo $value['did'] ?>">



                                                            <span class="fa-stack">



                                                            <i class="fa fa-square fa-stack-2x"></i>



                                                            <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>



                                                            </span>



                                                            </a>





                                                           

                                                            <a class="table-link" onclick="return confirm('Are you sure?')"  href="delete_driver?id=<?php echo $value['did'] ?>">



                                                            <span class="fa-stack">



                                                            <i class="fa fa-square fa-stack-2x"></i>



                                                            <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>



                                                            </span>



                                                            </a>



                                                            



                                                           <!--  <a class="table-link driverde" href="javascript:"  time=" <?php echo $value['updated_at'] ?>" did=" <?php echo $value['did'] ?>" carnum=" <?php echo $value['car_type'] ?> , <?php echo $value['car_no'] ?>" onclick="initialize(<?php echo $value['driver_id'] ?>,<?php echo $value['latitude'] ?>,<?php echo $value['longlatitude'] ?>)" class="driverDetais" username="<?php echo $value['user_name'] ?>" status="<?php echo $value['driver_booking_status'] ?>" data-toggle="modal" data-target="#myModal">



                                                            <span class="fa-stack">



                                                            <i class="fa fa-square fa-stack-2x"></i>



                                                            <i class="fa fa-clock-o fa-stack-1x fa-inverse"></i>



                                                            </span>



                                                            </a>-->



                                                            



                                                           <!--  <a class="table-link" href="javascript:" data-toggle="modal" data-target="#wa<?php echo $value['did'] ?>">



                                                            <span class="fa-stack">



                                                            <i class="fa fa-square fa-stack-2x"></i>



                                                            <i class="fa fa-usd fa-stack-1x fa-inverse"></i>



                                                            </span>



                                                            </a>-->



														</td>



													</tr>



													



												<!-- Modal -->



                                                    <div id="wa<?php echo $value['did'] ?>" class="modal fade" role="dialog">



                                                      <div class="modal-dialog">



                                                    



                                                        <!-- Modal content-->



                                                        <div class="modal-content">



                                                          <div class="modal-header">



                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>



                                                            <h4 class="modal-title">Driver Wallet</h4>



                                                          </div>



                                                          <div class="modal-body">



                                                            <ul class="list-group">



                                                                      <li class="list-group-item">Cash   <span class="badge"><i class="fa fa-usd"></i><?php echo $value['case_amount']; ?></span></li>



                                                                      <li class="list-group-item">Online <span class="badge"><i class="fa fa-usd"></i><?php echo $value['online_amount']; ?></span></li>



                                                                       <li class="list-group-item">Due  <span class="badge"><i class="fa fa-usd"></i><?php echo $value['due_amount']; ?></span></li>







                                                                      <li class="list-group-item">Total  <span class="badge"><i class="fa fa-usd"></i><?php echo $value['wallet_amount']; ?></span></li>



                                                                    </ul>



                                                          </div>



                                                          <div class="modal-footer">



                                                              <a type="button" class="btn btn-default" href="<?php echo base_url() ?>admin/driver_payments?type=pay&id=<?php echo $value['did']; ?>">View Statement</a>



                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>



                                                          </div>



                                                        </div>



                                                    



                                                      </div>



                                                    </div>







													<?php $i++; } ?>



												</tbody>



												</thead>







											</table>







										</div>











										<!--<ul class="pagination pull-right">







                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>







                                            <li><a href="javascript:void(0);">1</a></li>







                                            <li><a href="javascript:void(0);">2</a></li>







                                            <li><a href="javascript:void(0);">3</a></li>







                                            <li><a href="javascript:void(0);">4</a></li>







                                            <li><a href="javascript:void(0);">5</a></li>







                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>







                                        </ul>-->







									</div>







								</div>







							</div>







						</div>







					</div>







				</div>















				<?php include "includes/admin-footer.php"?>







				<input type="hidden" name="filter_col" id="filter_col" value="<?php echo $query; ?>"/>







			</div>







		</div>







	</div>







</div>











<div id="myModal" class="modal fade" role="dialog">



  <div class="modal-dialog">







    <!-- Modal content-->



    <div class="modal-content">



      <div class="modal-header">



        <button type="button" class="close" data-dismiss="modal">&times;</button>



        <h4 class="modal-title">View Last Activity</h4>



      </div>



      <div class="modal-body">



       <div class="">



	<div class="row">



		<div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6" style="width: 100%;text-align: center;margin: auto;">



    	 <div class="profile">



            <div class="col-sm-12">



                



                <div class="col-xs-12 col-sm-8" style="text-align: left;">



                    



                    <h2 style="text-align: left;margin: auto;padding: 0 0;" id="username"><?php echo $value['user_name'] ?></h2>



                    



                    <p><strong >Car Number: <span id="cnum"></span></strong> </p>



                   



                     <p><strong >Status: <span id="dstatus"></span></strong> </p>



                       <p><strong>Last Time: <span id="time"></span></strong></p>



                      <p><strong>Last Location: </strong></p>



                      



                     <div id="map" style="width: 543px;height: 200px;"></div>







                    



                </div>             



               



            </div>



            



          



    	 </div>                 



		</div>



		



	</div>



</div>



      </div>



      <div class="modal-footer">



           <a href="" class="btn btn-success" id="timel">View Time Log</a>



           <a href="" class="btn btn-success" id="pay">View Payments</a>



		    <a href="" class="btn btn-success" id="book">View Bookings</a>



        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>



      </div>



    </div>







  </div>



</div>







<div id="config-tool" class="closed" style="display:none;">







	<a id="config-tool-cog">







		<i class="fa fa-cog"></i>







	</a>















	<div id="config-tool-options">







		<h4>Layout Options</h4>







		<ul>







			<li>







				<div class="checkbox-nice">







					<input type="checkbox" id="config-fixed-header" checked />







					<label for="config-fixed-header">







						Fixed Header







					</label>







				</div>







			</li>







			<li>







				<div class="checkbox-nice">







					<input type="checkbox" id="config-fixed-sidebar" checked />







					<label for="config-fixed-sidebar">







						Fixed Left Menu







					</label>







				</div>







			</li>







			<li>







				<div class="checkbox-nice">







					<input type="checkbox" id="config-fixed-footer" checked />







					<label for="config-fixed-footer">







						Fixed Footer







					</label>







				</div>







			</li>







			<li>







				<div class="checkbox-nice">







					<input type="checkbox" id="config-boxed-layout" />







					<label for="config-boxed-layout">







						Boxed Layout







					</label>







				</div>







			</li>







			<li>







				<div class="checkbox-nice">







					<input type="checkbox" id="config-rtl-layout" />







					<label for="config-rtl-layout">







						Right-to-Left







					</label>







				</div>







			</li>







		</ul>







		<br/>







		<h4>Skin Color</h4>







		<ul id="skin-colors" class="clearfix">







			<li>







				<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">







				</a>







			</li>







			<li>







				<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">







				</a>







			</li>







			<li>







				<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">







				</a>







			</li>







			<li>







				<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">







				</a>







			</li>







			<li>







				<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">







				</a>







			</li>







			<li>







				<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">







				</a>







			</li>







			<li>







				<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">







				</a>







			</li>







			<li>







				<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">







				</a>







			</li>







		</ul>







	</div>







</div>







<script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>







<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>







<!-- global scripts -->







<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->















<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>







<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>







<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>















<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->















<!-- this page specific scripts -->







<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>







<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>















<!-- theme scripts -->







<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>







<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>















<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>







<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>



      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyARycrYqBtU2Eb69rG0hIMxa32DTD28S4o"></script>







<!-- this page specific inline scripts -->







<script type="text/javascript">



$('#example').dataTable({bFilter: false, bInfo: false,'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
    }]});




 $('.status').change(function(){



        



        var id=$(this).attr('id');



        var status=$(this).val();



        $.ajax({



        url: "<?php echo base_url(); ?>admin/update_driver_status",



        type: "post",



        data: {id:id,status:status} ,



        success: function (response) {



            alert("Status Updated Successfully");



            location.reload();



        },



        error: function(jqXHR, textStatus, errorThrown) {



           console.log(textStatus, errorThrown);



        }



    });        



    });



	$(document).ready(function() {



$('.driverde').click(function(){



    var did=$(this).attr('did');



    did = did.replace(/\s/g, '');







    var paylink='<?php echo base_url() ?>admin/driver_payments?type=pay&id='+did;



    var book='<?php echo base_url() ?>admin/manage_booking?type=book&id='+did;



    var time='<?php echo base_url() ?>admin/driver_timelog?id='+did;



    $('#timel').attr('href',time);



    $('#pay').attr('href',paylink);



     $('#book').attr('href',book);



   $('#cnum').text($(this).attr('carnum')); 



     $('#time').text($(this).attr('time')); 



   var status='';



   if($(this).attr('status')=='free')



   {



       var status='Online';



   }else{



       status=$(this).attr('status');



   }



    $('#dstatus').text(status); 



    



    $('#username').text($(this).attr('username')); 



});



		//CHARTS







		function gd(year, day, month) {







			return new Date(year, month - 1, day).getTime();







		}







	});







</script>







<script type="text/javascript" language="javascript" >







	$(window).load(function() {







		$(".cover").fadeOut(2000);







	});







	function initialize(driverid,lat,lng){



    var uluru = {lat,lng};



        var map = new google.maps.Map(document.getElementById('map'), {



          zoom: 4,



          center: uluru



        });







        var contentString = '<div id="content">'+



            '<div id="siteNotice">'+



            '</div>'+



            '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+



            '<div id="bodyContent">'+



            '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +



            'sandstone rock formation in the southern part of the '+



            'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+



            'south west of the nearest large town, Alice Springs; 450&#160;km '+



            '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+



            'features of the Uluru - Kata Tjuta National Park. Uluru is '+



            'sacred to the Pitjantjatjara and Yankunytjatjara, the '+



            'Aboriginal people of the area. It has many springs, waterholes, '+



            'rock caves and ancient paintings. Uluru is listed as a World '+



            'Heritage Site.</p>'+



            '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+



            'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+



            '(last visited June 22, 2009).</p>'+



            '</div>'+



            '</div>';







        var infowindow = new google.maps.InfoWindow({



          content: contentString



        });







        var marker = new google.maps.Marker({



          position: uluru,



          map: map,



          title: 'Uluru (Ayers Rock)'



        });



        marker.addListener('click', function() {



          infowindow.open(map, marker);



        });



} 







//google.maps.event.addDomListener(window,'load', initialize);







</script>







</body>







</html>  