

<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Messaging - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>

    <script src="js/html5shiv.js"></script>

    <script src="js/respond.min.js"></script>

    <![endif]-->



    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}

    img{ max-width:100%;}

.inbox_people {

  background: #f8f8f8 none repeat scroll 0 0;

  float: left;

  overflow: hidden;

  width: 40%; border-right:1px solid #c4c4c4;

}

.inbox_msg {

  border: 1px solid #c4c4c4;

  clear: both;

  overflow: hidden;

}

.top_spac{ margin: 20px 0 0;}





.recent_heading {float: left; width:40%;}

.srch_bar {

  display: inline-block;

  text-align: right;

  width: 60%; padding:

}

.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}



.recent_heading h4 {

  color: #05728f;

  font-size: 21px;

  margin: auto;

}

.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}

.srch_bar .input-group-addon button {

  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;

  border: medium none;

  padding: 0;

  color: #707070;

  font-size: 18px;

}

.srch_bar .input-group-addon { margin: 0 0 0 -27px;}



.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}

.chat_ib h5 span{ font-size:13px; float:right;}

.chat_ib p{ font-size:14px; color:#989898; margin:auto}

.chat_img {

  float: left;

  width: 11%;

}

.chat_ib {

  float: left;

  padding: 0 0 0 15px;

  width: 88%;

}



.select2-container--default .select2-selection--multiple .select2-selection__rendered {

    line-height: 30px;

}

.input_msg_write {

    height: 80px;  border-color:#aaa;  border-radius: 5px;

}



.chat_people{ overflow:hidden; clear:both;}

.chat_list {

  border-bottom: 1px solid #c4c4c4;

  margin: 0;

  padding: 18px 16px 10px;

}

.inbox_chat { height: 550px; overflow-y: scroll;}



.active_chat{ background:#ebebeb;}



.incoming_msg_img {

  display: inline-block;

  width: 6%;

}

.received_msg {

  display: inline-block;

  padding: 0 0 0 10px;

  vertical-align: top;

  width: 92%;

 }

 .received_withd_msg p {

  background: #ebebeb none repeat scroll 0 0;

  border-radius: 3px;

  color: #646464;

  font-size: 14px;

  margin: 0;

  padding: 5px 10px 5px 12px;

  width: 100%;

}

.time_date {

  color: #747474;

  display: block;

  font-size: 12px;

  margin: 8px 0 0;

}

.received_withd_msg { width: 57%;}

.mesgs {

  float: left;

  padding: 15px 15px 15px 25px;

  width: 100%;

  margin-top:0px;

}



 .sent_msg p {

  background: #05728f none repeat scroll 0 0;

  border-radius: 3px;

  font-size: 14px;

  margin: 0; color:#fff;

  padding: 5px 10px 5px 12px;

  width:100%;

}

.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}

.sent_msg {

  float: right;

  width: 46%;

}

.input_msg_write input {

  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;

  border: medium none;

  color: #4c4c4c;

  font-size: 15px;

  min-height: 48px;

  width: 100%;

}



span.select2.select2-container.select2-container--default {

    width: 100% !important;

}





.type_msg {border-top: 0px solid #c4c4c4;position: relative;}

.msg_send_btn {

  background: #05728f none repeat scroll 0 0;

  border: medium none;

  border-radius: 50%;

  color: #fff;

  cursor: pointer;

  font-size: 17px;

  height: 33px;

  position: absolute;

  right: 0;

  top: 28px;

  width: 33px;

}

.messaging { padding: 0 0 50px 0;}

.msg_history {

  

}

textarea{

    width: 100%;

    height: 78px;

    border: none;

}

    </style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/admin_header.php";

    ?>

    <div id="page-wrapper" class="container">

        <div class="row">

            <?php

            include"includes/admin_sidebar.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Messages </h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Messaging </span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <?php if ($this->session->flashdata('success')) { ?>

        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>

    <?php } ?>

                            <div class="col-lg-12">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                            <h2>Message Send </h2>

                                        </div>

                                    </div>



                                    <div class="main-box-body clearfix">

                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->

                                        <?php echo form_open_multipart('admin/send_chat',array('id' => 'fromId','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>

                                        <?php if($this->session->flashdata('error_msg')) {

                                            echo $this->session->flashdata('error_msg');

                                            }

                                        ?>

                                            

                                           

                                            <div class="container">



<div class="messaging">

      <div class="inbox_msg">

        <!-- <div class="inbox_people">

          <div class="headind_srch">

            <div class="recent_heading">

              <h4>Recent</h4>

            </div>

            

          </div>

          <div class="inbox_chat">

              <?php foreach($result as $val){ ?>

            <div class="chat_list active_chat">

              <div class="chat_people">

                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>

                <div class="chat_ib">

                  <h5><?php echo $val['user_name']; ?> <span class="chat_date"><?php echo $val['created_at']; ?></span></h5>

                  <p><?php echo $val['message']; ?></p>

                </div>

              </div>

            </div>

           <?php } ?>

           

          </div>

        </div> -->

        <div class="mesgs">

            

           

          

          <div class="msg_history">

                <div class="form-group">

                <label for="sel1">Title</label>

                <br>

                            <input class="form-control" placeholder="Title" name="title" required="" autocomplete="off">



                </div>



          </div>

          

          <div class="msg_history">

                <div class="form-group">

                <label for="sel1">Select Cabs:</label>

                <br>

                <select class="form-control" id="select2" multiple name="driver[]" required>

                    <option value="all">All</option>

              <?php foreach($driverData as $value){ ?>

                <option value="<?php echo $value['id']; ?>"><?php echo $value['car_number']; ?></option>

             <?php } ?>

                </select>

                </div>



          </div>

          <div class="type_msg">

            <div class="input_msg_write">

              <textarea type="text" class="write_msg" name="msg" required placeholder="Type a message" /></textarea>

              <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>

            </div>

          </div>

        </div>

      </div>

      

      

    

      

    </div></div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php include "includes/admin-footer.php"?>

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>



<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>



<!-- Modal -->

<input type="hidden" id="bookingId" name="">

<div id="bookinmodal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Searching Driver</h4>

      </div>

      <div class="modal-body">

        <div id="search">

            <img src="https://i.gifer.com/YCZH.gif" style="text-align: center;width: 100%;">

        </div>

        <div id="bookingdata" style="display: none;">

            <div class="container">

  <h2 style="text-align: center;">Booking Details</h2>

  <ul class="list-group">

    <li class="list-group-item">Booking Id <span class="badge" id="bid">12</span></li>

    <li class="list-group-item">Driver Name <span class="badge" id="dname">5</span></li>

    <li class="list-group-item">Car Number <span class="badge" id="cnum">3</span></li>

     <li class="list-group-item">PickUp Address <span class="badge" id="padd">3</span></li>

      <li class="list-group-item">Drop Address <span class="badge" id="dadd">3</span></li>

     

  </ul>

</div>

            

        </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<!-- this page specific inline scripts -->

<script type="text/javascript">





$("#select2").select2({

placeholder: "Select Cabs",

    allowClear: true

});

$('#select2').on("select2:select", function (e) { 

           var data = e.params.data.text;

           if(data=='All'){

            //$("#select2 > option").prop("selected","selected");

            $("#select2").trigger("change");

           }

      });

    $('#notification-trigger-bouncyflip').click(function(){

        var formdata=$('#fromId').serialize();

        $.ajax({

        url: "<?php echo base_url() ?>admin/add_booking",

        type: "post",

        data: formdata ,

        success: function (response) {



          if(response=='')

          {

            alert("Something Went Wrong");

          }else{

            $('#bookingId').val(response);

            $('#bookinmodal').modal('show');;

          }

        },

        error: function(jqXHR, textStatus, errorThrown) {

           console.log(textStatus, errorThrown);

        }

    });

        });



    var refInterval = window.setInterval('update()', 10000); // 30 seconds



var update = function() {

    var bookingId=$('#bookingId').val();

    if(bookingId!='')

    {

    $.ajax({

        type : 'POST',

        url : '<?php echo base_url() ?>web_service/searchDriver',

        data:{ booking_id:bookingId },

        success : function(data){

            var datanew = JSON.parse(data);

            if(datanew.bookingId!='')

            {

            $('#search').hide();

            $('#bid').text(datanew.bookingData.bookingId);

            $('#dname').text(datanew.bookingData.driverName);

            $('#padd').text(datanew.bookingData.pickupLocation);

            $('#dadd').text(datanew.bookingData.dropLocation);

            $('#cnum').text(datanew.bookingData.drivercar_no);

            $('#bookingdata').show();

        }

           console.log(datanew.bookingData.bookingId);

        },

    });

}

};

update();



    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

</script>

<!--	<script>-->

<!--		function validate() {-->

<!--			var x = document.forms["add_user"]["cartype"].value;-->

<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->

<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->

<!--			var filename=document.getElementById('uploadImageFile').value;-->

<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			//alert(extension);-->

<!--		 if(image=='')-->

<!--			{-->

<!--				alert('car Image must be filled out');-->

<!--				return false;-->

<!--			}-->

<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->

<!--//				return true;-->

<!--//			}-->

<!--//-->

<!--//		 else-->

<!--//		 {-->

<!--//			 alert('Not Allowed Extension!');-->

<!--//			 return false;-->

<!--//		 }-->

<!--		else if (x == null || x == "") {-->

<!--				alert("Car Type must be filled out");-->

<!--				return false;-->

<!--			}-->

<!--			 else if (car_rate == null || car_rate == "") {-->

<!--			 alert("car rate must be filled out");-->

<!--			 return false;-->

<!--		 	}-->

<!--		 else if (seating_capacity == null || seating_capacity == "") {-->

<!--			 alert("seating capacity must be filled out");-->

<!--			 return false;-->

<!--		 }-->

<!---->

<!--		}-->

<!--	</script>-->

</body>

</html>