  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 
					<script type="text/javascript" src="https://js.stripe.com/v2/"></script>    
    
    <script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('pk_test_npAflnOwoQOckN49eDWjH6xi00LMruqQfZ');
        
        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $('#payBtn').removeAttr("disabled");
                //display the errors on the form
                // $('#payment-errors').attr('hidden', 'false');
                $('#payment-errors').addClass('alert alert-danger');
                $("#payment-errors").html(response.error.message);
                 location.href="/cartinvalid";
            } else {
                var form$ = $("#paymentFrm");
                //get token id
                var token = response['id'];
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                //submit form to the server
                form$.get(0).submit();
            }
        }
        $(document).ready(function() {
            //on form submit
            $("#paymentFrm").submit(function(event) {
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", "disabled");
                
                //create single-use token to charge the user
                Stripe.createToken({
                    number: $('#card_num').val(),
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                }, stripeResponseHandler);
                
                //submit from callback
                return false;
            });
        });
         $(document).ready(function(){
            
           $('#paymentFrm').submit();
        });
    </script>

  
	
	<div class="crl"></div>
	
 
    <section class="top-title-in" style="display:none;">

        <div class="container">
            <div class="row">
                <div class="col-md-4"> </div>
                <div class="col-md-4">
                    <div class="shop-1"> <span>Please fill the below details...</span>
                     
						
						</div>
                </div>
               
            </div>
        </div>
    </section>
    <section class="inner-page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
					 
			       
					 <div class="card">
                <div class="card-header bg-success text-white">Insert Your Card Information </div>
                <div class="card-body bg-light">
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                     <form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo base_url(); ?>web_service/check/<?php echo $result['bookingid'] ?>">
						<input type="hidden" name="order_id" value="<?php echo $result['bookingid'] ?>" />
                        <div class="form-group">
                            <input type="text" name="name" value="<?php echo $result['first_name'] ?>" class="form-control" placeholder="Name" required>
                        </div>  

                        <div class="form-group">
                            <input type="email" name="email" value="<?php echo $result['email'] ?>" class="form-control" placeholder="email@you.com" required />
                        </div>

                         <div class="form-group">
                            <input type="text" name="card_num" id="card_num" value="<?php echo $result['card_number'] ?>" class="form-control" placeholder="Card Number" autocomplete="off"  required>
                        </div>
                       
                        <?php
                        $card=explode("/",$result['expairy_date']);
                        $edate=$card[0];
                        $emonth=$card[1];
                        ?>
                        <div class="row">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $edate ?>" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" placeholder="MM"  required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $emonth ?>" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" placeholder="YYYY" required="" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" value="<?php echo $result['csv_no'] ?>" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" required>
                                </div>
                            </div>
                        </div>
                        

                       

                        <div class="form-group text-right">
                          <button class="btn btn-secondary" type="reset">Reset</button>
                          <button type="submit" id="payBtn" class="btn btn-success">Submit Payment</button>
                        </div>
                    </form>     
                </div>
            </div> 
					
					
					 
                </div>
				
				
				
       
 
    
				
                
        </div>
    </section>
	
	
	</form>
	
	
	
	
    <div class="crl"></div>
	
	
<script src="<?php echo base_url(); ?>assets/jquery.min.js"></script>

<script>
 
 
//$('#paymentFrm').submit();
 

</script>