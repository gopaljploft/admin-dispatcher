<?php 
$nd_booking_booking_form_final_price = 10;
include 'stripe/vendor/autoload.php';

			\Stripe\Stripe::setApiKey('sk_test_oAYEKnsUJPZ9Zyda9Xj8LqPb00lM6C6LwP');

					  $intent =\Stripe\PaymentIntent::create([
					  "amount" => $nd_booking_booking_form_final_price*100,
					  "currency" => 'USD',
					  "payment_method_types" => ["card"],
					  'capture_method' => 'manual',
					]);
					
?>
<style>
* {
  font-family: "Helvetica Neue", Helvetica;
  font-size: 15px;
  font-variant: normal;
  padding: 0;
  margin: 0;
}

html {
  height: 100%;
}

body {
  background: #E6EBF1;
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100%;
}

form {
  width: 480px;
  margin: 20px 0;
}

.group {
  background: white;
  box-shadow: 0 7px 14px 0 rgba(49,49,93,0.10),
              0 3px 6px 0 rgba(0,0,0,0.08);
  border-radius: 4px;
  margin-bottom: 20px;
}

label {
  position: relative;
  color: #8898AA;
  font-weight: 300;
  height: 40px;
  line-height: 40px;
  margin-left: 20px;
  display: flex;
  flex-direction: row;
}

.group label:not(:last-child) {
  border-bottom: 1px solid #F0F5FA;
}

label > span {
  width: 80px;
  text-align: right;
  margin-right: 30px;
}

.field {
  background: transparent;
  font-weight: 300;
  border: 0;
  color: #31325F;
  outline: none;
  flex: 1;
  padding-right: 10px;
  padding-left: 10px;
  cursor: text;
}

.field::-webkit-input-placeholder { color: #CFD7E0; }
.field::-moz-placeholder { color: #CFD7E0; }

button {
  float: left;
  display: block;
  background: #666EE8;
  color: white;
  box-shadow: 0 7px 14px 0 rgba(49,49,93,0.10),
              0 3px 6px 0 rgba(0,0,0,0.08);
  border-radius: 4px;
  border: 0;
  margin-top: 20px;
  font-size: 15px;
  font-weight: 400;
  width: 100%;
  height: 40px;
  line-height: 38px;
  outline: none;
}

button:focus {
  background: #555ABF;
}

button:active {
  background: #43458B;
}

.outcome {
  float: left;
  width: 100%;
  padding-top: 8px;
  min-height: 24px;
  text-align: center;
}

.success, .error {
  display: none;
  font-size: 13px;
}

.success.visible, .error.visible {
  display: inline;
}

.error {
  color: #E4584C;
}

.success {
  color: #666EE8;
}

.success .token {
  font-weight: 500;
  font-size: 13px;
}

</style>
<script src="https://js.stripe.com/v3/"></script>
<body>
  <form action="<?php echo base_url(); ?>web_service/check" method="post" id="payment-form">
    <div class="group">
      <label>
        <span>Name</span>
        <input name="cardholder-name" value="<?php echo $result['first_name'].' '.$result['last_name'] ?>" class="field" placeholder="Jane Doe" />
      </label>
      <label>
        <span>Email</span>
        <input class="field" value="<?php echo $result['email']; ?>" placeholder="abc@example.com" type="email"  name="cardholder-email" />
      </label>
    </div>
    <div class="group">
      <label>
        <span>Card</span>
        <div id="card-element" class="field"></div>
      </label>
    </div>
	<input type="hidden" name="paymentintent" value="<?php echo $intent->id; ?>"/>
    <button type="submit" data-secret="<?php echo $intent->client_secret; ?>" id="card-button">Pay $<?php echo $result['amount']; ?></button>
    <div class="outcome">
      <div class="error" id="card-errors"></div>
    </div>
  </form>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
 $("input[name=cardnumber]").val('4111111111111111');
 var stripe = Stripe("pk_test_npAflnOwoQOckN49eDWjH6xi00LMruqQfZ", {
				  betas: ["payment_intent_beta_3"]
				});
var elements = stripe.elements();

var card = elements.create('card', {
  hidePostalCode: true,
  style: {
    base: {
      iconColor: '#666EE8',
      color: '#31325F',
      lineHeight: '40px',
      fontWeight: 300,
      fontFamily: 'Helvetica Neue',
      fontSize: '15px',

      '::placeholder': {
        color: '#CFD7E0',
      },
    },
  }
});
card.mount('#card-element');

/* function setOutcome(result) {
  var successElement = document.querySelector('.success');
  var errorElement = document.querySelector('.error');
  successElement.classList.remove('visible');
  errorElement.classList.remove('visible');

  if (result.token) {
    // Use the token to create a charge or a customer
    // https://stripe.com/docs/charges
    successElement.querySelector('.token').textContent = result.token.id;
    successElement.classList.add('visible');
  } else if (result.error) {
    errorElement.textContent = result.error.message;
    errorElement.classList.add('visible');
  }
} */

//card.on('change', function(event) {
	 card.addEventListener("change", function(event) {
                  var displayError = document.getElementById("card-errors");
                  if (event.error) {
                    displayError.textContent = event.error.message;
                  } else {
                    displayError.textContent = "";
                  }
                });
  //setOutcome(event);
//});

document.querySelector('form').addEventListener('submit', function(e) {
  e.preventDefault();
  var form = document.querySelector('form');

	 	var cardButton = document.getElementById("card-button");
					var clientSecret = cardButton.dataset.secret;

					  stripe.handleCardPayment(
						clientSecret, card, {
						  source_data: {
							owner: {name: form.querySelector('input[name=cardholder-name]').value,
									email: form.querySelector('input[name=cardholder-email]').value}
						  }
						}
					  ).then(function(result) {
						if (result.error) {
							alert('Cancelled');
						} else {
								 form.submit(); 

						}
					  });
					  //stripe.createToken(card, extraDetails).then(setOutcome);
});



</script>
