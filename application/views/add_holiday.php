

<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



    <title>Add New P Holiday - Infinite Cab</title>



    <!-- bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />



    <!-- RTL support - for demo only -->

    <script src="js/demo-rtl.js"></script>

    <!--

    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />

    And add "rtl" class to <body> element - e.g. <body class="rtl">

    -->



    <!-- libraries -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />



    <!-- global styles -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />



    <!-- this page specific styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />

        <link rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css" type="text/css" />



    <!-- Favicon -->

    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.png" rel="shortcut icon" />



    <!-- google font libraries -->

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>

    <script src="js/html5shiv.js"></script>

    <script src="js/respond.min.js"></script>

    <![endif]-->



    <style type="text/css">.modal-open .modal{ background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;}</style>

    <style>

    .remove_button{

         margin-left: 20px;

    }

        .add_button{

                margin-left: 20px;

        }

    </style>

</head>

<body>

<div class="cover"></div>

<div id="theme-wrapper">

    <?php

    include"includes/admin_header.php";

    ?>

    <div id="page-wrapper" class="container">

        <div class="row">

            <?php

            include"includes/admin_sidebar.php";

            ?>

            <div id="content-wrapper">

                <div class="row" style="opacity: 1;">

                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-lg-12">

                                <div id="content-header" class="clearfix">

                                    <div class="pull-left">

                                        <h1>Add New P Holiday</h1>

                                    </div>

                                    <div class="pull-right">

                                        <ol class="breadcrumb">

                                            <li><a href="#">Home</a></li>

                                            <li class="active"><span>Add New P Holiday </span></li>

                                        </ol>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="main-box clearfix">

                                    <div class="panel">

                                        <div class="panel-body">

                                            <h2>Add New P Holiday </h2>

                                        </div>

                                    </div>



                                    <div class="main-box-body clearfix">

                                        <!--<form  enctype="multipart/form-data" method="post" class="form-horizontal" id="formAddUser" name="add_user" role="form"  action="<?php echo base_url()?>admin/insert_car" onsubmit="return validate()">-->

                                        <?php echo form_open_multipart('admin/save_holiday',array('id' => 'formAddUser','class' => 'form-horizontal','role' => 'from', 'onsubmit' => 'return validate()')); ?>

                                        <?php if($this->session->flashdata('error_msg')) {

                                            echo $this->session->flashdata('error_msg');

                                            }

                                        ?>

                             

                                            

                                         <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">

                                         <?php

                                            if($_GET['id'])

                                            {

                                                ?>

                                                  <div class="field_wrapper">

                                            <div class="row">

                                                <div class="col-md-6">

                                                      <div class="form-group">

                                               

                                                    <input type="text" value="<?php echo $holidayData['title']; ?>" required placeholder="Title" name="title" id="" class="form-control" required>

                                                

                                            </div>

                                                </div>

                                                

                                                  <div class="col-md-6">

                                                      <div class="form-group">

                                               

                                                    <input type="text" value="<?php echo $holidayData['date']; ?>" required placeholder="Date" name="date" id="" class="form-control stime" required>
                                                    <span class="dagner"></span>

                                                

                                            </div>

                                                </div>

                                                

                                               

                                            </div>

                                            </div>

                                                <?php

                                            }else{

                                         ?>

                                          <div class="field_wrapper">

                                            <div class="row">

                                                <div class="col-md-5 jb-pt">

                                                      <div class="form-group">

                                               

                                                    <input type="text" value="<?php echo $fareData['title']; ?>" required placeholder="Title" name="title[]" id="" class="form-control" required>

                                                

                                            </div>

                                                </div>

                                                

                                                  <div class="col-md-5">

                                                      <div class="form-group">

                                               

                                                    <input type="text" value="<?php echo $fareData['startTime']; ?>" required placeholder="Date" name="date[]" id="" class="form-control stime" required>

                                                

                                            </div>

                                                </div>

                                                

                                                  <div class="col-md-2">

                                                      <div class="form-group">

                                               

                                                  <a href="javascript:void(0);" class="add_button" title="Add field"><img src="https://demos.codexworld.com/add-remove-input-fields-dynamically-using-jquery/images/add-icon.png"/></a>

                                            </div>

                                                </div>

                                            </div>

                                            </div>

                                           

                                           <?php } ?>



                                            <div class="form-group">

                                                <div class="col-lg-1">

                                                    <button style="display:block;margin:0 auto;" class="btn btn-success" name="save" id="bb" type="submit">

                                                        <span id="category_button" class="content">SUBMIT</span>

                                                    </button>

                                                </div>

                                            </div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php include "includes/admin-footer.php"?>

            </div>

        </div>

    </div>

</div>



<div id="config-tool" class="closed" style="display:none;">

    <a id="config-tool-cog">

        <i class="fa fa-cog"></i>

    </a>



    <div id="config-tool-options">

        <h4>Layout Options</h4>

        <ul>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-header" checked />

                    <label for="config-fixed-header">

                        Fixed Header

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-sidebar" checked />

                    <label for="config-fixed-sidebar">

                        Fixed Left Menu

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-fixed-footer" checked />

                    <label for="config-fixed-footer">

                        Fixed Footer

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-boxed-layout" />

                    <label for="config-boxed-layout">

                        Boxed Layout

                    </label>

                </div>

            </li>

            <li>

                <div class="checkbox-nice">

                    <input type="checkbox" id="config-rtl-layout" />

                    <label for="config-rtl-layout">

                        Right-to-Left

                    </label>

                </div>

            </li>

        </ul>

        <br/>

        <h4>Skin Color</h4>

        <ul id="skin-colors" class="clearfix">

            <li>

                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">

                </a>

            </li>

            <li>

                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">

                </a>

            </li>

            <li>

                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">

                </a>

            </li>

        </ul>

    </div>

</div>


<style type="text/css">
    .disable-click{
    pointer-events:none;
}
</style>
<!-- global scripts -->

<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->



<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>

<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>



<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->



<!-- this page specific scripts -->

<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>

<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>



<!-- theme scripts -->

<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>

<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>

<script src="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">





  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- this page specific inline scripts -->

<script type="text/javascript">

$(document).ready(function(){

	var maxField = 10; //Input fields increment limitation

	var addButton = $('.add_button'); //Add button selector

	var wrapper = $('.field_wrapper'); //Input field wrapper

	var fieldHTML = '<div class="row"><div class="col-md-5"><div class="form-group"><input type="text" value="" required placeholder="Title" name="title[]" id="" class="form-control" required></div></div><div class="col-md-5"><div class="form-group"><input type="text" value="" required placeholder="Date" name="date[]" id="" class="form-control stime" required></div></div><div class="col-md-2"><div class="form-group"><a href="javascript:void(0);" class="remove_button"><img src="https://demos.codexworld.com/add-remove-input-fields-dynamically-using-jquery/images/remove-icon.png"></a></div></div></div>'; //New input field html 

	var x = 1; //Initial field counter is 1

	

	//Once add button is clicked

	$(addButton).click(function(){

		//Check maximum number of input fields

		if(x < maxField){ 

			x++; //Increment field counter

			$(wrapper).append(fieldHTML); //Add field html

		}

	});

	

	//Once remove button is clicked

	$(wrapper).on('click', '.remove_button', function(e){

		e.preventDefault();

		$(this).closest('.row').remove(); //Remove field html

		x--; //Decrement field counter

	});

});

$( ".stime" ).datepicker({

    dateFormat: "dd/mm/yy",

      changeMonth: true,

      changeYear: true,
       onSelect: function(dateText) {
        $.ajax({

            url: "<?php echo base_url(); ?>admin/checkDate",

            type: "post",

            data: {date:dateText},

            success: function (response) {

                if(response==1)
                {
                    $(this).next('span').text("wewew");
                     // alert(response)
                    $(".add_button").addClass("disable-click");
               
                    $('#bb').attr('disabled',true);
                    alert("This date already in use!");
                }else{
                    $(".add_button").removeClass("disable-click");
                    $('#bb').removeAttr('disabled');
                }
        }

    }); 
    }

    });

   $(document).on("focusin",".stime", function () {

       $(this).datepicker({

            dateFormat: "dd/mm/yy",

            changeMonth: true,

            changeYear: true,
            onSelect: function(dateText) {
        $.ajax({

            url: "<?php echo base_url(); ?>admin/save_fare",

            type: "post",

            data: $('#formAddUser').serialize(),

            success: function (response) {



           if(response==1)

           {

               alert("Please select differt time.this time is already in Records for this cab");

           }else{

              location.href="fare_manage";

           }

        }

    }); 
    }

           

        });

    });



    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

     

      

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

       $('#high').change(function(){

           

           if($(this).val()==1)

           {

               $('.feebox').show();

           }

           else

           {

               $('.feebox').hide();

           }

       });

        $('#btn_add').click(function(){

            

            var cab=$('#car_type_id').val();

            var high=$('#high').val();

            var stime=$('#stime').val();

            var etime=$('#etime').val();

            var flagfall=$('#flagfall').val();

            //var distance=$('#distance').val();

            var tolls=$('#tolls').val();

            var fee=$('#fee').val();

            

            

            if(cab=='')

            {

                alert("Please Select Cab");

            }

            else if(stime=='')

            {

                alert("Please Select Start Time");

            }

            else if(etime=='')

            {

                alert("Please Enter End Time");

            }

            else if(flagfall=='')

            {

                alert("Please Enter Flagfall");

            }else{

            

            $.ajax({

            url: "<?php echo base_url(); ?>admin/save_fare",

            type: "post",

            data: $('#formAddUser').serialize(),

            success: function (response) {



           if(response==1)

           {

               alert("Please select differt time.this time is already in Records for this cab");

           }else{

              location.href="fare_manage";

           }

        }

    }); 

            }

        });

</script>

<!--	<script>-->

<!--		function validate() {-->

<!--			var x = document.forms["add_user"]["cartype"].value;-->

<!--			var car_rate = document.forms["add_user"]["carrate"].value;-->

<!--			var seating_capacity = document.forms["add_user"]["seating_capacity"].value;-->

<!--			var filename=document.getElementById('uploadImageFile').value;-->

<!--			var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			var image=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();-->

<!--			//alert(extension);-->

<!--		 if(image=='')-->

<!--			{-->

<!--				alert('car Image must be filled out');-->

<!--				return false;-->

<!--			}-->

<!--//		 else if(extension=='jpg' || extension=='gif' || extension=='jpeg' || extension=='png' ) {-->

<!--//				return true;-->

<!--//			}-->

<!--//-->

<!--//		 else-->

<!--//		 {-->

<!--//			 alert('Not Allowed Extension!');-->

<!--//			 return false;-->

<!--//		 }-->

<!--		else if (x == null || x == "") {-->

<!--				alert("Car Type must be filled out");-->

<!--				return false;-->

<!--			}-->

<!--			 else if (car_rate == null || car_rate == "") {-->

<!--			 alert("car rate must be filled out");-->

<!--			 return false;-->

<!--		 	}-->

<!--		 else if (seating_capacity == null || seating_capacity == "") {-->

<!--			 alert("seating capacity must be filled out");-->

<!--			 return false;-->

<!--		 }-->

<!---->

<!--		}-->

<!--	</script>-->

</body>

</html>