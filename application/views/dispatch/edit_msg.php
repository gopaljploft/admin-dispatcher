<!DOCTYPE html>







<html>







<head>







    <meta charset="UTF-8" />







    <meta name="viewport" content="width=device-width, initial-scale=1.0" />







    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />















    <title>Schedule Overview - Infinite Cab</title>















    <!-- bootstrap -->







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />















    <!-- RTL support - for demo only -->







    <script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>







    <!--







    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />







    And add "rtl" class to <body> element - e.g. <body class="rtl">







    -->















    <!-- libraries -->







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />















    <!-- global styles -->







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />















    <!-- this page specific styles -->







    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />







    <link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">















    <!-- Favicon -->







    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />















    <!-- google font libraries -->







    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>















    <!--[if lt IE 9]>







    <script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>







    <script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>







    <![endif]-->















    <style type="text/css">.modal-open .modal{ /*background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;*/    width: 100%;

            left: 1% !important;

    }

    .filter-block.geet-sec span.select2-selection.select2-selection--single {

    height: 28px !important;

}



div#DataTables_Table_0_paginate ul.pagination {

    font-size: 13px;

}

  

 

 

body.theme-red.fixed-header.fixed-footer.fixed-leftmenu.pace-done .modal-dialog {

    position: fixed !important;

    right: 0 !important;

    top: 50px !important;

}



/*.main-box.clearfix.net-sev .table-responsive {

    height: 472px;

}*/



.main-box.clearfix.net-sev .table-responsive {

    height: 67vh;

}





/*.main-box{min-height: 80vh;}*/

#map {

    height: 65vh !important;}


.main-box .main-box-body {
    padding: 20px 20px 20px 20px;
}


/*.modal.in .modal-dialog{width: 90%;}*/







/*div#bookings .modal-dialog {



    width: 800px;



}



div#users .modal-dialog {



    width: 800px;



}



div#msgd .modal-dialog {



    width: 800px;



}



div#alertss .modal-dialog {



    width: 800px;



}



div#cabss .modal-dialog {



    width: 800px;



}



div#suspension .modal-dialog {



    width: 800px;



}







div#drivers .modal-dialog {



    width: 800px;



}*/



.dispatcher-dash .form-control{
        height: 34px !important;
}



</style>







</head>







<body>







<div class="cover"></div>







<div id="theme-wrapper">







    <?php











    include"includes/dispatch_header.php";







    ?>







    <div id="page-wrapper" class="container nav-small">







    <div class="row">







    <?php







    include"includes/dispatch_side.php";



?>



            <div id="content-wrapper">







                <div class="row" style="opacity: 1;">







                    <div class="col-lg-12">







                        <div class="row">







                            <div class="col-lg-12">







                                <div id="content-header" class="clearfix">







                                    <div class="pull-left">







                                        <h1>Edit Schedule</h1>







                                    </div>







                                    <div class="pull-right">







                                        <ol class="breadcrumb">







                                            <li><a href="#">Home</a></li>







                                            <li class="active"><span>Schedule Overview</span></li>







                                        </ol>







                                    </div>







                                </div>







                            </div>







                        </div>







                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->







                        <div class="col-lg-12">







                            <!-- Single Delete -->







                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">







                                <div class="modal-dialog">







                                    <div class="modal-content">







                                        <div class="modal-header">







                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>







                                        </div>







                                        <div class="modal-title">Are you sure you want to delete the selected user?</div>







                                        <div class="modal-body"></div>







                                        <div class="modal-footer">







                                            <button id="confirm-delete-button" onclick="delete_single_user_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>







                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>







                                            <input type="hidden" value="" id="bookedid" name="bookedid">







                                            <button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>







                                        </div>







                                    </div> <!-- / .modal-content -->







                                </div> <!-- / .modal-dialog -->







                            </div> <!-- / .modal -->







                            <!-- / Single Delete -->







                            <!-- Multipal Delete -->







                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">







                                <div class="modal-dialog">







                                    <div class="modal-content">







                                        <div class="modal-header">







                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>







                                        </div>







                                        <div class="modal-title">Are you sure you want to delete selected user?</div>







                                        <div class="modal-body"></div>







                                        <div class="modal-footer">







                                            <button onclick="delete_user()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>







                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>







                                            <button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>







                                        </div>







                                    </div> <!-- / .modal-content -->







                                </div> <!-- / .modal-dialog -->







                            </div> <!-- / .modal -->







                            <!-- / Multipal Delete -->







                        </div>







                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->



<div class="panel">







                        <div class="row">







                            <div class="col-md-12">







                                <div class="main-box clearfix net-sev">







                                    







                                    <div class="main-box-body clearfix">


                                            <?php echo form_open_multipart('dispatcher/save_seduled',array('id' => 'fromId','class' => 'form-horizontal mt-5','role' => 'from', 'onsubmit' => 'return validate()')); ?>

                                        <?php if($this->session->flashdata('error_msg')) {

                                            echo $this->session->flashdata('error_msg');

                                            }

                                        ?>

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">Title</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="text" value="<?php echo @$promodetais['title'] ?>" required  placeholder="Enter Title" name="pname" id="inputname" class="form-control" required>

                                                </div>

                                            </div>

                                            

                                            <div class="form-group">

                                                <label class="col-lg-2 control-label" for="seatingcapecity">Message</label>

                                                <div id="carmodel" class="col-lg-10">

                                                    <textarea class="form-control" name="note"><?php echo @$promodetais['massage'] ?></textarea>

                                                    </div>

                                                </div>

                                                <?php

                                                        if(!$_GET['id'])

                                                        {

                                                            ?>

                                                <div class="form-group">

                                                <label class="col-lg-2 control-label" for="cartype">Number Of Time</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <input type="number" min="0" max="10" onkeypress="return isNumberKey(this.value)"  value="<?php echo @$promodetais['title'] ?>" required  placeholder="Enter Number" name="number" onchange="createInput(this.value)" class="form-control" required>

                                                </div>
                                                

                                            </div>
                                            <span id="errorM" style="color: red;text-align: center;margin-left:240px;display: none;">Number of time value should not greater than 50</span>
                                            <?php } ?>

                                                <div class="my">

                                                    

                                                    <?php

                                                        if($_GET['id'])

                                                        {

                                                            $getmsg=$this->db->query("SELECT * FROM admin_message WHERE sid=".$_GET['id']."")->result_array();

                                                            foreach($getmsg as $val)

                                                            {

                                                            ?>

                                                           <div class="row"><div class="col-md-12">

                                                                <div class="form-group">

                                                                <label class="col-lg-2 control-label" for="cartype">Date</label><div id="inputname" class="col-lg-10"><input type="date" value="<?php echo date('Y-m-d',strtotime($val['datetime'])) ?>" required  placeholder="Date" name="sdate[]" id="sdate" class="form-control date" required></div></div></div>

                                                                <div class="col-md-12"><div class="form-group"><label class="col-lg-2 control-label" for="cartype">Time</label>

                                                                <div id="inputname" class="col-lg-10"><input type="time" value="<?php echo date('H:i:s',strtotime($val['datetime'])) ?>" required  placeholder="Time" name="time[]" id="sdate" class="form-control" required></div></div>

                                                                </div></div> 

                                                            <?php

                                                        }

                                                        }

                                                    ?>

                                                </div>



                                            

                                             <div class="form-group">

                                                <label class="col-lg-2 control-label" for="type">Type</label>

                                                <div id="inputname" class="col-lg-10">

                                                    <select class="form-control" id="ptype" name="ptype">

                                                        <option value="">--Select--</option>

                                                        <option value="all" <?php if($promodetais['type']=='all'){ echo 'selected'; } ?>>All Cab</option>

                                                        <option value="fix" <?php if($promodetais['type']=='fix'){ echo 'selected'; } ?>>Fixed Cab</option>

                                                    </select>

                                                </div>

                                            </div>

                                            <div class="form-group" id="user"  style="<?php if($promodetais['type']=='all'){ echo 'display:none;'; } else { echo 'display:block;'; } ?>">

                                                <label class="col-lg-2 control-label" for="users"> Cab</label>

                                                <div id="cartype1" class="col-lg-10">

                                                    <!-- <input type="text" required placeholder="Enter Car Type" name="car_type_id" id="cartype1" class="form-control"> -->

                                                    <select name="car_type_id[]" multiple class="form-control" id="select2" style="width:100% !important">

                                                        <option>All</option>

                                                        <?php

                                                            $users=explode(",",$promodetais['cab']);

                                                        ?>

                                                      <?php foreach ($userList as $value) { ?>

                                                         <option <?php if(in_array($value['id'], $users)){ echo 'selected'; } ?> value="<?php echo $value['id'] ?>"><?php echo $value['car_no'] ?></option>

                                                          <?php 

                                                      }

                                                      ?>

                                                    </select>

                                                </div>

                                            </div>

                                    



                                            <input type="hidden" value="<?php echo @$_GET['id'] ?>" name="id">



                                            <div class="form-group">

                                                <div class="col-lg-offset-2 col-lg-1">

                                                    <button style="display:block;" class="btn btn-success sb-bt" name="save" id="" type="submit">

                                                        <span id="category_button" class="content">SUBMIT</span>

                                                    </button>

                                                </div>

                                            </div>

                                        </form>



                                    </div>







                                </div>







                            </div>



                            



                            



                             



                        </div>







                    </div>







                </div>















                <?php include "includes/dispatch_footer.php"; ?>







            </div>







        </div>







    </div>







</div>















<div id="config-tool" class="closed" style="display:none;">







    <a id="config-tool-cog">







        <i class="fa fa-cog"></i>







    </a>















    <div id="config-tool-options">







        <h4>Layout Options</h4>







        <ul>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-fixed-header" checked />







                    <label for="config-fixed-header">







                        Fixed Header







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-fixed-sidebar" checked />







                    <label for="config-fixed-sidebar">







                        Fixed Left Menu







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-fixed-footer" checked />







                    <label for="config-fixed-footer">







                        Fixed Footer







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-boxed-layout" />







                    <label for="config-boxed-layout">







                        Boxed Layout







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-rtl-layout" />







                    <label for="config-rtl-layout">







                        Right-to-Left







                    </label>







                </div>







            </li>







        </ul>







        <br/>







        <h4>Skin Color</h4>







        <ul id="skin-colors" class="clearfix">







            <li>







                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">







                </a>







            </li>







            <li>







                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">







                </a>







            </li>







        </ul>







    </div>







</div>



















  <script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>







<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>







<!-- global scripts -->







<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->















<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>







<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>







<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>















<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->















<!-- this page specific scripts -->







<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>







<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>















<!-- theme scripts -->







<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>







<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>















<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>







<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>







<!-- this page specific inline scripts -->



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">







<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />



<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>



<!-- this page specific inline scripts -->


<script type="text/javascript">
var limitInput = function () {
        var value = parseInt(this.value, 10);
        var max = parseInt(this.max, 10);
        var min = parseInt(this.min, 10);

        if (value > max) {
            this.value = max;
        } else if (value < min) {
            this.value = min
        }
    };

function createInput(number)

{
   if(number <= 50)
   {
     $('#errorM').hide();
 var i;

 $('.my').html('');

for (i = 0; i < number; i++) {

  $('.my').append('<div class="row"><div class="col-md-12"><div class="form-group"><label class="col-lg-2 control-label" for="cartype">Date</label><div id="inputname" class="col-lg-10"><input type="date" required  placeholder="Date" name="sdate[]" id="sdate" class="form-control date" required></div></div></div><div class="col-md-12"><div class="form-group"><label class="col-lg-2 control-label" for="cartype">Time</label><div id="inputname" class="col-lg-10"><input type="time" required  placeholder="Time" name="time[]" id="sdate" class="form-control date" required></div></div></div></div>');

}   
}else{
    $('#errorM').show();
}
}

$( function() {

     $("input[class*=date]").live('click', function() {

           $(this).datepicker({showOn:'focus'}).focus();

        });

  } );

$("#select2").select2({



});

$('#ptype').change(function(){

   var thisvalue=$(this).val();

   if(thisvalue=='all')

   {

      $('#user').hide();

   }else{

        

        

         $('#user').show();

   }

});

    $('#notification-trigger-bouncyflip').click(function(){

        var formdata=$('#fromId').serialize();

        $.ajax({

        url: "<?php echo base_url() ?>admin/add_booking",

        type: "post",

        data: formdata ,

        success: function (response) {



          if(response=='')

          {

            alert("Something Went Wrong");

          }else{

            $('#bookingId').val(response);

            $('#bookinmodal').modal('show');;

          }

        },

        error: function(jqXHR, textStatus, errorThrown) {

           console.log(textStatus, errorThrown);

        }

    });

        });



    var refInterval = window.setInterval('update()', 10000); // 30 seconds



var update = function() {

    var bookingId=$('#bookingId').val();

    if(bookingId!='')

    {

    $.ajax({

        type : 'POST',

        url : '<?php echo base_url() ?>web_service/searchDriver',

        data:{ booking_id:bookingId },

        success : function(data){

            var datanew = JSON.parse(data);

            if(datanew.bookingId!='')

            {

            $('#search').hide();

            $('#bid').text(datanew.bookingData.bookingId);

            $('#dname').text(datanew.bookingData.driverName);

            $('#padd').text(datanew.bookingData.pickupLocation);

            $('#dadd').text(datanew.bookingData.dropLocation);

            $('#cnum').text(datanew.bookingData.drivercar_no);

            $('#bookingdata').show();

        }

           console.log(datanew.bookingData.bookingId);

        },

    });

}

};

update();



    $(window).load(function() {

        $(".cover").fadeOut(2000);

    });

    $(document).ready(function() {

        //CHARTS

        function gd(year, day, month) {

            return new Date(year, month - 1, day).getTime();

        }

    });

</script>



<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">



</script>



<script type="text/javascript" language="javascript" >







    $(document).ready(function() {







        var dataTable = $('#example').DataTable( {







            "processing": true,







            "serverSide": true,







            "order": [[ 3, "desc" ]],







            "columnDefs": [







                {







                    "targets": [ 0 ],







                    "visible": true,







                    "searchable": false,







                    "sortable" :false















                },







                {







                    "targets": [ 1 ],







                    "visible": true,







                    "searchable": false,







                    "sortable" :false















                },







                {







                    "targets": [ 2 ],







                    "visible": true,







                    "searchable": true,







                    "sortable" :true















                },







                {







                    "targets": [ 3 ],







                    "visible": true,







                    "type": "numeric",







                    "searchable": true,







                    "sortable" :true















                },







                {







                    "targets": [ 4 ],







                    "visible": true,







                    "searchable": true,







                    "sortable" :true















                },







                {







                    "targets": [ 5 ],







                    "visible": true,







                    "searchable": false,







                    "sortable" :false















                }







            ],







            "ajax":{







                url : '<?php echo base_url(); ?>zone/get_car_data', // json datasource







                type: "post",  // method  , by default get







                error: function(){  // error handling







                    $(".booking-grid-error").html("");







                    $("#example").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');







                    $("#booking-grid_processing").css("display","none");







                }







            }







        } );























        $("#allcheck").on('click',function() { // bulk checked







            var status = this.checked;







            $(".deleteRow").each( function() {







                $(this).prop("checked",status);







            });







        });







    } );







    function delete_user(){







        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked







            var ids = [];







            $('.deleteRow').each(function(){







                if($(this).is(':checked')) {







                    ids.push($(this).val());







                }







            });







            var ids_string = ids.toString();  // array to string conversion







            $.ajax({







                type: "POST",







                url: "<?php echo base_url(); ?>zone/delete_car_data",







                data: {data_ids:ids_string},







                success: function(result) {







                    var oTable1 = $('#example').DataTable();







                    oTable1.ajax.reload(null, false);







                },







                async:false







            });







        }







    }







    function delete_zone(single_id){







        $('#bookedid').val(single_id);







    }







    function delete_single_user_action()







    {







        var single_id = $('#bookedid').val();







        $.ajax({







            type: "POST",







            url: "<?php echo base_url(); ?>zone/delete_zone",







            data: {data_id: single_id},







            success: function (result) {







             location.reload();







            },







            async: false







        });







    }







</script>







</body>







</html>