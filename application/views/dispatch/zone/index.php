<!DOCTYPE html>







<html>







<head>







    <meta charset="UTF-8" />







    <meta name="viewport" content="width=device-width, initial-scale=1.0" />







    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />















    <title>Zone Overview - Infinite Cab</title>















    <!-- bootstrap -->







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/bootstrap/bootstrap.min.css" />















    <!-- RTL support - for demo only -->







    <script src="<?php echo base_url();?>application/views/js/demo-rtl.js"></script>







    <!--







    If you need RTL support just include here RTL CSS file <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-rtl.min.css" />







    And add "rtl" class to <body> element - e.g. <body class="rtl">







    -->















    <!-- libraries -->







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/font-awesome.css" />







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/libs/nanoscroller.css" />















    <!-- global styles -->







    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>application/views/css/compiled/theme_styles.css" />















    <!-- this page specific styles -->







    <link rel="stylesheet" href="<?php echo base_url();?>application/views/css/libs/daterangepicker.css" type="text/css" />







    <link href="<?php echo base_url();?>application/views/css/alerts-popup/pixel-admin.min.css" rel="stylesheet" type="text/css">















    <!-- Favicon -->







    <link type="image/x-icon" href="<?php echo base_url();?>upload/favicon.ico" rel="shortcut icon" />















    <!-- google font libraries -->







    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>















    <!--[if lt IE 9]>







    <script src="<?php echo base_url();?>application/views/js/html5shiv.js"></script>







    <script src="<?php echo base_url();?>application/views/js/respond.min.js"></script>







    <![endif]-->















    <style type="text/css">.modal-open .modal{ /*background:url(<?php echo base_url();?>application/views/img/transpharant.png) top left repeat;*/    width: 100%;

            left: 1% !important;

    }

    .filter-block.geet-sec span.select2-selection.select2-selection--single {

    height: 28px !important;

}



div#DataTables_Table_0_paginate ul.pagination {

    font-size: 13px;

}

  

 

 

body.theme-red.fixed-header.fixed-footer.fixed-leftmenu.pace-done .modal-dialog {

    position: fixed !important;

    right: 0 !important;

    top: 50px !important;

}



/*.main-box.clearfix.net-sev .table-responsive {

    height: 472px;

}*/



.main-box.clearfix.net-sev .table-responsive {

    height: 67vh;

}





/*.main-box{min-height: 80vh;}*/

#map {

    height: 65vh !important;}





/*.modal.in .modal-dialog{width: 90%;}*/







/*div#bookings .modal-dialog {



    width: 800px;



}



div#users .modal-dialog {



    width: 800px;



}



div#msgd .modal-dialog {



    width: 800px;



}



div#alertss .modal-dialog {



    width: 800px;



}



div#cabss .modal-dialog {



    width: 800px;



}



div#suspension .modal-dialog {



    width: 800px;



}







div#drivers .modal-dialog {



    width: 800px;



}*/







</style>







</head>







<body>







<div class="cover"></div>







<div id="theme-wrapper">







    <?php











    include"includes/dispatch_header.php";







    ?>







    <div id="page-wrapper" class="container nav-small">







    <div class="row">







    <?php







    include"includes/dispatch_side.php";



?>



            <div id="content-wrapper">







                <div class="row" style="opacity: 1;">







                    <div class="col-lg-12">







                        <div class="row">







                            <div class="col-lg-12">







                                <div id="content-header" class="clearfix">







                                    <div class="pull-left">







                                        <h1>Zone Overview</h1>







                                    </div>







                                    <div class="pull-right">







                                        <ol class="breadcrumb">







                                            <li><a href="#">Home</a></li>







                                            <li class="active"><span>Zone Overview</span></li>







                                        </ol>







                                    </div>







                                </div>







                            </div>







                        </div>







                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->







                        <div class="col-lg-12">







                            <!-- Single Delete -->







                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-user">







                                <div class="modal-dialog">







                                    <div class="modal-content">







                                        <div class="modal-header">







                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>







                                        </div>







                                        <div class="modal-title">Are you sure you want to delete the selected user?</div>







                                        <div class="modal-body"></div>







                                        <div class="modal-footer">







                                            <button id="confirm-delete-button" onclick="delete_single_user_action()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>







                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>







                                            <input type="hidden" value="" id="bookedid" name="bookedid">







                                            <button id="cancel-delete-button" data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>







                                        </div>







                                    </div> <!-- / .modal-content -->







                                </div> <!-- / .modal-dialog -->







                            </div> <!-- / .modal -->







                            <!-- / Single Delete -->







                            <!-- Multipal Delete -->







                            <div class="modal modal-alert modal-danger fade" id="uidemo-modals-alerts-delete-multipaluser">







                                <div class="modal-dialog">







                                    <div class="modal-content">







                                        <div class="modal-header">







                                            <i style="font-size:35px;" class="glyphicon glyphicon-trash"></i>







                                        </div>







                                        <div class="modal-title">Are you sure you want to delete selected user?</div>







                                        <div class="modal-body"></div>







                                        <div class="modal-footer">







                                            <button onclick="delete_user()" data-dismiss="modal" class="btn btn-primary" type="button">&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;</button>







                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>







                                            <button data-dismiss="modal" class="btn btn-primary" type="button">CANCEL</button>







                                        </div>







                                    </div> <!-- / .modal-content -->







                                </div> <!-- / .modal-dialog -->







                            </div> <!-- / .modal -->







                            <!-- / Multipal Delete -->







                        </div>







                        <!-- CONTEST Popup -------------------------------------------------------------------------------------------------------------------->



<div class="panel">







                                        <div class="panel-body">







                                            <div class="filter-block geet-sec">



                                                  <div class="row">



                                                <form action="https://admin.infinitecabs.com.au/dispatcher/indexzone">



                                                

                                                <div style="" class="form-group col-md-2">



                                                    <input style="height: 28px;" type="text" placeholder="Zone Name"  value="<?php echo @$_GET['name']; ?>" name="name" class="form-control">



                                                    <i class="fa fa-clock search-icon"></i>



                                                </div>



                                                



                                                



                                                <div style="" class="form-group col-md-2">



                                                     <select name="cab" class="form-control"   id="select2">



                                        <option value="">Cab No</option>



                                        <?php foreach ($cars as $value) { ?>



                                        <option <?php if($value['id']== @$_GET['cab']) { echo 'selected'; } ?> value="<?php echo $value['id'] ?>"><?php echo $value['car_number'] ?></option>



                                        <?php 



                                        }



                                        ?>



                                        </select>



                                                    <i class="fa fa-clock search-icon"></i>



                                                </div>



                                                



                                             <div style="" class="form-group col-md-4">



                                                     



                                                     <a style="padding: 4px 20px;" href="https://admin.infinitecabs.com.au/dispatcher/indexzone" class="btn btn-primary">



                                                    <i class="fa fa-refresh fa-lg"></i> Reset



                                                </a>     



                                                



                                                <button type="submit" style="padding: 4px 20px;" class="btn btn-primary" href="javascript:void(0)">



                                                    <i class="fa fa-search fa-lg"></i> Search



                                                </button>



                                               



                                          </div>







                                            </div>



 </form>



 </div>



                                        </div>







                                    </div>



                        <div class="row">







                            <div class="col-md-6">







                                <div class="main-box clearfix net-sev">







                                    <div class="panel">







                                        <div class="panel-body">











                                            <div class="filter-block pull-right">







                                                <!--







                                                <a href="add-company.html" class="btn btn-primary pull-right">







                                                  <i class="fa fa-plus-circle fa-lg"></i> Add Company







                                                </a>







                                                -->















                                                







                                                <span>&nbsp;</span>







                                                <!--                                                <div style="margin:0px !important;" class="form-group pull-right">-->







                                                <!--                                                    <input type="text" placeholder="Search..." class="form-control">-->







                                                <!--                                                    <i class="fa fa-search search-icon"></i>-->







                                                <!--                                                </div>-->







                                            </div>







                                        </div>







                                    </div>







                                    <div class="main-box-body clearfix">







                                        <div class="table-responsive">







                                            <table id="" class="zones table table-hover table-bordered user-list">







                                                <thead>







                                                <tr>







                                                    <th><a href="javascript:void(0);">Zone Name</a></th>

                                                    <th><a href="javascript:void(0);">Post Code</a></th>







                                                    <th><a href="javascript:void(0);">Total Cabs </a></th>



                                                    



                                                      <th><a href="javascript:void(0);">Trips</a></th>







                                                   







                                                    <th><a href="javascript:void(1);">Action</a></th>























                                                </tr>







                                                </thead>



                                                <?php



                                                $i=1;



                                                foreach ($zone as $key => $value) {



                                                    # code...



                                               $totalDrivers=$this->db->query("SELECT count(id) as totalDriver FROM diver_live_location WHERE zoneId=".$value['id']."  and (driver_booking_status='free' or driver_booking_status='booked' )")->row_array();



                                                



                                                 $totaltrip=$this->db->query("SELECT count(id) as totaltrip FROM bookingdetails WHERE zoneId=".$value['id']." and (status_code='pending' or status_code='on-trip' or status_code='completed' or status_code='accepted' or booking_status='booklater')")->row_array();







                                                ?>



                                                <tr>









                                            <td><a href="<?php echo base_url() ?>dispatcher/zoneDetails/<?php echo $value['id'] ?>?zipcode=<?php echo $value['zipcode'] ?>"><?php echo $value['zone_title'] ?></a></td>



                                            <td><?php echo $value['zipcode'] ?></td>





                                            <td>



                                                <?php



                                                $query=$this->db->query("SELECT COUNT(*) as subrab FROM zone_details where zoneId=".$value['id']."")->row_array();



                                                



                                                //echo $this->db->last_query();



                                               



                                                ?>



                                                <a href="javascript:void(0);"><?php echo $totalDrivers['totalDriver'] ?></a></td>



                                            <td>



                                              <?php echo $totaltrip['totaltrip'] ?>  



                                            </td>



                                           







                                           <td>







                                            <a class="table-link" href="<?php echo base_url() ?>dispatcher/zoneDetails/<?php echo $value['id'] ?>?zipcode=<?php echo $value['zipcode'] ?>">



                <span class="fa-stack">



                    <i class="fa fa-square fa-stack-2x"></i>



                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>



                </span>



            </a>



                                               <a class="table-link edit" id="<?php echo $value['id'] ?>"  data-toggle="modal" data-target="#myModal<?php echo $value['id'] ?>" href="javascript:">



                <span class="fa-stack">



                    <i class="fa fa-square fa-stack-2x"></i>



                    <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>



                </span>



            </a>



            



                                               



          



            <a data-target="#uidemo-modals-alerts-delete-user" data-toggle="modal" class="table-link danger" href="javascript:void(0);" onclick="delete_zone(<?php echo $value['id'] ?>)">



                <span class="fa-stack">



                    <i class="fa fa-square fa-stack-2x"></i>



                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>



                </span>



            </a></td>























                                                </tr>



                                                



                                                



<!-- Modal -->



<div id="myModal<?php echo $value['id'] ?>" class="modal fade" role="dialog">



  <div class="modal-dialog">







    <!-- Modal content-->



    <div class="modal-content">



      <div class="modal-header">



        <button type="button" class="close" data-dismiss="modal">&times;</button>



        <h4 class="modal-title">Edit Zone</h4>



      </div>



      <div class="modal-body">



       <form action="/action_page.php">



  <div class="form-group">



    <label for="email">Zone Title:</label>



    <input type="texi" value="<?php echo $value['zone_title'] ?>" class="form-control" id="email<?php echo $value['id'] ?>">



  </div>

<div style="" class="form-group">

<lable>Post Code</lable>

<input type="text" id="zipcode<?php echo $value['id'] ?>" value="<?php echo $value['zipcode'] ?>" name="zipcode" placeholder="Post Code" class="form-control" autocomplete="off">



</div>

  <div class="form-group">



    <label for="pwd">Dispatch Time:</label>



    <input type="texi" value="<?php echo $value['time'] ?>"  class="form-control" id="pwd<?php echo $value['id'] ?>">



  </div>



 



  <button type="button" id="<?php echo $value['id'] ?>" class="btn btn-primary  update">Submit</button>



</form>



      </div>



      <div class="modal-footer">



        <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>



      </div>



    </div>







  </div>



</div>



                                            <?php $i++; } ?>



                                            </table>







                                        </div>











                                        <!--<ul class="pagination pull-right">







                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></li>







                                            <li><a href="javascript:void(0);">1</a></li>







                                            <li><a href="javascript:void(0);">2</a></li>







                                            <li><a href="javascript:void(0);">3</a></li>







                                            <li><a href="javascript:void(0);">4</a></li>







                                            <li><a href="javascript:void(0);">5</a></li>







                                            <li><a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a></li>







                                        </ul>-->







                                    </div>







                                </div>







                            </div>



                            



                            



                             <div class="col-md-6">







                                <div class="main-box clearfix">







                                    <div class="panel">







                                        <div class="panel-body">







                                            <h2 class="pull-left">Manage Zone</h2>







                                            <div class="filter-block pull-right">



  



                                              



                                                    <a class="btn btn-primary pull-right" href="javascript:void(0)" onclick="window.location.href='<?php echo base_url() ?>dispatcher/geofancing'">







                                                    <i class="fa fa-plus-circle fa-lg"></i> Create New Zone







                                                </a>



                                            </div>







                                        </div>







                                    </div>







                                    <div class="main-box-body clearfix">







                                        







                                        <style>

.modal-open .modal{

    left:1% !important !important;

}

                                                #map {



                                                width: 100%;



                                                height: 446px;



                                                margin: 0 auto;



                                                }



                                        </style>



                                       <div id="map"></div>







                                    </div>







                                </div>







                            </div>







                        </div>







                    </div>







                </div>















                <?php include "includes/dispatch_footer.php"; ?>







            </div>







        </div>







    </div>







</div>















<div id="config-tool" class="closed" style="display:none;">







    <a id="config-tool-cog">







        <i class="fa fa-cog"></i>







    </a>















    <div id="config-tool-options">







        <h4>Layout Options</h4>







        <ul>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-fixed-header" checked />







                    <label for="config-fixed-header">







                        Fixed Header







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-fixed-sidebar" checked />







                    <label for="config-fixed-sidebar">







                        Fixed Left Menu







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-fixed-footer" checked />







                    <label for="config-fixed-footer">







                        Fixed Footer







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-boxed-layout" />







                    <label for="config-boxed-layout">







                        Boxed Layout







                    </label>







                </div>







            </li>







            <li>







                <div class="checkbox-nice">







                    <input type="checkbox" id="config-rtl-layout" />







                    <label for="config-rtl-layout">







                        Right-to-Left







                    </label>







                </div>







            </li>







        </ul>







        <br/>







        <h4>Skin Color</h4>







        <ul id="skin-colors" class="clearfix">







            <li>







                <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">







                </a>







            </li>







            <li>







                <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">







                </a>







            </li>







            <li>







                <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">







                </a>







            </li>







        </ul>







    </div>







</div>



















  <script src="<?php echo base_url();?>application/views/js/jquery-1.12.3.js"></script>







<script src="<?php echo base_url();?>application/views/js/jquery.dataTables.js"></script>







<!-- global scripts -->







<script src="<?php echo base_url();?>application/views/js/demo-skin-changer.js"></script> <!-- only for demo -->















<script src="<?php echo base_url();?>application/views/js/jquery.js"></script>







<script src="<?php echo base_url();?>application/views/js/bootstrap.js"></script>







<script src="<?php echo base_url();?>application/views/js/jquery.nanoscroller.min.js"></script>















<script src="<?php echo base_url();?>application/views/js/demo.js"></script> <!-- only for demo -->















<!-- this page specific scripts -->







<script src="<?php echo base_url();?>application/views/js/moment.min.js"></script>







<script src="<?php echo base_url();?>application/views/js/gdp-data.js"></script>















<!-- theme scripts -->







<script src="<?php echo base_url();?>application/views/js/scripts.js"></script>







<script src="<?php echo base_url();?>application/views/js/pace.min.js"></script>















<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.js"></script>







<script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>







<!-- this page specific inline scripts -->



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">







<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />



<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>



<!-- this page specific inline scripts -->



<script type="text/javascript">



$('.select2').select2();



   $('.update').click(function(){



       



 var single_id= $(this).attr('id');



 var title=$('#email'+single_id).val();



  var time=$('#pwd'+single_id).val();

   var zipcode=$('#zipcode'+single_id).val();







      







        $.ajax({







            type: "POST",







            url: "<?php echo base_url(); ?>zone/updateZone",







            data: {data_id: single_id,title: title,time: time,zipcode:zipcode},







            success: function (result) {



            alert("Update Successfully");



             location.reload();







            }



        });







   })



   



$("#select2").select2({







});



var map;

<?php

//print_r($query2);die;

if($query2)

{

?>

function initMap() {



var map;



var bounds = new google.maps.LatLngBounds();



var mapOptions = {



mapTypeId: 'roadmap'



};







// Display a map on the web page



map = new google.maps.Map(document.getElementById("map"), mapOptions);



map.setTilt(50);







// Multiple markers location, latitude, and longitude







var markers = [



<?php







foreach ($query2 as $key => $value) {







?>



['<?php echo $value['car_number'] ?>', <?php echo $value['latitude'] ?>, <?php echo $value['longlatitude'] ?>],



<?php } ?>



];







console.log(markers);         



// Info window content



var infoWindowContent = [



<?php

    

foreach ($query2 as $key => $value) {







?>



['<div class="info_content">' +



'<h3><?php echo $value['name'] ?>,<?php echo $value['car_no'] ?></h3>' +



'<p><strong>Driver ID : <?php echo $value['user_name'] ?></strong></p>'+'<p><strong>Driver First Name : <?php echo $value['first_name'] ?></strong></p>'+' <p><strong>Driver Last Name : <?php echo $value['last_name'] ?></strong></p>'+' <p><strong>Driver Mobile No : <?php echo $value['phone'] ?></strong></p>'+' <p><strong>Cab No : <?php echo $value['car_number'] ?></strong></p>'+' <p><strong>Driver Status  : <?php echo ucwords($value['driver_booking_status']); ?></strong></p>'+' <p><strong>Current Zone  : <?php echo $value['zone_title'] ?></strong></p>'+' <p><strong>Lat : <?php echo $value['latitude'] ?></strong></p>'+' <p><strong>Lng : <?php echo $value['longlatitude'] ?></strong></p> '+ '</div>'],



<?php } ?>



];







// Add multiple markers to map



var infoWindow = new google.maps.InfoWindow(), marker, i;







// Place each marker on the map  



for( i = 0; i < markers.length; i++ ) {



console.log(markers[0][1]);



var position = new google.maps.LatLng(<?php echo $value['latitude'] ?>,<?php echo $value['longlatitude'] ?>);



bounds.extend(position);



marker = new google.maps.Marker({



position: position,



map: map,



title: markers[i][0],



icon:"https://admin.infinitecabs.com.au/upload/car.png"



});







// Add info window to marker    



google.maps.event.addListener(marker, 'click', (function(marker, i) {



return function() {



infoWindow.setContent(infoWindowContent[i][0]);



infoWindow.open(map, marker);



}



})(marker, i));







// Center the map to fit all markers on the screen



map.fitBounds(bounds);



}







// Set zoom level



var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {



this.setZoom(12);



google.maps.event.removeListener(boundsListener);



});







}

<?php } else {

    ?>

    function initMap() {

    var mapOptions = {

  center: new google.maps.LatLng(52.640143,1.28685),

      zoom: 15,

      mapTypeId: google.maps.MapTypeId.ROADMAP

    };

    var map = new google.maps.Map(document.getElementById("map"),

        mapOptions);



var marker = new google.maps.Marker({

    position: new google.maps.LatLng(52.640143,1.28685),

    map: map,

    title: "Mark On Map"

});

  }

    <?php

} ?>



    $(window).load(function() {







        $(".cover").fadeOut(2000);







    });







    $(document).ready(function() {







        //CHARTS







        function gd(year, day, month) {







            return new Date(year, month - 1, day).getTime();







        }







    });







</script>



<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGfN_DpffbZVFPb7BafMVEgWIE7VItEE8&callback=initMap">



</script>



<script type="text/javascript" language="javascript" >







    $(document).ready(function() {







        var dataTable = $('#example').DataTable( {







            "processing": true,







            "serverSide": true,







            "order": [[ 3, "desc" ]],







            "columnDefs": [







                {







                    "targets": [ 0 ],







                    "visible": true,







                    "searchable": false,







                    "sortable" :false















                },







                {







                    "targets": [ 1 ],







                    "visible": true,







                    "searchable": false,







                    "sortable" :false















                },







                {







                    "targets": [ 2 ],







                    "visible": true,







                    "searchable": true,







                    "sortable" :true















                },







                {







                    "targets": [ 3 ],







                    "visible": true,







                    "type": "numeric",







                    "searchable": true,







                    "sortable" :true















                },







                {







                    "targets": [ 4 ],







                    "visible": true,







                    "searchable": true,







                    "sortable" :true















                },







                {







                    "targets": [ 5 ],







                    "visible": true,







                    "searchable": false,







                    "sortable" :false















                }







            ],







            "ajax":{







                url : '<?php echo base_url(); ?>zone/get_car_data', // json datasource







                type: "post",  // method  , by default get







                error: function(){  // error handling







                    $(".booking-grid-error").html("");







                    $("#example").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');







                    $("#booking-grid_processing").css("display","none");







                }







            }







        } );























        $("#allcheck").on('click',function() { // bulk checked







            var status = this.checked;







            $(".deleteRow").each( function() {







                $(this).prop("checked",status);







            });







        });







    } );







    function delete_user(){







        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked







            var ids = [];







            $('.deleteRow').each(function(){







                if($(this).is(':checked')) {







                    ids.push($(this).val());







                }







            });







            var ids_string = ids.toString();  // array to string conversion







            $.ajax({







                type: "POST",







                url: "<?php echo base_url(); ?>zone/delete_car_data",







                data: {data_ids:ids_string},







                success: function(result) {







                    var oTable1 = $('#example').DataTable();







                    oTable1.ajax.reload(null, false);







                },







                async:false







            });







        }







    }







    function delete_zone(single_id){







        $('#bookedid').val(single_id);







    }







    function delete_single_user_action()







    {







        var single_id = $('#bookedid').val();







        $.ajax({







            type: "POST",







            url: "<?php echo base_url(); ?>zone/delete_zone",







            data: {data_id: single_id},







            success: function (result) {







             location.reload();







            },







            async: false







        });







    }







</script>







</body>







</html>