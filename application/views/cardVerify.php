  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 
					<script type="text/javascript" src="https://js.stripe.com/v2/"></script>    
    
    <script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('pk_live_ADefzoPRs7aBTrawQ5B6lP4t00HHcwRXul');
        
        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            
			var form$ = $("#paymentFrm");
                //get token id
                var token = response['id'];
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                //submit form to the server
                form$.get(0).submit();
			
        }
        $(document).ready(function() {
            //on form submit
            $("#paymentFrm").submit(function(event) {
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", "disabled");
                
                //create single-use token to charge the user
                Stripe.createToken({
                    number: $('#card_num').val(),
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                }, stripeResponseHandler);
                
                //submit from callback
               // alert()
                return false;
            });
        });
        $(document).ready(function(){
            
         //  $('#paymentFrm').submit();
        });
        window.onload=function(){ 
     //window.setTimeout($('#paymentFrm').submit(), 30000); 
   
};
    </script>

  <body style="">
	
	<div class="crl"></div>
	
   <img src="https://sample.jploftsolutions.in/homesBasket/images/imgpsh_fullsize_anim.gif" style="margin-left: 0;margin-top: 45%;mix-blend-mode: darken;    width: 100%; display: none;">
    <section class="top-title-in" style="display:none;">

        <div class="container">
            <div class="row">
                <div class="col-md-4"> </div>
                <div class="col-md-4">
                     
						
						</div>
                </div>
               
            </div>
        </div>
    </section>
    <section class="inner-page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
					 
			       
					 <div class="card" >
                <div class="card-body bg-light">
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                     <form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo base_url(); ?>web_service/checkPayment/<?php echo $result['id'] ?>" >
						<input type="hidden" name="order_id" value="<?php echo $result['id'] ?>" />
                        <div class="form-group" style="display:none;">
                            <input type="text" name="name" value="<?php echo $result['card_holder_name'] ?>" class="form-control" placeholder="Name" required>
                        </div>  

                        <div class="form-group" style="display:none;">
                            <input type="email" name="email" value="<?php echo $result['email'] ?>" class="form-control" placeholder="email@you.com" required />
                        </div>

                         <div class="form-group" style="display:none;">
                            <input type="text" name="card_num" id="card_num" value="<?php echo $result['card_number'] ?>" class="form-control" placeholder="Card Number" autocomplete="off"  required>
                        </div>
                       
                        <?php
                        $card=explode("/",$result['expairy_date']);
                        $edate=$card[0];
                        $emonth=$card[1];
                        ?>
                        <div class="row" style="display:none;">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $edate ?>" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" placeholder="MM"  required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $emonth ?>" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" placeholder="YYYY" required="" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" value="<?php echo $result['csv_no'] ?>" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" required>
                                </div>
                            </div>
                        </div>
                        

                       

                        <div class="form-group text-center" style="margin-top:15px;">
                            <img src="https://admin.infinitecabs.com.au/upload/check.png" style="height:60px; width: 60px; margin-bottom: 20px;">
                            <p style="margin-bottom: 20px; font-size: 16px;">Press “Submit” to Verify your card Details.</p>
                          <a class="btn" href="https://admin.infinitecabs.com.au/payment_failed" style="margin-right: 20px; border: 1px solid #888; font-size: 16px; padding: 8px 20px;border-radius: 5px;">Cancel</a>
                          <button type="submit" id="payBtn" class="btn btn-success" style="background: #f8a41b; padding: 10px 20px; color: #000; border: none; font-size: 16px; border-radius: 5px;">Submit</button>
                        </div>
                    </form>     
                </div>
            </div> 
					
					
					 
                </div>
				
				
				
       
 
    
				
                
        </div>
    </section>
	
	
	</form>
	
	
	
	
    <div class="crl"></div>
	
	
<script src="<?php echo base_url(); ?>assets/jquery.min.js"></script>


</body>