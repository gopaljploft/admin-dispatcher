<style>
  /*    
    Side Navigation Menu V2, RWD
    ===================
    License:
    https://goo.gl/EaUPrt
    ===================
    Author: @PableraShow

 */

  @charset "UTF-8";
  @import url(https://fonts.googleapis.com/css?family=Open+Sans:300,400,700);

  body {
    font-family: 'Open Sans', sans-serif;
    font-weight: 300;
    line-height: 1.42em;
    color: #A7A1AE;
    background-color: #fff;
  }

  h1 {
    font-size: 3em;
    font-weight: 300;
    line-height: 1em;
    text-align: center;
    color: #4DC3FA;
  }

  h2 {
    font-size: 1em;
    font-weight: 300;
    text-align: center;
    display: block;
    line-height: 1em;
    padding-bottom: 2em;
    color: #FB667A;
  }

  h2 a {
    font-weight: 700;
    text-transform: uppercase;
    color: #FB667A;
    text-decoration: none;
  }

  .blue {
    color: #185875;
  }

  .yellow {
    color: #FFF842;
  }

  .container th h1 {
    font-weight: bold;
    font-size: 1em;
    text-align: left;
    color: #185875;
  }

  .container td {
    font-weight: normal;
    font-size: 1em;
    -webkit-box-shadow: 0 2px 2px -2px #0E1119;
    -moz-box-shadow: 0 2px 2px -2px #0E1119;
    box-shadow: 0 2px 2px -2px #0E1119;
  }

  .container {
    text-align: left;
    overflow: hidden;
    width: 90%;
    margin: 0 auto;
    display: table;
    padding: 0 0 8em 0;
  }

  .container td,
  .container th {
    padding-bottom: 2%;
    padding-top: 2%;
    padding-left: 2%;
  }

  /* Background-color of the odd rows */
  .container tr:nth-child(odd) {
    background-color: #323C50;
  }

  /* Background-color of the even rows */
  .container tr:nth-child(even) {
    background-color: #2C3446;
  }

  .container th {
    background-color: #1F2739;
  }

  .container td:first-child {
    color: #FB667A;
  }

  .container tr:hover {
    background-color: #464A52;
    -webkit-box-shadow: 0 6px 6px -6px #0E1119;
    -moz-box-shadow: 0 6px 6px -6px #0E1119;
    box-shadow: 0 6px 6px -6px #0E1119;
  }

  .container td:hover {
    background-color: #FFF842;
    color: #403E10;
    font-weight: bold;

    box-shadow: #7F7C21 -1px 1px, #7F7C21 -2px 2px, #7F7C21 -3px 3px, #7F7C21 -4px 4px, #7F7C21 -5px 5px, #7F7C21 -6px 6px;
    transform: translate3d(6px, -6px, 0);

    transition-delay: 0s;
    transition-duration: 0.4s;
    transition-property: all;
    transition-timing-function: line;
  }

  @media (max-width: 800px) {

    .container td:nth-child(4),
    .container th:nth-child(4) {
      display: none;
    }
  }
</style>
<h1>API Doc</h1>


<table class="container">
  <thead>
    <tr>
      <th>
        <h1>Screens</h1>
      </th>
      <th>
        <h1>Url</h1>
      </th>
      <th>
        <h1>Method</h1>
      </th>
      <th>
        <h1>Parameter</h1>
      </th>
      <th>
        <h1>Response</h1>
      </th>
    </tr>
  </thead>
  <tbody>

    <tr>
      <td>3</td>
     <td>http://admin.infinitecabs.com.au/web_service/driver_login</td>
      <td>Post</td>
      <td>
        <pre>
      email:kamal@gmail.com
      password:123456
      deviceToken:ssssss
      </pre>
      </td>
      <td>
        <pre>
         {
          "statusCode": "200",
          "message": "Successfully Logged in",
          "image_path": "http://admin.infinitecabs.com.au/driverimages/",
          "data": {
              "id": "80",
              "name": "ba",
              "user_type": "driver",
              "user_name": "ba",
              "phone": "1478524560",
              "address": "asdad",
              "email": "1sfsf47@gmail.com",
              "license_no": "cad",
              "car_type": "Suv",
              "car_no": "asdas",
              "gender": "male",
              "dob": "10.10.2012",
              "Lieasence_Expiry_Date": "20.10.2023",
              "Insurance": "sads",
              "license_plate": "asdsd",
              "Seating_Capacity": "4",
              "Car_Model": "daada",
              "image": "",
              "status": "Active",
              "user_status": "online"
          }
      }
      </pre>
      </td>
    </tr>
    
    <tr>
      <td>4</td>
     <td>http://admin.infinitecabs.com.au/web_service/driverCars</td>
      <td>Post</td>
      <td>
        <pre>
      
      </pre>
      </td>
      <td>
        <pre>
         {
            "statusCode": "200",
            "message": "success",
            "image_path": "http://admin.infinitecabs.com.au/driverimages/",
            "data": [
                {
                    "car_id": "1",
                    "car_type": "Suv",
                    "car_name": "Suv",
                    "car_number": "RJ-14"
                },
                {
                    "car_id": "2",
                    "car_type": "Sedan",
                    "car_name": "Sedan",
                    "car_number": "RJ-15"
                },
                {
                    "car_id": "3",
                    "car_type": "Hatchback ",
                    "car_name": "Hatchback ",
                    "car_number": "RJ-16"
                },
                {
                    "car_id": "4",
                    "car_type": "MPV",
                    "car_name": "MPV",
                    "car_number": "RJ-17"
                },
                {
                    "car_id": "5",
                    "car_type": "Suv",
                    "car_name": "Suv",
                    "car_number": "RJ-18"
                },
                {
                    "car_id": "6",
                    "car_type": "Sedan",
                    "car_name": "Sedan",
                    "car_number": "RJ-19"
                }
            ]
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>4</td>
     <td>http://admin.infinitecabs.com.au/web_service/searchCar</td>
      <td>Post</td>
      <td>
        <pre>
         car_number:RJ-15<br>
        car_type:Suv<br>
      </pre>
      </td>
      <td>
        <pre>
         {
            "statusCode": "200",
            "message": "success",
            "data": [
                {
                    "car_id": "1",
                    "car_type": "Suv",
                    "car_number": "RJ-14"
                },
                {
                    "car_id": "5",
                    "car_type": "Suv",
                    "car_number": "RJ-18"
                }
            ]
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>4</td>
     <td>http://admin.infinitecabs.com.au/web_service/searchCar</td>
      <td>Post</td>
      <td>
        <pre>
         car_number:RJ-15<br>
        car_type:Suv<br>
      </pre>
      </td>
      <td>
        <pre>
         {
            "statusCode": "200",
            "message": "success",
            "data": [
                {
                    "car_id": "1",
                    "car_type": "Suv",
                    "car_number": "RJ-14"
                },
                {
                    "car_id": "5",
                    "car_type": "Suv",
                    "car_number": "RJ-18"
                }
            ]
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>4</td>
     <td>http://admin.infinitecabs.com.au/web_service/addDriverCar</td>
      <td>Post</td>
      <td>
        <pre>
         driver_id:2<br>
         car_id:4<br>
      </pre>
      </td>
      <td>
        <pre>
         {
            "statusCode": "200",
            "message": "success",
            "data": {
                "user_id": "2",
                "user_type": "driver",
                "car_id": "4"
            }
        }
      </pre>
      </td>
    </tr>
    

    

    <!-- <tr>
      <td>https://stageofproject.com/texiapp-new/web_service/InsertDriverLiveCation</td>
      <td>Post</td>
      <td>
        <pre>
        driver_id:5
        cab_id:6
        lat:11
        lng:122
      </pre>
      </td>
      <td>
        <pre>
            {
            "statusCode": "200",
            "message": "Driver location Successfully Insert",
            "data": {
                "driver_id": "5",
                "cab_id": "6",
                "latitude": "11",
                "longlatitude": "122"
            }
        }
      </pre>
      </td>
    </tr> -->
    
    <tr>
      <td>5</td>
      <td>https://stageofproject.com/texiapp-new/web_service/driverDetails</td>
      <td>Post</td>
      <td>
        <pre>
      user_id:72
      </pre>
      </td>
      <td>
        <pre>
              {
              "statusCode": "200",
              "message": "success",
              "image_path": "https://stageofproject.com/texiapp-new/driverimages/avatar.png",
              "data": {
                  "name": "sumna",
                  "user_name": "suan",
                  "phone": "9509360088",
                  "address": "645-A DEVI NAGAR NEW SANGANER ROAD\nSODALA",
                  "email": "su@gmail.com",
                  "license_no": "123",
                  "car_type": "Suv",
                  "gender": "male",
                  "dob": "10-10-2013",
                  "user_status": "active",
                  "Lieasence_Expiry_Date": "31-8-2016",
                  "license_plate": "12345",
                  "Insurance": "da",
                  "Car_Model": "dsfs",
                  "image": "avatar.png",
                  "status": "Active"
              }
          }
      </pre>
      </td>
    </tr>
    <tr>
      <td>5</td>
      <td>http://admin.infinitecabs.com.au/web_service/driveLiveStatus</td>
      <td>Post</td>
      <td>
        <pre>
        driver_id:1<br>
        status:offline<br>
        status = offline, online;<br> 
      </pre>
      </td>
      <td>
        <pre>
              {
            "statusCode": "201",
            "message": "offline"
        }
      </pre>
      </td>
    </tr>
    <tr>
      <td>9</td>
      <td>http://admin.infinitecabs.com.au/web_service/driverJob</td>
      <td>Post</td>
      <td>
        <pre>
        driver_id:1<br> 
      </pre>
      </td>
      <td>
        <pre>
              {
              "statusCode": "200",
              "message": "success",
              "data": [
                  {
                      "booking_id": "1700",
                      "created_at": "17/12/2019  03:30:am",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150",
                      "user_id": "45",
                      "user_pickup_address": "Sandy Creek Trail, Laguna NSW 2325, Australia",
                      "user_drop_address": "Unnamed Road, Brogans Creek NSW 2848, Australia",
                      "status": "New"
                  },
                  {
                      "booking_id": "1709",
                      "created_at": "17/12/2019  02:54:am",
                      "pickup_lat": "-33",
                      "pickup_long": "151",
                      "drop_lat": "-33",
                      "drop_long": "150",
                      "user_id": "45",
                      "user_pickup_address": "Sandy Creek Trail, Laguna NSW 2325, Australia",
                      "user_drop_address": "Unnamed Road, Brogans Creek NSW 2848, Australia",
                      "status": "New"
                  }
              ]
          }
      </pre>
      </td>
    </tr>
    <tr>
      <td>9</td>
      <td>http://admin.infinitecabs.com.au/web_service/driverJobAccept</td>
      <td>Post</td>
      <td>
        <pre>
        booking_id:1700<br>
        driver_id:1<br>
        status:yes<br>
      </pre>
      </td>
      <td>
        <pre>
              {
            "statusCode": "200",
            "message": "Driver  Request Successfully Accepting"
        }
      </pre>
      </td>
    </tr>

    <tr>
      <td>10</td>
      <td>http://admin.infinitecabs.com.au/web_service/adminMesage</td>
      <td>Post</td>
      <td>
        <pre>
        driver_id:1<br>
      </pre>
      </td>
      <td>
        <pre>
              {
            "statusCode": "200",
            "message": "success",
            "data": [
                {
                    "driver_id": "1",
                    "message": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,",
                    "created_at": "12:52:26",
                    "admin_name": "admin",
                    "admin_image": "http://admin.infinitecabs.com.au/adminimage/u327.png"
                },
                {
                    "driver_id": "1",
                    "message": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,",
                    "created_at": "12:52:26",
                    "admin_name": "admin",
                    "admin_image": "http://admin.infinitecabs.com.au/adminimage/u327.png"
                }
            ]
        }
      </pre>
      </td>
    </tr>
    
    <tr>
      <td></td>
      <td>https://stageofproject.com/texiapp-new/web_service/driverCompeteRide</td>
      <td>get</td>
      <td>
        <pre>
      user_id:5
      </pre>
      </td>
      <td>
        <pre>
        {
          "statusCode": "200",
          "message": "success",
          "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
          "data": [
              {
                  "booking_id": "",
                  "amount": "3741",
                  "driver_name": "test",
                  "driver_image": "Hydrangeas1.jpg",
                  "driver_car_number": "1213",
                  "pickup_area": "Rajkot, Gujarat, India",
                  "car_type": "Suv",
                  "user_pickup_latitude": "22.303887",
                  "user_pickup_long_latitude": "70.802139",
                  "user_drop_latitude": "23.241996",
                  "user_drop_long_latitude": "69.666935",
                  "pickup_address": "Hospital Chowk, Sadar, Rajkot, Gujarat 360001, India",
                  "drop_address": "Spa Point Opp Jubely Grund Near Allahabad Bank Bhuj Deep Kruti Billding, Bhuj, Gujarat 370001, India",
                  "book_create_date_time": "2016-08-13 04:32:57"
              },
              {
                  "booking_id": "",
                  "amount": "1087",
                  "driver_name": "test",
                  "driver_image": "Hydrangeas1.jpg",
                  "driver_car_number": "1213",
                  "pickup_area": "Silwar Stand Road 2,360003,Rajkot,Gujarat,India",
                  "car_type": "Suv",
                  "user_pickup_latitude": "22.300260",
                  "user_pickup_long_latitude": "70.808202",
                  "user_drop_latitude": "21.761218",
                  "user_drop_long_latitude": "70.626797",
                  "pickup_address": "Shamaseth St, Soni Bazar, Old walled City, Rajkot, Gujarat 360001, India",
                  "drop_address": "Unnamed Road, Jai Shree Krishna Housing Society, Jetpur, Gujarat 360370, India",
                  "book_create_date_time": "2016-08-13 11:54:15"
              }
          ]
      }
      </pre>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>https://stageofproject.com/texiapp-new/web_service/driverConceledRide</td>
      <td>get</td>
      <td>
        <pre>
      user_id:5
      </pre>
      </td>
      <td>
        <pre>
                {
                  "statusCode": "200",
                  "message": "success",
                  "image_path": "https://stageofproject.com/texiapp-new/driverimages/",
                  "data": [
                      {
                          "booking_id": "",
                          "amount": "1895",
                          "driver_name": "test",
                          "driver_image": "Hydrangeas1.jpg",
                          "driver_car_number": "1213",
                          "pickup_area": "Ring Road,360003,Rajkot,Gujarat,India",
                          "car_type": "Suv",
                          "user_pickup_latitude": "22.339199",
                          "user_pickup_long_latitude": "70.807649",
                          "user_drop_latitude": "21.761218",
                          "user_drop_long_latitude": "70.626797",
                          "pickup_address": "Ring road, Rajkot, Gujarat 360003, India",
                          "drop_address": "Unnamed Road, Jai Shree Krishna Housing Society, Jetpur, Gujarat 360370, India",
                          "book_create_date_time": "2016-08-13 11:56:41"
                      }
                  ]
              }
      </pre>
      </td>
    </tr>

    <tr>
      <td></td>
      <td>https://stageofproject.com/texiapp-new/web_service/updateDriverStatus</td>
      <td>post</td>
      <td>
        <pre>
      driver_id:55<br>
      status:offline<br>
      </pre>
      </td>
      <td>
        <pre>
                {
                "statusCode": "200",
                "message": "offline",
                "data": []
            }
            {
              "statusCode": "200",
              "message": "online",
              "data": []
          }
      </pre>
      </td>
    </tr>
    
    


  <tr>
    <td></td>
      <td>https://stageofproject.com/texiapp-new/web_service/get_user_driver_latlong</td>
      <td>Post</td>
      <td>
        <pre>
      booking_id:200<br>
      user_id:2<br>
      </pre>
      </td>
      <td>
        <pre>
        if send booking_id Get (diver_live_location according to booking_id)
        {
            "statusCode": "200",
            "message": "success",
            "data": {
                "user_id": "12",
                "booking_id": "200",
                "latitude": "33.58855",
                "longlatitude": "151.666"
            }
        }
        if send user_id Get (diver_live_location according to user_id)
        {
          "statusCode":"200",
          "message":"success",
          "data":{
              "user_id":"2",
              "latitude":"33",
              "longlatitude":"55"
          }
        }
      </pre>
      </td>
    </tr>

    <tr>
    <td></td>
      <td>http://admin.infinitecabs.com.au/web_service/adminMesage</td>
      <td>Post</td>
      <td>
        <pre>
      driver_id:1<br>
      </pre>
      </td>
      <td>
        <pre>
                {
            "statusCode": "200",
            "message": "success",
            "data": [
                {
                    "driver_id": "1",
                    "message": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,",
                    "created_at": "12:52:26",
                    "admin_name": "admin",
                    "admin_image": "http://admin.infinitecabs.com.au/adminimage/u327.png"
                },
                {
                    "driver_id": "1",
                    "message": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,",
                    "created_at": "12:52:26",
                    "admin_name": "admin",
                    "admin_image": "http://admin.infinitecabs.com.au/adminimage/u327.png"
                }
            ]
        }
      </pre>
      </td>
    </tr>
    
    <tr>
      <td></td>
      <td>http://admin.infinitecabs.com.au/web_service/InsertDriverlivelatlong</td>
      <td>Post</td>
      <td>
        <pre>
      driver_id:3
      booking_id:1709
      lat:33.58855
      lng:151.666
      </pre>
      </td>
      <td>
        <pre>
        {
            "statusCode": "200",
            "message": "Update Successfully"
        }
      </pre>
      </td>
    </tr>

  </tbody>
</table>