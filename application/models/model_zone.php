<?php
class Model_zone extends MY_Model{

	public $zone_name = CUSTOM_ZONE_NAME;
	
	// Construct call
	function __construct()
	{
		parent::__construct();
	}
	function get_zone(){

		$this->db->select('zone.*,diver_live_location.id as did');
		$this->db->from('zone');
		$this->db->join('diver_live_location', 'diver_live_location.zoneId = zone.id',"left");
		if(@$_GET['name'])
		{
		    $query=$this->db->like("zone.zone_title",@$_GET['name']);
		    }
		    	if(@$_GET['cab'])
		{
		    $query=$this->db->where("diver_live_location.cab_id",@$_GET['cab']);
		    }
		      $this->db->order_by("zone.zone_title","ASC");
		      $this->db->group_by("zone.id");
		$query=$this->db->get()->result_array();
       // echo $this->db->last_query();die;
	
		return $query;
	}
	function get_suburbs(){

		$this->db->select('*');
		$this->db->from('suburbs');
		$query = $this->db->get();
		$result=$query->result_array();
		/*foreach ($result as $key => $value) {
				$zipcode=$value['postCode'];

    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false&key=AIzaSyC4-ED4s0K_jXpyqPUHXDVwo8jmzJmdbEk";
    $details=file_get_contents($url);
    $result = json_decode($details,true);

    $lat=$result['results'][0]['geometry']['location']['lat'];

    $lng=$result['results'][0]['geometry']['location']['lng'];
     $data=array(
                    'lat' => $lat,
                    'lng' => $lng,
                );
              
                $this->db->where('id',$value['id']);
                $this->db->update('suburbs',$data);
		}*/
		return $result;
	}
	function save_zone($data)
	{
		$title=$data['title'];
		$time=$data['time'];
		$json=$data['json'];
		$zipcode=$data['zipcode'];

                $insert_data=array(
			'zone_title'=>$title,
			'zipcode'=>$zipcode,
		    'time'=>$time

		);

		$this->db->insert('zone', $insert_data);
		 $lastId=$this->db->insert_id();

		return true;
	


	}
	
	
}
?>