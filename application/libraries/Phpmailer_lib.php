<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class PHPMailer_Lib
{
    public function __construct(){
        log_message('Debug', 'PHPMailer class is loaded.');
    }
    public function load() {
    	//require_once dirname(__FILE__).'/MY_Email_3_1_x.php';
    	 // Include PHPMailer library files
        require_once dirname(__FILE__).'/PHPMailer/Exception.php';
        require_once dirname(__FILE__).'/PHPMailer/PHPMailer.php';
        require_once dirname(__FILE__).'/PHPMailer/SMTP.php';
        
        $mail = new PHPMailer;
        return $mail;
        
    }
}