<?php
// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
require_once 'vendor/autoload.php';

use Twilio\Rest\Client;

// Find your Account Sid and Auth Token at twilio.com/console
// DANGER! This is insecure. See http://twil.io/secure
$sid    = "AC9532bdf74def628f9e2282584e681f2f";
$token  = "58e2a58e97004e90c5670bf0ed938634";
$twilio = new Client($sid, $token);

$call = $twilio->calls
               ->create("+15017122661", // to
                        "+15558675310", // from
                        ["url" => "http://demo.twilio.com/docs/voice.xml"]
               );

print($call->sid);